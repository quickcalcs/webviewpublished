﻿<%@ WebHandler Language="VB" Class="SummaryHandler" %>

Imports DigiSonics
Imports DigiSonics.Backend
Imports System.Web.Script.Serialization
Imports System.Diagnostics
Imports System.Threading.Tasks

Public Structure structTrackingResponse
    Public Result As Integer
    Public TrackingID As Integer
    Public TimeoutReached As Boolean

    Public Sub New(ByVal ResultIn As Integer, TrackingIDIn As Integer)
        Result = ResultIn
        TrackingID = TrackingIDIn
    End Sub 'New

End Structure

Public Structure structLockStudyResponse
    Public Result As Integer
    Public OpenBy As String
    Public TimeoutReached As Boolean

    Public Sub New(ByVal ResultIn As Integer, OpenByIn As String)
        Result = ResultIn
        OpenBy = OpenByIn
    End Sub 'New

End Structure

Public Structure structHyperlinkOptions
    Public Result As Integer
    Public DelimitedOptions As String
    Public TimeoutReached As Boolean

    Public Sub New(ByVal ResultIn As Integer, DelimitedOptionsIn As String)
        Result = ResultIn
        DelimitedOptions = DelimitedOptionsIn
    End Sub 'New

End Structure

Public Class SummaryHandler : Implements IHttpHandler
    Private m_objDVComClient As DVComClient

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim session_guid As String = context.Request.QueryString("GUID")
        Dim userlevel As String = 0
        Dim username As String = ""

        ' TODO: CWT - make sure that timeout is honored here, user is currently allowed to edit and save after timeouts.  
        'Dim SessionIsValid As Boolean = wvSessions.IsValidSession(session_guid, userlevel, username)
        'If wvSessions.IsValidSession(session_guid, userlevel, username) Then
        '
        'End If

        Dim ss As New DigiSonics.StudyStatusStruct(context.Request.QueryString("StudyID"))
        Dim cu As New DigiSonics.CurrUserStruct(context.Request.QueryString("username"))
        Dim nTrackingID As Integer
        Dim bRetainLock As Boolean
        ErrHandler.WriteError("SummaryHandler.ashx ProcessRequest query string: " & context.Request.QueryString.ToString())

        Select Case context.Request.QueryString("Op")
            Case "EditSummaryBegin"
                Dim SessionIsValid As Boolean = wvSessions.IsValidSession(session_guid, userlevel, username, True)
                'cu.strGUID = context.Request.QueryString("GUID")
                cu.strGUID = session_guid
                cu.strPipeDelimUsernames = context.Request.QueryString("fullname")
                Dim structLSR As structLockStudyResponse = New structLockStudyResponse
                structLSR.OpenBy = String.Empty
                structLSR.TimeoutReached = Not SessionIsValid

                If (SessionIsValid) Then
                    structLSR.Result = If(EditSummaryBegin(ss, cu, structLSR.OpenBy), 1, 0)
                End If

                context.Response.ContentType = "text/json"
                SerializeJSON_DataStruct(context, structLSR)
            Case "EditSummaryCancel"
                'cu.strGUID = context.Request.QueryString("GUID")
                cu.strGUID = session_guid
                context.Response.Write(IIf(EditSummaryCancel(ss, cu), "1", "0"))
            Case "EditSummaryAutosave"
                Dim SessionIsValid As Boolean = wvSessions.IsValidSession(session_guid, userlevel, username, True)
                'cu.strGUID = context.Request.QueryString("GUID")
                nTrackingID = context.Request.QueryString("TrackingID")
                cu.strGUID = session_guid

                context.Response.ContentType = "text/json"

                Dim scs As New DigiSonics.SummaryChangeStruct(context.Request.Form("hiddenNewValComments"), context.Request.Form("hiddenNewValSummaryString"), context.Request.Form("hiddenNewValTxText"))
                'Dim structTR As structTrackingResponse
                '
                'If EditSummaryAutosave(ss, cu, scs) Then
                '   structTR = New structTrackingResponse(1, 0)
                'Else
                '   structTR = New structTrackingResponse(0, 0)
                'End If

                Dim structTR As structTrackingResponse = New structTrackingResponse(0, 0)

                If EditSummaryAutosave(ss, cu, scs) Then
                    structTR = New structTrackingResponse(1, 0)
                End If

                If (Not SessionIsValid) Then
                    Dim appsettings As NameValueCollection = System.Configuration.ConfigurationManager.AppSettings
                    Dim strWebPathRoot As String = appsettings("imagesroot")

                    OnStudyClose(ss.nStudyID, nTrackingID, cu)
                    'EditSummaryCleanup(ss, cu)
                    'RegenPDF(ss.nStudyID, strWebPathRoot, cu)
                    Task.Factory.StartNew(Function() {RegenPDF(ss.nStudyID, strWebPathRoot, cu)})
                End If

                structTR.TimeoutReached = Not SessionIsValid

                SerializeJSON_DataStruct(context, structTR)

            Case "EditSummaryEnd"
                Dim SessionIsValid As Boolean = wvSessions.IsValidSession(session_guid, userlevel, username, False)
                'cu.strGUID = context.Request.QueryString("GUID")
                cu.strGUID = session_guid
                nTrackingID = context.Request.QueryString("TrackingID")
                context.Response.ContentType = "text/json"
                ErrHandler.WriteError("SummaryHandler.ashx ProcessRequest EditSummaryEnd Before SummaryChangesStruct")
                Dim scs As New DigiSonics.SummaryChangeStruct(context.Request.Form("hiddenNewValComments"), context.Request.Form("hiddenNewValSummaryString"), context.Request.Form("hiddenNewValTxText"))
                ErrHandler.WriteError("SummaryHandler.ashx ProcessRequest EditSummaryEnd After SummaryChangesStruct")
                'Dim structTR As structTrackingResponse
                '
                'If EditSummaryEnd(ss, cu, scs, nTrackingID) Then
                '    structTR = New structTrackingResponse(1, nTrackingID)
                'Else
                '    structTR = New structTrackingResponse(0, 0)
                'End If

                Dim structTR As structTrackingResponse = New structTrackingResponse(0, 0)

                If SessionIsValid Then

                    If EditSummaryEnd(ss, cu, scs, nTrackingID) Then
                        structTR = New structTrackingResponse(1, nTrackingID)
                    End If

                Else
                    OnStudyClose(ss.nStudyID, nTrackingID, cu)
                    'EditSummaryCleanup(ss, cu)
                End If

                structTR.TimeoutReached = Not SessionIsValid

                SerializeJSON_DataStruct(context, structTR)
            Case "CanESign"
                'cu.strGUID = context.Request.QueryString("GUID")
                cu.strGUID = session_guid
                cu.strPipeDelimUsernames = context.Request.QueryString("fullname")
                context.Response.Write(IIf(CanESign(ss, cu), "1", "0"))
            Case "DoESign"
                Dim SessionIsValid As Boolean = wvSessions.IsValidSession(session_guid, userlevel, username, True)
                nTrackingID = context.Request.QueryString("TrackingID")
                bRetainLock = (context.Request.QueryString("RetainLock") = "1")

                cu.strPipeDelimUsernames = context.Request.QueryString("fullname")
                context.Response.ContentType = "text/json"

                Dim structTR As structTrackingResponse = New structTrackingResponse(0, 0)
                structTR.TimeoutReached = Not SessionIsValid

                If SessionIsValid Then

                    If DoESign(ss, cu, nTrackingID, bRetainLock) Then
                        structTR = New structTrackingResponse(1, nTrackingID)
                    End If

                Else
                    'Dim appsettings As NameValueCollection = System.Configuration.ConfigurationManager.AppSettings
                    'Dim strWebPathRoot As String = appsettings("imagesroot")

                    OnStudyClose(ss.nStudyID, nTrackingID, cu)
                    'EditSummaryCleanup(ss, cu)
                End If

                'Dim structTR As structTrackingResponse
                '
                'If DoESign(ss, cu, nTrackingID, bRetainLock) Then
                '   structTR = New structTrackingResponse(1, nTrackingID)
                'Else
                '   structTR = New structTrackingResponse(0, 0)
                'End If

                SerializeJSON_DataStruct(context, structTR)
            Case "RegenPDF"
                Dim appsettings As NameValueCollection = System.Configuration.ConfigurationManager.AppSettings
                Dim strWebPathRoot As String = appsettings("imagesroot")

                context.Response.ContentType = "text/plain"
                ' CWT (4/16/20) - changed call to pass current user parameter.  
                context.Response.Write(IIf(RegenPDF(ss.nStudyID, strWebPathRoot, cu), "1", "0"))
                'context.Response.Write(IIf(RegenPDF(ss.nStudyID, strWebPathRoot), "1", "0"))
            Case "CloseStudy"
                nTrackingID = context.Request.QueryString("TrackingID")
                'cu.strGUID = context.Request.QueryString("GUID")
                cu.strGUID = session_guid

                context.Response.ContentType = "text/plain"
                context.Response.Write(IIf(OnStudyClose(ss.nStudyID, nTrackingID, cu), "1", "0"))
            Case "CloseStudyAndRegen"
                Dim appsettings As NameValueCollection = System.Configuration.ConfigurationManager.AppSettings
                Dim strWebPathRoot As String = appsettings("imagesroot")
                nTrackingID = context.Request.QueryString("TrackingID")
                'cu.strGUID = context.Request.QueryString("GUID")
                cu.strGUID = session_guid
                OnStudyClose(ss.nStudyID, nTrackingID, cu)

                context.Response.ContentType = "text/plain"
                context.Response.Write(IIf(RegenPDF(ss.nStudyID, strWebPathRoot, cu), "1", "0"))
            Case "GetRTF"
                Dim strTree As String = context.Request.QueryString("Tree")
                Dim nPKey As Integer = context.Request.QueryString("PKey")
                Dim bGetHyperlinks As Boolean

                If Not String.IsNullOrEmpty(context.Request.QueryString("GetHyperlinks")) Then
                    bGetHyperlinks = context.Request.QueryString("GetHyperlinks") = "1"
                End If

                context.Response.ContentType = "text/json"
                'Dim structRTFOut As New StructRTFOptions()
                Dim structRTFOut As DigiSonics.StructRTFOptions
                Dim objSummaryMacros = New SummaryMacros
                Dim strMDBFile As String = SummaryMacros.GetSummaryMacrosMDB()
                objSummaryMacros.Init_MDB(strMDBFile)

                objSummaryMacros.GetRTFForNode(strTree, nPKey, bGetHyperlinks, structRTFOut)
                ' objSummaryMacros.GetDummyRTFForNode(structRTFOut)
                SerializeJSON_DataStruct(context, structRTFOut)
            Case "GetHyperlinkOptions"
                Dim SessionIsValid As Boolean = wvSessions.IsValidSession(session_guid, userlevel, username, True)
                Dim nPKey As Integer = context.Request.QueryString("PKey")
                nTrackingID = context.Request.QueryString("TrackingID")

                context.Response.ContentType = "text/json"
                Dim objSummaryMacros = New SummaryMacros
                Dim strMDBFile As String = SummaryMacros.GetSummaryMacrosMDB()
                objSummaryMacros.Init_MDB(strMDBFile)
                Dim structOut As New structHyperlinkOptions()
                structOut.TimeoutReached = Not SessionIsValid

                If SessionIsValid Then
                    structOut.Result = objSummaryMacros.GetOptionsForPKey(nPKey, structOut.DelimitedOptions, True)
                Else
                    OnStudyClose(ss.nStudyID, nTrackingID, cu)
                    'EditSummaryCleanup(ss, cu)
                End If

                SerializeJSON_DataStruct(context, structOut)
            Case "CheckValidSession"
                'nTrackingID = context.Request.QueryString("TrackingID")
                context.Response.Write(IIf(wvSessions.IsValidSession(session_guid, userlevel, username, False), "1", "0"))
        End Select

    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable

        Get
            Return False
        End Get

    End Property

    Public Function EditSummaryBegin(ss As DigiSonics.StudyStatusStruct, cu As DigiSonics.CurrUserStruct, Optional ByRef strOpenByOther As String = Nothing) As Boolean
        Dim obj As New SummaryESignHandler
        Dim bOpenedStudy As Boolean
        Dim bReturn As Boolean

        obj.ConnectionString = DecryptedSQLServerConnectionstring
        obj.WVConnectionString = DecryptedUserAccountsConnectionstring

        Try
            bReturn = obj.EditSummaryBegin(ss.nStudyID, cu, strOpenByOther)
            If bReturn Then bOpenedStudy = obj.SaveState_OpenStudy(ss.nStudyID, cu)
            Debug.Print(String.Format("EditSummaryBegin - bOpenedStudy={0}", bOpenedStudy))
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try

        Return bReturn
    End Function

    Public Function EditSummaryCancel(ss As DigiSonics.StudyStatusStruct, cu As DigiSonics.CurrUserStruct) As Boolean
        Dim objAutosave As New AutosaveSummary
        Dim objESign As New SummaryESignHandler
        Dim nAutosaveTrackingID As Integer
        Dim scsRevertTo As New DigiSonics.SummaryChangeStruct
        Dim scsBeforeRevert As DigiSonics.SummaryChangeStruct
        Dim bReturn As Boolean, bRevertSuccess As Boolean

        objAutosave.ConnectionString = DecryptedUserAccountsConnectionstring
        objESign.ConnectionString = DecryptedSQLServerConnectionstring

        Try

            If objAutosave.FindPreviousAutosave(ss.nStudyID, cu.strGUID, scsRevertTo, nAutosaveTrackingID) Then
                bRevertSuccess = objESign.EditSummaryAutosave(ss.nStudyID, cu, scsRevertTo, scsBeforeRevert)
                objAutosave.DeletePreviousAutosave(cu.strGUID, nAutosaveTrackingID)
            End If

            bReturn = objESign.EditSummaryCancel(ss.nStudyID, cu)
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try

        Return bReturn
    End Function

    Public Function EditSummaryAutosave(ss As DigiSonics.StudyStatusStruct, cu As DigiSonics.CurrUserStruct, scs As DigiSonics.SummaryChangeStruct) As Boolean
        Dim objAutosave As New AutosaveSummary
        Dim objESign As New SummaryESignHandler
        Dim bReturn As Boolean
        Dim scsBefore As New DigiSonics.SummaryChangeStruct

        objAutosave.ConnectionString = DecryptedUserAccountsConnectionstring
        objESign.ConnectionString = DecryptedSQLServerConnectionstring

        Try
            objESign.EditSummaryAutosave(ss.nStudyID, cu, scs, scsBefore)
            bReturn = objAutosave.AddChanges_SummaryUpdate(ss.nStudyID, cu.strGUID, scsBefore, scs)
        Catch ex As Exception
        End Try

        Return bReturn
    End Function

    Public Function EditSummaryEnd(ss As DigiSonics.StudyStatusStruct, cu As DigiSonics.CurrUserStruct, scs As DigiSonics.SummaryChangeStruct, ByRef nTrackingID As Integer) As Boolean

        ErrHandler.WriteError("SummaryHandler.ashx EditSummaryEnd Entered ")
        Dim objAutosave As New AutosaveSummary
        Dim objESign As New SummaryESignHandler
        Dim scsBeforeIn As New DigiSonics.SummaryChangeStruct
        Dim nAutosaveTrackingID As Integer
        Dim bReturn As Boolean

        objAutosave.ConnectionString = DecryptedUserAccountsConnectionstring
        objESign.ConnectionString = DecryptedSQLServerConnectionstring

        Try
            If objAutosave.FindPreviousAutosave(ss.nStudyID, cu.strGUID, scsBeforeIn, nAutosaveTrackingID) Then
                ErrHandler.WriteError("SummaryHandler.ashx EditSummaryEnd FindPreviousAutosave = True")
                bReturn = objESign.EditSummaryEnd(ss.nStudyID, cu, scs, nTrackingID, scsBeforeIn)
                objAutosave.DeletePreviousAutosave(cu.strGUID, nAutosaveTrackingID)
            Else
                ErrHandler.WriteError("SummaryHandler.ashx EditSummaryEnd FindPreviousAutosave = False")
                bReturn = objESign.EditSummaryEnd(ss.nStudyID, cu, scs, nTrackingID, Nothing)
            End If
        Catch ex As Exception
            ErrHandler.WriteError("SummaryHandler.ashx EditSummaryEnd Error: " & ex.Message.ToString())
            Debug.Print(ex.Message)
        End Try

        Return bReturn
    End Function

    Public Sub EditSummaryCleanup(ss As StudyStatusStruct, cu As DigiSonics.CurrUserStruct)
        Dim objESign As New SummaryESignHandler

        objESign.EditSummaryCleanup(ss.nStudyID, cu)
    End Sub

    Public Function CanESign(ss As DigiSonics.StudyStatusStruct, cu As DigiSonics.CurrUserStruct) As Boolean
        Dim obj As New SummaryESignHandler
        Dim bOpenedStudy As Boolean
        Dim bReturn As Boolean

        obj.ConnectionString = DecryptedSQLServerConnectionstring
        obj.WVConnectionString = DecryptedUserAccountsConnectionstring

        Try
            bReturn = obj.CanESign(ss.nStudyID, cu)
            If bReturn Then bOpenedStudy = obj.SaveState_OpenStudy(ss.nStudyID, cu)
            Debug.Print(String.Format("CanESign - bOpenedStudy={0}", bOpenedStudy))
        Catch ex As Exception
            ErrHandler.WriteError("SummaryHandler.ashx CanESign Error: " & ex.Message.ToString())
        End Try

        'bReturn = True
        ErrHandler.WriteError("SummaryHandler.ashx CanESign return: " & bReturn)

        Return bReturn
    End Function

    Public Function DoESign(ss As DigiSonics.StudyStatusStruct, cu As DigiSonics.CurrUserStruct, ByRef nTrackingID As Integer, bRetainLock As Boolean) As Boolean
        Dim obj As New SummaryESignHandler
        Dim bReturn As Boolean

        obj.ConnectionString = DecryptedSQLServerConnectionstring

        Try
            bReturn = obj.DoESign(ss.nStudyID, cu, nTrackingID, bRetainLock)
        Catch ex As Exception
        End Try

        Return bReturn
    End Function

    Public Function GetRTFForPKey(strTree As String, nPKey As Integer) As Boolean
        Dim obj As New SummaryESignHandler
        Dim bReturn As Boolean

        obj.ConnectionString = DecryptedSQLServerConnectionstring

        Try
        Catch ex As Exception
        End Try

        Return bReturn
    End Function

    Protected Sub SerializeJSON_DataStruct(ByVal context As HttpContext, structDS As Object)
        Dim jsSerialize As New JavaScriptSerializer
        Dim strJSON As String = jsSerialize.Serialize(structDS)
        context.Response.Write(strJSON)
    End Sub

    Protected Function RegenPDF_OLD(nStudyID As Integer, strWVReportsRoot As String) As Boolean
        Dim ss As DigiSonics.StudyStruct

        ErrHandler.WriteError("SummaryHandler.ashx RegenPDF StudyID: " & nStudyID)
        If m_objDVComClient Is Nothing Then m_objDVComClient = New DVComClient

        Try

            If m_objDVComClient IsNot Nothing Then
                Dim objSummaryESign = New SummaryESign
                objSummaryESign.ConnectionString = DecryptedSQLServerConnectionstring
                ss = objSummaryESign.QueryStudyInfo(nStudyID)
                ErrHandler.WriteError("SummaryHandler.ashx RegenPDF m_ObjDVComClient IsNot Nothin' ")
                Return m_objDVComClient.GenerateReportPDF(ss, strWVReportsRoot)
            Else
                ErrHandler.WriteError("SummaryHandler.ashx RegenPDF m_ObjDVComClient Is Nothin' ")
            End If

        Catch ex As Exception
        End Try

        Return False
    End Function

    ' CWT (4/16/20) - updated this method to accept current users as a parameter.  Probably should find a better way to handle to current user.  
    Protected Function RegenPDF(nStudyID As Integer, strWVReportsRoot As String, cu As DigiSonics.CurrUserStruct) As Boolean
        Dim ss As DigiSonics.StudyStruct

        If m_objDVComClient Is Nothing Then m_objDVComClient = New DVComClient

        Try

            If m_objDVComClient IsNot Nothing Then
                Dim objSummaryESign = New SummaryESign
                objSummaryESign.ConnectionString = DecryptedSQLServerConnectionstring
                ss = objSummaryESign.QueryStudyInfo(nStudyID)

                If (m_objDVComClient.GenerateReportPDF(ss, strWVReportsRoot)) Then
                    objSummaryESign.InsertDBStatusChange(nStudyID, "Export PDF")
                    Dim retTrackingId As Integer = objSummaryESign.InsertDBTracking(ss, cu.strUser, "Created PDF")
                    Dim finalPDFPath As String = m_objDVComClient.WebViewFilenameForStudy(strWVReportsRoot, ss)
                    objSummaryESign.AddPDFToAuditLog(retTrackingId, finalPDFPath, nStudyID)
                    Return True
                End If

                'Return m_objDVComClient.GenerateReportPDF(ss, strWVReportsRoot)
            End If

        Catch ex As Exception
            ErrHandler.WriteError("SummaryHandler.ashx RegenPDF Error: " & ex.Message.ToString())
        End Try

        Return False
    End Function

    Protected Function OnStudyClose(nStudyID As Integer, nTrackingID As Integer, cu As DigiSonics.CurrUserStruct) As Boolean
        Dim obj As New SummaryESignHandler
        Dim bReturn As Boolean
        Dim strNewPDF As String

        obj.ConnectionString = DecryptedSQLServerConnectionstring
        obj.WVConnectionString = DecryptedUserAccountsConnectionstring

        Try
            obj.EditSummaryCleanup(nStudyID, cu)
            bReturn = obj.DidStateChange(nStudyID, cu)

            If bReturn Then
                SaveEncryptedPDF(nStudyID, nTrackingID, strNewPDF)
                ExportRolodex(nStudyID, strNewPDF)
            End If

            obj.RemovePrevState_OpenStudy(nStudyID, cu)
        Catch ex As Exception
        End Try

        Return bReturn
    End Function

    ''' <summary>
    ''' Generates encrypted PDF in the SavedPDFs folder and adds that PDF filename in the audit log.
    ''' </summary>
    ''' <param name="nStudyID"></param>
    ''' <param name="nStudyIDnTrackingID></param>
    ''' <param name="strNewPDF">Out value</param>
    ''' <returns></returns>
    Protected Function SaveEncryptedPDF(nStudyID As Integer, nTrackingID As Integer, ByRef strNewPDF As String) As Boolean

        If m_objDVComClient Is Nothing Then m_objDVComClient = New DVComClient

        Try

            If m_objDVComClient IsNot Nothing Then
                m_objDVComClient.GeneratePDFForAuditLog(nStudyID, appsettings("SharePath"), strNewPDF)

                If Not String.IsNullOrEmpty(strNewPDF) Then
                    Dim objSummaryESign = New SummaryESign
                    objSummaryESign.ConnectionString = DecryptedSQLServerConnectionstring
                    objSummaryESign.AddPDFToAuditLog(nTrackingID, strNewPDF, nStudyID)
                    Return True
                End If

            End If

        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try

        Return False
    End Function

    Protected Function ExportRolodex(nStudyID As Integer, strNewPDF As String) As Boolean

        If Not String.IsNullOrEmpty(strNewPDF) Then
            Dim objSummaryESign = New SummaryESign
            objSummaryESign.ConnectionString = DecryptedSQLServerConnectionstring
            Dim ss As DigiSonics.StudyStruct = objSummaryESign.QueryStudyInfo(nStudyID)

            ' Ask Rolodex object to transmit existing PDF (it will make a copy first itself)
            Dim objRolodexClient = New RolodexClientWV
            objRolodexClient.Send(ss, strNewPDF)

            Return True
        End If

        Return False
    End Function

End Class