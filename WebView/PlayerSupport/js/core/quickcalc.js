﻿var dicomRegion;
var arDicomRegion = [];
var dicomLabel;
var rowSpacing = 0;
var columnSpacing = 0;
var mode;
var region;
var referencePixelX0;
var referencePixelY0;
var regionLocationMinX0;
var regionLocationMinY0;
var regionLocationMaxX1;
var regionLocationMaxY1;

function startQuickCalcApp() {
    dicomRegion = $("div.thumbnail.selected #dicomregion").html();

    if (typeof dicomRegion !== 'undefined') {
        arDicomRegion = dicomRegion.split("|");
        if (arDicomRegion[1] !== "" && arDicomRegion[2] !== "" && arDicomRegion[3] !== "") {
            dicomLabel = arDicomRegion[0];
            rowSpacing = arDicomRegion[1];
            columnSpacing = arDicomRegion[2];
            mode = arDicomRegion[3];
            region = arDicomRegion[4];
            referencePixelX0 = arDicomRegion[5];
            referencePixelY0 = arDicomRegion[6];
            regionLocationMinX0 = arDicomRegion[7];
            regionLocationMinY0 = arDicomRegion[8];
            regionLocationMaxX1 = arDicomRegion[9];
            regionLocationMaxY1 = arDicomRegion[10];
        } else {
            return;
        }
    }

    var drawDiv = document.getElementById("drawDiv");
    var width;
    var height;

    if ($("div.thumbnail.selected").find("p").html().split('|')[0] === "jpg") {
        width = $("#canvasImage").width();
        height = $("#canvasImage").height();
    }
    else {
        width = $("#canvasVideo").width();
        height = $("#canvasVideo").height();
    }

    $("#drawDiv").width(width);
    $("#drawDiv").height(height);

    var group = new Konva.Group();

    // first we need Konva core things: stage and layer
    var stage = new Konva.Stage({
        container: drawDiv,
        width: width,
        height: height
    });

    var layer = new Konva.Layer();
    stage.add(layer);

    // then we are going to draw into special canvas element
    var canvas = document.createElement('canvas');
    canvas.width = stage.width();
    canvas.height = stage.height();
    canvas.setAttribute('class', "canvasId");

    // created canvas we can add to layer as "Konva.Image" element
    var image = new Konva.Image({
        image: canvas,
        x: 0,
        y: 0
    });
    layer.add(image);
    stage.draw();

    // Good. Now we need to get access to context element
    var context = canvas.getContext('2d');
    context.strokeStyle = '#81b214';
    context.lineJoin = 'round';
    context.lineWidth = 2;

    var isPaint = false;
    var lastPointerPosition;
    var moveX;
    var moveY;
    var initialPoint;
    var finalPoint;

    // now we need to bind some events
    // we need to start drawing on mousedown
    // and stop drawing on mouseup
    stage.on('mousedown touchstart', function () {
        isPaint = true;
        lastPointerPosition = stage.getPointerPosition();
        moveX = lastPointerPosition.x - image.x();
        moveY = lastPointerPosition.y - image.y();

		if (moveX < regionLocationMinY0
			|| moveY < regionLocationMinX0 - 8
			|| moveX > regionLocationMaxY1
			|| moveY > regionLocationMaxX1) {
			isPaint = false;
		}

        initialPoint = new Core.math.Point2D(moveX, moveY);
        layer.clear();
        layer.clearCache();
        context.clearRect(0, 0, canvas.width, canvas.height);
        // dp for Free Hand
        if (mode === 'dp') {
            context.beginPath();
        }
    });

    // will it be better to listen move/end events on the window?
    stage.on('mouseup touchend', function () {
        isPaint = false;
    });

    // and core function - drawing
	stage.on('mousemove touchmove', function () {
		var pos = stage.getPointerPosition();
		const rect = canvas.getBoundingClientRect();
		var x = pos.x - rect.left;
		var y = pos.y - rect.top;

		if (y > regionLocationMaxX1) {
			y = regionLocationMaxY1
		}

		if (y < regionLocationMinX0) {
			y = regionLocationMinY0
		} 

		if (x > regionLocationMaxY1) {
			x = regionLocationMaxX1
		}

		if (x < regionLocationMinY0) {
			x = regionLocationMinX0
			isPaint = false;
		}
		 

        if (!isPaint) {
            return;
        }

        //if (mode === '2d' || mode === 'mm' || mode === 'dp') {
        context.globalCompositeOperation = 'source-over';
        //}
        if (mode === 'eraser') {
            context.globalCompositeOperation = 'destination-out';
        }

        context.clearRect(0, 0, canvas.width, canvas.height);

        finalPoint = new Core.math.Point2D(x, y);
        lastPointerPosition = pos;
        //free hand
        if (mode === 'dp') {
            context.lineTo(x, y);
            context.stroke();
            layer.add(drawFreeHand([initialPoint, finalPoint], group));
        } else {
            layer.add(drawRuler([initialPoint, finalPoint], group));
        }
        layer.draw();
    });
}

function drawFreeHand(points, group) {
    // points stored the Konvajs way
    var arr = [];
    var arrCal = [];
    for (var i = 0; i < points.length; ++i) {
        arr.push(points[i].getX());
        arr.push(points[i].getY());
        arrCal.push(points[i].getX());
        arrCal.push(points[i].getY());
    }

    // text
    var ktext = new Konva.Text({
        fontSize: 12,
        fontFamily: 'Verdana',
        fill: '#81b214',
        name: 'text'
    });

    if (Math.abs(489 - arr[arr.length - 1]) > 10) {
        arrCal.push(points[points.length - 1].getX());
        arrCal.push(489);
    }

    var quant = quantDopplerCal(arrCal);
    ktext.textExpr =
        ' Pk Vel: {PeakV}\n Pk PG: {PeakPG}\n Mn Vel: {MeanV}\n Mn PG: {MeanPG}\n VTI: {VTI}';
    ktext.longText = '';
    ktext.quant = quant;
    ktext.setText(Core.utils.replaceFlags(ktext.textExpr, ktext.quant));

    // label
    var klabel = new Konva.Label({
        x: 0,
        y: 300,
        name: 'label'
    });
    klabel.add(ktext);
    klabel.add(new Konva.Tag());

    // return group
    group.destroyChildren();
    group.name('freeHand-group');
    group.add(klabel);
    group.visible(true); // dont inherit
    return group;
}

function quantDopplerCal(arr) {
    var spacing = new Core.image.Spacing(columnSpacing, rowSpacing);
    var areaSum = 0;
    var YStart = 0;
    var YPointCur1 = 0;
    var YPointCur2 = 0;
    var YAvg = 0;
    var XPointCur = 0;
    var AreaCur = 0;
    var quant = {};
    var VTI = 0;
    var PeakV = 0;
    var MeanV = 0;
    var MDist = 0;
    var PeakVal = 0;
    var PeakPG = 0;
    var MeanPG = 0;
    var XStart = 0;
    var Xend = 0;
    var DopTime = 0;

    XStart = arr[0];
    Xend = arr[arr.length - 2];
    DopTime = Math.abs(Xend - XStart) * spacing.getColumnSpacing() * 1000;

    YStart = arr[1];
    for (i = 0; i + 3 < arr.length; i = i + 2) {
        YPointCur1 = Math.abs(arr[i + 1] - YStart);
        YPointCur2 = Math.abs(arr[i + 3] - YStart);
        YAvg = (YPointCur1 + YPointCur2) / 2;
        XPointCur = Math.abs(arr[i + 2] - arr[i]);
        AreaCur = XPointCur * YAvg;
        areaSum = areaSum + AreaCur;
    }

    VTI = Math.abs(
        areaSum * spacing.getRowSpacing() * spacing.getColumnSpacing()
    );
    VTI = Math.round(VTI);
    for (i = 3; i < arr.length; i = i + 2) {
        MDist = Math.abs(arr[i] - YStart);
        if (MDist > PeakVal) PeakVal = MDist;
    }
    PeakV = Math.abs(PeakVal * spacing.getRowSpacing());
    PeakPG = Math.abs(4 * (PeakV / 100) * (PeakV / 100));

    if (DopTime != 0) {
        MeanV = (VTI / DopTime) * 1000;
        MeanPG = Math.abs(4 * (MeanV / 100) * (MeanV / 100));
    }

    quant.VTI = { value: VTI, unit: 'cm' };
    quant.PeakV = { value: PeakV, unit: 'cm/sec' };
    quant.PeakPG = { value: PeakPG, unit: 'mmHg' };
    quant.MeanV = { value: MeanV, unit: 'cm/sec' };
    quant.MeanPG = { value: MeanPG, unit: 'mmHg' };

    return quant;
}

function quantifyLine(line) {
    var spacing = new Core.image.Spacing(columnSpacing, rowSpacing);
    var quant = {};
    // length
    if (mode === '2d') {
        var length = line.getWorldLength(
            spacing.getColumnSpacing(),
            spacing.getRowSpacing()
        );
        if (length !== null) {
            if (length < 0) length = length * -1;
            quant.length = { value: length, unit: 'cm' };
        }
    } else if (mode === 'mm') {
        var length = line.getWorldLengthY(
            spacing.getColumnSpacing(),
            spacing.getRowSpacing()
        );
        var mmodeT = line.getWorldLengthX(
            spacing.getColumnSpacing(),
            spacing.getRowSpacing()
        );
        if (length < 0) length = length * -1;
        if (mmodeT < 0) mmodeT = mmodeT * -1;
        quant.length = { value: length, unit: 'cm' };
        quant.mmodeT = { value: mmodeT, unit: 'msec' };
    } else if (mode === 'dp') {
        var Velocity = line.getWorldLengthY489(
            spacing.getColumnSpacing(),
            spacing.getRowSpacing()
        );
        var mmodeT = line.getWorldLengthX(
            spacing.getColumnSpacing(),
            spacing.getRowSpacing()
        );
        if (Velocity < 0) Velocity = Velocity * -1;
        if (mmodeT < 0) mmodeT = mmodeT * -1;

        var Pg = 4 * ((Velocity / 100) * (Velocity / 100));
        quant.Velocity = { value: Velocity, unit: 'cm/sec' };
        quant.mmodeT = { value: mmodeT, unit: 'msec' };
        quant.Pg = { value: Pg, unit: 'mgHg' };
    }
    // return
    return quant;

}

function drawRuler(points, group) {
    var line = new Core.math.Line(points[0], points[1]);
    // draw shape
    var style = new Core.html.Style();
    var kshape = new Konva.Line({
        points: [
            line.getBegin().getX(),
            line.getBegin().getY(),
            line.getEnd().getX(),
            line.getEnd().getY()
        ],
        stroke: style.getLineColour(),
        strokeWidth: style.getScaledStrokeWidth(),
        name: 'shape'
    });

    var tickLen = 5 * style.getScaledStrokeWidth();

    // tick begin
    var linePerp0 = Core.math.getPerpendicularLine(line, points[0], tickLen);
    var ktick0 = new Konva.Line({
        points: [
            linePerp0.getBegin().getX(),
            linePerp0.getBegin().getY(),
            linePerp0.getEnd().getX(),
            linePerp0.getEnd().getY()
        ],
        stroke: style.getLineColour(),
        strokeWidth: style.getScaledStrokeWidth(),
        name: 'shape-tick0'
    });

    // tick end
    var linePerp1 = Core.math.getPerpendicularLine(line, points[1], tickLen);
    var ktick1 = new Konva.Line({
        points: [
            linePerp1.getBegin().getX(),
            linePerp1.getBegin().getY(),
            linePerp1.getEnd().getX(),
            linePerp1.getEnd().getY()
        ],
        stroke: style.getLineColour(),
        strokeWidth: style.getScaledStrokeWidth(),
        name: 'shape-tick1'
    });

    // larger hitfunc
    kshape.hitFunc(function (context) {
        context.beginPath();
        context.moveTo(linePerp0.getBegin().getX(), linePerp0.getBegin().getY());
        context.lineTo(linePerp0.getEnd().getX(), linePerp0.getEnd().getY());
        context.lineTo(linePerp1.getEnd().getX(), linePerp1.getEnd().getY());
        context.lineTo(linePerp1.getBegin().getX(), linePerp1.getBegin().getY());
        context.closePath();
        context.fillStrokeShape(this);
    });

    // quantification
    var quant = quantifyLine(line);
    //var quant = '';
    var ktext = new Konva.Text({
        fontSize: style.getScaledFontSize(),
        fontFamily: style.getFontFamily(),
        fill: style.getLineColour(),
        name: 'text',
    });
    if (mode === '2d') {
        ktext.textExpr = '{length}';
    } else if (mode === 'mm') {
        ktext.textExpr = '{length} \n {mmodeT}';
    } else if (mode === 'dp') {
        ktext.textExpr = '{Velocity}\n {Pg} \n {mmodeT}';
    }
    ktext.longText = '';
    ktext.quant = quant;
    ktext.setText(Core.utils.replaceFlags(ktext.textExpr, ktext.quant));

    // label
    var dX = line.getBegin().getX() > line.getEnd().getX() ? 0 : -1;
    var dY = line.getBegin().getY() > line.getEnd().getY() ? -1 : 0.5;
    var klabel = new Konva.Label({
        x: line.getEnd().getX() + dX * 25,
        y: line.getEnd().getY() + dY * 15,
        name: 'label'
    });

    klabel.add(ktext);
    klabel.add(new Konva.Tag());

    // return group
    group.destroyChildren();
    group.name('ruler-group');
    group.add(kshape);
    group.add(ktick0);
    group.add(ktick1);
    group.add(klabel);
    group.visible(true); // dont inherit

    return group;
}

function setDrawDivStyle() {
    clearDrawDiv();
    $("#drawDiv").width($("#canvasVideo")[0].getBoundingClientRect().width);
    $("#drawDiv").height($("#canvasVideo")[0].getBoundingClientRect().height);
}

function clearDrawDiv() {
    document.getElementById("drawDiv").innerHTML = '';
}

function clearCanvas() {
    var canvas = document.querySelector('canvas');
    var context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
}