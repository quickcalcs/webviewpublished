﻿var isPortrait = false;
var imageListMinHeight = 110;
var hMargin = .99;
var vMargin = .99;
var pvMargin = "99%"
var phMargin = "99%"

function SetPortrait() {
    
    if (debug) { fnDebug("SetPortrait() start"); }
        
    isPortrait = true;

    SizeImagePortrait();

    if (debug) { fnDebug("SetPortrait() after SizeImagePortrait()"); }

    var divImages = $("#ImagesList").detach();
    var divToolbar = $("#toolbar").detach();

    //redundant myiframe is sized in SizeImagePortrait
//    $("#myiframe").height(pvMargin);
//    $("#myiframe").width($("#mainpanel").width());

    //divImages.width($(window).width() * hMargin);

    divImages.css("float", "left");

    divToolbar.width("100%");

    if (!fullscreenMode) {
        divToolbar.css("position", "relative");
    }
    else {
        divToolbar.css("position", "absolute");
    }

//    if (!fullscreenMode) {
//        SetImageListHeight();
//    }

    $("#myiframe").append(divToolbar);

    $("#myiframe").append(divImages);
    $("#myiframe").css("overflow", "hidden");

    //preview mode should check quads hight, otherwise, check video container height.
    if (previewMode) 
    {
        var ImageListHeight = $("#imageMainPanel").innerHeight() - $("#quads").height() - 50;
        divImages.css("height", ImageListHeight);
    }
    else 
    {

        if (!fullscreenMode) 
        {
            SetImageListHeight();
        }

    }
    
    divImages.width($("#mainpanel").width() - 10);

    divImages.css("overflow-y", "auto");

    if (fullscreenMode) {

        divToolbar.insertAfter($("#canvasImage"));
        divToolbar.css("bottom", "20px");
    }

    //4.1 DWC removed for build six, it was redundant 
    //sizeImage();
}

function SetLandscape() {

    if (debug) { ("SetLandScape"); }

    isPortrait = false;

    var divImages = $("#ImagesList").detach();
    var divToolbar = $("#toolbar").detach();

    divImages.css("float", "left");

    divImages.css("height", "100%");
    divImages.css("overflow-y", "auto");

    $("#myiframe").css("overflow", "");

    divToolbar.css("display", "inline-block");
    divToolbar.css("width", "100%");
    divToolbar.css("z-index", "3");

    if (fullscreenMode) {
        divToolbar.css("bottom", "20px");
    }

    divToolbar.insertAfter("#video_container");

    divImages.insertAfter("#myiframe");

    if (previewMode) {
        $("#imageMainPanel").width("99%");
    }
}

function SizeImagePortrait() 
{
    //Get aspect ratio from hidden text in the selected thumbnail
    var imgTypeTemp = $("div.thumbnail.selected").find("p").html();
    var arImgType = [];
    //arImgType = imgTypeTemp.split("|");
    var imgType = arImgType[0];       //first field in pipe delimited array is the image type (dcm,jpg,mp4,webm) or it could be a status such as mia
    var imgAspRatio = arImgType[1];   //Second field is the aspect ratio, width/height

    //set offset to center the toolbar in portrait
    var toolbarWidth = 500;
    var offset = ($(window).innerWidth() - toolbarWidth) / 2;
    $("#toolbar").css("left", offset);

    //set all containers to the width of the main panel
    $("#myiframe").height(pvMargin);
    $("#myiframe").width($("#mainpanel").width() - 10);

    $("#imageMainPanel").height(pvMargin);
    $("#imageMainPanel").width($("#mainpanel").width());

    //set height according to aspect ratio here.
    //console.log("SizeImagePortrait(): setting $('#video_container').height() to " + (($("#mainpanel").width() * vMargin) / imgAspRatio));
    $("#video_container").height(($("#mainpanel").width() * vMargin) / imgAspRatio);
    $("#video_container").width($("#mainpanel").width() - 10); 

    if (debug) { fnDebug("canvasVideo.width() " + $("#canvasVideo").width() + " canvasVideo.height() " + $("#canvasVideo").width() + " imagAspRatio " + imgAspRatio); }
    
    if (previewMode) 
    {
        //console.log("SizeImagePortrait(): setting $('#video_container').height() to " + (tdHeight * 2));
        $("#video_container").height(tdHeight * 2);
    }

    // 4.1 DWC Container cannot be smaller than the video or control elements will be misplaced
    //if ($("#canvasVideo").height() > $("#video_container").height()) { $("#video_container").height($("#canvasVideo").height()); }
    //$("#myiframe").height($("#video_container").height());

    //if height of all elements is greater than total screen space, limit the height and set width to aspect ratio
    if ($("#video_container").height() + $("#toolbar").height() + imageListMinHeight > $("#imageMainPanel").height()) {

        SetImageListHeight();

        if (debug) { fnDebug("containers too tall"); }

        //console.log("SizeImagePortrait(): setting $('#video_container').height() to " + ($("#imageMainPanel").height() - $("#toolbar").height() - imageListMinHeight));
        $("#video_container").height($("#imageMainPanel").height() - $("#toolbar").height() - imageListMinHeight);
        $("#video_container").width($("#video_container").height() * imgAspRatio);
        if (debug) { fnDebug("video container height limited."); }
    }

    //if height of all elements is greater than total screen space, limit the height and set width to aspect ratio
    if ($("#quads").height() + $("#toolbar").height() + imageListMinHeight > $("#imageMainPanel").height()) 
    {
        SetImageListHeight();

        if (debug) { fnDebug("quads too tall"); }

        //console.log("SizeImagePortrait(): setting $('#quads').height() to ($('#imageMainPanel').height() - $('#toolbar').height() - imageListMinHeight) = " + ($("#imageMainPanel").height() - $("#toolbar").height() - imageListMinHeight));
        $("#quads").height($("#imageMainPanel").height() - $("#toolbar").height() - imageListMinHeight);
        //console.log("SizeImagePortrait(): setting  $('#quads').width() = " + ($("#quads").height() * imgAspRatio));
        $("#quads").width($("#quads").height() * imgAspRatio);
        //console.log("SizeImagePortrait(): $('#quads').height() = " + $("#quads").height());
        //console.log("SizeImagePortrait(): $('#quads').width() = " + $("#quads").width());

        if (debug) { fnDebug("quads height limited."); }
    }

    $("#canvasVideo").height($("#video_container").height());
    $("#canvasVideo").width($("#video_container").width());

    $("#canvasImage").width($("#video_container").width());

    //set height according to aspect ratio here.
    $("#canvasImage").height($("#canvasImage").width() / imgAspRatio);

    var containerHeight;

    if (fullscreenMode) 
    {
        containerHeight = $(window).height();
        if (debug) { fnDebug("Portrait Fullscreen"); }

        //set myiframe to width of the main imaging panel (-5% for margin)
        $("#video_container").width($(window).width() * hMargin)

        //console.log("SizeImagePortrait(): setting $('#video_container').height() to " + ($("#video_container").width() / imgAspRatio));
        $("#video_container").height($("#video_container").width() / imgAspRatio);
        $("#canvasVideo").width($("#video_container").width());
        $("#canvasVideo").height($("#video_container").height());

        $("#toolbar").css("bottom", 20);
    }
    else 
    {
        containerHeight = $("#imageMainPanel").height();
    }
        
    //if height of all elements is greater than total screen space, limit the height and set width to aspect ratio
    if ($("#canvasImage").height() + $("#toolbar").height() + imageListMinHeight > $("#imageMainPanel").height()) 
    {

        if (debug) { fnDebug("canvasimage height - " + $("#canvasImage").height() + " toolbar height - " + $("#toolbar").height()); }

        if (isPortrait) 
        {

            $("#canvasImage").width($(window).width() * .98);
            $("#canvasImage").height($("#canvasImage").width() / imgAspRatio);
            $("#video_container").width($(window).width());

            if ($("#canvasImage").height() > containerHeight) 
            {
                if (debug) { fnDebug("SizeImagePortrait() cavansImage too tall"); }
                $("#canvasImage").height(containerHeight - $("#toolbar").height() - imageListMinHeight);
                $("#canvasImage").width($("#canvasImage").height() * imgAspRatio);
            }

        }
        else 
        {
            $("#canvasImage").height($("#imageMainPanel").height() - $("#toolbar").height() - imageListMinHeight);
        }

        if (debug) { fnDebug("canvas image height limited."); }
    }

    $("#canvasImage").css("z-index", "");
    $("#canvasVideo").css("z-index", "");

    SetImageListHeight();

    if (fullscreenMode) 
    {
        SetToolBar();
	}

	$("#drawDiv").height($("#canvasVideo").height());
	$("#drawDiv").width($("#canvasVideo").width());
	$("#canvas").height($("#canvasVideo").height());
	$("#canvas").width($("#canvasVideo").width());
}

function SetToolBar() 
{
    $("#toolbar").css("position", "absolute");
    $("#toolbar").height(51);

    var imgTypeTemp = $("div.thumbnail.selected").find("p").html();
    var arImgType = [];
    arImgType = imgTypeTemp.split("|");
    //var imgType = arImgType[0];
    //var aspRatio = arImgType[1];

    //4.1 DWC Set toolbar 10% left of screen on all Landscape orentation to prevent shifting of controls
    if (isPortrait) 
    {
        var tbPosition = parseFloat(($(window).width() - 500) / 2);
        if (debug) { fnDebug("SetToolBar() tbPosition - " + tbPosition); }

        if (debug) { fnDebug("SetToolBar() canvasVideo Height - " + $("#canvasVideo").height()); }

        var verticalOffset

        if (fullscreenMode) 
        {

        }
        else 
        {
            $("#toolbar").css("bottom", "");
            verticalOffset = ($("#video_container").height() + 20)
        }

        $("#toolbar").css("top", verticalOffset);     
    }
    else 
    {
        var tbPosition = parseFloat($(window).width() * .20);
        $("#toolbar").css("position", "relative");
        $("#toolbar").css("top", "");
        $("#toolbar").css("bottom", 0);
    }

    $("#toolbar").css("left", tbPosition);

    if (fullscreenMode) 
    {
        verticalOffset = $(window).height() - 70;
            
        $("#toolbar").css("position", "absolute");
        $("#toolbar").css("top", verticalOffset);
    }

    //console.log("SetToolBar(): $('#toolbar').css('top') = " + $("#toolbar").css("top"));
    //console.log("SetToolBar(): verticalOffset = " + verticalOffset);
    //console.log("SetToolBar(): $('#video_container').innerHeight() = " + $("#video_container").innerHeight());
    //console.log("SetToolBar(): $('#video_container').innerWidth() = " + $("#video_container").innerWidth());
}

function SetImageListHeight() 
{

    if (debug) { fnDebug("Panel: " + $("#imageMainPanel").height() + " Views: " + $("#video_container").height() + " Toolbar: " + $("#toolbar").height()); }

    var ImageListHeight = ($("#imageMainPanel").height() - ($("#video_container").height() + $("#toolbar").height())) * vMargin;

    if (ImageListHeight < imageListMinHeight) { ImageListHeight = imageListMinHeight; }

    if (debug) { ("SetImageListHeight() ImageListHeight: " + ImageListHeight); }

    $("#ImagesList").css("height", ImageListHeight);
}