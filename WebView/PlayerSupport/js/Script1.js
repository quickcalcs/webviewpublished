//var canvas = document.getElementsByTagName('canvas')[0];

var rtime = new Date(1, 1, 2000, 12, 00, 00);
var timeout = false;
var delta = 200;

$(window).resize(function () {
	rtime = new Date();
	if (timeout === false) {
		timeout = true;
		setTimeout(resizeend, delta);
	}
});

function resizeend() {

	if (new Date() - rtime < delta) {
		setTimeout(resizeend, delta);
	}
	else {
		timeout = false;
		//console.log("resizeend - calling sizeImage");
        sizeImage();
        setDrawDivStyle();//implemented in quickcals.js
	}

}

var studyid = document.getElementById("SelectedStudy");

var browserName = navigator.appName;
var begintime, endtime;
var BrightnessAmount;
var dragStart, dragged;
var inverted = 0;
var sepia = "0";
// var liIndex = 0;
var video;
var isSlideShow = false;
var slidedelay = 10000;
var previewMode = false;
var fullscreenMode = false;
var margin = .99;
var drawStage = null;

//var bustcachevar = 0 //bust potential caching of external pages after initial request? (1=yes, 0=no)
var loadedobjects = ""
var rootdomain = "http://" + window.location.hostname
var bustcacheparameter = ""
var alertOn = false;

var view1 = document.getElementById("View1");
var view2 = document.getElementById("View2");
var view3 = document.getElementById("View3");
var view4 = document.getElementById("View4");
var aspRatio;
var TO = false;

var imgFrameRate;                   //4.2.1 sf 11397 Add Frame rate to improve stepping

var IElastFrameBuffer = .9;         //4.2.1 sf 11397 set how close to the end of the video duration we can go for IE to prevent crashes

var strCathText;                    //4.2.1 sf 11999 Cath Overlay

//4.1 DWC make tdHeight and tdWidth global as sizes of quad table cells will be fixed
var tdHeight;
var tdWidth;

var isMSIE = getInternetExplorerVersion();

if (isMSIE != -1) { browserName = "Microsoft Internet Explorer"; }

function getInternetExplorerVersion() {
	var rv = -1;

	if (navigator.appName == 'Microsoft Internet Explorer') {
		var ua = navigator.userAgent;
		var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		if (re.exec(ua) != null)
			rv = parseFloat(RegExp.$1);
	}
	else if (navigator.appName == 'Netscape') {
		var ua = navigator.userAgent;
		var re = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
		if (re.exec(ua) != null)
			rv = parseFloat(RegExp.$1);
	}

	return rv;
}

function sizeImage() {
	//console.log("sizeImage - isPotrait = " + isPortrait);
	if (debug) { fnDebug("sizeImage - isPotrait = " + isPortrait); }

	//4.1 DWC - Call Set Portrait or Set Landscape based on browser orientation.  
	//Only call Set Portrait if it is not yet set as resetting each time an image is 
	//sized causes the thubmnails div to scroll back to top losing the user's position
	//console.log("previewMode = " + previewMode);
	//console.log("$(window).innerWidth() = " + $(window).innerWidth());
	//console.log("$(window).innerHeight() = " + $(window).innerHeight());

	if ($(window).innerWidth() < $(window).innerHeight() && !isPortrait) {
		//console.log("about to SetPortrait for W < H and !isPortrait");
		SetPortrait();

		if (previewMode) {
			//console.log("in previewMode");
			previewMode = false;
			PreviewsBtn_onclick();
		}

		return;
	}

	//Set Landscape if screen is wide, if Portrait is set return, if not allow sizing of all elements
	if (isPortrait && $(window).innerWidth() > $(window).innerHeight()) {
		//console.log("about to SetLandscape for W > H and isportrait");
		SetLandscape();

		if (previewMode) {
			//console.log("in previewMode");
			previewMode = false;
			PreviewsBtn_onclick();
		}
		else {
			//console.log("ShowVideo");
			ShowVideo();
		}

		return;
	}

	if (isPortrait && $(window).innerWidth() < $(window).innerHeight()) {
		//console.log("about to SetPortrait for W < H and isportrait");
		SetPortrait();

		if (previewMode) {
			//console.log("in previewMode");
			previewMode = false;
			PreviewsBtn_onclick();
		}

		return;
	}

	//4.1 DWC resize previews (by going through Preview BTN press function upon resize from landscape to landscape
	if (!isPortrait && $(window).innerWidth() > $(window).innerHeight() && previewMode) {
		//console.log("prevMode and W > H and !isportrait");

		if (previewMode) {
			//console.log("in previewMode");
			previewMode = false;
			PreviewsBtn_onclick();
		}

		return;
	}

	if (debug) { fnDebug("sizeImage(): Called "); }

	//4.1 size images dynamically

	//console.log("sizeImage() fullscreen mode: " + fullscreenMode);
	if (debug) { fnDebug("sizeImage() fullscreen mode: " + fullscreenMode); }

	var imgTypeTemp = $("div.thumbnail.selected").find("p").html();

	//console.log("imgTypeTemp = " + imgTypeTemp);

	var arImgType = [];
	arImgType = imgTypeTemp.split("|");
	var imgType = arImgType[0];
	var imgAspRatio = arImgType[1];

	//console.log("imgAspRatio = " + imgAspRatio);

	imgFrameRate = arImgType[2];            //4.2.1 sf 11397 Add Frame rate to improve stepping
	strCathText = arImgType[3];             //4.2.1 sf 11999 Cath Overlay

	//4.1 size images dynamically
	aspRatio = imgAspRatio;

	if (alertOn) { alert(imgType); }

	//if aspect ratio is unknown, set to 4:3
	if (aspRatio == undefined) {
		//console.log("Aspect Ratio is Undefined");
		if (debug) { fnDebug("Aspect Ratio is Undefined"); }
		aspRatio = (4 / 3);
	}

	if (debug) { fnDebug("sizeImage(): " + aspRatio); }

	if (debug) {
		fnDebug("sizeImage() previewMode = " + previewMode);
	}
	else {

		if (imgType == "mp4" || imgType == "webm") {

			if (alertOn) { alert("video_container resized"); }

			if (fullscreenMode) {
				if (alertOn) { alert("fullscreeenmode"); }

				$("#video_container").height(0);

				//console.log("$('#canvasVideo').width() = " + $("#canvasVideo").width());
				//console.log("$(window).innerWidth() = " + $(window).innerWidth());

				if ($("#canvasVideo").width() >= ($(window).innerWidth() * margin)) {
					$("#canvasVideo").width(($(window).innerWidth() * margin));

					//4.1 DWC change from first image asp ratio to selected image aspect ratio
					$("#canvasVideo").height(($(window).innerWidth() * margin) / imgAspRatio);
				}
				else {
					$("#canvasVideo").width(($(window).innerWidth() * margin));

					//4.1 DWC change from first image asp ratio to selected image aspect ratio
					var newImgHeight = ($(window).innerWidth() * margin) / imgAspRatio;
					var newImgWidth = ($(window).innerWidth() * margin);

					$("#canvasVideo").height(newImgHeight);
				}

				if ($("#canvasVideo").height() >= ($(window).innerHeight() * margin) - 70) {
					var newImgHeight = (($(window).innerHeight() * margin) - 70);
					$("#canvasVideo").height(newImgHeight);

					//4.1 DWC change from first image asp ratio to selected image aspect ratio
					var newImgWidth = (newImgHeight * imgAspRatio);
					$("#canvasVideo").width(newImgWidth);
				}

				$("#toolbar").width($("#canvasVideo").width());
			}
			else {

				//4.1 DWC change from first image asp ratio to selected image aspect ratio
				//if (imgAspRatio == null)
				if ((imgAspRatio == null) || (imgAspRatio == "")) {

					if (debug) { fnDebug("aspect not available"); }
					video = $("#canvasVideo");
					video.load();

					$(document).ready(function () {
						video.on("loadedmetadata", function () {
							//alert("test");
							$("#blankImage").hide();
							$("#ImagesList").css("min-width", "");

							//console.log("video.width() = " + video.width());
							if (alertOn) { alert(video.width()); }

							//4.1 DWC change from first image asp ratio to selected image aspect ratio
							var imgAspRatio = video.width() / video.height();

							//console.log("imgAspRatio = " + imgAspRatio);

							//4.1 DWC change from first image asp ratio to selected image aspect ratio
							if (alertOn) { alert(imgAspRatio); }

							//console.log("SizeImage(): setting $('#video_container').height() to " + ($("#imageMainPanel").height() - 40));
							$("#video_container").height(($("#imageMainPanel").height() - 40));

							//Set thumbnails container to take what is left of the screen after image is rendered
							$("#ImagesList").width($("#imageMainPanel").width() - $("#video_container").width() - 20);

							if ($("#ImagesList").width() < 160) {
								if (alertOn) { alert($("#ImagesList").width()); }

								//In case screen is small or image is large, set the mininum width of the container to one column of thumbnails
								$("#ImagesList").css("min-width", "160px");

								//In case the thumbnail container changed sizes to minimum, resize the video container width to fit
								$("#video_container").width($("#imageMainPanel").width() - 170);

								//4.1 DWC change from first image asp ratio to selected image aspect ratio
								//console.log("SizeImage(): setting $('#video_container').height() to " + ($("#video_container").width() / imgAspRatio));
								$("#video_container").height($("#video_container").width() / imgAspRatio);
								if (alertOn) { alert("thumbnails minium width set"); }
							}

							$("#canvasVideo").width($("#video_container").width());
							$("#canvasVideo").height($("#video_container").height());
						});
					});
				}
				else {
					if (debug) { fnDebug("ratio available"); }

					$("#ImagesList").css("min-width", "");

					if (alertOn) { alert(video.width()); }

					//4.1 DWC change from first image asp ratio to selected image aspect ratio
					if (alertOn) { alert(imgAspRatio); }

					$("#imageMainPanel").width("99%");
					//alert("imageMainPanel width: " + $("#imageMainPanel").width());

					//alert("ImageMainPanel Height: " + $("#imageMainPanel").height())
					//console.log("SizeImage(): setting $('#video_container').height() to " + ($("#imageMainPanel").height() - 40));
					$("#video_container").height(($("#imageMainPanel").height() - 40));

					$("#myiframe").height(($("#imageMainPanel").height() - 40));
					$("#myiframe").width($("#myiframe").height() * imgAspRatio);
					//alert (" myiframe Width: " + $("#myiframe").width() );

					//4.1 DWC change from first image asp ratio to selected image aspect ratio
					$("#video_container").width($("#video_container").height() * imgAspRatio);

					$("#canvasVideo").width($("#myiframe").width());
					$("#canvasVideo").height($("#canvasVideo").width() / imgAspRatio);

					$("#ImagesList").width($("#imageMainPanel").width() - $("#myiframe").width() - 10);

					//alert("ImagesList width: " + $("#ImagesList").width());

					if ($("#ImagesList").width() < 160) {
						if (alertOn) { alert($("#ImagesList").width()); }

						//In case screen is small or image is large, set the mininum width of the container to one column of thumbnails
						$("#ImagesList").css("min-width", "160px");

						//In case the thumbnail container changed sizes to minimum, resize the video container width to fit
						$("#myiframe").width($("#imageMainPanel").width() - 170);

						//4.1 DWC change from first image asp ratio to selected image aspect ratio
						$("#myiframe").height($("#myiframe").width() / imgAspRatio);

						$("#canvasVideo").height($("#myiframe").height());
						$("#canvasVideo").width($("#canvasVideo").height() * imgAspRatio);

						//console.log("SizeImage(): setting $('#video_container').height() to " + $("#canvasVideo").height());
						$("#video_container").height(($("#canvasVideo").height()));
						if (alertOn) { alert("thumbnails minium width set"); }

						$("video_container").width($("#myiframe").width());
					}

				}

			}

		}
		else if (imgType == "jpg") {

			//4.1 DWC change from first image asp ratio to selected image aspect ratio
			//if (imgAspRatio == null)
			if ((imgAspRatio == null) || (imgAspRatio == "")) {
				//console.log("imgAspRatio == null OR imgAspRatio == ''");
				//4.1 DWC change from first image asp ratio to selected image aspect ratio
				if (debug) { fnDebug(imgAspRatio); }

				var img = new $("#canvasImage");
				img.load();

				$(document).ready(function () {
					img.load(function () {
						$("#blankImage").hide();
						//console.log("hide #canvasImage");
						$("#canvasImage").hide();

						var canvasImageWidth = $("#canvasImage").width();
						var canvasImageHeight = $("#canvasImage").height();

						//console.log("canvasImageWidth = " + canvasImageWidth);
						//console.log("canvasImageHeight = " + canvasImageHeight);

						var canvasImageRatio = canvasImageWidth / canvasImageHeight;

						if (fullscreenMode) {

							if ($("#canvasImage").width() >= ($(window).innerWidth()) * margin) {

								$("#canvasImage").width(($(window).innerWidth() * margin));
								$("#canvasImage").height(($(window).innerWidth() * margin) / canvasImageRatio);
							}
							else {
								$("#canvasImage").width(($(window).innerWidth() * margin));
								var newImgHeight = ($(window).innerWidth() * margin) / canvasImageRatio;
								var newImgWidth = ($(window).innerWidth() * margin);
								$("#canvasImage").height(newImgHeight);
							}

							if ($("#canvasImage").height() >= ($(window).innerHeight() * .95) - 100) {
								var newImgHeight = (($(window).innerHeight() * margin) - 100);
								$("#canvasImage").height(newImgHeight);
								var newImgWidth = (newImgHeight * canvasImageRatio);
								$("#canvasImage").width(newImgWidth);
							}

							$("#toolbar").width($("#canvasImage").width());
						}
						else {
							$("#canvasImage").height($("#imageMainPanel").height() - 40);
							$("#video_container").width($("#canvasImage").width());

							$("#ImagesList").width($("#mainpanel").width() - $("#video_container").width() - 40);

							if ($("#ImagesList").width() < 160) {
								if (alertOn) { alert($("#ImagesList").width()); }

								//In case screen is small or image is large, set the mininum width of the container to one column of thumbnails
								$("#ImagesList").css("min-width", "160px");

								//In case the thumbnail container changed sizes to minimum, resize the video container width to fit
								$("#video_container").width($("#imageMainPanel").width() - 170);

								//4.1 DWC change from first image asp ratio to selected image aspect ratio
								$("#canvasImage").height($("#video_container").width() / imgAspRatio);
								//console.log("$('#canvasImage').height() = " + $("#canvasImage").height());
								if (alertOn) { alert("thumbnails minium width set"); }
							}

						}

						//console.log("show #canvasImage");
						$("#canvasImage").show();

						imgTypeTemp = $("div.thumbnail.selected").find("p").html();
						if (imgTypeTemp == "jpg" || imgTypeTemp == "dcm") { }   //****Why???*** 
					});
				});
			}
			else {
				//4.1 DWC change from first image asp ratio to selected image aspect ratio
				if (debug) { fnDebug(imgAspRatio); }

				$("#imageMainPanel").width("99%");
				$("#ImagesList").css("min-width", "");

				if (fullscreenMode == false) {
					if (alertOn) { alert("about to size video container, fullscreenmode was false"); }
					//console.log("SizeImage(): setting $('#video_container').height() to " + ($("#imageMainPanel").height() - 40));
					$("#video_container").height(($("#imageMainPanel").height() - 40));

					$("#myiframe").height($("#imageMainPanel").height() - 40);
					$("#myiframe").width($("#myiframe").height() * imgAspRatio);

					//4.1 DWC change from first image asp ratio to selected image aspect ratio
					$("#video_container").width($("#video_container").height() * imgAspRatio);

					//if setting jpg width exceded container width, then set max size of jpg by width
					//if ($("#canvasImage").width() > $("#myiframe").width()) {

					$("#canvasImage").width($("#myiframe").width());
					$("#canvasImage").height($("#canvasImage").width() / imgAspRatio);
					//}

					if (alertOn) { alert($("#video_container").width()); }

					//Set thumbnails container to take what is left of the screen after image is rendered
					$("#ImagesList").width($("#imageMainPanel").width() - (($("#imageMainPanel").height() - 40) * imgAspRatio) - 10);

					//alert($("#ImagesList").width());

					if ($("#ImagesList").width() < 160) {

						if (alertOn) {
							alert($("#ImagesList").width());
						}

						//In case screen is small or image is large, set the mininum width of the container to one column of thumbnails
						$("#ImagesList").css("min-width", "160px");

						$("#canvasImage").width($("#imageMainPanel").width() - 170);
						$("#canvasImage").height($("#canvasImage").width() / imgAspRatio);

						//In case the thumbnail container changed sizes to minimum, resize the video container width to fit
						$("#myiframe").width($("#canvasImage").width());
						//alert($("#myframe").width());
						//4.1 DWC change from first image asp ratio to selected image aspect ratio
						$("#myframe").height($("#myiframe").width() / imgAspRatio);

						$("#video_container").width($("#canvasImage").width());
						//$("#canvasImage").height($("#myframe").height());
						//$("#canvasImage").height($("#imageMainPanel").height() - 70);
						//$("#canvasImage").width($("#canvasImage").height() * imgAspRatio);

						if (alertOn) { alert($("#video_container").width()); }

						if (alertOn) { alert("thumbnails minium width set"); }
					}

					if (alertOn) { alert("sized video container, fullscreenmode was false"); }
				}

				if (fullscreenMode) {
					$("#video_container").height(0);

					if ($("#canvasImage").width() >= ($(window).innerWidth()) * margin) {
						$("#canvasImage").width(($(window).innerWidth() * margin));

						//4.1 DWC change from first image asp ratio to selected image aspect ratio
						$("#canvasImage").height(($(window).innerWidth() * margin) / imgAspRatio);
					}
					else {
						$("#canvasImage").width(($(window).innerWidth() * margin));

						//4.1 DWC change from first image asp ratio to selected image aspect ratio
						var newImgHeight = ($(window).innerWidth() * margin) / imgAspRatio;

						var newImgWidth = ($(window).innerWidth() * margin);
						$("#canvasImage").height(newImgHeight);
					}

					if ($("#canvasImage").height() >= ($(window).innerHeight() * margin) - 40) {
						var newImgHeight = (($(window).innerHeight() * margin) - 40);
						$("#canvasImage").height(newImgHeight);

						//4.1 DWC change from first image asp ratio to selected image aspect ratio
						var newImgWidth = (newImgHeight * imgAspRatio);

						$("#canvasImage").width(newImgWidth);
					}

					$("#toolbar").width($("#canvasImage").width());
				}

			}

		}

	}

	SetToolBar();

	//4.2.1 SF 11999 Cath Overlay
	DisplayCathOverlay(strCathText);
	//console.log("Made it back from DisplayCathOverlay(strCathText)");
}

function sizeQuadScreen(aspRatio) {
	//console.log("sizeQuadScreen(" + aspRatio + "): Entering method");
	//4.1 DWC SET ASPECT RATIO TO 4:3 FOR ALL TILES
	aspRatio = 4 / 3;

	if (debug) { fnDebug("sizeQuadScreen(" + aspRatio + "): Called "); }
	if (debug) { fnDebug("sizeQuadScreen() fullscreenMode = " + fullscreenMode + " isPortrait= " + isPortrait); }

	//console.log("sizeQuadScreen(" + aspRatio + "): setting tdHeight to (($('#imageMainPanel').height() - 50) / 2) = " + (($("#imageMainPanel").height() - 50) / 2));
	tdHeight = (($("#imageMainPanel").height() - 50) / 2);
	tdWidth = tdHeight * aspRatio;
	var quadWidth = $("#quads").width();

	//4.1 DWC CSS and jquery doesn't always report table width the same, correct if discrpency exits
	if (quadWidth < (tdWidth * 2)) {
		//console.log("sizeQuadScreen(" + aspRatio + "): setting $('#quads').width() to (tdWidth * 2) = " + (tdWidth * 2));
		$("#quads").width(tdWidth * 2);
		//console.log("sizeQuadScreen(aspRatio): $('#quads').width() = " + $("#quads").width());
		quadWidth = $("#quads").width();
	}

	if (debug) { fnDebug("sizeQuadScreen() after set table cells tdHeight: " + tdHeight + " tdWidth: " + tdWidth); }

	//4.1 set tdWidth to half the screen, with a minimal of 170 pixels for one column of thumbnails.
	if ($("#imageMainPanel").width() - (quadWidth) < 175) {
		tdWidth = (($("#imageMainPanel").width() - 175) / 2);
	}

	//4.1 DWC added logic for portrait mode, both full screen and portrat use full screen width.
	if (fullscreenMode || isPortrait) {
		//console.log("sizeQuadScreen(" + aspRatio + "): setting tdHeight to ((($(window).innerHeight() * margin) - 40) / 2) = " + ((($(window).innerHeight() * margin) - 40) / 2));
		tdHeight = (($(window).innerHeight() * margin) - 40) / 2;
		tdWidth = (($(window).innerWidth() * margin) - 30) / 2;

		$("#ViewsDiv").width(tdWidth * 2);
		$("#video_container").width(tdWidth * 2);
	}

	//4.1 DWC if in portrait mode need to size table and views for maximum width available.
	if (isPortrait) {
		if (debug) { fnDebug("Set Portrait for Quads"); }
		if (debug) { fnDebug("tdWidth - " + tdWidth); }
		if (debug) { fnDebug("aspRatio - " + aspRatio); }
		if (debug) { fnDebug("sizeQuadScreen() quads table width before set: " + $("#quads").width()); }

		//in portrait, quads table should be as wide as the image panel
		tdWidth = ($("#mainpanel").width() - 20) / 2
		//console.log("sizeQuadScreen(" + aspRatio + "): setting $('#quads').width() to (tdWidth * 2) = " + (tdWidth * 2));
		$("#quads").width(tdWidth * 2);
		//console.log("sizeQuadScreen(" + aspRatio + "): $('#quads').width() = " + $("#quads").width());
		//console.log("sizeQuadScreen(" + aspRatio + "): setting tdHeight to (tdWidth / aspRatio) = " + (tdWidth / aspRatio));
		tdHeight = (tdWidth / aspRatio);

		if (debug) { fnDebug("sizeQuadScreen() quads table width after set: " + $("#quads").width()); }

		if (!fullscreenMode && $("#ImagesList").height() < imageListMinHeight) {
			//console.log("sizeQuadScreen(" + aspRatio + "): setting tdHeight to (tdHeight - (($('#ImagesList').height() + $('#toolbar').height()))) = " + (tdHeight - (($("#ImagesList").height() + $("#toolbar").height()))));
			tdHeight = tdHeight - ($("#ImagesList").height() + $("#toolbar").height());
		}

		tdHeight = parseInt(tdHeight, 10);

		if (debug) { fnDebug("tdHeight - " + tdHeight); }

		//DWC 4.1 Size quads table height
		//console.log("sizeQuadScreen(" + aspRatio + "): setting $('#quads').height() to (tdHeight * 2) = " + (tdHeight * 2));
		$("#quads").height(tdHeight * 2);
		//console.log("sizeQuadScreen(viewImgRatio): $('#quads').height() = " + $("#quads").height());

		$("#myiframe").width($("#mainpanel").width() - 20);
		$("#ImagesList").width($("#mainpanel").width() - 20);

		$("ViewsDiv").css("width", $("#quads").width());
		$("#video_container").width($("#mainpanel").width() - 20);
		//alert($("#video_container").width());

		if (fullscreenMode) {
			$("#toolbar").css("bottom", "");
			$("#ViewsDiv").css("position", "");
			$("#video_container").css("position", "");
		}

		//4.1 DWC Set imageslist to the total window height minus the height of the quad images and 10% for border at the bottom.
		if (debug) { fnDebug("sizeQuadScreen() after portrait settings tdHeight: " + tdHeight + " tdWidth: " + tdWidth); }

		return;
	}

	//4.1 DWC Test - set toolbar after sizing tiles?
	//SetToolBar();

	if (debug) { fnDebug("sizeQuadScreen() tdHeight: " + tdHeight + " tdWidth: " + tdWidth); }

	// 4.1 Set all DIVs inside table cells to fixed size
	for (var ii = 1; ii < 4; ii++) {
		$("divView" + ii).height(tdHeight);
		$("divView" + ii).width(tdWidth);
	}

	//4.1 DWC - Set ViewsDiv and video_container to View1 height plus View 3 height
	//console.log("sizeQuadScreen(" + aspRatio + "): setting quadHeight to (tdHeight * 2) = " + (tdHeight * 2));
	var quadHeight = tdHeight * 2;
	quadWidth = $("#quads").width();            //*****Should this be 2 times tdWidth?  Remove table padding *****

	//4.1 DWC CSS and jquery doesn't always report table width the same, correct if discrpency exits
	if (quadWidth < (tdWidth * 2)) {
		//console.log("sizeQuadScreen(" + aspRatio + "): setting $('#quads').width() to (tdWidth * 2) = " + (tdWidth * 2));
		$("#quads").width(tdWidth * 2);
		//console.log("sizeQuadScreen(viewImgRatio): $('#quads').width() = " + $("#quads").width());
		quadWidth = $("#quads").width();
	}

	$("#ImagesList").width($("#imageMainPanel").width() - quadWidth - 15);

	if ($("#ImagesList").width() < 175) {
		$("#ImagesList").width(160);
		//console.log("sizeQuadScreen(" + aspRatio + "): setting $('#quads').width() to ($('#imageMainPanel').width() - 180) = " + ($("#imageMainPanel").width() - 180));
		$("#quads").width($("#imageMainPanel").width() - 180);
		//console.log("sizeQuadScreen(viewImgRatio): $('#quads').width() = " + $("#quads").width());
		quadWidth = $("#quads").width();
	}

	//4.1 DWC myiframe is size is lost in transition beteeen fullscreen previews and default display mode 
	//console.log("sizeQuadScreen(" + aspRatio + "): setting $('#ViewsDiv').height() to quadHeight = " + quadHeight);
	$("#ViewsDiv").height(quadHeight);
	//console.log("sizeQuadScreen(" + aspRatio + "): setting $('#video_container').height() to quadHeight = " + quadHeight);
	$("#video_container").height(quadHeight);
	$("#myiframe").height(quadHeight);
	$("#ViewsDiv").width(quadWidth);           //*****Remove 10?  imageMaminPanel offset 10 and 10 *****
	$("#video_container").width(quadWidth);    //                THE ANSWER WAS YES                *****
	$("#myiframe").width(quadWidth);           //*******************************************************

	//console.log("sizeQuadScreen(" + aspRatio + "): $('#video_container').innerHeight() = " + $("#video_container").innerHeight());
	//console.log("sizeQuadScreen(" + aspRatio + "): $('#video_container').innerWidth() = " + $("#video_container").innerWidth());
	//console.log("sizeQuadScreen(" + aspRatio + "): $('#quads').height() = " + $("#quads").height());

	if ($("#quads").height() > $("#video_container").height()) {
		//console.log("Somehow 100% is greater than 100% - WHY????");
		//$("#quads").css("height", "100%");
		//$("#quads").css("height", quadHeight);
		$("#quads").height(quadHeight);
		//console.log("what is CSS value of $('#quads').css('height')? => " + $("#quads").css("height"));
		//$("#quads").css("height", "100%");
		//console.log("what is CSS value of $('#quads').css('height')? => " + $("#quads").css("height"));
	}
	else {
		$("#quads").height("100%");
	}

	//4.1 DWC Test - set toolbar after sizing tiles?
	SetToolBar();

	if (debug) { fnDebug("sizeQuadScreen(" + aspRatio + "): tdHeight: " + tdHeight); }
}

function sizePreview(i, viewImgRatio) {
	if (debug) { fnDebug("sizePreview(): " + i + " viewImgRatio: " + viewImgRatio); }

	//get position of selected thumnail and then add the quad index position
	var index = parseInt($("#imagepicker").val(), 10) + i - 1;

	//get metadata from selected image, or the first, second, or third image after selected, depending on quad position
	var imgTypeTemp = $(".thumbnails li:eq(" + index + ")").find("div.thumbnail").find("p").html();

	if (debug) { fnDebug("sizePreview(): " + i + " tdHeight: " + tdHeight + " tdWidth: " + tdWidth); }

	//parse meta data and get image type and aspect ratio
	var arImgType = [];

	arImgType = imgTypeTemp.split("|");
	var imgType = arImgType[0];
	var imgAspRatio = arImgType[1];

	if (fullscreenMode) {
		tdHeight = (($(window).innerHeight() * .95) - 40) / 2;
		tdWidth = ($(window).innerWidth() * .95) / 2;

		$("#toolbar").width(($("#View1").width()) * 2);
	}

	//set image width to container width
	$("#ViewImg" + i).width(tdWidth);

	//set height by the image's aspect ratio
	$("#ViewImg" + i).height($("#ViewImg" + i).width() / imgAspRatio);

	//if image height is greater than the container, set to container height, then set width by aspect ratio
	if ($("#ViewImg" + i).height() > tdHeight) {
		if (debug) { fnDebug("img height styling"); }
		$("#ViewImg" + i).height(tdHeight);
		$("#ViewImg" + i).width(tdHeight * imgAspRatio);
	}

	//set image width to container width
	$("#View" + i).width(tdWidth);

	//set height by the image's aspect ratio
	$("#View" + i).height($("#View" + i).width() / imgAspRatio);

	//if image height is greater than the container, set to container height, then set width by aspect ratio
	if ($("#View" + i).height() > tdHeight) {
		if (debug) { fnDebug("img height styling"); }
		$("#View" + i).height(tdHeight);
		$("#View" + i).width(tdHeight * imgAspRatio);
	}

	//4.1 DWC - Set ViewsDiv and video_container to quad table height and width
	//console.log("sizePreview(i, viewImgRatio): $('#quads').height() = " + $("#quads").height());
	//console.log("sizePreview(i, viewImgRatio): $('#quads').width() = " + $("#quads").width());
	//console.log("sizePreview(i, viewImgRatio): setting quadHeight to $('#quads').height() = " + $("#quads").height());
	var quadHeight = $("#quads").height();
	var quadWidth = $("#quads").width();

	//4.1 DWC CSS and jquery doesn't always report table width the same, correct if discrpency exits
	if (quadWidth < (tdWidth * 2)) {
		//console.log("sizePreview(i, viewImgRatio): setting $('#quads').width() to " + (tdWidth * 2));
		$("#quads").width(tdWidth * 2);
		//console.log("sizePreview(i, viewImgRatio): $('#quads').width() = " + $("#quads").width());
		quadWidth = $("#quads").width();
	}

	//console.log("sizePreview(i, viewImgRatio): $('#ViewsDiv').height() = " + $("#ViewsDiv").height());
	//console.log("sizePreview(i, viewImgRatio): $('#ViewsDiv').width() = " + $("#ViewsDiv").width());

	//console.log("sizePreview(i, viewImgRatio): setting $('#ViewsDiv').height() to quadHeight = " + quadHeight);
	$("#ViewsDiv").height(quadHeight);
	//console.log("sizePreview(i, viewImgRatio): setting $('#video_container').height() to quadHeight = " + quadHeight);
	$("#video_container").height(quadHeight);

	$("#ViewsDiv").width(quadWidth);
	$("#video_container").width(quadWidth);
	$("#myiframe").width(quadWidth);

	//console.log("sizePreview(i, viewImgRatio): $('#ViewsDiv').height() = " + $("#ViewsDiv").height());
	//console.log("sizePreview(i, viewImgRatio): $('#ViewsDiv').width() = " + $("#ViewsDiv").width());

	//4.1 DWC Thumnmail div is phushing to far to the right, losing scrollbar, so limit width
	if (isPortrait) {
		$("#myiframe").width($("#mainpanel").width() - 10);
		$("#ImagesList").width($("#mainpanel").width() - 10);
		$("#video_container").width($("#mainpanel").width() - 10);
		SetImageListHeight()
	}

}

function StepForwardBtn_onclick() {
	video = document.getElementById('canvasVideo');

	///4.2.3 sf 12860 fetch framerate on demand
	if (imgFrameRate == 0) {
		PauseVideo();
	}
	else {
		video.pause();
	}

	// 4.1 DWC Allow all frame advances to proceed at .024 seconds, but fall back to last time if err condition occurs

	if (debug) { fnDebug("Frame Advance video.duration: " + video.duration + " video.currentTime: " + video.currentTime); }

	var frameRate = imgFrameRate;               //4.2.1 sf 11397 Add Frame rate to improve stepping
	var frameTime = 1 / frameRate;
	var currentTime = video.currentTime;
	var theCurrentFrame = Math.floor(currentTime / frameTime);
	var currentFrameStartTime = theCurrentFrame * frameTime;

	if (debug) { fnDebug("frameRate=" + frameRate); }
	if (debug) { fnDebug("currentTime=" + currentTime); }
	if (debug) { fnDebug("theCurrentFrame=" + theCurrentFrame); }
	if (debug) { fnDebug("currentFrameStartTime=" + currentFrameStartTime); }

	//4.1 limit frame advance to 99% of file length to prevent crash in IE

	//4.2.1 sf 11397 Add Frame rate to improve stepping**************************************************

	if (browserName == "Microsoft Internet Explorer") {
		if (debug) { fnDebug("frameTime =" + frameTime); }

		if ((video.currentTime + frameTime) <= (video.duration - (frameTime))) {

			try {
				video.currentTime = video.currentTime + (frameTime);
				//video.currentTime = 2.9;
				currentTime = video.currentTime
				if (debug) { fnDebug("newTime=" + currentTime); }
				if (debug) { fnDebug("newFrame=" + theCurrentFrame); }
			}
			catch (e) {
				if (debug) { fnDebug(e.Message); }

				video.currentTime = currentTime;
			}

		}
		else {
			//            if (video.currentTime + (frameTime / 2) < (video.duration * IElastFrameBuffer)) {
			//                try {
			//                    if (debug) { fnDebug("about to add half frameTime for IE"); }
			//                    video.currentTime = 3 // video.currentTime + (frameTime / 2);
			//                }
			//                catch (e) {
			//                    if (debug) { fnDebug(e.Message); }

			//                    video.currentTime = currentTime;
			//                }
			//            }
			//            else {
			try {
				//video.currentTime = (video.currentTime + .024);
				video.currentTime = 0;

				if (debug) {
					currentTime = video.currentTime;
					theCurrentFrame = Math.floor(currentTime / frameTime);
					fnDebug("Last Frame");
					fnDebug("newTime=" + currentTime);
					fnDebug("newFrame=" + theCurrentFrame);
				}

			}
			catch (e) {
				if (debug) { fnDebug(e.Message); }

				video.currentTime = currentTime;
			}
		}
		//}
	}
	else {
		if ((video.currentTime + (frameTime * 1.5)) < (video.duration)) {
			try {
				//video.currentTime = (video.currentTime + .024);
				//video.currentTime = (video.currentTime + frameTime);
				//if (debug) {
				video.currentTime = currentFrameStartTime + (frameTime * 1.5);
				currentTime = video.currentTime
				if (debug) { fnDebug("newTime=" + currentTime); }
				if (debug) { fnDebug("newFrame=" + theCurrentFrame); }
				//}
			}
			catch (e) {
				if (debug) { fnDebug(e.Message); }

				video.currentTime = currentTime;
			}
		}
		else {
			if ((video.currentTime + (frameTime)) < (video.duration)) {
				try {
					//video.currentTime = (video.currentTime + .024);
					//video.currentTime = (video.currentTime + frameTime);
					//if (debug) {
					video.currentTime = video.currentTime + (frameTime);
					currentTime = video.currentTime
					if (debug) { fnDebug("newTime=" + currentTime); }
					if (debug) { fnDebug("newFrame=" + theCurrentFrame); }
					//}
				}
				catch (e) {
					if (debug) { fnDebug(e.Message); }

					video.currentTime = currentTime;
				}
			}
			else {
				try {
					//video.currentTime = (video.currentTime + .024);
					video.currentTime = 0;

					if (debug) {
						currentTime = video.currentTime;
						theCurrentFrame = Math.floor(currentTime / frameTime);
						fnDebug("Last Frame");
						fnDebug("newTime=" + currentTime);
						fnDebug("newFrame=" + theCurrentFrame);
					}
				}
				catch (e) {
					if (debug) { fnDebug(e.Message); }

					video.currentTime = currentTime;
				}
			}
		}
	}

	//4.2.1 sf 11397 Add Frame rate to improve stepping***********************************************************************
}

function StepBackBtn_onclick() {
	video = document.getElementById('canvasVideo');

	///4.2.3 sf 12860 fetch framerate on demand
	if (imgFrameRate == 0) {
		PauseVideo();
	}
	else {
		video.pause();
	}

	//4.2.1 sf 11397 Add Frame rate to improve stepping***********************************************************************

	var frameRate = imgFrameRate;
	var frameTime = 1 / frameRate;
	var currentTime = video.currentTime;
	var theCurrentFrame = Math.floor(currentTime / frameTime);

	if (debug) { fnDebug("currentTime=" + currentTime); }
	if (debug) { fnDebug("theCurrentFrame=" + theCurrentFrame); }

	//video.currentTime = (video.currentTime - .024);
	video.currentTime = (video.currentTime - frameTime);

	//4.2.1 sf 11397 Add Frame rate to improve stepping***********************************************************************
}

function changesrc(src) {
	myVideo.src = src;
}

function getComboA(sel) {
	var value = sel.options[sel.selectedIndex].value;
	grayscale();

}

function SelectPrevImage(option) {
	enforceTimeout("SelectPrevImage(option)");

	if (previewMode) {
		SelectPrevPreview(option);
		return;
	}

	var lisize = $('li').size()

	if (option == 0) {
		option = lisize
		var lastoption = 0
		var nextoption = lisize - 1
	}
	else {
		var lastoption = parseInt(option, 10) + 1;
		var nextoption = parseInt(option, 10) - 1;
	}

	$("#imagepicker").val(nextoption);

	$(".thumbnails li:eq(" + lastoption + ")").find("div.thumbnail").removeClass("selected");
	$(".thumbnails li:eq(" + option + ")").find("div.thumbnail").removeClass("selected");
	$(".thumbnails li:eq(" + nextoption + ")").find("div.thumbnail").addClass("selected");

	if ($("#base64Video").attr("height") == "95%") {
		ShowVideo("fullscreen");
	}
	else {
		ShowVideo();
	}

}

function SelectPrevPreview(option) {
	var lisize = $('li').size()

	$("div").removeClass("quadSelected")
	$("div").removeClass("selected")
	var nextoption = parseInt(option, 10) + 4;
	var lastoption = parseInt(option, 10) - 4;

	if (lastoption < 0 && lastoption > -4) {
		$("#imagepicker").val(0);
		lastoption = 0;
	}
	else {

		if (lastoption == -4) {
			lastoption = lisize + lastoption;
			$("#imagepicker").val(lastoption);
		}
		else {
			$("#imagepicker").val(lastoption);
		}

	}

	$(".thumbnails li:eq(" + parseInt(lastoption, 10) + ")").find("div.thumbnail").addClass("selected");

	if (fullscreenMode) {
		PreviewFullScreen();
	}
	else {
		previewMode = false;
		PreviewsBtn_onclick();
	}

}

function SelectNextImage(option) {
	enforceTimeout("SelectNextImage(option)");

	if (previewMode) {

		SelectNextPreview(option);
		return;
	}

	$("#blankImage").show();

	var lisize = $('li').size()

	if (option >= (lisize - 1)) { option = -1 }
	var nextoption = parseInt(option, 10) + 1;

	var lastoption = parseInt(option, 10) - 1;
	$("#imagepicker").val(nextoption);

	$(".thumbnails li:eq(" + parseInt(lastoption, 10) + ")").find("div.thumbnail").removeClass("selected");
	$(".thumbnails li:eq(" + parseInt(option, 10) + ")").find("div.thumbnail").removeClass("selected");
	$(".thumbnails li:eq(" + parseInt(nextoption, 10) + ")").find("div.thumbnail").addClass("selected");

	if ($("#base64Video").attr("height") == "95%") {
		ShowVideo("fullscreen");
	}
	else {
		ShowVideo();
	}
}

function SelectImage(option, div) {

	$("div.thumbnail.selected").removeClass("selected");
	$(".thumbnail").removeClass("quadSelected");
	$("#imagepicker").val(option);
	$(".thumbnails li:eq(" + parseInt(option, 10) + ")").find("div.thumbnail").addClass("selected");

	ShowVideo('user');

}

function SelectNextPreview(option) {

	var lisize = $('li').size()

	var nextoption = parseInt(option, 10) + 4;

	if (nextoption >= (lisize)) { nextoption = 0; endOfArray = true; }

	var lastoption = parseInt(option, 10) - 4;
	$("#imagepicker").val(nextoption);

	$(".thumbnails li:eq(" + parseInt(lastoption, 10) + ")").find("div.thumbnail").removeClass("selected");
	$(".thumbnails li:eq(" + parseInt(option, 10) + ")").find("div.thumbnail").removeClass("selected");
	$(".thumbnails li:eq(" + parseInt(nextoption, 10) + ")").find("div.thumbnail").addClass("selected");

	if (fullscreenMode) {
		PreviewFullScreen();
	}
	else {
		previewMode = false;
		PreviewsBtn_onclick();
	}

	for (var ii = 1; ii < 4; ii++) {

		var nextImage = parseInt(nextoption, 10) + ii;
		if (nextImage > (lisize - 1)) {

			var view = parseInt(ii, 10) + 1;
			$("#View" + view).hide();
			$("#ViewImg" + view).hide();
		}
		else {
			$("#View" + view).show();
			$("#ViewImg" + view).show();
		}
	}
}

function SlideShow(lisize) {
	var i = $("#imagepicker").val();
	if (isSlideShow) {

		$("#SlideshowBtn").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/SlideShow_0.png");
		isSlideShow = false;
		clearInterval(timer);
	}
	else {

		$("#SlideshowBtn").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/SlideShow_1.png");
		isSlideShow = true;
	}

	var checker = function () {

		if (isSlideShow) {
			if (previewMode) {
				SelectNextPreview($('#imagepicker').val())
			}
			else {
				SelectNextImage(parseInt(i, 10));
				i++;
				if (i >= lisize - 1) i = -1;
			}

		}
		else {
			clearInterval(timer);
		}
	}
	timer = setInterval(checker, slidedelay);
	if (!isSlideShow) { clearInterval(timer) }
}

function showpreviewmain(ind, div) {

	fullscreenMode = false;
	$("#FullscreenBtn").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/FullScreen_0.png");

	if (isSlideShow) {
		SlideShow();
	}

	SelectImage(ind, div);
	//Previewsclose();
}

function PreviewsBtn_onclick() {
	//console.log("PreviewsBtn_onclick() Called");
	if (debug) { fnDebug("Preview_OnClick() Called "); }

	if (previewMode) {
		//console.log("PreviewsBtn_onclick(): Leave Preview Mode");
		Previewsclose();
		$(".CathOverlayText").show();       //4.2.1 Hide cath overlay text for previews sf 11999
		return;
	}
	else {
		//console.log("PreviewsBtn_onclick(): Enter Preview Mode");
		$(".CathOverlayText").hide();       //4.2.1 Hide cath overlay text for previews sf 11999

		$("#PreviewsBtn").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/Icons_1.png");
		$("#StepForwardBtn").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/StepForward_1.png");
		$("#StepBackBtn").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/StepBack_1.png");
		previewMode = true;
	}

	if (fullscreenMode) {
		//console.log("PreviewsBtn_onclick(): Fullscreen Mode");
		//set onclick of next and previous buttons to Select Preview set of 4
		//4.3 sf 13864 Previews should always group in orndinal sets of 4
		//$("#NextBtn").attr("onclick", "SelectNextPreview($('#imagepicker').val())");
		//$("#PreviousBtn").attr("onclick", "SelectPrevPreview($('#imagepicker').val())");

		$("#NextBtn").attr("onclick", "SelectNextPreview(($('#imagepicker').val() - $('#imagepicker').val() % 4))");
		$("#PreviousBtn").attr("onclick", "SelectPrevPreview(($('#imagepicker').val() - $('#imagepicker').val() % 4))");
		fullscreenMode = false;
		PreviewFullScreen();
	}
	else {
		//console.log("PreviewsBtn_onclick(): regular Mode");
		video = document.getElementById('canvasVideo');
		video.pause();

		//hide current images
		$("#canvasVideo").hide();
		$("#viewsDiv").show();
		$("#base64video").hide();
		$("#base64_container").hide();
		//console.log("PreviewsBtn_onclick(): hide #canvasImage");
		$("#canvasImage").hide();

		//set styles for previews div and attach to video container, limit height to parent div height
		var viewsDiv = $("#ViewsDiv").attr("style", "display: inline-block; z-index: 1; position: absolute; top: 0px; left: 0px;");
		if (debug) { fnDebug("Preview_Btn_Onclick(): after views div"); }
		viewsDiv.detach();
		$("#video_container").prepend(viewsDiv);

		//console.log("PreviewsBtn_onclick(): $('#video_container').innerHeight() = " + $("#video_container").innerHeight());
		//console.log("PreviewsBtn_onclick(): $('#video_container').innerWidth() = " + $("#video_container").innerWidth());

		viewsDiv.height($("#video_container").innerHeight());
		viewsDiv.width($("#video_container").innerWidth());

		//console.log("PreviewsBtn_onclick(): viewsDiv.height() = " + viewsDiv.height());
		//console.log("PreviewsBtn_onclick(): viewsDiv.width() = " + viewsDiv.width());

		//set onclick of next and previous buttons to Select Preview set of 4
		//4.3 sf 13864 Previews should always group in orndinal sets of 4
		//$("#NextBtn").attr("onclick", "SelectNextPreview($('#imagepicker').val())");
		//$("#PreviousBtn").attr("onclick", "SelectPrevPreview($('#imagepicker').val())");

		$("#NextBtn").attr("onclick", "SelectNextPreview(($('#imagepicker').val() - $('#imagepicker').val() % 4))");
		$("#PreviousBtn").attr("onclick", "SelectPrevPreview(($('#imagepicker').val() - $('#imagepicker').val() % 4))");

		//set onclick of full screen button to display dynamic preview div
		$("#FullscreenBtn").attr("onclick", "PreviewFullScreen()");

		var imgType = [];
		var viewVideo = [];
		var li;
		var lisize;
		var urlAR = [];
		var currentLI;
		var srcUrls = [];
		var videoSources = [];

		var imgTypeTemp = $("div.thumbnail.selected").find("p").html();
		var arImgType = [];
		arImgType = imgTypeTemp.split("|");
		var imgType = arImgType[0];       //first field in pipe delimited array is the image type (dcm,jpg,mp4,webm) or it could be a status such as mia
		var imgAspRatio = arImgType[1];   //Second field is the aspect ratio, width/hieght

		var i = 0;
		var option = 0;
		var lastoption = 0;

		//set onclick of next and previous buttons to Select Preview set of 4
		li = $("#imagepicker").val() - ($("#imagepicker").val() % 4);
		currentLI = parseInt(li, 10);

		//4.1 DWC instead of removing one quadSelected at a time, remove all then highlight the next four.
		$("div").removeClass("quadSelected");
		$("div").removeClass("selected");

		$(".thumbnails li:eq(" + (parseInt(currentLI, 10)) + ")").find("div.thumbnail").addClass("selected");

		if (debug) { fnDebug("before for loop"); }

		for (i = 0; i < 4; i++) {
			$("#View" + (i + 1)).hide();

			option = currentLI + i;
			lastoption = currentLI - i;
			nextoption = currentLI + 8 + i;
			anotheroption = currentLI + 4 + i;

			//4.1 DWC instead of removing one quadSelected at a time, remove all then highlight the next four.
			//if (i < 3) {
			//4.3 sf 13864 first thumbnail is not being highlighted
			$(".thumbnails li:eq(" + (parseInt(option, 10)) + ")").find("div.thumbnail").addClass("quadSelected");
			//}

		}

		if (debug) { fnDebug("finished for loop"); }

		//size elements and containers for quads
		if (debug) { fnDebug("Call sizeQuad from Peview Button Click"); }

		sizeQuadScreen(imgAspRatio);

		if (debug) { fnDebug("Before loadPreviews()"); }
		loadPreviews();
		if (debug) { fnDebug("After loadPreviews()"); }

		return true;
	}

}

function loadPreviews() {
	if (debug) { fnDebug("Start loadPreviews()"); }

	var imgType = [];
	var imgTags = [];
	var viewVideo = [];
	var li;
	var lisize;
	var urlAR = [];
	var currentLI;
	var srcUrls = [];
	var videoSources = [];
	var imgInfo = [];
	var imgAspRatio = [];
	var i = 0;

	//current selected image
	//set onclick of next and previous buttons to Select Preview set of 4
	li = $("#imagepicker").val() - ($("#imagepicker").val() % 4);
	currentLI = parseInt(li, 10)
	lisize = $("li").size();

	function doLoop(i) {

		if (debug) { fnDebug("loadPreviews(): doLoop index: " + i); }
		if (debug) { fnDebug("loadPreviews(): CurrentLI: " + currentLI); }

		//var firstSelectedLI = parseInt($('#imagepicker').val(), 10);
		var firstSelectedLI = currentLI;

		//var lastSelectedLI = parseInt($('#imagepicker').val(), 10) + 4;
		var lastSelectedLI = currentLI + 4;

		if (debug) { fnDebug("loadPreviews(): firstSelectedLI: " + firstSelectedLI); }
		if (debug) { fnDebug("loadPreviews(): lastSelectedLI: " + lastSelectedLI); }

		if (currentLI >= firstSelectedLI && currentLI <= lastSelectedLI) {

			if (currentLI > (lisize - 1)) { return; }

			$("#divView" + i).show();
			$("#View" + i).attr("ondblclick", "showpreviewmain(" + currentLI + ", 'divView" + i + "')");
			$("#ViewImg" + i).attr("ondblclick", "showpreviewmain(" + currentLI + ")");


			imgTags[i] = $(".thumbnails li:eq(" + currentLI + ")").find("div.thumbnail").find("p").html();
			imgInfo = imgTags[i].split('|');
			imgType[i] = imgInfo[0];
			imgAspRatio[i] = imgInfo[1];

			urlAR[i] = $(".thumbnails li:eq(" + currentLI + ")").find("div.thumbnail").find("img").attr("src");
			urlAR[i] = urlAR[i].replace("i.bmp", "0." + imgType[i]);

			if (debug) { fnDebug("loadpreviews() doLoop(): ImgType: " + imgType[i]); }

			//Fix for IE
			if (imgType[i] == "mp4" || (imgType[i] == "webm" && browserName != "Microsoft Internet Explorer")) {

				if (debug) { fnDebug("doLoop(), Browser: " + browserName); }
				//if this is IE, loop showvideo until imgType is mp4
				if (browserName == "Microsoft Internet Explorer" && imgType[i] == "webm") {
					if (debug) { fnDebug("MSIE Detected, wait for MP4"); }
					setTimeout(function () {

						$("#ViewImg" + i).width($("#myiframe").width() / 2);
						$("#ViewImg" + i).attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/msgCompressingImage.png");
						$("#ViewImg" + i).width($("#myiframe").width() / 2);
						$("#ViewImg" + i).show();

						ConvertImage(url, "dcm", currentLI, function () { doLoop(i); });
					}, 1000);
				}

				if (debug) { fnDebug("video: " + imgType[i]); }

				$("#ViewImg" + i).hide();
				$("#View" + i).show();

				if (debug) { fnDebug("#View" + i); }

				srcUrls[0] = urlAR[i].replace(imgType[i], "webm");
				srcUrls[1] = urlAR[i].replace(imgType[i], "mp4");

				viewVideo[i] = document.getElementById("View" + i);

				videoSources = viewVideo[i].getElementsByTagName("source");

				videoSources[0].src = srcUrls[0];
				videoSources[1].src = srcUrls[1];
				if (debug) { fnDebug(videoSources[0].src); }
				if (debug) { fnDebug(videoSources[1].src); }

				viewVideo[i].style.visibility = "visible";

				//4.1 DWC if IOS then play only the first available preview.
				//******remvoed IOS code, see 4.1 Build 5 or below as reference********

				if (viewVideo[i]) {
					viewVideo[i].load();
					viewVideo[i].play();
				}

			}
			else if (imgType[i] == "jpg") {
				if (debug) { fnDebug("Preview_OnClick(): imgType JPG, urlAR[" + i + "]: " + urlAR[i]); }

				$("#View" + i).hide();
				$("#ViewImg" + i).attr("src", urlAR[i]);
				$("#ViewImg" + i).show();
			}
			else if (imgType[i] == "dcm" || (imgType[i] == "webm" && browserName == "Microsoft Internet Explorer")) {
				var url = $(".thumbnails li:eq(" + currentLI + ")").find("img").attr('src');

				if (debug) { fnDebug("Preview_OnClick(): Called, ext DCM found "); }
				if (debug) { fnDebug("Preview_OnClick(): ImageType[" + i + "]: ext: " + imgType[i]); }
				if (debug) { fnDebug("Preview_OnClick(): browserName: " + browserName); }
				if (debug) { fnDebug("Preview_OnClick(): Called, URL: " + url); }

				$("#ViewImg" + i).width($("#myiframe").width() / 2);
				$("#ViewImg" + i).attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/msgCompressingImage.png");
				$("#ViewImg" + i).width($("#myiframe").width() / 2);
				$("#ViewImg" + i).show();

				ConvertImage(url, "dcm", currentLI, function () { });

				i = i - 1;
			}
			else if (imgType[i] == "wip") {
				if (debug) { fnDebug("WIP Status") }

				$("#ViewImg" + i).attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/msgCompressingImage.png");
				$("#ViewImg" + i).show();

				if (debug) { fnDebug(Date().toString() + " Before Time Out to make Convert Req") }

				setTimeout(function () {
					$("div.thumbnail.selected").find("p").html("dcm");

					ConvertImage(url, "dcm", currentLI, function () { });
				}, 20000);

				if (debug) { fnDebug(Date().toString() + " After Time Out to make Convert Req") }
				i = i - 1;
			}
			//4.1 DWC Add err status message for invald paths
			else if (imgType[i] == "err") {
				$("#ViewImg" + i).attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/msgUnableToDisplayImage.png");

				$("#ViewImg" + i).show();
			}
			else if (imgType[i] == "mia") {
				$("#ViewImg" + i).attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/msgUnableToDisplayImage.png");

				$("#ViewImg" + i).show();
			}

			//console.log("loadPreviews(): about to call sizePreview(" + i + ")");
			sizePreview(i);

			currentLI = parseInt(li, 10) + parseInt(i, 10);

			if (i < 4) {
				setTimeout(function () { doLoop(i + 1); }, 100);
			}
			//            else {
			//                $("#canvasImage").hide();
			//            }
			//end for loop ****** update, end doLoop function
		}

	}

	if (debug) { fnDebug("loadPreviews() before doLoop(1)"); }
	doLoop(1);

	//console.log("hide #canvasImage");
	$("#canvasImage").hide();
}

function ConvertImage(url, imgType, currentLI, callback) {
	if (debug) { fnDebug("ConvertImage(): Called, URL: " + url); }

	url = url.replace("i.bmp", "0." + imgType);
	url = url.replace(/\\/g, "|");
	url = url.replace("MediaLoader.aspx?mpath=", "")

	var re = /\/[^\/]*.aspx/gi;
	var appname = location.pathname.toString();

	if (appname.match(re)) {
		appname = appname.replace(re, "\/converthandler.ashx")
		url = location.protocol + "//" + location.hostname + ":" + location.port + appname + "/path/?path=" + url
		url = url.toLowerCase();
		url = url.replace("/process.aspx", "")
	}
	else {
		url = location.protocol + "//" + location.hostname + ":" + location.port + location.pathname + "/converthandler.ashx" + "/path/?path=" + url
		url = url.toLowerCase();
		url = url.replace("/process.aspx", "")
	}

	// console.log("Script1.js -> " + url);
	var i = 0;
	var msg;

	if (debug) { fnDebug("ConvertImage() Before poll() function"); }

	(function poll() {
		url = url + "&t=" + Math.random();
		if (debug) { fnDebug("ConvertImage() poll() url: " + url) }
		$.ajax({
			url: url, success: function (data) {
				if (debug) { fnDebug("ConvertImage() AJAX URL: " + url); }
				//4.1 DWC IE11 fails to display image after compression due to the script failing upon the intial null string response
				try {
					var parsedData = JSON.parse(data);
					msg = parsedData.msg;

					if (debug) { fnDebug("ConvertImage() AJAX MSG: " + msg); }

					$(".thumbnails li:eq(" + currentLI + ")").find("div.thumbnail").find("p").html(msg);
				}
				catch (err) {
					if (debug) { fnDebug("ConvertImage() poll() AJAX Error:" + err); }
				}

			}, timeout: 30000
		});
	})();

	if (debug) { fnDebug("ConvertImage() After poll() function"); }

	setTimeout(function () {

		if (debug) { fnDebug("ConvertImage() Before callback()"); }
		callback();
		if (debug) { fnDebug("ConvertImage() After callback()"); }
	}, 1000);
}

//4.2.3 SF 12860 Add function to fetch frame rate on demand 

function GetFrameRate(url, currentLI, callback) {

	if (debug) { fnDebug("GetFrameRate(): Called, URL: " + url); }

	url = url.replace("i.bmp", "0." + "mp4");
	url = url.replace(/\\/g, "|");
	url = url.replace("MediaLoader.aspx?mpath=", "")

	if (location.pathname.indexOf("default.aspx", 5) == -1) {

		url = location.protocol + "//" + location.hostname + ":" + location.port + location.pathname + "/frameratehandler.ashx" + "?studyid=" + studyid + "&path=" + url
		url = url.toLowerCase();
		url = url.replace("/process.aspx", "")
	}
	else {
		url = location.protocol + "//" + location.hostname + ":" + location.port + location.pathname.replace("/default.aspx", "/frameratehandler.ashx") + "?studyid=" + studyid + "&path=" + url
		url = url.toLowerCase();
		url = url.replace("/process.aspx", "")
	}

	var i = 0;
	var msg;

	if (debug) { fnDebug("GetFrameRate() Before poll() function"); }

	(function poll() {
		url = url + "&t=" + Math.random();
		if (debug) { fnDebug("GetFrameRate() poll() url: " + url) }
		$.ajax({
			url: url, success: function (data) {

				if (debug) { fnDebug("GetFrameRate() AJAX URL: " + url); }
				//4.1 DWC IE11 fails to display image after compression due to the script failing upon the intial null string response
				try {
					//var parsedData = JSON.parse(data);
					//msg = parsedData.msg;

					var msg = data;

					imgFrameRate = msg;

					var strTmp = $("div.thumbnail.selected").find("p").html();

					strTmp = strTmp.replace("|0|", "|" + imgFrameRate + "|");

					if (debug) { fnDebug("GetFrameRate() AJAX MSG: " + msg); }

					if (debug) { fnDebug("GetFrameRate() strTmp: " + strTmp); }

					$(".thumbnails li:eq(" + currentLI + ")").find("div.thumbnail").find("p").html(strTmp);
				}
				catch (err) {
					if (debug) { fnDebug("GetFrameRate() poll() AJAX Error:" + err); }
				}

			}, timeout: 30000
		});
	})();

	if (debug) { fnDebug("GetFrameRate() After poll() function"); }

	setTimeout(function () {
		if (debug) { fnDebug("GetFrameRate() Before callback()"); }
		callback();
		if (debug) { fnDebug("GetFrameRate() After callback()"); }
	}, 1000);

}

function PreviewFullScreen() {
	$("#PreviewsBtn").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/Icons_1.png");
	$("#FullscreenBtn").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/FullScreen_1.png");
	$("#StepForwardBtn").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/StepForward_1.png");
	$("#StepBackBtn").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/StepBack_1.png");

	if (!fullscreenMode) {
		video = document.getElementById('canvasVideo')
		video.pause();
		$("#canvasVideo").hide();
		//console.log("hide #canvasImage");
		$("#canvasImage").hide();

		$("div").hide();

		var viewsDiv = $("#ViewsDiv").attr("style", "display: block; z-index: 1; position: absolute; top: 0px; left: 0px;");
		viewsDiv.detach();
		$("#video_container").prepend(viewsDiv);
		viewsDiv.css("height", $("#myiframe").innerHeight());

		$("#imgLogo").css("display", "none");
		$("body").css("background-color", "black");

		//console.log("PreviewFullScreen(): setting $('#video_container').css('height') to 100%");
		$("#video_container").css("height", "100%");
		$("#video_container").show();

		var videocontainer = $("#video_container").detach();

		$("body").append(videocontainer);

		$("#video_container").attr("style", "display: inline; z-index: 1; position: absolute; height: 95%; text-align: left");
		$("#toolbar").css("bottom", "20px");

		//show buttons on full screen mode
		$("#toolbar").show();
		var toolbar = $("#toolbar").detach();
		toolbar.insertAfter(videocontainer);

		$("#FullscreenBtn").attr("onclick", "FullscreenBtn_onclick()");
		$("#PreviewsBtn").attr("onclick", "PreviewsBtn_onclick()");
	}

	previewMode = true;
	fullscreenMode = true;

	var View = [];
	var imgType = [];
	var url = [];
	var currentLI;
	var viewVideo = [];
	var srcUrls = [];
	var videoSources = [];
	var imgInfo = [];
	var imgAspRatio = [];
	var i = 0;

	if (debug) { fnDebug("before for loop"); }
	for (i = 1; i < 5; i++) {

		//******************blank out tiles from prior quad view

		if (debug) { fnDebug("before get View" + i); }
		viewVideo[i] = document.getElementById("View" + i);

		if (debug) { fnDebug("before get Video source"); }
		videoSources = viewVideo[i].getElementsByTagName("source");

		if (debug) { fnDebug("before video src"); }
		videoSources[0].src = "";
		videoSources[1].src = "";

		viewVideo[i].load()
		if (debug) { fnDebug("after video load"); }

		$("#ViewImg" + i).attr("src", "")

		//******************end blank tiles
	}

	//console.log("hide #canvasImage");
	$("#canvasImage").hide();
	$("#canvasVideo").hide();

	//current selected image
	li = $("#imagepicker").val();

	currentLI = parseInt(li, 10)

	//size quad screen elements
	sizeQuadScreen(aspRatio);

	loadPreviews();
}

function Previewsclose() {

	//alert("Previewsclose()");

	$("div").removeClass("quadSelected");
	$("#PreviewsBtn").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/Icons_0.png");
	$("#StepForwardBtn").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/StepForward_0.png");
	$("#StepBackBtn").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/StepBack_0.png");
	previewMode = false;
	if (debug) { fnDebug("PreviewClose() hide ViewsDiv"); }
	$("#ViewsDiv").hide();
	$("#FullscreenBtn").attr("onclick", "FullscreenBtn_onclick()");

	//set onclick of next and previous buttons back to single image increment/decrement
	$("#NextBtn").attr("onclick", "SelectNextImage($('#imagepicker').val())");
	$("#PreviousBtn").attr("onclick", "SelectPrevImage($('#imagepicker').val())");

	if (fullscreenMode) {

		fullscreenMode = false;
		FullscreenBtn_onclick();
		//return;
	}
	else {    //*****removed to allow element posistion after full screen close

		$("#PreviewsBtn").attr("onclick", "PreviewsBtn_onclick()");

		$("body").css("background-color", "#99CCFF");
		$("#imgLogo").show();
		$("#Nav").show();
		$("#Info").show();
		$("#mainpanel").show();
		$("#Images").show();
		$("#imageMainPanel").show();
		$("#ImagesList").show();
		$(".thumbnail").show();
		$("#myiframe").show();
		$("#toolbar").show();
		var videocontainer = $("#video_container").detach();
		videocontainer.attr("style", "text-align: left; height: inherit");
		$("#myiframe").prepend(videocontainer);
		videocontainer.prepend($("#canvasImage"));
		$("#closeBtn").attr("style", "visibility: hidden;");

		var imgTypeTemp = $("div.thumbnail.selected").find("p").html();
		var arImgType = [];
		arImgType = imgTypeTemp.split("|");
		var imgType = arImgType[0];

		//alert(imgType);

		if (imgType == "mp4") {
			$("#canvasVideo").attr("style", "display: block");
			$("#canvasImage").attr("style", "display: none");
			ShowVideo();
		}
		else {
			$("#canvasVideo").attr("style", "display: none");
			$("#canvasImage").attr("style", "display: block");
			ShowVideo();
		}

		$("#FullscreenBtn").attr("onclick", "FullscreenBtn_onclick()");

		//return toolbar to imageing window
		var toolbar = $("#toolbar").detach();
		$("#myiframe").append(toolbar);
		$("#toolbar").show();
		$("#toolbar").attr("style", "z-index: 3; position: absolute; display: inline-block; width: 100%");

		ShowVideo();

		return true;
	}     //*****removed to allow element posistion after full screen close
}

function FullscreenBtn_onclick() {

	var imgTypeTemp = $("div.thumbnail.selected").find("p").html();
	var arImgType = [];
	arImgType = imgTypeTemp.split("|");
	var imgType = arImgType[0];
	var imgAspRatio = arImgType[1];

	if (debug) { fnDebug("FullscreenBtn_onclick() fullscreenMode = " + fullscreenMode); }

	if (isPortrait) {
		SetPortrait();
	}
	else {
		SetLandscape();
	}

	if (fullscreenMode) {
		FullscreenClose();
		if (!previewMode) { $(".CathOverlayText").show(); }      //4.2.1 Hide cath overlay text for fullscreen sf 11999
		return;
	}
	else {
		$("#FullscreenBtn").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/FullScreen_1.png");
		$(".CathOverlayText").hide();       //4.2.1 Hide cath overlay text for fullscreen sf 11999
		fullscreenMode = true;
	}
	$("div").hide();
	if (debug) { $("#debug").show(); }

	$("#imgLogo").css("display", "none");
	$("body").css("background-color", "black");

	//4.1 DWC declare outside of browser check
	var videocontainer;

	objbrowserName = "";

	if (objbrowserName == "Chrome") {
		$("#base64_container").show();
		videocontainer = $("#base64_container").detach();
		$("#base64_container").attr("style", "display: block; z-index: 1; position: absolute; height: 95%; text-align: right");
	}
	else {
		$("#video_container").show();
		videocontainer = $("#video_container").detach();
		//  $("#video_container").attr("style", "display: block; z-index: 1; position: absolute; text-align: right");
	}

	// 4.1 Set video containter height to fix toolbar at top of screen in portrait
	//console.log("FullscreenBtn_onclick(): setting $('#video_container').height() to " + $("#canvasImage").height());
	$("#video_container").height($("#canvasImage").height());
	$("body").append(videocontainer);
	videocontainer.show();

	var canvasImage = $("#canvasImage").detach();
	$("#video_container").append(canvasImage);

	$("#canvasImage").attr("style", "display: block; z-index: 2; position: absolute;");

	if (!isPortrait) {
		$("#toolbar").css("bottom", "20px");
	}

	//show buttons on full screen mode
	$("#toolbar").show();

	//set the toolbar width to be the same as the active canvas video so controls will center
	var toolbar = $("#toolbar").detach();

	if (imgType == 'mp4' || imgType == 'webm') {
		toolbar.insertAfter("#canvasVideo");
	}
	else {
		//console.log("FullscreenBtn_onclick(): setting $('#video_container').height() to " + $("#canvasImage").height());
		$("#video_container").height($("#canvasImage").height());
		$("#video_container").append(toolbar);
		$("#toolbar").css("top", ($("#canvasImage").height() + 50));
	}

	if (previewMode) {
		$("#PreviewsBtn").attr("onclick", "PreviewFullScreen();")

		if (isPortrait) {
			sizeImage();
		}
		else {
			sizeQuadScreen();
			loadPreviews();
		}

		PlayVideo();
	}
	else {
		ShowVideo();
	}

}

function FullscreenClose() {
	$("#FullscreenBtn").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/FullScreen_0.png");

	fullscreenMode = false;

	//4.1 DWC Set landscape mode on main or preview modes
	if (!isPortrait) {
		SetLandscape();

		if (previewMode) {
			previewMode = false;
			PreviewsBtn_onclick();
		}
	}

	$("body").css("background-color", "#99CCFF");
	$("#imgLogo").show();
	$("#Nav").show();
	$("#mainpanel").show();
	$("#Images").show();
	$("#imageMainPanel").show();
	$("#ImagesList").show();
	$(".thumbnail").show();
	$("#myiframe").show();
	$("#toolbar").show();
	$("#Info").show();
	$("#base64_container").attr("style", "");
	var videocontainer = $("#video_container").detach();
	var base64container = $("#base64_container").detach();
	var canvasImage = $("#canvasImage").detach()
	videocontainer.attr("style", "text-align: left; height: inherit");
	base64container.attr("style", "text-align: left; height: inherit");
	$("#myiframe").prepend(videocontainer);

	videocontainer.prepend(canvasImage);
	$("#closeBtn").attr("style", "visibility: hidden;");

	if (debug) { fnDebug("FullscreenClose() after container show"); }

	var imgTypeTemp = $("div.thumbnail.selected").find("p").html();
	var arImgType = [];
	arImgType = imgTypeTemp.split("|");
	var imgType = arImgType[0];

	var toolbar = $("#toolbar").detach();

	if (isPortrait) {

	}
	else {
		$("#myiframe").append(toolbar);
		//SetLandScape();
	}

	if (!previewMode) {

		if (imgType == "mp4" || imgType == "webm") {
			$("#canvasVideo").attr("style", "display: block");
			$("#canvasImage").attr("style", "display: none");
			//alert("Fullscreenclose() no previews - imgType: " + imgType);
			ShowVideo();
		}
		else {
			$("#canvasVideo").attr("style", "display: none");
			$("#canvasImage").attr("style", "display: block");
			ShowVideo();
		}

		$("#FullscreenBtn").attr("onclick", "FullscreenBtn_onclick()");
		$("#PreviewsBtn").attr("onclick", "PreviewsBtn_onclick()");
	}
	else {
		$("#ViewsDiv").show();
		previewMode = false;
		PreviewsBtn_onclick();
	}

	//return toolbar to imaging window
	if (isPortrait) {
		$("#myiframe").width($(window).width() * margin);
		toolbar.css("bottom", "");
		toolbar.css("top", "");
		toolbar.css("position", "relative");
		toolbar.insertAfter("#video_container");
		var divImages = $("#ImagesList").detach();
		divImages.insertAfter("#toolbar");
	}
	else {
		//SetLandscape();
		return;
	}
}

function contains(str, text) {
	return (str.indexOf(text) >= 0);
}

var started = false;


//function startApp()

//function mousedown(event) {
//    debugger;
//    if (started) {
//        return;
//    }
//    var stage = drawStage;
//    var kshape = stage.getIntersection({
//        x: event._xs,
//        y: event._ys,
//    });

//    if (kshape) {
//        var group = kshape.getParent();
//        var selectedShape = group.find('.shape')[0];
//        // reset editor if click on other shape
//        // (and avoid anchors mouse down)
//        if (selectedShape) {
//            alert(selectedShape);
//        }
//    }
//}


function ShowVideo(div, url) {
	//console.log("ShowVideo('" + div + "', '" + url + "')");
    clearDrawDiv();//implemented in quickcals.js

	if (div == 'user') {
		enforceTimeout("ShowVideo(div, url)");
	}

	if (debug) { fnDebug("ShowVideo(" + div + ", " + url + ") Called "); }
	//console.log("hide #canvasImage");
	$("#canvasImage").hide();

	if (div == undefined) { div = "" }

	//console.log("ShowVideo('" + div + "')");

	if (div == 'user') {

		if (previewMode) { Previewsclose(); }

		if (isSlideShow == true) {
			//console.log("SlideShow()");
			SlideShow();
		}

	}

	var p = document.getElementById("PlayBtn");
	var video = document.getElementById("canvasVideo");
	var data;
	var isFullscreen = 0;
	var srcUrls = [];

	//4.2.1 sf 11397 Add Frame rate to improve stepping***********************************************************************
	var imgTypeTemp

	//4.2.1 Add a small delay for single click vs double click of thumbnails.
	setTimeout(function () {
		imgTypeTemp = $("div.thumbnail.selected").find("p").html();

		if (debug) { fnDebug("ShowVideo() Getting Image Type"); }
		if (debug) { fnDebug("ShowVideo() imgTypeTemp: " + imgTypeTemp); }

		//4.2.1 sf 11397 Add Frame rate to improve stepping***********************************************************************
		// CWT 07/01/20 - added check to ensure imgTypeTemp is defined

		//var arImgType = [];
		//arImgType = imgTypeTemp.split("|");
		//var imgType = arImgType[0];

		var imgType = '';

		if (typeof imgTypeTemp !== 'undefined') {
			var arImgType = [];
			arImgType = imgTypeTemp.split("|");
			imgType = arImgType[0];
		}

		//console.log("ShowVideo() imgType: " + imgType);

		if (debug) { fnDebug("ShowVideo() imgType: " + imgType); }
		//alert(imgType);

		if (!url) {
			url = $("div.thumbnail.selected").find("img").attr('src');
		}

		if (div == "fullscreen") {
			div = "base64_container"
			isFullscreen = 1;
		}

		if (url) {

			if (imgType == "jpg") {
				url = url.replace("i.bmp", "0." + imgType);
				//console.log(url);
			}
			else {
				srcUrls[0] = url.replace("i.bmp", "0.webm");
				srcUrls[1] = url.replace("i.bmp", "0.mp4");
			}

		}

		if (debug) { fnDebug("ShowVideo() after URL find url: " + url); }

		var videoSources = [];

		videoSources = video.getElementsByTagName("source");

		if (imgType == "mp4" || (imgType == "webm" && browserName != "Microsoft Internet Explorer")) {
			//console.log("hide #canvasImage");
			$("#canvasImage").hide();
			$("#canvasImage").css("display", "none");

			objbrowserName = "";

			if (objbrowserName == "Chrome") {

				if (!(contains(div, "divView"))) {
					$("#base64_container").show();
				}

				$("#base64video").show();
				$("#base64video").attr("class", "base64video");

				$("#canvasVideo").hide();
				ajaxpage(url, div);

				if (isFullscreen == 1) {
					$("#base64video").attr("style", "display: block; z-index: 2; height: 95%");
				}

				var base64video = document.getElementById("base64video");
				base64video.play();
			}
			else {

				if (!(contains(div, "divView"))) {
					if (debug) { fnDebug("MP4 hide views div"); }
					$("#ViewsDiv").hide();

					//console.log("hide #canvasImage");
					$("#canvasImage").hide();
					videoSources[0].src = srcUrls[0];
					videoSources[1].src = srcUrls[1];

					video.load();

					//console.log("video loaded");

					$("#canvasVideo").attr("style", "display: block; z-index: 2;");

					sizeImage();

					//hide compression message
					//console.log("hide #canvasImage");
					$("#canvasImage").hide();

					if (debug) { fnDebug("ShowVideo() video.currentTime= " + video.currentTime); }

					video.onloaded = video.play();

					if (debug) { fnDebug("ShowVideo() video.duration= " + video.duration); }

					//console.log("hide #canvasImage");
					$("#canvasImage").hide();
				}
				else {
					if (debug) { fnDebug("ShowVideo(): before get view1-4"); }
					var view1 = document.getElementById("View1");
					var view2 = document.getElementById("View2");
					var view3 = document.getElementById("View3");
					var view4 = document.getElementById("View4");

					if (view1) {
						view1.load();
						view1.play();
					}
					if (view2) {
						view2.load();
						view2.play();
					}
					if (view3) {
						view3.load();
						view3.play();
					}
					if (view4) {
						view4.load();
						view4.play();
					}

					//console.log("hide #canvasImage");
					$("#canvasImage").hide();
				}

			}

		}

		if (imgType == "jpg") {
			//console.log("still jpg type");

			//if divView is passed, this is quads
			if (!(contains(div, "divView"))) {
				//console.log("JPG hide views div");
				if (debug) { fnDebug("JPG hide views div"); }
				$("#ViewsDiv").hide();

				$("#canvasImage").attr("src", "");
				$("#canvasImage").attr("src", url);
			}

			sizeImage();
			//console.log("after size image");

			if (alertOn) { ("after size image"); }
			$("#base64_container").hide();
			$("#base64video").hide();
			$("#canvasVideo").hide();

			//console.log("show #canvasImage");
			$("#canvasImage").show();
		}

		if (imgType == "dcm" || (imgType == "webm" && browserName == "Microsoft Internet Explorer")) {
			//if this is IE, loop showvideo until imgType is mp4
			$("#canvasVideo").hide();

			$("#canvasImage").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/msgCompressingImage.png");
			//console.log("show #canvasImage");
			$("#canvasImage").show();

			if (debug) { fnDebug("MSIE Detected, wait for MP4"); }

			li = $("#imagepicker").val();
			currentLI = parseInt(li, 10);

			ConvertImage(url, "dcm", currentLI, function () { setTimeout(function () { ShowVideo(); }, 500); });

			$("#canvasVideo").hide();

			if (debug) { fnDebug("ShowVideo(): " + imgType + " " + url); }
		}

		if (imgType == "wip") {
			$("#canvasVideo").hide();
			$("#canvasImage").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/msgCompressingImage.png");
			//console.log("show #canvasImage");
			$("#canvasImage").show();

			// Wait 20 seconds and reset the image type to dcm and run ShowVideo again, which will the call ConvertImage()
			setTimeout(function () {

				$("div.thumbnail.selected").find("p").html("dcm");

				ShowVideo();
			}, 20000);
		}

		//4.1 DWC Add err status message for invald paths
		if (imgType == "err") {
			$("#canvasImage").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/msgUnableToDisplayImage.png");
			//console.log("show #canvasImage");
			$("#canvasImage").show();
		}

		// 4.1 Add MIA type for displaying unable to compress image to user
		if (imgType == "mia") {
			$("#canvasImage").attr("src", "./PlayerSupport/ImagingIcons/PlayerButtons/msgUnableToDisplayImage.png");
			//console.log("show #canvasImage");
			$("#canvasImage").show();
		}

		//below is needed to start playing previews when first preview is not motion
		if (contains(div, "divView")) {
			//console.log("contains(div, 'divView') = true");

			if (view1) {
				view1.load();
				view1.play();
			}

			if (view2) {
				view2.load();
				view2.play();
			}

			if (view3) {
				view3.load();
				view3.play();
			}

			if (view4) {
				view4.load();
				view4.play();
			}

		}

	}, 10);            //4.2.1 SF 12353 Add time for single click and CSS
	//startApp();
}

function PauseVideo() {

	if (previewMode) {

		if (view1) {
			view1.pause();
		}

		if (view2) {
			view2.pause();
		}

		if (view3) {
			view3.pause();
		}

		if (view4) {
			view4.pause();
		}

	}
	else {
		var video = document.getElementById('canvasVideo');
		video.pause();
		var p = document.getElementById("PlayBtn");
		p.src = "./PlayerSupport/ImagingIcons/PlayerButtons/Play_0.png";

		imgTypeTemp = $("div.thumbnail.selected").find("p").html();

		if (debug) { fnDebug("PauseVideo() Getting Image Type"); }
		if (debug) { fnDebug("PauseVideo() imgTypeTemp: " + imgTypeTemp); }

		//4.2.1 sf 11397 Add Frame rate to improve stepping***********************************************************************

		var arImgType = [];
		arImgType = imgTypeTemp.split("|");
		var imgType = arImgType[0];

		if (debug) { fnDebug("PauseVideo() imgType: " + imgType); }
		//alert(imgType);

		url = $("div.thumbnail.selected").find("img").attr('src');

		if (imgType == "mp4") {
			//4.2.3 sf 12860 load frame rate on demand 

			li = $("#imagepicker").val();
			currentLI = parseInt(li, 10);

			imgFrameRate = arImgType[2];

			if (debug) { fnDebug("PauseVideo() imgFrameRate: " + imgFrameRate); }

			if (imgFrameRate == "0") {
				url = url.replace("i.bmp", "0.mp4");
				GetFrameRate(url, currentLI, function () { setTimeout(function () { PauseVideo(); }, 500); });
			}

		}

	}

}

function PlayVideo() {

	if (previewMode) {

		if (view1) {
			view1.play();
		}

		if (view2) {
			view2.play();
		}

		if (view3) {
			view3.play();
		}

		if (view4) {
			view4.play();
		}

	}
	else {
		ShowVideo();
	}

}

function DisplayCathOverlay(overlayText) {
	//4.2.1 sf 11999 Add Cath Overlay Text***********************************************************************
	//console.log(overlayText);
	var arOverlayText

	if (typeof overlayText === 'undefined') {
		//console.log("no overlay text provided");
		arOverlayText = "";
		$(".CathOverlayText").hide();
		return;
	}
	else {
		arOverlayText = overlayText.split("^");

		//4.2.2 sf 12686 hide cath overlay on images without cath text
		//4.2.3 change for IE11 support
		if (overlayText.indexOf("^") == -1) {
			$(".CathOverlayText").hide();
			return;
		}

	}

	var strHTMLTemp = "";
	var i = 0;

    /*
    var arOverlayText = overlayText.split("^");
    var strHTMLTemp = "";
    var i = 0;

    //4.2.2 sf 12686 hide cath overlay on images without cath text
    //4.2.3 change for IE11 support
    if (overlayText.indexOf("^") == -1)
    {
        $(".CathOverlayText").hide();
        return;
    }
    */

	for (i = 2; i < arOverlayText.length; i++) {
		$("#co" + i).remove();
	}

	for (i = 2; i < arOverlayText.length; i++) {
		strHTMLTemp = strHTMLTemp + "<div id='co" + i + "' style='position: absolute;' class='CathOverlayText'>" + arOverlayText[i] + "</div>";
	}

	$("#video_container").append(strHTMLTemp);

	$(".CathOverlayText").css("font-family", "Courier New");
	$(".CathOverlayText").css("font-size", "12px");

	//Patient Name
	$("#co5").css('top', '2px');
	$("#co5").css('left', '2px');

	//Patient Sex
	$("#co9").css('top', '20px');
	$("#co9").css('left', '2px');

	//Patient MRN
	$("#co6").css('top', '38px');
	$("#co6").css('left', '2px');

	//Patient Date of Birth
	$("#co10").css('top', '56px');
	$("#co10").css('left', '2px');

	//Institution Name
	$("#co11").css('top', '2px');
	$("#co11").css('right', '2px');

	//Manufacturer Model Name
	$("#co12").css('top', '20px');
	$("#co12").css('right', '2px');

	//Study Date
	$("#co7").css('top', '38px');
	$("#co7").css('right', '2px');

	//Study Time
	$("#co8").css('top', '56px');
	$("#co8").css('right', '2px');

	//Primary Positioner Angle
	$("#co3").css('bottom', '38px');
	$("#co3").css('left', '2px');

	//Sencondary Positioner Angle
	$("#co4").css('bottom', '20px');
	$("#co4").css('left', '2px');

	//Series Description
	$("#co2").css('bottom', '2px');
	$("#co2").css('left', '2px');

	if (fullscreenMode) { $(".CathOverlayText").hide(); }
	//4.2.1 sf 11999 Add Cath Overlay Text***********************************************************************
}

jQuery("select.image-picker").imagepicker({
	hide_select: false,
	show_label: true
});