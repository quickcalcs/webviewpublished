﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="DigiSonics.Report" Codebehind="Pdftopng.aspx.vb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />

        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=IE7" />
        <title></title>
    </head>

    <body>
        <form id="form1" runat="server">
            <asp:HiddenField ID="hiddenGUID" runat="server" />
            <asp:Label runat="server" ID="lblReport" Text="" />

            <br />
            <br />

            <div class="PDF" style="height: 100%">
                <asp:Literal ID="litReport" runat="server"></asp:Literal>
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            </div>

        </form>
    </body>
</html>