﻿<%@ Page Title="Admin" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" Inherits="DigiSonics.Admin" ClientIDMode="Static" Codebehind="Admin.aspx.vb" %>
<%@ MasterType virtualPath="~/Site.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script language="javascript" type="text/javascript" src="js/Admin.aspx.js"></script>
    <style type="text/css">
        #btnAddUser
        {
            width: 100px;
        }
        #Button1
        {
            width: 216px;
        }
        .formfield
        {}
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Literal ID="litAdmin" runat="server" Visible="False"></asp:Literal>
        <div class="Slide" id="Admin">
                <div class="Hidden"><asp:HiddenField ID="txtUserID" runat="server" /></div>
                <table border="0" width="95%" class="tbluser">
                  <tr>
                    <td>
                        <div class="tbluser">
                        <asp:GridView 
                            runat="server" 
                            ID="tbluser" 
                            AutoGenerateColumns="false" 
                            CssClass="tbluserGrid"
                            AlternatingRowStyle-CssClass="alt"
                            AllowPaging="false"
                            PageSize="10"
                            AllowSorting="true" 
                            PagerStyle-CssClass="pgr" EnableSortingAndPagingCallbacks="False"
                         >
                            <AlternatingRowStyle BackColor="#323232" CssClass="alt" />
                            <Columns>
                                <asp:BoundField DataField="username" HeaderText="User Name" SortExpression="username" />
                                <asp:BoundField DataField="firstname" HeaderText="First Name" SortExpression="firstname" />
                                <asp:BoundField DataField="lastname" HeaderText="Last Name" SortExpression="lastname" />                        
                                <asp:BoundField DataField="userid" HeaderText="User ID" Visible="false" ReadOnly="True" SortExpression="userid" />

                                    <asp:TemplateField HeaderText="Role" SortExpression="admin">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Role" Text='<%=ConvertFlag(Eval("admin"))%>'>
                                            </asp:Label><br>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                            </Columns>
                            <PagerStyle CssClass="pgr" />
                            <SelectedRowStyle BackColor="#CCCCCC" />
                        </asp:GridView>   
                        </div>
                    </td>
                    <td>
                    <table cellspacing="5" cellpadding="0" border="0" id="tblGlobalSettings" hidden>
								<tbody>
									<tr>
										<td class="formtext" style="HEIGHT: 18px" nowrap align="right"><strong>Patient Studies<strong></td>
										<td style="HEIGHT: 18px">&nbsp;</td>
										<td style="HEIGHT: 18px">
                                            <asp:RadioButton ID="rdoShowAll" runat="server" GroupName="DigiLink" 
                                                Text="Show All" />
                                            <asp:RadioButton ID="rdoSelectedPatient" runat="server" GroupName="DigiLink" 
                                                Text="All Studies for Selected Patient" />
                                            <asp:RadioButton ID="rdoSelectedStudy" runat="server" GroupName="DigiLink" 
                                                Text="Selected Study Only" />
                                        </td>
									</tr>
									<tr>
										<td class="formtext" style="HEIGHT: 18px" nowrap align="right"><strong>Patient Status</strong></td>
										<td style="HEIGHT: 18px">&nbsp;</td>
										<td style="HEIGHT: 18px">
                                            <asp:RadioButton ID="rdoPrelimFinalRevised" runat="server" 
                                                GroupName="StudyStatus" Text="Show Final, Revised, and Preliminary Studies" />
                                            <asp:RadioButton ID="rdoFinalRevised" runat="server" GroupName="StudyStatus" 
                                                Text="Show Final and Revised Studies Only " />
                                            <br />
                                            <asp:RadioButton ID="rdoShowAllStatus" runat="server" GroupName="StudyStatus" 
                                                Text="Show All" />
                                        </td>
									</tr>
									<tr>
										<td class="formtext" style="HEIGHT: 18px" nowrap align="right"><strong>Active Directory</strong></td>
										<td style="HEIGHT: 18px">&nbsp;</td>
										<td style="HEIGHT: 18px">&nbsp;
											<asp:RadioButton id="chkActiveDirectory" tabIndex="1" runat="server" Width="100px"
												Text="Enabled" CssClass="formfield" GroupName="DirectoryStatus"></asp:RadioButton>
                                            <asp:RadioButton id="chkActiveDirectoryFalse" tabIndex="1" runat="server" Width="100px"
												Text="Disabled" CssClass="formfield" GroupName="DirectoryStatus"></asp:RadioButton>
										</td>
									</tr>
									<tr>
										<td class="formtext" style="HEIGHT: 18px" nowrap align="right"><strong>Disable Right 
                                            Click</strong></td>
										<td style="HEIGHT: 18px">&nbsp;</td>
										<td style="HEIGHT: 18px">&nbsp;
                                            <asp:RadioButton ID="chkDisableRtClick" runat="server" CssClass="formfield" 
                                                tabIndex="1" Text="Enabled" Width="100px" GroupName="RightClickStatus"/>
                                            <asp:RadioButton ID="chkDisableRtClickFalse" runat="server" CssClass="formfield" 
                                                tabIndex="1" Text="Disabled" Width="100px" GroupName="RightClickStatus"/>
                                            &nbsp;</td>
									</tr>
									<tr>
                                        <td align="right" class="formtext" nowrap style="HEIGHT: 18px">
                                            <strong>Active Directory Server</strong></td>
                                        <td style="HEIGHT: 18px">
                                            &nbsp;</td>
                                        <td style="HEIGHT: 18px">
                                            &nbsp;
                                            <asp:TextBox ID="txtAdServer" runat="server" CssClass="formfield" tabIndex="2"></asp:TextBox>
                                        </td>
                                    </tr>
									<tr>
										<td class="formtext" style="HEIGHT: 18px" nowrap align="right"><strong>Active Directory 
												User Workgroup</strong></td>
										<td style="HEIGHT: 18px">&nbsp;</td>
										<td style="HEIGHT: 18px">&nbsp;
											<asp:textbox id="txtAdUser" tabIndex="3" runat="server" CssClass="formfield"></asp:textbox></td>
									</tr>
									<tr>
										<td class="formtext" style="HEIGHT: 18px" noWrap align="right"><strong>Active Directory 
												Staff Workgroup</strong></td>
										<td style="HEIGHT: 18px">&nbsp;</td>
										<td style="HEIGHT: 18px">&nbsp;
											<asp:textbox id="txtAdStaff" tabIndex="4" runat="server" CssClass="formfield"></asp:textbox></td>
									</tr>
									<tr>
										<td class="formtext" style="HEIGHT: 18px" noWrap align="right"><strong>Active Directory 
												Admin Workgroup</strong></td>
										<td style="HEIGHT: 18px">&nbsp;</td>
										<td style="HEIGHT: 18px">&nbsp;
											<asp:textbox id="txtAdAdmin" tabIndex="5" runat="server" CssClass="formfield"></asp:textbox></td>
									</tr>
									<tr>
										<td class="formtext" style="HEIGHT: 18px" noWrap align="right"><strong>Active Directory 
												Authenticator Name</strong></td>
										<td style="HEIGHT: 18px">&nbsp;</td>
										<td style="HEIGHT: 18px">&nbsp;
											<asp:textbox id="txtAdAuthenticatorName" tabIndex="6" runat="server" CssClass="formfield"></asp:textbox></td>
									</tr>
									<tr>
										<td class="formtext" style="HEIGHT: 18px" noWrap align="right"><strong>Active Directory 
												Authenticator Password</strong></td>
										<td style="HEIGHT: 18px">&nbsp;</td>
										<td style="HEIGHT: 18px">&nbsp;
											<asp:textbox id="txtAdAuthenticatorPassword" tabIndex="7" runat="server" CssClass="formfield"
												TextMode="password"></asp:textbox></td>
									</tr>
									<tr>
										<td class="formtext" noWrap align="right"><strong>Password timeout (number of days) </strong>
										</td>
										<td width="11">&nbsp;</td>
										<td>&nbsp;
											<asp:textbox id="txtTimeout" tabIndex="8" runat="server" CssClass="formfield" MaxLength="20"></asp:textbox></td>
									</tr>
									<tr>
                                        <td align="right" class="formtext" noWrap>
                                            <strong>Open Study To </strong>
                                        </td>

                                        <td style="HEIGHT: 18px">
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;
                                            <asp:RadioButton ID="radOpenToReport" runat="server" CssClass="formfield" 
                                                GroupName="OpenStudyTo" tabIndex="12" Text="Report" />
                                            <asp:RadioButton ID="radOpenToImages" runat="server" CssClass="formfield" 
                                                GroupName="OpenStudyTo" TabIndex="13" Text="Images" />
                                        </td>
                                    </tr>
                                    <tr style="width: 500px">
                                        <td align="right" class="formtext" noWrap>
                                            <strong>Image Conversion Service URL </strong>
                                        </td>
                                        <td width="11">
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;
                                            <asp:TextBox ID="txtConvertSvcURL" runat="server" CssClass="formfield" 
                                                tabIndex="14" Width="400px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr style="width: 500px">
                                        <td align="right" class="formtext" noWrap>
                                            <strong>Secondary Image Conversion Service URL </strong>
                                        </td>
                                        <td width="11">
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;
                                            <asp:TextBox ID="txtSecondaryConvertSvcURL" runat="server" CssClass="formfield" 
                                                tabIndex="14" Width="400px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="formtext" noWrap>
                                            <strong>Maximum Recordset Size </strong>
                                        </td>
                                        <td width="11">
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;
                                            <asp:TextBox ID="txtMaxRecords" runat="server" CssClass="formfield" 
                                                tabIndex="14" Width="50px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="formtext" noWrap>
                                            <strong>Default Date Filter</strong>
                                        </td>
                                        <td width="11">
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;
                                            <select ID="selectStudyFilter" runat="server">
                                                <option value="0">Current Date</option>
                                                <option value="1">Last 3 Days</option>
                                                <option value="2">Last 7 Days</option>
                                                <option value="3">Last 30 Days</option>
                                                <option value="4">Last 90 Days</option>
                                                <option value="5">Last 180 Days</option>
                                                <option value="6">Last 365 Days</option>
                                                <option value="7">All</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="formtext" colSpan="3" height="18">
                                            <asp:Button ID="save" runat="server" OnClientClick="save" tabIndex="9" Text="Save" />
                                            <asp:Button ID="cancel" runat="server" tabIndex="10" Text="Cancel" />
                                        </td>
                                    </tr>
                                    </TBODY>
                                </caption>
							</table>
                        </td>
                    </tr>
 
                    </table>
                     
                    

                    <table border="0" width="95%" id="tblUserInfo">
                    <tr>
                        <td colspan="2"><asp:Literal runat="server" ID="litUser"></asp:Literal></td>

                        
                        <td class="listBoxLabel"><asp:Literal runat="server" ID="litUser2"></asp:Literal></td>
                        <td class="listBoxLabel"><asp:Literal runat="server" ID="litUser3"></asp:Literal></td>
                        <td class="listBoxLabel"><asp:Literal runat="server" ID="LitUser4"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lblusername" runat="server" CssClass="formfield" Text="Username"></asp:Label></td>
                        <td><asp:textbox id="tbusername" runat="server" CssClass="formfield" MaxLength="20" 
                                ReadOnly="false"></asp:textbox></td>    
                        
                        <td rowspan="5">
                            <asp:ListBox ID="lstReportName" runat="server" Class="formfield" ondblclick="javascript:lstRemove()" Height="261px" 
                                Width="215px"></asp:ListBox>
                        </td>
                        <td rowspan="5">
                            <asp:ListBox ID="lstIntMD" runat="server" Class="formfield" ondblclick="javascript:lstDblClickedInt()" Height="261px" 
                                Width="215px"></asp:ListBox>
                        </td>
                        <td rowspan="5">
                            <asp:ListBox ID="lstRefMD" runat="server" Class="formfield" ondblclick="javascript:lstDblClickedRef()" Height="261px" 
                                Width="215px"></asp:ListBox>
                        </td>
                        <tr>
                            <td>
                                <asp:Label ID="lblfirstname" runat="server" CssClass="formfield" 
                                    Text="First Name"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="tbfirstname" runat="server" CssClass="formfield" 
                                    MaxLength="20"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbllastname" runat="server" CssClass="formfield" 
                                    Text="Last Name"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="tblastname" runat="server" CssClass="formfield"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblrole" runat="server" CssClass="formfield" Text="Role"></asp:Label>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rblRole" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem ID="radUser" runat="server" GroupName="Role" text="User" value="0" />
                                    <asp:ListItem ID="radAdmin" runat="server" GroupName="Role" text="Admin" value="1" />
                                    <asp:ListItem ID="radStaff" runat="server" GroupName="Role" text="Staff" value="2" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Button id="btnadduser" runat="server" text="Add New User" />
                                <asp:Button ID="btnedituser" runat="server" Text="Save Changes" Width="114px" />
                                <asp:Button ID="btndeleteuser" runat="server" Text="Delete User" />
                                <asp:Button ID="Pass_reset" runat="server" Text="Password Reset" Width="117px" />
                                <br/>
                                <input id="btnViewUsers" type="button" value="View All Users" onclick="$('btnadduser').hide();Update_User();" style="width: 124px" />
                                <input id="btnGlobalFunctions" type="button" value="Company Global Functions" onclick="ShowGlobalFunctions()" />
                            </td>
                        </tr>
                    </tr>
                    <asp:Literal ID="ltrlscript" runat="server"></asp:Literal>
                </table>
                <asp:HiddenField ID="assocMD" runat="server" />
    </div>
</asp:Content>

