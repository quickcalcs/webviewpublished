﻿<%@ WebHandler Language="VB" Class="FrameRateHandler" %>

Imports DigiSonics
Imports System
Imports System.Web
Imports System.IO
Imports System.Net
Imports System.Data
Imports Shell32
Imports System.Data.SqlClient
Imports System.Web.Script.Serialization
Imports System.ServiceModel.Web

Public Class FrameRateHandler : Implements IHttpHandler
    
    Dim SessionID As Integer
    Dim tmp As String
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        
        Dim studyid As String = context.Request.QueryString("studyid")
        Dim path As String = context.Request.QueryString("path")
        path = Replace(path, "|", "\")
        
        Dim strTmp As String
        
        context.Response.ContentType = "text/plain"
        
        If InStr(path, "\") > 0 Then
            
            strTmp = path.Substring(path.LastIndexOf("\"), path.Length - path.LastIndexOf("\"))
            
        Else
            
            context.Response.Write("Path not valid: path=" & path)
            Exit Sub
    
        End If
        
        path = Replace(path, strTmp, "\webimages" & strTmp)
   
        Try
            If studyid <> "" And path <> "" Then
                
                StudyIdToImages(studyid, SessionID)
                tmp = GetFrameRate(path)
                context.Response.Write(tmp)
                
            End If
        Catch ex As Exception
            ErrHandler.WriteError("FrameRateHandler.ashx - error: " & ex.Message())
        End Try
        
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Private Function GetFrameRate(f As String) As Double

        '4.2.1 SF 11397 Get Frame Rate of MP4 for improved stepping*****************************************

        Dim strFrameRate As String
        Dim objShell As Shell
        Dim objFolder As Folder
        Dim arStrSplit As String()
        Dim strTemp As String
        Dim index As Integer

        Dim FrameRate As Double

        Dim strPath = DecryptedSQLServerConnectionstring_Images
        Dim cmd As System.Data.SqlClient.SqlCommand
        Dim Reader As SqlDataReader

        Dim View As Integer = GetViewNumber(f)
        Dim Sql As String = "SELECT FrameRate from [Views] WHERE SessionID = " & SessionID & " AND [View] = " & View

        Using Conn As New System.Data.SqlClient.SqlConnection(strPath)

            cmd = Conn.CreateCommand()
            cmd.CommandType = CommandType.Text

            cmd.CommandText = Sql

            Try
                Conn.Open()

                Reader = cmd.ExecuteReader()

                If Reader.Read() Then

                    index = Reader.GetOrdinal("FrameRate")

                    If Not Reader.IsDBNull(index) Then

                        FrameRate = Reader("FrameRate")
                        
                    Else
                        
                        FrameRate = 0

                    End If
                    
                End If
                
            Catch ex As Exception
                ErrHandler.WriteError("GetAspectRatio() error getting Aspect Ratio: " & ex.Message)
                'Return 30
            End Try

        End Using


        If Not FrameRate = 0 Then

            Return FrameRate

        Else

            strTemp = Path.GetDirectoryName(f) '& "\webimages\"

            objShell = CreateObject("Shell.Application")
            objFolder = objShell.NameSpace(strTemp) ' path to the foldercontaining the file

            f = Path.GetFileName(f)

            Try

                '4.2.2 sf 11397 find frame rate for multiple platforms.

                For x As Integer = 250 To 400
                    strFrameRate = objFolder.GetDetailsOf(objFolder.ParseName(f), x) ' Filename

                    If InStr(strFrameRate.ToString, "frames/second", vbTextCompare) > 0 Then
                        index = x
                        Exit For
                    End If
                Next

                strFrameRate = objFolder.GetDetailsOf(objFolder.ParseName(f), index)

            Catch ex As Exception
                ErrHandler.WriteError("GetFrameRate(" & f & ") failed error: " & ex.Message)
                Return 30
            End Try

            arStrSplit = strFrameRate.Split(" ")

            arStrSplit(0) = arStrSplit(0).Replace(ChrW(8206), "")

            Try
                If CInt(arStrSplit(0)) Then
                    Return CDbl(arStrSplit(0))
                Else
                    Return 30
                End If
            Catch ex As Exception
                ErrHandler.WriteError("GetFrameRate(" & f & ") failed error: " & ex.Message)
                Return 30
            End Try
            '4.2.1 SF 11397 Get Frame Rate of MP4 for improved stepping*****************************************
        End If

    End Function
    
    Private Function GetViewNumber(f) As Integer   '4.2.3 performance improvement sf 12860
        Dim strTmp As String = f.Substring(0, f.LastIndexOf("_"))
        strTmp = strTmp.Substring(strTmp.LastIndexOf("_") + 1)

        Return strTmp

    End Function
        
    Private Sub StudyIdToImages(ByVal StudyId As String, ByRef SessionID As String)


        Dim strPath As String
        Dim Sql As String = "SELECT ID, [Study Date] as StudyDate, [Study Time] as StudyTime FROM Study WHERE [Study ID] = " & StudyId

        strPath = DecryptedSQLServerConnectionstring 'ConfigurationManager.ConnectionStrings("SQLServerConnectionstring").ConnectionString


        Dim Conn As New System.Data.SqlClient.SqlConnection(strPath)
        Dim cmd As System.Data.SqlClient.SqlCommand = Conn.CreateCommand()
        cmd.CommandType = CommandType.Text

        If appsettings("database_type") = "OBW" Then
            Sql = Sql.Replace("[Study ID]", "RecordID")
            Sql = Sql.Replace("[Study Date]", "StudyDate")
            Sql = Sql.Replace("[Study Time]", "StudyTime")
            Sql = Sql.Replace("[Interpreting MD]", "InterpretingMD AS [Interpreting MD]")
            Sql = Sql.Replace("[Referring MD]", "RefMD AS [Referring MD]")
            Sql = Sql.Replace("[Study Type]", "StudyType AS [Study Type]")
            Sql = Sql.Replace("[Study Status]", "Status AS [Study Status]")
            Sql = Sql.Replace("FROM Study", "FROM Table1")
        End If

        cmd.CommandText = Sql

        Try
            Conn.Open()

            Dim Reader As SqlDataReader = cmd.ExecuteReader()

            Reader.Read()

            Dim ID As String = Reader("ID").ToString()

            '4.2.2 sf 12698 Trim trailing alpha caracter if project type OBW for multi fetus support.
            If appsettings("database_type") = "OBW" Then
                Dim strLast As String = ID.Substring(ID.Length - 1)

                If Regex.IsMatch(strLast, "[a-zA-Z]") Then
                    ID = ID.Substring(0, ID.Length - 1)
                End If

            End If

            Dim studydate = Reader("StudyDate").ToString()
            Dim studytime = Reader("StudyTime").ToString()

            strPath = DecryptedSQLServerConnectionstring_Images ' ConfigurationManager.ConnectionStrings("SQLServerConnectionstring_images").ConnectionString

            '4.2.1 Cath Overlay sf 11999
            Sql = "SELECT SessionID FROM Sessions WHERE PatientID = '" & ID & "' AND StudyDate = '" & studydate & "' AND StudyTime = '" & studytime & "'"


        Catch ex As Exception
            '4.1 DWC Response.Write("Exception: " & ex.Message & ", trace: " & ex.StackTrace & "<br>")
            ErrHandler.WriteError(ex.Message())

        Finally

            'DWC 4.1
            If Conn.State = ConnectionState.Open Then
                Conn.Close()
            End If
        End Try


        Conn = New System.Data.SqlClient.SqlConnection(strPath)
        cmd = Conn.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = Sql

        Try
            Conn.Open()

            Dim SessionReader As SqlDataReader = cmd.ExecuteReader()

            SessionReader.Read()

            '        If SessionReader.Read() Then

            SessionID = SessionReader("SessionID").ToString()                               '4.2.1 Cath Overlay sf 11999

        Catch ex As Exception
            '4.1 DWC Response.Write("Exception: " & ex.Message & ", trace: " & ex.StackTrace & "<br>")
            ErrHandler.WriteError(ex.Message() & ", trace: " & ex.StackTrace())
        Finally

            'DWC 4.1
            If Conn.State = ConnectionState.Open Then
                Conn.Close()
            End If
        End Try

    End Sub
    
    'Private Function WriteJson(ByVal value As Object) As Stream
    '    Dim javaScriptSerializer = New JavaScriptSerializer()
    '    Dim json = Encoding.UTF8.GetBytes(javaScriptSerializer.Serialize(value))
    '    Dim memoryStream = New MemoryStream(json)
    '    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8"
    '    Return memoryStream
    'End Function
    
End Class