﻿<%@ Page Title="Images" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" Inherits="DigiSonics.Images" ClientIDMode="Static" Codebehind="Images.aspx.vb" %>
<%@ MasterType virtualPath="~/Site.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript">
        isPortrait = false;

        function loadZoomHtml() 
        {
            $("#myiframe").load("zoom.html", function () { $("#imagepicker").val(0); ShowVideo(); $("#imageMainPanel").css('visibility', 'visible'); });
        }
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="Slide" id="Images">
        <asp:Literal ID="litImages" runat="server"></asp:Literal>
        <br />
        <asp:Label runat="server" ID="lblImages" Text="" />
        <br /><br />
 
        <div id="imageMainPanel" style="visibility:hidden; position: absolute; width: 99%; height: 99%; top: 10px; left: 10px; z-index: 0;">
                        
            <asp:Literal ID="litUserMessages" runat="server"></asp:Literal>

            <div id="myiframe" style="display: inline-block; position: relative; float: left; height: 99%;"></div>

            <div style="display: inline-block; position: relative; float: right; height: 99%; overflow-y: auto;" id="ImagesList"> 
                <asp:Literal ID="ltrlThumbs" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="endofbody" Runat="Server">
    <script src="./PlayerSupport/js/image-picker.js" type="text/javascript"></script>
		<script src="PlayerSupport/js/core/jsreferences/konva.js" type="text/javascript"></script>
		<script src="PlayerSupport/js/core/Core.js" type="text/javascript"></script>	
		<script src="PlayerSupport/js/core/quickcalc.js" type="text/javascript"></script>  
		<script src="./PlayerSupport/js/Script1.js" type="text/javascript"></script> 
		<script src="./PlayerSupport/js/ScreenOrientation.js" type="text/javascript"></script> 
    <script src="./PlayerSupport/js/iPadDoubleTap.js" type="text/javascript">//4.1 DWC add ipad doubletap event hander code</script>
    <script src="./PlayerSupport/js/iPadNoZoom.js" type="text/javascript">//4.1 DWC add ipad doubletap event hander code</script>
    <script  src="./PlayerSupport/js/iPadTouch.js" type="text/javascript">//4.1 DWC add mobile/touch event hander code</script>
    <script type="text/javascript">
        $("#mainpanel").css("overflow-y", "hidden");

        var checkLoginTimeout;
        var msecCheckTimeout = 60000;
        //var msecCheckTimeout = 6000;
        var timeoutWasReached = false;

        checkLoginTimeout = setTimeout(checkSessionValid, msecCheckTimeout);

        function checkSessionValid()
        {
            let guid = $("#hiddenGUID").val();
            var strUrl = 'SummaryHandler.ashx?op=CheckValidSession&guid=' + guid;

            //start ajax request
            $.ajax({
                url: strUrl,
                //force to handle it as text
                dataType: "text",
                success: function (data)
                {
                    console.log("RCV FOR: " + strUrl);

                    if (data == 1)
                    {
                        // session is still valid restart the timer
                        console.log("Session is still valid - restart the timer");
                        checkLoginTimeout = setTimeout(checkSessionValid, msecCheckTimeout);
                    }
                    else
                    {
                        console.log("Session has expired, set a flag for the next click");
                        timeoutWasReached = true;
                    }

                }
            });

        }

        function enforceTimeout(callingPage)
        {
            //console.log("enforceTimeout() called from " + callingPage);

            if (timeoutWasReached)
            {
                ShowTimeoutMessage();
            }

        }

    </script>
</asp:Content>
