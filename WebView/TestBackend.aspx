﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="DigiSonics.TestBackend" Codebehind="TestBackend.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
    </style>
    <!-- load jQuery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script language="javascript" type="text/javascript">
        var arrFields = ["username", "fullname", "StudyID", "NewValComments", "NewValSummaryString", "NewValTxText", "TrackingID", "GUID"];
        function ClearAllFields() {
            for (i = 0; i < arrFields.length; i++) {
                $("#" + arrFields[i]).val("");
            }
        }
        function GenerateQueryString(bitmaskFields) {
            if (bitmaskFields == 0)
                return "";

            var strQueryString = "";
            var strKey, strVal, bitmaskTest;
            for (i = 0; i < arrFields.length; i++) {
                bitmaskTest = Math.pow(2, i);
                if ((bitmaskFields & bitmaskTest) != 0) {
                    strKey = arrFields[i];
                    // alert(strKey);
                    strVal = document.getElementById(strKey).value;
                    // alert(strVal);
                    if (i > 0)
                        strQueryString = strQueryString + '&';
                    strQueryString = strQueryString + strKey + '=' + encodeURIComponent(strVal);
                }
            }
            return strQueryString;
        }
        function InvokeHandler(strOp, bitmaskFields) {
            var strUrl = 'SummaryHandler.ashx?op=' + strOp + '&' + GenerateQueryString(bitmaskFields);
            //alert(strUrl);

            if (strOp == 'EditSummaryBegin') 
            {
                //start ajax request
                $.ajax({
                    url: strUrl,
                    //force to handle it as text
                    dataType: "text",
                    success: function (data) 
                    {
                        var json = $.parseJSON(data);

                        if (json != null) 
                        {
                            alert('Result=' + json.Result);
                            alert('OpenBy=' + json.OpenBy);
                        }

                    }
                });
            }
            else if (strOp == 'EditSummaryEnd' || strOp == 'DoESign') 
            {
                //start ajax request
                $.ajax({
                    url: strUrl,
                    //force to handle it as text
                    dataType: "text",
                    success: function (data) 
                    {
                        var json = $.parseJSON(data);

                        if (json != null) 
                        {
                            alert('Result=' + json.Result);
                            alert('TrackingID=' + json.TrackingID);
                            $("#TrackingID").val(json.TrackingID);
                        }

                    }

                });
            }
            else if (bitmaskFields == 0 && strOp == 'RegenPDF')
            {
                // Study IDs in PV-SQL212-S0001\SI07;database=ersrdb
                var fatstudies = [57, 101, 110, 165, 644, 700, 712, 768, 828, 838, 1103, 1104, 1105];

                for (i = 0; i < arrFields.length; i++) 
                {
                    //alert('Generating report for Study ID ' + fatstudies[i] + '\n' + strUrl);
                    var strUrl = 'SummaryHandler.ashx?op=' + strOp + '&StudyID=' + fatstudies[i];

                    $.ajax({
                        url: strUrl,
                        //force to handle it as text
                        dataType: "text",
                        success: function (data) 
                        {
                            alert("HTTP returned " + data);
                        }
                    });
                }

            }
            else 
            {
                $.ajax({
                    url: strUrl,
                    //force to handle it as text
                    dataType: "text",
                    success: function (data) 
                    {
                        alert("HTTP returned " + data);
                    }
                });
                // GetResponse(strUrl);
            }

        }
    </script>
    <script language="javascript" type="text/javascript" src="JS/GetResponse.js"></script>
</head>
<body>
    <form id="form1" method="GET">
        <div class="input-w">
        <label for="username">Username</label><input type="text" id="username" value="mchou"/>
        </div>
        <div class="input-w">
        <label for="fullname">Full name</label><input type="text" id="fullname" value="Mary Chou"/>
        </div>
        <div class="input-w">
        <label for="StudyID">Study ID</label><input type="text" id="StudyID" value="226823"/>
        </div>
        <div class="input-w">
        <label for="FreeformComments">Freeform Comments</label><input type="text" id="NewValComments"/>
        </div>
        <div class="input-w">
        <label for="SummaryString">SummaryString</label><input type="text" id="NewValSummaryString"/>
        </div>
        <div class="input-w">
        <label for="TxText">TxText</label><input type="text" id="NewValTxText"/>
        </div>
        <div class="input-w">
        <label for="username">Tracking ID</label><input type="text" id="TrackingID" value="0"/>
        </div>
        <div class="input-w">
        <label for="username">GUID</label><input type="text" id="GUID" value=""/>
        </div>
        <div class="input-w">
            <button type="button" id="btnClearAll" onclick="ClearAllFields();">Clear All</button>
        </div>
        <div class="input-w">
        <button type="button" id="btnEditSummary" onclick="InvokeHandler('EditSummaryBegin', 7+128);">Check Edit Summary</button>
        <button type="button" id="btnEditSummaryCancel" onclick="InvokeHandler('EditSummaryCancel', 5);">Cancel Edit Summary</button>
        <button type="button" id="btnCommitSummary" onclick="InvokeHandler('EditSummaryEnd', 127);">Commit Summary</button>
        <button type="button" id="btnCanESign" onclick="InvokeHandler('CanESign', 7+128);">Check Can ESign</button>
        <button type="button" id="btnDoESign" onclick="InvokeHandler('DoESign', 71);">Do ESign</button>
        <button type="button" id="btnCloseStudy" onclick="InvokeHandler('CloseStudy', 7+64+128);">Close Study</button>
        </div>
        <div class="input-w">
        <button type="button" id="btnRegenPDF" onclick="InvokeHandler('RegenPDF', 4);">Generate PDF</button>
        <button type="button" id="btnHydratePDFs" onclick="InvokeHandler('RegenPDF', 0);">Hydrate PDF folder</button>
        </div>
    </form>
</body>
</html>
