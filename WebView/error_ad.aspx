<%@ Page Language="vb" AutoEventWireup="false" Inherits="DigiSonics.error_ad" Codebehind="error_ad.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>Error: Unable to Validate Active Directory User</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="library\digiview.css" rel="stylesheet">
  </HEAD>
	<body>
		<form id="ErrorAd" method="post" runat="server">
			<p class="center">
			<strong>WebView is unable to validate the user name from the <% Response.Write(Session("adserver")) %> Domain.</strong>
			<br>
			<strong>Press OK, and login directly to WebView using your WebView login.</strong>
			<br>
			<strong>If you have no WebView account, please contact your Administrator for assistance.</strong>
			<br>
			<br>
			<asp:Button id="btnOK" runat="server" Text="OK" Width="56px" Height="32px"></asp:Button>
			</p>
		</form>
	</body>
</HTML>
