﻿<%@ Page Title="WebView" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" Inherits="DigiSonics.Login" Codebehind="Login.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="Login" id="LoginBox" style="position: absolute; top: 65px; left: 50px; z-index: 1; border-radius: 10px; border-style:none">
        <table border='0'>
            <tr>
                <td width='100%' align='center'>
                    <br /><br />
                    <br /><br />
                    <br /><br />
                    <div id="version" style="font-family: 'Segoe UI'; font-size: 80px; font-weight: bold; font-style: normal; color: #0078EC"><strong style="font-family: times new roman">WebView</strong></div>
                    <%--<hr align='center' color='blue' size='2' width='90%' />--%>
                    <br /><br />
                    <table style="border: thick solid #FFFFFF; table-layout:fixed;" align="center"; frame="box" >
                        <tr>
                            <td colspan = "3" bgcolor="#ffffff";" style="padding: 0px; margin: 0px;"><img src="images/d_logo.gif" /></td>
                        </tr>
                        <tr>
                            <td colspan = "3" align="center">Version 4.3</td><td hidden>Build 1</td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td width = "30%">&nbsp;&nbsp;Username:</td>
                            <td align="left">
                                <asp:TextBox ID="txtLogin" runat="server" class="formfield" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width = "30%">&nbsp;&nbsp;Password:</td>
                            <td align="left">
                                <asp:TextBox ID="txtPass" TextMode="Password" runat="server" class="formfield" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="30%"><asp:Label ID="lblDomain" class="formfield" runat="server" Text="&nbsp;&nbsp;Domain:"></asp:Label></td>
                            <td align="left">
                                <br />
                                <asp:TextBox ID="txtDomain" class="formfield" runat="server"></asp:TextBox>
                                <p></p>
                            </td>
                            <td>
                                <br />
                                <asp:Button runat="server" ID="btnLogin" OnClick="Update_Login" Text="Login" />
                                <p></p>
                            </td>
                        </tr>
                    </table>
                    <asp:Literal ID="litLogin" runat="server"></asp:Literal>
                    </td>
                    <td width='0%'></td>
                </tr>
            </table>
    </div>
</asp:Content>

