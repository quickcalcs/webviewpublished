//E Sig Modal Pop Up canEsign
function modalEsig() 
{
    $('#modalEsig').show();
    
    $(window).click(function (e) 
    {
        if (e.target !== $('.hover_bkgr_fricc')) 
        {
            $('#modalEsig').hide();
            ToggleEditMode(false);
        }
    });
    $('#modalCancel').click(function () 
    {
        $('#modalEsig').hide();
        ToggleEditMode(false);
    });
};

//E Sig Modal Pop Up canEsign FAIL
function modalEsigFail()
{
    $('#modalEsignFail').show();

    $(window).click(function (e)
    {
        if (e.target !== $('.hover_bkgr_fricc'))
        {
            $('#modalEsignFail').hide();
            ToggleEditMode(false);
        }
    });

    $('#modalEsignFailCancel').click(function ()
    {
        $('#modalEsignFail').hide();
        ToggleEditMode(false);
    });

};

//Wrong MD Modal Pop Up
function modalWrongMD() 
{
    $('#modalWrongMD').show();
    $(window).click(function (e) 
    {
        if (e.target !== $('.hover_bkgr_fricc')) 
        {
            $('#modalWrongMD').hide();
            ToggleEditMode(false);
        }
    });
    $('#modalCancelWrongMD').click(function () 
    {
        $('#modalWrongMD').hide();
        ToggleEditMode(false);
    });
};

// study is locked by Modal Pop Up
function modalLockedBy(strOpenBy) 
{
    $('#lockedByInput').html(strOpenBy);
    $('#modalLockedBy').show();
    $(window).click(function (e) 
    {
        if (e.target !== $('.hover_bkgr_fricc')) 
        {
            $('#lockedByInput').html('');
            $('#modalLockedBy').hide();
            ToggleEditMode(false);
        }
    });
    $('#modalCancelLockedBy').click(function () 
    {
        $('#lockedByInput').html('');
        $('#modalLockedBy').hide();
        ToggleEditMode(false);
    });
};