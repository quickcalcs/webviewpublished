﻿let counter = 0;
let filterMode = false;
let filterClick = false;
// Handles the Filter On 

//const filterOn = () => {
const filterOn = function()
{
    $(".filter-alert").css('display', 'block');
    $("#filter-applied").css('visibility', 'visible');
    $(".gvTable tr").removeClass('gvSelectedRowHover1');
    $(".gvTable tr").addClass('gvSelectedRowHover2');
    $(".SelectedPage").removeClass('gvSelectedRowHover2');
    $(".gvHeader").removeClass('gvSelectedRowHover2');
    $("#date-filter").removeClass('gvSelectedRowHover2');
    $('#hiddenRSStartPosition').val(0);
    $("#tbl1 td").click(function returnCellValue() 
    {
        let tdText = this.innerHTML;
        let value;
        let column;
        filterMode = true;
        filterClick = true;

        if (tdText.includes(">")) 
        {
            tdText = tdText.split("<")[1];
            tdText = tdText.split(">")[1];
            value = tdText.trim();
            //console.log(value);
        }
        else
        {
            tdText;
            value = tdText.trim();
        }
        // Check for null value
        if (tdText.trim() == "") 
        {
            tdText = null;
            value = tdText;
        }

        // Cases for Column index
        column = this.cellIndex

        switch (column) 
        {
            case 0:
                column = "[Name]";
                break;
            case 1:
                column = "[ID]";
                break;
            case 2:
                column = "[Study Date]";
                break;
            case 3:
                column = "[Study Time]";
                break;
            case 4:
                column = "[Referring MD]";
                break;
            case 5:
                column = "[Date of Birth]";
                break;
            case 6:
                column = "[Site]";
                break;
            case 7:
                column = "[Interpreting MD]";
                break;
            case 8:
                column = "[Study Type]";
                break;
            case 9:
                column = "[Study Status]";
                break;

        }
        //Add wildcard date if value is a time
        if (value.includes(":")) 
        {
            value = "12/30/1899 " + value
        }

        //As the filters go up the counter rises adding more filter terms & col
        if (counter == 0) 
        {
            document.getElementById("hiddenFilterCol").value = column;
            document.getElementById("hiddenFilterTerm").value = value;
            counter++;
        }
        else if (counter == 1) 
        {
            document.getElementById("hiddenFilterCol2").value = column;
            document.getElementById("hiddenFilterTerm2").value = value;
            counter++;
        }
        else if (counter == 2)
        {
            document.getElementById("hiddenFilterCol3").value = column;
            document.getElementById("hiddenFilterTerm3").value = value;
            counter++;
        }
        else if (counter == 3)
        {
            document.getElementById("hiddenFilterCol4").value = column;
            document.getElementById("hiddenFilterTerm4").value = value;
            counter++;
        }
        else if (counter == 4) 
        {
            document.getElementById("hiddenFilterCol5").value = column;
            document.getElementById("hiddenFilterTerm5").value = value;
            counter++;
        }
        else if (counter == 5) 
        {
            document.getElementById("hiddenFilterCol6").value = column;
            document.getElementById("hiddenFilterTerm6").value = value;
            counter++;
        }
        else if (counter == 6)
        {
            document.getElementById("hiddenFilterCol7").value = column;
            document.getElementById("hiddenFilterTerm7").value = value;
            counter++;
        }
        else if (counter == 7)
        {
            document.getElementById("hiddenFilterCol8").value = column;
            document.getElementById("hiddenFilterTerm8").value = value;
            counter++;
        }
        else if (counter == 8)
        {
            document.getElementById("hiddenFilterCol9").value = column;
            document.getElementById("hiddenFilterTerm9").value = value;
            counter++;
        }
        else if (counter == 9)
        {
            document.getElementById("hiddenFilterCol10").value = column;
            document.getElementById("hiddenFilterTerm10").value = value;
            counter++;
        }

        //After each filter we call fnfillgrid() to rerender the table with the new changes
        fnFillGrid();
    });
}

function doFilterAlertDisplay() 
{

    if (filterMode) 
    {
        $(".filter-alert").css('display', 'block');
    }
    else
    {
        $(".filter-alert").css('display', 'none');
    }

}