﻿
function hideNavigation()
{
    $("#imgRecall").hide();
    $("#imgReport").hide();
    $("#imgImages").hide();
    $("#navAdmin").hide();
}

function showNavigation() 
{
    $("#imgRecall").show();
    $("#imgReport").show();
    $("#imgImages").show();

    if ($("#hiddenUserLevel").val() == '1') 
    {
        $("#navAdmin").show();
    }

}

function hideLogin(sessionGUID, studyID, displayMode) 
{
    //alert("hideLogin(): " + displayMode);
    $("#LoginBox").hide();

    //4.1 DWC Navigation bar hidden until successful login
    $("#Nav").css("visibility", "visible");

    //4.1 DWC Keep 50% for navigation tabs, sf# 3104
    //$("#Nav").attr("style", "display: inline-block; width: 30%");
    //alert(sessionGUID);
    $("#imgRecall").attr("onclick", "GotoDiv('Recall', '" + sessionGUID + "', '" + studyID + "');");
    $("#imgRecall").attr("src", "images/b_list_f3.gif");

    $("#imgReport").attr("onclick", "GotoDiv('Report', '" + sessionGUID + "', '" + studyID + "')");
    $("#imgReport").attr("src", "images/b_report_f3.gif");

    $("#imgImages").attr("onclick", "GotoDiv('Images', '" + sessionGUID + "', '" + studyID + "')");
    $("#imgImages").attr("src", "images/b_images_f3.gif");

    $("#imgProfile").attr("onclick", "GotoDiv('Profile', '" + sessionGUID + "', '" + studyID + "')");
    $("#imgProfile").attr("src", "images/b_profile_f3.gif");

    $("#mainpanel").css("background-image", "");

    if (displayMode == "wvp") { hideTabs(); }
}

function hideTabs() 
{
    // alert("hide tabs");
    $("#imgRecall").hide();
    $("#imgReport").hide();
    $("#imgImages").hide();
    // 4.3 Add Summary
    $("#imgSummary").hide();
    $("#imgProfile").hide();
}

function GotoDiv(id, guid, selectedstudy) 
{
    //alert("GoToDiv(): " + id);
    $("#hiddenGUID").attr("value", guid);

    switch (id) 
    {
        case "Recall":

            MouseoverImage("imgRecall", "images/b_list.gif");
            //$("#mainpanel").attr("src", "recall.aspx?guid=" + guid);
            $("#mainpanel").load("patientlist.aspx?guid=" + guid + "&selectedstudy=" + selectedstudy, function ()
            {
                //alert("fillgrid");
                fnFillGrid();
                //alert("grid filled");
            });
            break;
        case "Report":
            MouseoverImage("imgReport", "images/b_report.gif");

            $("#mainpanel").load("report.aspx?guid=" + guid + "&selectedstudy=" + selectedstudy);

            break;
        case "Images":
            console.log("GotoDiv('Images', guid, selectedstudy)");
            MouseoverImage("imgImages", "images/b_images.gif");

            var isPortrait = false;

            $("#mainpanel").css("oveflow-y", "");
            $("#divPatientList").html("<br/><br/><br/><br/><br/><center><p><h1>Opening Study...</h1></p></center>");

            $("#mainpanel").load("images.aspx?guid=" + guid + "&selectedstudy=" + selectedstudy, function ()
            {
                //alert(selectedstudy);
                if (selectedstudy != 'undefined')
                {
                    $("#myiframe").load("zoom.html", function () { ShowVideo(); } );
                }
                
            });
            break;
        case "Profile":
            MouseoverImage("imgProfile", "images/b_profile.gif");
            $("#mainpanel").load("profile.aspx?guid=" + guid);

            break;
        case "Admin":
            MouseoverImage("imgAdmin", "images/b_admin.gif");
            $("#mainpanel").load("admin.aspx?guid=" + guid) //, function () { hideLogin(); });

            break;
    }

}