
        var debug = false;

        var bustcachevar = 1 //bust potential caching of external pages after initial request? (1=yes, 0=no)

        var Pagelink = '';
        //keypress handlers
        $(document).keyup(function (e) {

            if (e.keyCode == 27) { FullscreenClose(); }
  //          if (e.keyCode == 37) { SelectPrevImage($("#imagepicker").val()); }
  //          if (e.keyCode == 39) { SelectNextImage($("#imagepicker").val()); }
        });

        function EnableDebugMode() {
            debug = true;
        }

        function ajaxpage(url, containerid) {
            if (debug) { fnDebug("ajaxpage url: " + url + " container: " + containerid + " bustcachevar: " + bustcachevar); }

            var page_request = false
            if (window.XMLHttpRequest) // if Mozilla, Safari etc
                page_request = new XMLHttpRequest()
            else if (window.ActiveXObject) { // if IE
                try {
                    page_request = new ActiveXObject("Msxml2.XMLHTTP")
                }
                catch (e) {
                    try {
                        page_request = new ActiveXObject("Microsoft.XMLHTTP")
                    }
                    catch (e) { }
                }
            }
            else
                return false

            page_request.onreadystatechange = function () {
                loadpage(page_request, containerid)
            }
            if (bustcachevar) //if bust caching of external page
                bustcacheparameter = (url.indexOf("?") != -1) ? "&" + new Date().getTime() : "?" + new Date().getTime()
            if (debug) { fnDebug("busted URL: " + url + bustcacheparameter); }
            page_request.open('GET', url + bustcacheparameter, false)
            page_request.send(null)
        }

        function loadpage(page_request, containerid) {
            if (page_request.readyState == 4 && (page_request.status == 200 || window.location.href.indexOf("http") == -1))
                document.getElementById(containerid).innerHTML = page_request.responseText
        }

//    alert("script");
        var objappVersion = navigator.appVersion;
        var objAgent = navigator.userAgent;
        var objbrowserName  = navigator.appName;
        var objfullVersion  = ''+parseFloat(navigator.appVersion);
        var objBrMajorVersion = parseInt(navigator.appVersion,10);
        var objOffsetName,objOffsetVersion,ix;
//           alert(objbrowserName);
        // In Chrome
        if ((objOffsetVersion=objAgent.indexOf("Chrome"))!=-1) {
         objbrowserName = "Chrome";

         objfullVersion = objAgent.substring(objOffsetVersion+7);
        }
        // In Microsoft internet explorer
        else if ((objOffsetVersion=objAgent.indexOf("MSIE"))!=-1) {
         objbrowserName = "Microsoft Internet Explorer";
         objfullVersion = objAgent.substring(objOffsetVersion+5);
        }
 
        // In Firefox
        else if ((objOffsetVersion=objAgent.indexOf("Firefox"))!=-1) {
         objbrowserName = "Firefox";
        }
        // In Safari
        else if ((objOffsetVersion=objAgent.indexOf("Safari"))!=-1) {
         objbrowserName = "Safari";
         objfullVersion = objAgent.substring(objOffsetVersion+7);
         if ((objOffsetVersion=objAgent.indexOf("Version"))!=-1)
           objfullVersion = objAgent.substring(objOffsetVersion+8);
        }
        // For other browser "name/version" is at the end of userAgent
        else if ( (objOffsetName=objAgent.lastIndexOf(' ')+1) <
                  (objOffsetVersion=objAgent.lastIndexOf('/')) )
        {
         objbrowserName = objAgent.substring(objOffsetName,objOffsetVersion);
         objfullVersion = objAgent.substring(objOffsetVersion+1);
         if (objbrowserName.toLowerCase()==objbrowserName.toUpperCase()) {
          objbrowserName = navigator.appName;
         }
        }
        // trimming the fullVersion string at semicolon/space if present
        if ((ix=objfullVersion.indexOf(";"))!=-1)
           objfullVersion=objfullVersion.substring(0,ix);
        if ((ix=objfullVersion.indexOf(" "))!=-1)
           objfullVersion=objfullVersion.substring(0,ix);
 
        objBrMajorVersion = parseInt(''+objfullVersion,10);
        if (isNaN(objBrMajorVersion)) {
         objfullVersion  = ''+parseFloat(navigator.appVersion);
         objBrMajorVersion = parseInt(navigator.appVersion,10);
        }