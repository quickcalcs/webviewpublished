var CurrentNodeValue;
var CurrentNodeText;
var x;                      //x position of click for HyperLink Div
var y;                      //y position of click for HyperLink Div
var linkID = 0;             //Unique identifier for each link

function mouseDownFunction(e) {
	x = e.clientX;
	y = e.clientY;
	//document.getElementById("HyperLinkDiv").innerHTML = "X = " + x + " Y = " + y;
}

function insertHtmlAtCursor(html, blankChar, hyperkey) {
	var sel, range, html;

	if (window.getSelection) {
		sel = window.getSelection();
		//alert(sel);
		if (sel.getRangeAt && sel.rangeCount) {
			var input = document.getElementById("box");
			input.focus()
			range = sel.getRangeAt(0);
			range.deleteContents();
			range.insertNode(document.createTextNode(text));
		}
	} else if (document.selection && document.selection.createRange) {
		document.selection.createRange().text = text;
	}
}

function insertHtml(html, blankChar, hyperkey, spanID) {
	var sel, range, node;

	var htmlString = html.toString()
	var htmls = htmlString.split('_');
	var firstLink;

	String.prototype.replaceAt = function (index, replacement) {
		return this.substr(0, index) + replacement + this.substr(index + replacement.length);
	}

	if (window.getSelection) {
		sel = window.getSelection();
		if (sel.getRangeAt && sel.rangeCount) {
			range = window.getSelection().getRangeAt(0);
			range.collapse(false);

			var string = hyperkey.toString();

			if (string.indexOf(',') == -1) {
				if (blankChar == "_") {

					html = html.replace(blankChar, "<span id = \"" + linkID + "\" style=\"font-family: " + $("#ddlFontFamily :selected").text() + ";font-size: " + $("#ddlFontSize :selected").text() + "px\" contentEditable=\"false\" display=\"inline-flex\"><a href=\"javascript:HyperLinkDiv('" + hyperkey.trim() + "','" + linkID + "');\">" + blankChar + "</a></span>");
					firstLink = linkID;

				} else {

					html = html.replace(blankChar, "<a href=\"javascript:HyperLinkDiv('" + hyperkey.trim() + "','" + spanID + "');\">" + blankChar + "</a>");
					firstLink = spanID;
				}
			}
			else {
				var hyperkeys = string.split(',');

				var fromIndex = 0;
				var blankPos = htmlString.indexOf('_', fromIndex);
				var output = "";

				for (i = 0; i < htmls.length - 1; i++) {

					if (output == "") {

						output = [htmlString.slice(fromIndex, blankPos), "<span id = \"" + linkID + "\" contentEditable=\"false\" display=\"inline-flex\"><a href=\"javascript:HyperLinkDiv('" + hyperkeys[i].trim() + "','" + linkID + "');\">_</a></span>", htmlString.slice(blankPos + 1)].join('');
						firstLink = linkID;

					} else {

						output = [output.slice(0, fromIndex), "<span id = \"" + linkID + "\" contentEditable=\"false\" display=\"inline-flex\"><a href=\"javascript:HyperLinkDiv('" + hyperkeys[i].trim() + "','" + linkID + "');\">_</a></span>", output.slice(fromIndex + 1)].join('');

					}
					blankPos = blankPos + 110   //add the characters from the span text 
					fromIndex = output.indexOf('_', blankPos + 1);
					html = output;
					if (blankChar == "_") {
						linkID = linkID + 1;
					}
				}
			}

			var el = document.createElement("div");

			el.innerHTML = html;
			var frag = document.createDocumentFragment(), node, lastNode;

			if (blankChar == "_") {
				while ((node = el.firstChild)) {
					lastNode = frag.appendChild(node);
					$("#box").css("font-family", $("#ddlFontFamily :selected").text());
					$("#box").css("font-size", $("#ddlFontSize :selected").text() + "px");;
				}
			}
			else {

				node = el.firstChild;
				frag.insertBefore(node, lastNode);

			}

			if (blankChar == "_") {

				range.insertNode(frag);
				$("#box").css("font-family", $("#ddlFontFamily :selected").text());
				$("#box").css("font-size", $("#ddlFontSize :selected").text() + "px");

			} else {

				var input = document.getElementById("box");
				input.focus();
				$("#" + spanID).html(frag);

			}

		}
	} else if (document.selection && document.selection.createRange) {

		range = window.getSelection().getRangeAt(0);
		range.deleteContents();
		range.pasteHTML(html);

	}

	if (blankChar == "_") {
		linkID = linkID + 1;
	}
	HyperLinkDiv(hyperkey, firstLink);

}

function CommentsNodeClick(id, attribute) {

	var nodeLink = document.getElementById(id);

	//do nothing for +/- signs
	if (nodeLink.innerHTML.indexOf("<img src") == -1) {
		insertHtml(nodeLink.innerHTML, "_", attribute);
	}
	//Execute the server side event.
	eval(attribute);
}
function MacrosNodeClick(id, attribute) {
	//Do Something
	var nodeLink = document.getElementById(id);
	//alert(nodeLink.innerHTML + " clicked");               // <--example     [+] triggers this too
	if (nodeLink.innerHTML.indexOf("<img src") == -1) {
		//document.getElementById("box").innerHTML = document.getElementById("box").innerHTML + nodeLink.innerHTML
		insertTextAtCursor(nodeLink.innerHTML)
		CurrentNodeValue = nodeLink.innerHTML;
	}
	//Execute the server side event.
	eval(attribute);
}

function CallDisplayTree(value) {
	PageMethods.DisplayTree(value);
}
function DisplayDemo() {
	//document.getElementById("<%--=TreeDemo.ClientID --%>").style.display = 'block';
	document.getElementById("<%=TreeComments.ClientID %>").style.display = 'none';
	document.getElementById("<%=TreeMacros.ClientID %>").style.display = 'none';
}
function DisplayComments() {
	//document.getElementById("<%--=TreeDemo.ClientID --%>").style.display = 'none';
	document.getElementById("<%=TreeComments.ClientID %>").style.display = 'block';
	document.getElementById("<%=TreeMacros.ClientID %>").style.display = 'none';
}
function DisplayMacros() {
	//document.getElementById("<%--=TreeDemo.ClientID --%>").style.display = 'none';
	document.getElementById("<%=TreeComments.ClientID %>").style.display = 'none';
	document.getElementById("<%=TreeMacros.ClientID %>").style.display = 'block';
}

function saveSel() {
	if (window.getSelection)//non IE Browsers
	{
		savedRange = window.getSelection().getRangeAt(0);
	}
	else if (document.selection)//IE
	{
		savedRange = document.selection.createRange();
	}
}

//to restore sel
function restoreSel() {
	$('#contenteditableId').focus();
	if (savedRange != null) {
		if (window.getSelection)//non IE and there is already a selection
		{
			var s = window.getSelection();
			if (s.rangeCount > 0)
				s.removeAllRanges();
			s.addRange(savedRange);
		}
		else if (document.createRange)//non IE and no selection
		{
			window.getSelection().addRange(savedRange);
		}
		else if (document.selection)//IE
		{
			savedRange.select();
		}
	}
}

function FormatText(command, option) {
	restoreSel();
	document.execCommand(command, false, option);
}

function HyperLinkDiv(HyperKeys, SpanID) 
{
	alert('inside HyperLinkDiv(HyperKeys, SpanID)');
	console.log('inside HyperLinkDiv(HyperKeys, SpanID)');
	var string = HyperKeys.toString()

	//if more than one set of word choices is sent, only display the first
	if (string.indexOf(",") > 0) {

		var ArrTemp = string.split(",");
		HyperKeys = ArrTemp[0];

	}

	//alert(x + "," + y);
	if (x == null) {
		//alert("undefined")
		x = 100;
		y = 300;
	}

	$("#HyperLinkDiv").css("border-width", "1px");
	$("#HyperLinkDiv").css("border-style", "solid");
	$("#HyperLinkDiv").css("border-color", "white");
	$("#HyperLinkDiv").css("position", "fixed");
	$("#HyperLinkDiv").css("padding", "5px");
	$("#HyperLinkDiv").css("background-color", "black");
	$("#HyperLinkDiv").css("color", "white");

	y = y - $("#HyperLinkDiv").outerHeight();

	$("#HyperLinkDiv").css("left", Math.abs(x));
	$("#HyperLinkDiv").css("top", Math.abs(y));

	$("#HyperLinkDiv").draggable();

	//only turn on the hyperlink div if a hyperlink is present.
	if (HyperKeys != "javascript:void(0)") 
	{
		$("#HyperLinkDiv").toggle();
	}

	//alert("HyperlinkDiv");
	var strHyperKeys = GetHyperlinks(HyperKeys);
	var arrHyperKeys = strHyperKeys.split("|");
	var strHTML = "";
	var strLeadHTML = "\<a href=\"javascript:insertHtml(\'";
	var strTrailHTML = "\<a\/\>\<br\/\>";

	//$("#HyperLinkDiv").html(GetHyperlinks(HyperKeys));

	for (i = 1; i < arrHyperKeys.length - 1; i++) 
	{
		//strHTML = strHTML + strLeadHTML + arrHyperKeys[i] + "', ''" + strTrailHTML;
		strHTML = strHTML + strLeadHTML + arrHyperKeys[i] + "\', \'" + arrHyperKeys[i] + "\', \'" + HyperKeys + "\', \'" + SpanID + "\')\">" + arrHyperKeys[i] + strTrailHTML;
	}

	$("#HyperLinkDiv").html(strHTML);

	//alert(x + "," + y);
}