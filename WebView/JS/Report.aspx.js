
//4.1 DWC Add Fix for only able to view first page of report in IE 
// 4.3 - MMC commented out.  Refactoring limits ability to set this outside (here)
// var CurrentPage = document.getElementById("CurrentPage");
// CWT (6/3/20) - Commented out this Logout function since it was overriding the global logout which was doing our proper cleanup.  
/*
function Logout()
{
    //alert("Report Page Logout");
    window.location = location.protocol + "//" + location.hostname + ":" + location.port + location.pathname;
    //            window.close();

}
*/

/*
function nextPage_original()
{
    img = document.getElementById("pic");
    counter = document.getElementById("CurrentPage");
    countert = parseInt(counter.value, 10) + 1;

    var ImgSrcStr = img.src;

    console.log(ImgSrcStr);

    ImgSrcStr = ImgSrcStr.replace("page=" + counter.value, "page=" + countert);

    console.log(ImgSrcStr);

    counter.value = countert
    img.src = ImgSrcStr;
}

function PreviousPage_original()
{
    img = document.getElementById("pic");
    counter = document.getElementById("CurrentPage");
    countert = parseInt(counter.value, 10) - 1;

    if (countert != 0)
    {
        var ImgSrcStr = img.src;

        console.log(ImgSrcStr);

        ImgSrcStr = ImgSrcStr.replace("page=" + counter.value, "page=" + countert);

        console.log(ImgSrcStr);

        counter.value = countert
        img.src = ImgSrcStr;
    }

}
*/

function nextPage_jqueryGood()
{
    var imgTag = $("#pic");
    var currentPage = $("#CurrentPage").val();
    var countert = parseInt(currentPage, 10) + 1;
    var ImgSrcStr = imgTag.attr("src");

    console.log(ImgSrcStr);

    ImgSrcStr = ImgSrcStr.replace("page=" + currentPage, "page=" + countert);

    console.log(ImgSrcStr);

    $("#CurrentPage").val(countert);
    imgTag.attr("src", ImgSrcStr);
}

function nextPage()
{
    var currentPage = $("#CurrentPage").val();
    var countert = parseInt(currentPage, 10) + 1;
    var maxPages = $("#ReportPageCount").val();

    if (countert <= maxPages)
    {
        // this is a valid page to load

        $("#pic").fadeOut(100, function () 
        {
            var ImgSrcStr = $(this).attr("src");

            console.log(ImgSrcStr);

            ImgSrcStr = ImgSrcStr.replace("page=" + currentPage, "page=" + countert);

            console.log(ImgSrcStr);

            $("#CurrentPage").val(countert);
            $(this).attr("src", ImgSrcStr);

            $(this).load(function () 
            {
                $(this).fadeIn(100);
            });

        });

    }

}

function nextPage_GoodWithFade()
{

    $("#pic").fadeOut(100, function () 
    {
        var currentPage = $("#CurrentPage").val();
        var countert = parseInt(currentPage, 10) + 1;
        var ImgSrcStr = $(this).attr("src");

        console.log(ImgSrcStr);

        ImgSrcStr = ImgSrcStr.replace("page=" + currentPage, "page=" + countert);

        console.log(ImgSrcStr);

        $("#CurrentPage").val(countert);
        $(this).attr("src", ImgSrcStr);

        $(this).load(function () 
        {
            $(this).fadeIn(100);
        });

    });

}

function PreviousPage_jqueyNoFade()
{
    var imgTag = $("#pic");
    var currentPage = $("#CurrentPage").val();
    var countert = parseInt(currentPage, 10) - 1;

    if (countert != 0)
    {
        var ImgSrcStr = imgTag.attr("src");

        console.log(ImgSrcStr);

        ImgSrcStr = ImgSrcStr.replace("page=" + currentPage, "page=" + countert);

        console.log(ImgSrcStr);

        $("#CurrentPage").val(countert);
        imgTag.attr("src", ImgSrcStr);
    }

}

function PreviousPage()
{
    var currentPage = $("#CurrentPage").val();
    var countert = parseInt(currentPage, 10) - 1;

    if (countert != 0)
    {

        $("#pic").fadeOut(100, function () 
        {
            var ImgSrcStr = $(this).attr("src");

            console.log(ImgSrcStr);

            ImgSrcStr = ImgSrcStr.replace("page=" + currentPage, "page=" + countert);

            console.log(ImgSrcStr);

            $("#CurrentPage").val(countert);
            $(this).attr("src", ImgSrcStr);

            $(this).load(function () 
            {
                $(this).fadeIn(100);
            });

        });

    }

}

function zoom_old(zm)
{
    img = document.getElementById("pic");
    wid = img.width;
    ht = img.height;
    img.style.width = (wid * zm) + "px";
    img.style.height = (ht * zm) + "px";
    img.style.marginLeft = 0 + "px";
}

function zoom(zm)
{
    img = $(".pic");
    wid = img.width();
    ht = img.height();
    img.width((wid * zm) + "px");
    img.height((ht * zm) + "px");
    //img.style.marginLeft = 0 + "px";
}

function onwindowload()
{
    //zoom(1.5);

}

// 4.3 - MMC refactoring - moved this here
window.onload = onwindowload;       //  function () { zoom(1) }