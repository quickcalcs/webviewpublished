﻿function LogoutAdmin() {
    var re = /admin.aspx/gi;
    var appname = location.pathname.toString();
    appname = appname.replace(re, "Login.aspx")
    //            alert(window.location = location.protocol + "//" + location.hostname + ":" + location.port + appname);
    window.location = location.protocol + "//" + location.hostname + ":" + location.port + appname;

}

function lstDblClickedInt() {
    //            alert("dblclicked int");
    var lstIntMD = document.getElementById('lstIntMD');
    var lstOptions = lstIntMD.options;
    var lstReportName = document.getElementById('lstReportName');
    var flag = false;

    for (var i = 0; i < lstIntMD.options.length; i++) {

        //                if (lstIntMD.options[i].selected == true) {
        if (lstOptions.selectedIndex == i) {

            var lstItems = lstReportName.options.length;

            for (var j = 0; j < lstItems; j++) {

                if (lstIntMD.options[i].text == lstReportName.options[j].text) {
                    //                            document.getElementById("Admin").innerHTML = "Error: Name already added to list."
                    var flag = true;
                }
                else {
                    //                            document.getElementById("Admin").innerHTML = ""
                    var flag = false;
                }
            }

            if (!(flag)) {

                lstReportName.options[lstItems] = new Option(lstIntMD.options[i].text);
                document.getElementById("assocMD").value = document.getElementById("assocMD").value + lstIntMD.options[i].text + "|"
                //                        alert(document.getElementById("<").value)
                lstIntMD.selectedIndex = -1;

            }
        }
    }
}

function lstDblClickedRef() {

    var lstRefMD = document.getElementById('lstRefMD');
    var lstOptions = lstRefMD.options;
    var lstReportName = document.getElementById('lstReportName');
    var flag = false;

    for (var i = 0; i < lstRefMD.options.length; i++) {

        if (lstOptions.selectedIndex == i) {

            var lstItems = lstReportName.options.length;

            for (var j = 0; j < lstItems; j++) {

                if (lstRefMD.options[i].text == lstReportName.options[j].text) {
                    //                            document.getElementById("errormsg").innerHTML = "Error: Name already added to list."
                    var flag = true;
                }
                else {
                    //                            document.getElementById("errormsg").innerHTML = ""
                    var flag = false;
                }
            }

            if (!(flag)) {

                lstReportName.options[lstItems] = new Option(lstRefMD.options[i].text);
                document.getElementById("assocMD").value = document.getElementById("assocMD").value + lstRefMD.options[i].text + "|"
                lstRefMD.SelectedIndex = -1;
            }
        }
    }
}

function lstRemove() {

    var lstReportName = document.getElementById('lstReportName');
    var lstOptions = lstReportName.options;
    var assocMD = document.getElementById("assocMD").value;
    var removeMD

    for (var i = 0; i < lstReportName.options.length; i++) {
        //                alert(lstReportName.options.length);
        if (lstOptions.selectedIndex == i) {

            removeMD = assocMD.replace(lstReportName.options[i].text, "");
            document.getElementById("assocMD").value = removeMD
            lstReportName.options[i] = null
        }
    }
}


function SelectUser(userid) {
    //            PopulateHeader(StudyID, "Admin");
    //            alert(userid);
    document.getElementById('assocMD').value = ""
    Update_User(userid);
    //            Update_Admin();
}

function Update_User(userid) {
    document.getElementById('txtUserID').value = userid;

    var guid = $("#hiddenGUID").val();
    //           alert("before load");
    //           alert("admin.aspx?guid=" + guid + "&userid=" + userid);

    // MMC - Before 4.3. Commented out
    // $("#mainpanel").load("admin.aspx?guid=" + guid + "&userid=" + userid);
    // MMC - 4.3
    var reQS = /\?.*$/gi;
    var appname = location.pathname.toString();
    appname = appname.replace(reQS, "")
    //            alert(window.location = location.protocol + "//" + location.hostname + ":" + location.port + appname);
    var strUrl = location.protocol + "//" + location.hostname + ":" + location.port + appname + "?guid=" + guid + "&userid=" + userid;
    window.location = strUrl;

    //                        alert("after load");


    //           document.forms["form2"].submit() //function () {
    //             $("#form2").jQuery.ajax({
    //                url: "admin.aspx"
    //                });

    //MMC - 4.3 Trial
    // NewUserScreen();
}

function ShowGlobalFunctions() 
{
    $("#tblGlobalSettings").show();

    //set the date filter from stored value
    var DateFilter = document.getElementById('hiddenStudyFilter').value;
    document.getElementById('selectStudyFilter').selectedIndex = DateFilter;

    $("#tblUserInfo").hide();
    $("#btnadduser").hide();
    $("#btnedituser").hide();
    $("#btndeleteuser").hide();
    $("#Pass_reset").hide();
    $("div.tbluser").hide();
    $("#btnViewUsers").hide();
    $("#lstReportName").hide();
    $("#lstIntMD").hide();
    $("#lstRefMD").hide();
    $("td.listBoxLabel").hide();
}

function showUserTable(bShow) 
{
    if (bShow)
        $("div.tbluser").show();
    else
        $("div.tbluser").hide();
}

function NewUserScreen() {
    // alert("NewUserScreen Called");
    var userid = $("#txtUserID").val();

    // MMC - 4.3 - These are the elements that are shown if we are editing a specific userid.  Show them only if we are editing a specific user.
    var arrSelectors_EditSpecificUser = ["#btnedituser", "#btndeleteuser", "#Pass_reset", "div.tbluser", "#lstReportName", "#lstIntMD", "#lstRefMD", "td.listBoxLabel"];
    
    // These are the elements that are shown if we are not editing a specific userid.  This includes if we are viewing all users.
    var arrSelectors_NotSpecific = ["#btnadduser", "#btnViewUsers"];

    $("#tblGlobalSettings").hide();

    if (userid) {
        // editing a specific user
        // alert("NewUserScreen true userid=" + userid);
        showUserTable(false);
        for (i = 0; i < arrSelectors_EditSpecificUser.length; i++)
            $(arrSelectors_EditSpecificUser[i]).show();
        if (userid == 'undefined') {
            $('#btnadduser').hide();
        }
        else {
            for (i = 0; i < arrSelectors_NotSpecific.length; i++)
                $(arrSelectors_NotSpecific[i]).hide();
        }
    }
    else {
        // default view - add new user, not viewing specific user
        // alert("NewUserScreen false userid=" + userid);
        for (i = 0; i < arrSelectors_EditSpecificUser.length; i++)
            $(arrSelectors_EditSpecificUser[i]).hide();
        for (i = 0; i < arrSelectors_NotSpecific.length; i++)
            $(arrSelectors_NotSpecific[i]).show();
    }
}

    