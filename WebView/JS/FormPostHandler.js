﻿
function getFormData_JSONObject($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

function getFormData_JSONString($form) {
    return JSON.stringify(getFormData_JSONObject($form));
}
