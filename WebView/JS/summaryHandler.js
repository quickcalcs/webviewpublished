﻿// Start Summary functions
let selectedStudy = document.getElementById("hiddenSelectedStudy").value;
let guid = document.getElementById("hiddenGUID").value;
let username = document.getElementById("hiddenUsername").value;
let trackingId = document.getElementById("hiddenTrackingID").value;

//open the TXTextControl 
//const openSummary = () =>
const openSummary = function()
{
    //$("#report-summary-wrapper").css("opacity", "1");
    $("#report-summary-wrapper").css("z-index", "1");
    //TXTextControl.zoomFactor = 150;
    //TXTextControl.showRibbonBar(false);
    //$("#report-summary-wrapper").draggable({
    //    handle: $("#summaryHead"),
    //    containment: $("#mainpanel")
    //});
    $(document).ready(function ()
    {
        //console.log("initialize summary dragging");
        $("#report-summary-wrapper").draggable({
            handle: $("#summaryHead"),
            containment: $("#mainpanel")
        });

        TXTextControl.zoomFactor = 150;

        // CWT 5/11/2020 - forcing resize event to get edit area to format correclty on load.  
        //window.dispatchEvent(new Event('resize'));
        if (document.createEventObject)
        {
            window.fireEvent("resize");
        }
        else
        {
            var evt = document.createEvent("HTMLEvents");
            evt.initEvent("resize", false, true);
            window.dispatchEvent(evt);
        }

        //$("#mainCanvas").scrollTop(0);
        //reformatSummaryEdit();
        // CWT 5/8/20 - removed this code hiding the TXTextControl horizontal scroll bar
        /*
        $("#slider-horizontal").css("display", "none");
        //TXTextControl.setTextFrameMarkerLines(false);
        $(document).ready(function ()
        {
            $("#scroll-button-left").css("display", "none");
            $("#scroll-button-right").css("display", "none");
        });
        */
    });
}

//close the TXTextControl 
//const closeSummary = () =>
const closeSummary = function()
{
    teardownAutosave();
    //$("#report-summary-wrapper").css("opacity", "0");
    $("#report-summary-wrapper").css("right", "2%");
    $("#report-summary-wrapper").css("z-index", "-1");
    ToggleEditMode(false);
}

// Start Image Tab Summary 
// still need to implement in the image tab 4.3
//const openSummaryImg = () =>
const openSummaryImg = function()
{
    $("#img-summary-wrapper").css("opacity", "1");
    ribbonbarSearch();
    $("#img-summary-wrapper").draggable({ handle: $("#imgSummaryHead"), containment: "parent" });
}

// Increase TXTextControl width 
//const restoreWindowRpt = () =>
const restoreWindowRpt = function()
{

    //CWT 5/11/2020 - initialize the report summary area for dragging.  
    $("#report-summary-wrapper").draggable({
        handle: $("#summaryHead"),
        containment: $("#mainpanel"),
        disabled: true
    });

    //console.log("restoreWindowRpt()");
    TXTextControl.zoomFactor = 150;
    $("#report-summary-wrapper").css("left", "");
    $("#report-summary-wrapper").css("width", "");
    //$("#report-summary-wrapper").css("width", "90%");
    $("#report-summary-wrapper").css("max-height", "");
    //$("#report-summary-wrapper").css("max-height", "85vh");
    $("#report-summary-wrapper").css("top", "");
    //$("#report-summary-wrapper").css("top", "0px");
    $("#report-summary-wrapper").css("height", "");

    //console.log("now set the button bar styles");
    $(".summaryRichText").css("height", "");
    //console.log("Do I get here 1?");
    $("#ribbonTabView_btnViewModePrintLayout").click();
    //console.log("Do I get here 2?");
    TXTextControl.setViewMode(TXTextControl.ViewMode.Normal);
    //console.log("Do I get here 3?");
    $(".summaryToolTip").css("display", "block");
    //console.log("Do I get here 4?");
    $("#btnUnderline").css("margin-right", "1%");
    //console.log("Do I get here 5?");
    $("#btnBulletPoint").css("margin-right", "1%");
    //console.log("Do I get here 6?");
    $("#btnClear").css("margin-right", "1%");
    //console.log("Do I get here 7?");
    $("#btnUndo").css("margin-right", "1%");
    //console.log("Do I get here 8?");
    //$("#report-summary-wrapper").draggable('enable');
    $("#report-summary-wrapper").draggable("enable");
    //console.log("Do I get here 9?");
    $(".fa-square-o").show();
    //console.log("Do I get here 10?");
    $(".fa-clone").hide();
    // CWT (4/23/2020) - force a resize event to get the TXTextControl to automatically redraw, otherwise there are odd consequences
    //+console.log("fire resize event");
    //window.dispatchEvent(new Event('resize'));
    if (document.createEventObject)
    {
        window.fireEvent("resize");
    }
    else
    {
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent("resize", false, true);
        window.dispatchEvent(evt);
    }

    //console.log("reportHeight");
    //console.log($("#reportRichText").height());
}

// make TXTextControl width fill screen
//const maximizeWindowRpt = () =>
const maximizeWindowRpt = function()
{
    TXTextControl.zoomFactor = 150;
    $("#ribbonTabView_btnViewModePrintLayout").click();
    $("#report-summary-wrapper").draggable("disable");

    /*
    $("#report-summary-wrapper").draggable({
        handle: $("#summaryHead"),
        containment: $("#mainpanel")
    });
    */

    $("#report-summary-wrapper").css("width", "100%");
    $("#report-summary-wrapper").css("max-height", "100vh");
    //$("#report-summary-wrapper").css("height", 'calc(100% - 46px)');
    $("#report-summary-wrapper").css("height", "100%");
    //console.log($('#thediv').height());
    //console.log($('#thediv').height() + 39);
    $("#report-summary-wrapper").css("top", "-39px");
    $("#report-summary-wrapper").css("left", "0px");
    $(".summaryRichText").css("height", "calc(80% - 106px)");
    //$('#thediv').height();
    $("#ribbonTabView_btnViewModePrintLayout").click();
    TXTextControl.setViewMode(TXTextControl.ViewMode.Normal);
    $(".summaryToolTip").css("display", "block");
    $("#btnUnderline").css("margin-right", "1%");
    $("#btnBulletPoint").css("margin-right", "1%");
    $("#btnClear").css("margin-right", "1%");
    $("#btnUndo").css("margin-right", "1%");
    $(".fa-square-o").hide();
    $(".fa-clone").show();

    /*
    // CWT (4/22/20) - adjusting drawing area on initial resize, the rest will be handled by the component
    $("#mainCanvas").height($("#reportRichText").height() - 47);

    console.log(($("#reportRichText").height() - 22) + "px");

    $("#statusBarCanvas").css("top", ($("#reportRichText").height() - 22) + "px");
    
    console.log("reportHeight");
    console.log($("#reportRichText").height());
    */
    // CWT (4/23/2020) - force a resize event to get the TXTextControl to automatically redraw, otherwise there are odd consequences
    //window.dispatchEvent(new Event('resize'));
    if (document.createEventObject)
    {
        window.fireEvent("resize");
    }
    else
    {
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent("resize", false, true);
        window.dispatchEvent(evt);
    }

}

// Decrease TXTextControl width 
//const minimizeWindowRpt = () => 
const minimizeWindowRpt = function()
{
    TXTextControl.zoomFactor = 150;

    $("#report-summary-wrapper").css("left", "");
    $("#report-summary-wrapper").css("top", "");

    $("#report-summary-wrapper").css("width", "737px");
    //$("#report-summary-wrapper").css("width", "50%");
    $("#report-summary-wrapper").css("max-height", "");
    //$("#report-summary-wrapper").css("max-height", "85vh");

    //$(".summaryRichText").css("height", "");

    $("#ribbonTabView_btnViewModeDraftLayout").click();
    TXTextControl.setViewMode(TXTextControl.ViewMode.Normal);
    $(".summaryToolTip").css("display", "none");
    $("#btnUnderline").css("margin-right", "0");
    $("#btnBulletPoint").css("margin-right", "0");
    $("#btnClear").css("margin-right", "0");
    $("#btnUndo").css("margin-right", "0");
    $("#report-summary-wrapper").draggable("enable");
    $(".fa-square-o").hide();
    $(".fa-clone").show();

    // CWT (4/23/2020) - force a resize event to get the TXTextControl to automatically redraw, otherwise there are odd consequences
    //window.dispatchEvent(new Event('resize'));
    if (document.createEventObject)
    {
        window.fireEvent("resize");
    }
    else
    {
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent("resize", false, true);
        window.dispatchEvent(evt);
    }
}

//change comments to macros viseversa
$('input[type=radio][name=cmtsmacros]').change(function ()
{

    if (this.value == 'Macros')
    {
        document.getElementById("TreeComments").style.display = "none";
        document.getElementById("TreeMacros").style.display = "inline-block";
    }
    else if (this.value == 'Comments')
    {
        document.getElementById("TreeComments").style.display = "block";
        document.getElementById("TreeMacros").style.display = "none";
    }

});

//-----------------------------------------------------
// Auto save related code
var bAnyAutosaves = false;
var timeoutAutosave;
var bAutosaveToreDown = false;
var bTXTextDirty = false;

// var msecAutosave = 10000;    // Defined in Report.aspx to integrate in server config values.
// var msecDelayPDFRegen;       // Defined in Report.aspx

function doAutosaveIfDirty()
{
    console.log("Autosave timer fired");
    reformatSummaryEdit();
    // var timestamp = '[' + new Date().toTimeString() + '] ';
    // console.log("Autosave timer fired at: " + timestamp + ", Dirty: " + bTXTextDirty);

    if (bAutosaveToreDown)
    {
        console.log("Autosave timer fired, but autosave has already been torn down. ")
        return;
    }

    teardownAutosave();

    //console.log("bTXTextDirty = " + bTXTextDirty);

    if (bTXTextDirty)
    {
        //console.log("[fn: doAutosaveIfDirty()] - bTXTextDirty set to false");
        bTXTextDirty = false;
        saveTxTextAndAutosave();
    }
    else
    {
        console.log("setting up autosave check again because text is not dirty");
        setupAutosave();
    }
    
}

function teardownAutosave()
{
    bAutosaveToredown = true;

    if (timeoutAutosave)
    {
        clearTimeout(timeoutAutosave);
    }

}

function setupAutosave()
{
    //console.log("fn: setupAutosave()");

    TXTextControl.addEventListener("textControlChanged", function ()
    {
        //console.log("textControlChanged");
        //console.log("[fn: doAutosaveIfDirty()] - bTXTextDirty set to true");
        bTXTextDirty = true;
    });

    teardownAutosave();
    timeoutAutosave = setTimeout(doAutosaveIfDirty, msecAutosave);
    bAutosaveToreDown = false;
}

function saveTxTextAndAutosave()
{
    //console.log("[fn: saveTxTextAndAutosave()]");
    var bGotPlainText = false;

    TXTextControl.saveDocument(TXTextControl.StreamType.PlainText, function (e)
    {
        document.getElementById("hiddenNewValSummaryString").value = e.data;
        bGotPlainText = true;
    });

    TXTextControl.saveDocument(TXTextControl.StreamType.RichTextFormat, function (e)
    {
        document.getElementById("hiddenNewValComments").value = e.data;
        document.getElementById("hiddenNewValTxText").value = e.data;

        if (bGotPlainText)
        {
            //console.log("anyAutoSaves flag set to true");
            bAnyAutosaves = true;
            InvokeHandler('EditSummaryAutosave', 1 + 4 + 16, 'SummaryEditForm');
        }
        else
        {
            console.log("Autosave: Problem - Got RTF value, but not yet Plain Text");
            console.log("setting up autosave on Save text and autosave");
            setupAutosave();
        }
    });
}
//-----------------------------------------------------

function saveTxTextAndSend()
{
    //console.log("[fn: saveTxTextAndSend()]");
    $('.loadingBackground').show();
    $('#PDFNotFound').hide();
    document.getElementById("thediv").style.position = "relative";
    document.getElementById("PDFRefresh").style.display = "block";
    //$("#PDFRefresh").draggable();

    teardownAutosave();

    var bGotPlainText = false;

    TXTextControl.saveDocument(TXTextControl.StreamType.PlainText, function (e)
    {
        $("#hiddenNewValSummaryString").val(e.data);
        //document.getElementById("hiddenNewValSummaryString").value = e.data;
        bGotPlainText = true;
    });

    TXTextControl.saveDocument(TXTextControl.StreamType.RichTextFormat, function (e)
    {
        // console.log(e.data);
        $("#hiddenNewValComments").val(e.data);
        //document.getElementById("hiddenNewValComments").value = e.data;
        $("#hiddenNewValTxText").val(e.data);
        //document.getElementById("hiddenNewValTxText").value = e.data;
        updateHiddenSummary();  // Be sure to update our own loadRtf element

        if (bGotPlainText)
        {
            closeSummary();
            InvokeHandler('EditSummaryEnd', 1 + 4 + 8 + 16, 'SummaryEditForm');
            //console.log("anyAutoSaves flag set to FALSE");
            bAnyAutosaves = false;
            //console.log("[fn: saveTxTextAndSend()] - bTXTextDirty set to false");
            bTXTextDirty = false;
        }
        else
        {
            console.log("OK button: Problem - Got RTF value, but not yet Plain Text");
        }

    });

    //document.getElementById("PDFRefresh").style.display = "none";
}

function cancelEdit()
{
    //alert("[fn: cancelEdit()]");
    teardownAutosave();
    loadNewSummary();
    closeSummary();
    InvokeHandler('EditSummaryCancel', 1 + 4 + 16);
}

// TXTextControl Load event
TXTextControl.addEventListener("textControlLoaded", function ()
{
    console.log("textControlLoaded");
    loadNewSummary();
    TXTextControl.showRibbonBar(false);
    TXTextControl.showStatusBar(false);
    TXTextControl.showHorizontalRuler(false);
    TXTextControl.showVerticalRuler(false);
});

/*
TXTextControl.addEventListener("documentLoaded", function (wtf)
{
    console.log(wtf);
    console.log("document load complete");
    //alert("Loaded!");
    reformatSummaryEdit();
});
*/

function loadTxText(html)
{
    console.log("loadTxText(html)");

    /*
    TXTextControl.setViewMode(TXTextControl.ViewMode.Normal);

    console.log("PageMargins:");
    console.log(TXTextControl.pageMargins);
    */

    //console.log("btoa(html) next");
    var encoded = btoa(html);  // btoa base-64-encodes strings.

    //console.log(encoded);
    //TXTextControl.loadDocument(TXTextControl.StreamType.RichTextFormat, encoded);
    TXTextControl.loadDocument(TXTextControl.StreamType.RichTextFormat, encoded, function (wtf)
    {
        console.log(wtf);
        console.log("document load complete");
        //alert("Loaded!");
        //console.log(TXTextControl.ScrollPosition);
        //TXTextControl.ScrollPosition = ScrollPosition.Top;
        /*
        console.log(TXTextControl.InputPosition);
        console.log(TXTextControl.inputPosition);
        TXTextControl.inputPosition.scrollTo(TXTextControl.InputPosition.ScrollPosition.Bottom, function () { console.log("in the callback"); });

        console.log(TXTextControl.ScrollPosition);
        */

        TXTextControl.inputPosition.scrollTo(TXTextControl.InputPosition.ScrollPosition.Top, function () { console.log("scrolled to top"); });

        /*
        TXTextControl.inputPosition.scrollTo(TXTextControl.InputPosition.ScrollPosition.Bottom, function ()
        {
            console.log("scrolling to bottom");
            //TXTextControl.inputPosition.scrollTo(TXTextControl.InputPosition.ScrollPosition.Top, function () { console.log("scrolling back to top"); });
        });
        */

        reformatSummaryEdit();
    });

    console.log("called loadDocument");

    /*
    TXTextControl.zoomFactor = 150;

    TXTextControl.pageMargins.setLeft(60);
    TXTextControl.pageMargins.setRight(70);

    restoreWindowRpt();
    */

    /*
    TXTextControl.pageMargins.getLeft(function (e)
    {
        console.log("get Left?:");
        console.log(e);
    });

    TXTextControl.pageMargins.getRight(function (e)
    {
        console.log("get Right?:");
        console.log(e);
    });
    */

    TXTextControl.pageSize.setWidth(850);
}

function updateHiddenSummary()
{
    console.log("updateHiddenSummary()");
    var newRTF = $('#hiddenNewValTxText').val();
    $('#loadRtf').html(newRTF);
}

// call this one each page load img/report 
function loadNewSummary()
{
    console.log("loadNewSummary()");
    let html = document.getElementById("loadRtf").innerHTML;

    console.log(html);

    if (html !== '' && html !== 'NULL')
    {
        loadTxText(html);
    }

    console.log(TXTextControl);
    console.log(TXTextControl.PageSize);
    console.log(TXTextControl.pageSize);

    // CWT (4/23/20) - Added this to make sure formatting happens even if source text is blank or null
    console.log("set view mode to normal");
    TXTextControl.setViewMode(TXTextControl.ViewMode.Normal);

    console.log("set zoom factor");
    TXTextControl.zoomFactor = 150;

    /*
    try
    {
        console.log("Set orientation to portrait");
        TXTextControl.SectionFormat.setLandscape(false);
    }
    catch (wierdError)
    {
        console.log(wierdError);
    }
    */

    TXTextControl.pageSize.getWidth(function (e)
    {
        console.log("TXTextControl.PageSize.getWidth: ");
        console.log(e);
    });

    //console.log("Set orientation to landscape");
    //TXTextControl.SectionFormat.setLandscape(true);

    /*
    // these margins work for Mark Braun 3/15/2014 but break others
    console.log("set margins");
    console.log("set left = 45");
    console.log("set right = 45");
    TXTextControl.pageMargins.setLeft(45);
    TXTextControl.pageMargins.setRight(45);
    //TXTextControl.pageMargins.setLeft(60);
    //TXTextControl.pageMargins.setRight(60);
    */

    // these margins work for Megan Forsythe 3/06/2014 but break others
    console.log("set margins");
    console.log("set left = 52");
    console.log("set right = 52");
    TXTextControl.pageMargins.setLeft(52);
    TXTextControl.pageMargins.setRight(52);
    //TXTextControl.pageMargins.setLeft(60);
    //TXTextControl.pageMargins.setRight(60);

    console.log("restore to reguler view");
    restoreWindowRpt();
}

function addPlainText(string)
{
    var textParts;

    // retrieve all TextParts
    TXTextControl.getTextParts(function (e)
    {
        textParts = e;

        // loop through all TextParts
        textParts.forEach(function (entry)
        {
            // set text at the beginning of each TextPart
            entry.selection.load(TXTextControl.StreamType.PlainText, btoa(string));
        });

    }, false);
}

function addRtfLine(string)
{
    var textParts;

    // retrieve all TextParts
    TXTextControl.getTextParts(function (e)
    {
        textParts = e;

        // loop through all TextParts
        textParts.forEach(function (entry)
        {
            var encoded = btoa(string);
            // set text at the beginning of each TextPart
            entry.selection.load(TXTextControl.StreamType.RichTextFormat, encoded);
        });

    }, false);
}

// Start function to get value for validation
function GenerateQueryString(bitmaskFields)
{
    //                 1            2            4           8          16        32
    var arrFields = ["username", "fullname", "StudyID", "TrackingID", "GUID", "RetainLock"];
    var arrFields2 = ["hiddenUsername", "hiddenFullname", "hiddenSelectedStudy", "hiddenTrackingID", "hiddenGUID", "hiddenRetainLock"];

    if (bitmaskFields == 0)
    {
        return "";
    }
    
    var strQueryString = "";
    var strKey, strVal, bitmaskTest;

    for (i = 0; i < arrFields.length; i++)
    {
        bitmaskTest = Math.pow(2, i);

        if ((bitmaskFields & bitmaskTest) != 0)
        {
            strKey = arrFields[i];
            strVal = document.getElementById(arrFields2[i]).value;

            if (i > 0)
            {
                strQueryString = strQueryString + '&';
            }
            
            strQueryString = strQueryString + strKey + '=' + encodeURIComponent(strVal);
        }

    }

    //console.log(strQueryString);
    return strQueryString;
}

function InvokeHandler(strOp, bitmaskFields, optionalForm)
{
    //alert('strOp=' + strOp);
    console.log(strOp);
    console.log(bitmaskFields);
    var strUrl = 'SummaryHandler.ashx?op=' + strOp + '&' + GenerateQueryString(bitmaskFields);
    //alert(strUrl);
    console.log("SENT: " + strUrl);

    if (strOp == 'DoESign')
    {
        //start ajax request
        $.ajax({
            url: strUrl,
            //force to handle it as text
            dataType: "text",
            success: function (data)
            {
                console.log("RCV FOR: " + strUrl);
                var json = $.parseJSON(data);
                console.log(json);

                if (json.TimeoutReached)
                {
                    ShowTimeoutMessage();
                }
                else
                {

                    if (json != null && json.Result == 1)
                    {
                        // acually esigning
                        console.log("Do ESign succeeded: ");
                        console.log('\t Result=' + json.Result);
                        console.log('\t TrackingID=' + json.TrackingID);
                        $("#hiddenTrackingID").val(json.TrackingID);

                        console.log("Do ESign - regenerate PDF pending");
                        doDelayedPDFRegen();
                    }
                    else
                    {
                        // prompt that they can not esign again
                        modalEsigFail();
                    }

                }

            }
        });
    }
    else if (strOp == 'EditSummaryEnd' || strOp == 'EditSummaryAutosave')
    {
        var bSummaryAutosave = (strOp == 'EditSummaryAutosave');
        var bSummaryEnd = (strOp == 'EditSummaryEnd');

        //start ajax request
        $.ajax({
            type: "POST",
            url: strUrl,
            data: getFormData_JSONObject($("#" + optionalForm)),
            //force to handle it as text
            dataType: "text",
            success: function (resultdata)
            {
                console.log("RCV FOR: " + strUrl);
                var json = $.parseJSON(resultdata);

                if (json != null)
                {
                    console.log("RESPONSE FOR: " + strUrl);

                    if (json.TimeoutReached)
                    {
                        ShowTimeoutMessage();
                    }
                    else
                    {

                        if (json.Result == 0)
                        {
                            console.log("result is empty / save format error");
                            // create pop up for summary save error
                        }
                        else if (json.Result == 1)
                        {

                            if (bSummaryEnd)
                            {
                                doDelayedPDFRegen();
                            }

                        }

                        console.log('\t Result=' + json.Result);
                        console.log('\t TrackingID=' + json.TrackingID);

                        //Set the TrackingID to a hidden input maybe?
                        if (bSummaryEnd)        // only for OK.  Do not set for Autosave case
                        {
                            $("#hiddenTrackingID").val(json.TrackingID);
                        }

                    }
                    
                }

                if (bSummaryAutosave)
                {
                    console.log("setting up autosave on EditSummaryEnd");
                    setupAutosave();
                }
                
            }
        });
    }
    else if (strOp == 'EditSummaryBegin')
    {
        console.log('EditSummaryBegin');
        //alert('EditSummaryBegin section');
        //start ajax request
        $.ajax({
            url: strUrl,
            //force to handle it as text
            dataType: "text",
            success: function (data)
            {
                console.log("RCV FOR: " + strUrl);
                var json = $.parseJSON(data);

                if (json != null) 
                {

                    if (json.TimeoutReached)
                    {
                        ShowTimeoutMessage();
                    }
                    else
                    {

                        if (json.Result == 1 && json.OpenBy == "")
                        {
                            // open summary if true
                            openSummary();
                            console.log("setting up autosave on EditSummaryBegin");
                            setupAutosave();
                            /*
                            setTimeout(function ()
                            {
                                console.log("Hide Ribbon Bar");
                                TXTextControl.showRibbonBar(false);
                            }, 1000);
                            */
                        }
                        else if (json.Result == 0 && json.OpenBy == "")
                        {
                            // modal pop up for wrong MD
                            modalWrongMD();
                        }
                        else if (json.Result == 0 && json.OpenBy.length >= 1)
                        {
                            // modal pop up for locked by
                            modalLockedBy(json.OpenBy)
                        }

                    }

                }
            }
        });
    }
    else if (strOp == 'RegenPDF')
    {
        //start ajax request
        $.ajax({
            url: strUrl,
            //force to handle it as text
            dataType: "text",
            success: function (data)
            {
                console.log("RCV FOR: " + strUrl);
                console.log(data);

                if (data == 1)
                {
                    // refresh report display

                    if ($('.pic').length)
                    {

                        $(".pic").each(function ()
                        {
                            var src = $(this).attr('src');
                            console.log("Resetting pic's src to trigger display refresh: " + src)
                            $(this).attr('src', src);
                        });

                        //console.log("Resetting pic's src to trigger display refresh: " + src);
                        //var src = $("#pic").attr('src');
                        //$('#pic').attr('src', src);
                        document.getElementById("PDFRefresh").style.display = "none";
                        document.getElementById("thediv").style.position = "absolute";
                    }
                    else
                    {
                        //reload
                        //document.ref
                        window.location.reload(true);
                    }

                }

            }
        });
    }
    else if (strOp == 'EditSummaryCancel')
    {
        $.ajax({
            url: strUrl,
            //force to handle it as text
            dataType: "text",
            success: function (data)
            {
                console.log("RCV FOR: " + strUrl);

                if (data == 1)
                {
                    // Skip all PDF regeneration, only OK can cause a PDF regen
                }

            }
        });
    }
    else 
    {
        // CanESign
        $.ajax({
            url: strUrl,
            //force to handle it as text
            dataType: "text",
            success: function (data)
            {
                console.log("RCV FOR: " + strUrl);
                //console.log("data = " + data);

                if (data == 1)
                {
                    // modalEsig();  // MMC - No popup in WV 4.3
                    //alert("modalEsig() - calling InvokeHandler");
                    $('#hiddenRetainLock').val('1');
                    InvokeHandler('DoESign', 1 + 2 + 4 + 8 + 16 + 32);
                    $('#hiddenRetainLock').val('0');
                }
                else
                {
                    //alert("calling modalEsigFail()");
                    modalEsigFail();
                }

                //alert("HTTP returned " + data);
            }

        });
        // GetResponse(strUrl);
    }
    //document.getElementById("PDFRefresh").style.display = "none";
}

function doDelayedPDFRegen()
{
    setTimeout(function ()
    {
        // Request PDF regeneration
        console.log("Requesting SummaryHandler to RegenPDF.");
        InvokeHandler('RegenPDF', 1 + 4);
    }, msecDelayPDFRegen);
}

function doEditCommitOnX()
{
    //console.log("bTXTextDirty = " + bTXTextDirty);
    //console.log("bAnyAutosaves = " + bAnyAutosaves);

    if (bTXTextDirty || bAnyAutosaves)
    {
        //console.log("About to call saveTxTextAndSend()");
        saveTxTextAndSend();
    }
    
}

//TODO: Figure out why this code is throwing javascript errors.  
/*
selectedStudy.addEventListener('onchange', () =>
{

    if (selectedStudy.value != null || '')
    {
        //use axios lib to do http request
        axios
            .get('SummaryHandler.ashx?op=CloseStudy&guid=' + guid + '&studyID=' + selectedStudy + '&username' + username + '&TrackingID=' + trackingId)
            .then((res) => {
                //fill the list with results
                console.log("Study Closed")
            })
            .catch((err) => {
                console.log(err);
            })
    }

});
*/

$(selectedStudy).change(function ()
{

    if (selectedStudy.value != null || '')
    {
        //use axios lib to do http request
        axios
            .get('SummaryHandler.ashx?op=CloseStudy&guid=' + guid + '&studyID=' + selectedStudy + '&username' + username + '&TrackingID=' + trackingId)
            //.then((res) =>
            .then(function(res)
            {
                //fill the list with results
                console.log("Study Closed")
            })
            //.catch((err) =>
            .catch(function(err)
            {
                console.log(err);
            })
    }

});