﻿
var messageTemplate = "<p>Your session has expired, please login again.  Automatically redirecting in {x} seconds. </p>";
var currentCountdown = 10;

function ShowTimeoutMessage()
{

    if ($(".dialog").length == 0)
    {
        createDialog("Session Timeout", messageTemplate.replace("{x}", currentCountdown));
        window.setTimeout(updateCountdown, 1000);
    }
    else
    {
        console.log("Timeout Message already open");
    }

}

function createDialog(title, text)
{
    return $("<div class='dialog' title='" + title + "'>" + text + "</div>")
        .dialog({
            resizable: false,
            height: 280,
            width: 340,
            modal: true,
            close: function () { RedirectToLogin(); },
            buttons:
            {
                "Confirm": function ()
                {
                    //$(this).dialog("close");
                    RedirectToLogin();
                }
            }
        });
}

function updateCountdown()
{
    currentCountdown--;
    $(".dialog").html(messageTemplate.replace("{x}", currentCountdown));

    if (currentCountdown == 1)
    {
        window.setTimeout(RedirectToLogin, 1000);
    }
    else
    {
        window.setTimeout(updateCountdown, 1000);
    }
    
}

function RedirectToLogin()
{
    window.location.href = "login.aspx";
}