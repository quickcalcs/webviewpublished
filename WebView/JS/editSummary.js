﻿// bRTF - Whether we expect RTF.  If it's not RTF, it's text.
// strTree - Either "Macros" or "Comments"
// nPKey - the PKey
//const HyperLinkList = document.getElementById("HyperlinkList");
//const HyperLinkList = $("#HyperlinkList");
//const HyperLinkDiv = document.getElementById("HyperLinkDiv");

// CWT 05/11/2020 - adding borders to summary edit box to make cleaner.  Boolean control property could be moved to config in the future.  
var bAddSummaryEditBorder = true;

if (bAddSummaryEditBorder)
{
    $("#summaryHead").css("border-right", "1px solid");
    $("#summaryHead").css("border-left", "1px solid");
    $("#summaryHead").css("border-top", "1px solid");
    $("#summaryMacroSection").css("border-right", "1px solid");
    $("#summaryMacroSection").css("border-left", "1px solid");
    $("#summaryRibbon").css("border-right", "1px solid");
    $("#summaryRibbon").css("border-left", "1px solid");
    $("#reportRichText").css("border-right", "1px solid");
    $("#reportRichText").css("border-left", "1px solid");
    $(".summaryBottom").css("border-right", "1px solid");
    $(".summaryBottom").css("border-left", "1px solid");
    $(".summaryBottom").css("border-bottom", "1px solid");
}

// CWT 05/11/2020 - function to ensure that resize event only happens when page is first resized then again when the resize is complete.  
var waitForFinalEvent = (function ()
{
    var timers = {};

    return function (callback, ms, uniqueId)
    {

        if (!uniqueId)
        {
            uniqueId = "Don't call this twice without a uniqueId";
        }

        if (timers[uniqueId])
        {
            clearTimeout(timers[uniqueId]);
        }

        timers[uniqueId] = setTimeout(callback, ms);
    };
})();

// CWT 05/11/2020 - handles page resize so that special TXTextControl formatting is maintained.  
$(window).resize(function ()
{

    waitForFinalEvent(function ()
    {
        console.log("resize happened");
        console.log("mainCanvas width = " + $("#mainCanvas").width());
        reformatSummaryEdit();
    }, 500, "reportResize");

});

// CWT 05/11/2020 - Add some padding around text edit area when the page loads.  
var summaryPaddingSize = 15;

$("#txTemplateDesignerContainer").css("background-color", "white");

$("#mainCanvas").css("margin-left", summaryPaddingSize + "px");
$("#mainCanvas").css("margin-top", summaryPaddingSize + "px");
//$("#horRulerCanvas").css("padding-left", summaryPaddingSize + "px");
$("#horRulerCanvas").css("background-color", "white");

var contWidth = $("#txTemplateDesignerContainer").width();
var origUpButtonTop = $("#scroll-button-up").css("top");
var origDownButtonTop = $("#scroll-button-down").css("top");

$("#scroll-button-up").closest(".txScrollbarContainer").css("left", contWidth - summaryPaddingSize);
$("#scroll-button-up").css("top", origUpButtonTop + summaryPaddingSize);
$("#scroll-button-down").css("top", origDownButtonTop + summaryPaddingSize);

console.log("mainCanvas width = " + $("#mainCanvas").width());
console.log("mainCanvas height = " + $("#mainCanvas").height());
console.log("txTemplateDesignerContainer width = " + $("#txTemplateDesignerContainer").width());
console.log("txTemplateDesignerContainer height = " + $("#txTemplateDesignerContainer").height());
console.log("vertical scrollbar left = " + $("#scroll-button-up").closest(".txScrollbarContainer").css("left"));

function reformatSummaryEdit()
{
    //$("#txTemplateDesignerContainer").css("background-color", "white");
    var containerHeight = $("#txTemplateDesignerContainer").height();
    var containerWidth = $("#txTemplateDesignerContainer").width();

    //$("#mainCanvas").width(containerWidth - 15 - summaryPaddingSize);
    //$("#mainCanvas").height(containerHeight - 15 - summaryPaddingSize);

    $("#horRulerCanvas").width(containerWidth - 15 - summaryPaddingSize);

    var oldUpButtonTop = $("#scroll-button-up").css("top");
    var oldDownButtonTop = $("#scroll-button-down").css("top");
    //var oldVerticalScrollbarLeft = $("#scroll-button-left").closest(".txScrollbarContainer").css("left");

    //console.log("oldVerticalScrollbarLeft = " + oldVerticalScrollbarLeft);
    console.log("oldVerticalScrollbarLeft = " + $("#scroll-button-up").closest(".txScrollbarContainer").css("left"));

    console.log("containerWidth = " + containerWidth);
    console.log("summaryPaddingSize = " + summaryPaddingSize);
    console.log("CALCED VerticalScrollbarLeft = " + (containerWidth - summaryPaddingSize));
    $("#scroll-button-up").closest(".txScrollbarContainer").css("left", (containerWidth - summaryPaddingSize));

    console.log("NEWVerticalScrollbarLeft = " + $("#scroll-button-up").closest(".txScrollbarContainer").css("left"));

    $("#scroll-button-up").css("top", oldUpButtonTop + summaryPaddingSize);
    $("#scroll-button-down").css("top", oldDownButtonTop + summaryPaddingSize);

    //console.log($("#scroll-button-left").closest(".txScrollbarContainer").html());
    $("#scroll-button-left").closest(".txScrollbarContainer").css("left", "0px");

    //console.log("newHorizaontalScrollbarLeft = " + (oldHorizaontalScrollbarLeft - summaryPaddingSize));
    
    //$("#vertical-scrollbar-wrapper")
    //console.log($("#scroll-button-up").closest("div").css("left"));
    TXTextControl.setViewMode(TXTextControl.ViewMode.Normal);
}

function getRTFForPKey(strTree, nPKey)
{
    var bCommentsTree = (strTree == "Comments");
    var bMacrosTree = (strTree == "Macros");

    if (!bCommentsTree && !bMacrosTree)
    {
        return;
    }

    var strUrl = 'SummaryHandler.ashx?op=GetRTF&GetHyperlinks=1&Tree=' + strTree + '&PKey=' + nPKey;
    //alert("Going to load RTF for PKey with \r\n" + strUrl);

    //start ajax request
    $.ajax({
        url: strUrl,
        //force to handle it as text
        dataType: "text",
        success: function (data)
        {
            var json = $.parseJSON(data);
            console.log(json);

            if (json != null)
            {

                if (json.TimeoutReached)
                {
                    ShowTimeoutMessage();
                }
                else
                {

                    if ((json.Result == 1) && (json.strRTF != ""))
                    {
                        // Verify response
                        console.log("Got RTF for this PKey " + nPKey);
                        console.log("Num hyperlinks=" + json.NumHyperlinks);
                        //console.log("RTF is " + json.strRTF);
                        var rtf = json.strRTF;
                        // Add to the end of the summary

                        console.log("rtf=" + rtf);

                        addRtfLine(rtf);
                    }

                }

            }

            /*
            if (json != null && json.Result == 1 && json.strRTF != "")
            {
                // Verify response
                console.log("Got RTF for this PKey " + nPKey);
                console.log("Num hyperlinks=" + json.NumHyperlinks);
                //console.log("RTF is " + json.strRTF);
                var rtf = json.strRTF;
                // Add to the end of the summary

                console.log("rtf=" + rtf);

                addRtfLine(rtf);
            }
            */

        }
    });
}

function getRTFForPKey_OLD(strTree, nPKey)
{
    var bCommentsTree = (strTree == "Comments");
    var bMacrosTree = (strTree == "Macros");

    if (!bCommentsTree && !bMacrosTree)
    {
        return;
    }

    var strUrl = 'SummaryHandler.ashx?op=GetRTF&GetHyperlinks=1&Tree=' + strTree + '&PKey=' + nPKey;
    //alert("Going to load RTF for PKey with \r\n" + strUrl);

    //start ajax request
    $.ajax({
        url: strUrl,
        //force to handle it as text
        dataType: "text",
        success: function (data)
        {
            var json = $.parseJSON(data);
            console.log(json);

            if (json != null && json.Result == 1 && json.strRTF != "")
            {
                // Verify response
                console.log("Got RTF for this PKey " + nPKey);
                console.log("Num hyperlinks=" + json.NumHyperlinks);
                //console.log("RTF is " + json.strRTF);
                var rtf = json.strRTF;
                // Add to the end of the summary
                addRtfLine(rtf);
            }

        }
    });
}

//Add custom input to the Hyperlink List
function addCustomListItem()
{
    let text = document.getElementById("currentSelection").value;
    createList(text);
}

// Adds each response to the list
function createList(item)
{
    //console.log(item);
    //console.log($("#HyperlinkList"));
    //console.log($("#HyperlinkList").html());
    //console.log($("#HyperlinkList").innerHTML);

    //$("#HyperlinkList").append($("<li>").addClass("HyperlinkItem").html(item));
    $("#HyperlinkList").append($("<li>").addClass("HyperlinkItem").append(item));
    //$("#HyperlinkList").append($("<li class='HyperlinkItem'>" + item + "</li>"));

    //var node = $("<li />", { html: item }).addClass("HyperlinkItem").appendTo($("#HyperlinkList"));
    //var node = $("<li />", { html: item }).appendTo($("#HyperlinkList"));
    //$("<li />").appendTo($("#HyperlinkList"));

    //console.log(node);
    //console.log(node.html());
    /*
    var node = document.createElement("LI");
    node.classList.add("HyperlinkItem")
    var textnode = document.createTextNode(item);
    node.appendChild(textnode);
    //HyperLinkList.appendChild(node);
    $("#HyperlinkList").append(node);
    */

    $("#currentSelection").focus();

    // Add click event on all Li in the HyperlinkList
    //$("#HyperlinkList li").click(function ()
    $("#HyperlinkList li").off("click").on("click", function (e)
    {
        e.preventDefault();
        e.stopPropagation();
        //let linkText = this.innerHTML;
        let linkText = $(this).html();
        $(".highlight").removeClass('highlight');
        //this.classList.add("highlight");
        $(this).addClass("highlight");
        $("#currentSelection").val(linkText);
        //document.getElementById("currentSelection").value = linkText;
    });

    // submit current selected with double click
    //$("#HyperlinkList li").dblclick(function (e)
    $("#HyperlinkList li").off("dblclick").on("dblclick", function (e)
    {
        //alert("event target innerHTML => " + e.target.innerHTML);
        //alert("current element innerHTML => " + this.innerHTML);
        e.preventDefault();
        e.stopPropagation();
        //alert("#HyperlinkList li dblclick");
        updateHyPerLink();
    });

    // submit currentSelection on enter key press
    //$("#currentSelection").keydown((e) =>
    //$("#currentSelection").keydown(function (e)
    $("#currentSelection").off("keydown").on("keydown", function (e)
    {
        //alert(e.keyCode);

        if (e.keyCode == 13)
        {
            //alert("will preventDefault");
            console.log("ENTER EVENT FIRED");
            console.log(e);
            e.preventDefault();
            e.stopPropagation();
            updateHyPerLink();
        }

    });
}

// Close the HyperLinkDiv and reset the contents
function closeHyperLinkDiv() 
{
    //HyperLinkList.innerHTML = '';
    console.log("closeHyperLinkDiv(): clear HyperLinkList");
    $("#HyperlinkList").html('');
    $("#HyperLinkDiv").hide();

    $(document).unbind("mouseup");
    //$("#HyperLinkDiv").dialog("close");
    document.getElementById("currentSelection").value = '';
    // TXTextControl.click()
    TXTextControl.focus();  // MMC - Attempted to get rid of .click() error.  Not sure what should be done here.
}

function openHyperlinkBox(e) 
{
    var xAxis = e.clientX - 120;
    var yAxis = e.clientY - 20;
    //var xAxis = e.x - 120;
    //var Yaxis = e.y - 20;
    $("#HyperLinkDiv").css({ 'left': xAxis, 'top': yAxis });
    //$("#HyperLinkDiv").css({ left: xAxis });
    //$("#HyperLinkDiv").css({ top: Yaxis });
    $("#HyperLinkDiv").show();

    $(document).mouseup(function (e)
    {
        var container = $("#HyperLinkDiv");

        // If the target of the click isn't the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            //alert('outside click');
            //container.hide();
            closeHyperLinkDiv();
        }

    });

}

function openHyperlinkBoxNew(e) 
{
    var xAxis = e.x - 120;
    var Yaxis = e.y - 20;
    //$("#HyperLinkDiv").css({ left: xAxis });
    //$("#HyperLinkDiv").css({ top: Yaxis });
    //$("#HyperLinkDiv").show();
    $("#HyperLinkDiv").dialog({ dialogClass: 'noTitlePopup', autoOpen: false, modal: true });
    $("#HyperLinkDiv").dialog("option", { position: [xAxis, Yaxis] });
    $("#HyperLinkDiv").dialog("open");
}

//Hyperlink clicked logic
//TXTextControl.addEventListener("hyperlinkClicked", (e) =>
TXTextControl.addEventListener("hyperlinkClicked", function(e)
{
    let pKey = e.target;
    console.log(pKey);
    window.pKey = pKey;
    //HyperLinkList.innerHTML = '';
    console.log("hyperlinkClicked: clear HyperLinkList");
    $("#HyperlinkList").html('');


    var strUrl = 'SummaryHandler.ashx?op=GetHyperlinkOptions&PKey=' + pKey + '&GUID=' + $("#hiddenGUID").val() + '&TrackingID=' + $("#hiddenTrackingID").val();

    $.ajax({
        url: strUrl,
        //force to handle it as json
        dataType: "json",
        success: function (data)
        {
            console.log("RCV FOR: " + strUrl);
            console.log(data);

            /*
            var json = $.parseJSON(data);
            console.log(json);

            if (json != null && json.Result == 1 && json.strRTF != "")
            {
                // Verify response
                console.log("Got RTF for this PKey " + nPKey);
                console.log("Num hyperlinks=" + json.NumHyperlinks);
                //console.log("RTF is " + json.strRTF);
                var rtf = json.strRTF;
                // Add to the end of the summary
                addRtfLine(rtf);
            }
            */

            if ($.trim($("#HyperlinkList").html()) == '')
            {

                if (data.TimeoutReached)
                {
                    ShowTimeoutMessage();
                }
                else
                {
                    let list = data.DelimitedOptions;
                    var listItem = list.split("|");
                    var newList = listItem.filter(function (item) { return item != '__' });
                    var newList2 = newList.filter(function (item) { return item != '' });
                    newList2.forEach(createList);
                }

            }

        }
    });

    /*
    alert('to use axios library');
    console.log(axios);
    //use axios lib to do http request
    axios
        //"hiddenGUID"
        //"TrackingID"
        //"hiddenTrackingID"
        //.get('SummaryHandler.ashx?op=GetHyperlinkOptions&PKey=' + pKey + '')
        //.get('SummaryHandler.ashx?op=GetHyperlinkOptions&PKey=' + pKey + '&GUID=' + $("#hiddenGUID").val())
        .get('SummaryHandler.ashx?op=GetHyperlinkOptions&PKey=' + pKey + '&GUID=' + $("#hiddenGUID").val() + '&TrackingID=' + $("#hiddenTrackingID").val())
        //.then((res) => 
        .then(function (res)
        {
            //fill the list with results
            console.log("Got return value from SummaryHandler.ashx?op=GetHyperlinkOptions&PKey=" + pKey + '&GUID=' + $("#hiddenGUID").val() + '&TrackingID=' + $("#hiddenTrackingID").val());
            //console.log(res.data);
            //console.log(document.getElementById("HyperlinkList"));
            //console.log($("#HyperlinkList"));
            //console.log(document.getElementById("HyperlinkList").innerHTML);
            //console.log($("#HyperlinkList").html());

            //if (document.getElementById("HyperlinkList").innerHTML == '')
            //if ($("#HyperlinkList").html() == '')
            if ($.trim($("#HyperlinkList").html()) == '')
            {

                if (res.data.TimeoutReached)
                {
                    ShowTimeoutMessage();
                }
                else
                {
                    //console.log("It is empty, parse the new list");
                    let list = res.data.DelimitedOptions;
                    var listItem = list.split("|");
                    //var newList = listItem.filter(item => item != '__');
                    var newList = listItem.filter(function (item) { return item != '__' } );
                    //var newList2 = newList.filter(item => item != '');
                    var newList2 = newList.filter(function (item) { return item != '' } );
                    newList2.forEach(createList);
                }

            }
        })
        //.catch((err) => 
        .catch(function (err)
        {
            console.log(err);
        });
    */

    // Gets mouse location on mouseup to postition HyperlinkDiv
    window.addEventListener('mouseup', function mousePosition(e) 
    {
        openHyperlinkBox(e);
        //openHyperlinkBoxNew(e);
        console.log('openHyperlinkBox');

        //remove event listener once it us up
        window.removeEventListener("mouseup", mousePosition)

        $("#HyperLinkDiv").draggable(
        {
            handle: $("#HyperLinkHead"),
            containment: $("#mainpanel")
        });
    });

    //setTimeout(() =>
    setTimeout(function ()
    {
        document.addEventListener('click', function windowClick(e) 
        {

            if (!$(event.target).closest(".HyperLinkDiv, .HyperlinkItem").length) 
            {
                e.stopPropagation;
                // MMC - remove test - seems to resolve problem with consecutive hyperlink edits
                // $("#HyperLinkDiv").hide();
                // console.log('hide HyperlinkBox');
                document.removeEventListener('click', windowClick)
                // TXTextControl.click()
                TXTextControl.focus();      // MMC - Attempted to get rid of .click() error.  Not sure what should be done here.
            }

        });

    }, 500);
   
    return pKey;
    console.log("pkey sent")
});

// Take the selected input value and update the current HyperLink with this value
function updateHyPerLink()
{
    console.log(TXTextControl);
    let text = $("#currentSelection").val().trim();
    let realLinkStart = 0;

    //HyperLinkList.innerHTML = '';

    //Enable commands to update hyperlink using object
    TXTextControl.enableCommands(true);
    TXTextControl.sendCommand(55, 0, 0, 0, { linkedText: text, linkTarget: pKey });
    console.log("Hyperlink pKey: " + pKey);
    console.log("NEW Hyperlink Text: " + text);

    try
    {

        TXTextControl.inputPosition.getColumn(function (o)
        {
            console.log("inputPosition.getColumn: " + o);

            TXTextControl.inputPosition.getLine(function (e)
            {
                console.log("inputPosition.getLine: " + e);
                //console.log(e);

                TXTextControl.inputPosition.getPage(function (p)
                {
                    console.log("inputPosition.getPage: " + p);
                    //console.log(p);
                    //console.log("now do stuff for your page");
                    var currentPageStartIndex = 0;

                    TXTextControl.lines.forEach(function (pline)
                    {

                        pline.getPage(function (lpage)
                        {

                            pline.getNumber(function (lnum)
                            {

                                if (lpage < p)
                                {
                                    currentPageStartIndex = lnum;
                                }
                                else
                                {

                                    if ((lnum - currentPageStartIndex) == e)
                                    {
                                        // let's do it!
                                        //console.log("found the item: Page = " + lpage + ", Line = " + lnum);
                                        //console.log(pline);
                                        //let textIndex = 0;
                                        let textIndex = o - text.length;

                                        pline.getText(function (t)
                                        {
                                            console.log("text of line with link: " + t);
                                            console.log("search start position: " + textIndex);
                                            //textIndex = t.indexOf(text, 0);
                                            textIndex = t.indexOf(text, textIndex);
                                            console.log("start of replaced link text: " + textIndex);
                                        });

                                        pline.getStart(function (h)
                                        {
                                            //console.log("pline.getStart:");
                                            //console.log(h);
                                            realLinkStart = h + textIndex + text.length;
                                            //console.log("realLinkStart 2:");
                                            //console.log(realLinkStart);

                                            let bounds = { "start": realLinkStart, "length": 0 };
                                            //console.log("bounds:");
                                            //console.log(bounds);
                                            TXTextControl.selection.setBounds(bounds);
                                        });

                                    }

                                }

                            });

                        });

                    });

                });

            });

        });

    }
    catch (e)
    {
        console.log("Error setting cursor position, leaving at default");
    }

    closeHyperLinkDiv();
    //remove contents of list after completion
    //HyperLinkList.innerHTML = '';
    console.log("updateHyPerLink(): clear HyperLinkList");
    $("#HyperlinkList").html('');
}

function updateHyPerLink_GOOD_07_21()
//function updateHyPerLink()
{
    console.log(TXTextControl);
    let text = $("#currentSelection").val().trim();
    let realLinkStart = 0;

    //HyperLinkList.innerHTML = '';

    //Enable commands to update hyperlink using object
    TXTextControl.enableCommands(true);
    TXTextControl.sendCommand(55, 0, 0, 0, { linkedText: text, linkTarget: pKey });
    console.log("Hyperlink pKey: " + pKey);
    console.log("NEW Hyperlink Text: " + text);

    try
    {

        TXTextControl.inputPosition.getLine(function (e)
        {
            console.log("inputPosition.getLine: " + e);
            //console.log(e);

            TXTextControl.inputPosition.getPage(function (p)
            {
                console.log("inputPosition.getPage: " + p);
                //console.log(p);
                //console.log("now do stuff for your page");
                var currentPageStartIndex = 0;

                TXTextControl.lines.forEach(function (pline)
                {

                    pline.getPage(function (lpage)
                    {

                        pline.getNumber(function (lnum)
                        {

                            if (lpage < p)
                            {
                                currentPageStartIndex = lnum;
                            }
                            else
                            {

                                if ((lnum - currentPageStartIndex) == e)
                                {
                                    // let's do it!
                                    //console.log("found the item: Page = " + lpage + ", Line = " + lnum);
                                    //console.log(pline);
                                    let textIndex = 0;

                                    pline.getText(function (t)
                                    {
                                        console.log(t);
                                        textIndex = t.indexOf(text, 0);
                                    });

                                    pline.getStart(function (h)
                                    {
                                        //console.log("pline.getStart:");
                                        //console.log(h);
                                        realLinkStart = h + textIndex + text.length;
                                        //console.log("realLinkStart 2:");
                                        //console.log(realLinkStart);

                                        let bounds = { "start": realLinkStart, "length": 0 };
                                        //console.log("bounds:");
                                        //console.log(bounds);
                                        TXTextControl.selection.setBounds(bounds);
                                    });

                                }

                            }

                        });

                    });

                });

            });

        });
    }
    catch (e)
    {
        console.log("Error setting cursor position, leaving at default");
    }

    closeHyperLinkDiv();
    //remove contents of list after completion
    //HyperLinkList.innerHTML = '';
    console.log("updateHyPerLink_GOOD_07_21(): clear HyperLinkList");
    $("#HyperlinkList").html('');
}

/*
function updateHyPerLink_WorkingButBad()
{
    console.log(TXTextControl);

    let text = $("#currentSelection").val();
    let realLinkStart = 0;

    //HyperLinkList.innerHTML = '';

    //Enable commands to update hyperlink using object
    TXTextControl.enableCommands(true);
    TXTextControl.sendCommand(55, 0, 0, 0, { linkedText: text, linkTarget: pKey });

    try
    {

        TXTextControl.inputPosition.getLine(function (e)
        {
            console.log("inputPosition.getLine:");
            console.log(e);

            TXTextControl.inputPosition.getPage(function (p)
            {
                console.log("inputPosition.getPage:");
                console.log(p);

                var pageStartIndex = 0;

                TXTextControl.pages.forEach(function (pp)
                {
                    console.log(pp);

                    pp.getNumber(function (pn)
                    {

                        if (pn < p)
                        {
                            pp.getLength(function (pl)
                            {
                                pageStartIndex += pl;
                                console.log("page length after page " + pn + ":");
                                console.log(pageStartIndex);
                            });
                        }
                        else if (pn == p)
                        {
                            console.log("now do stuff for your page");
                            var currentPageStartIndex = 0;

                            TXTextControl.lines.forEach(function (pline)
                            {

                                pline.getPage(function (lpage)
                                {

                                    pline.getNumber(function (lnum)
                                    {

                                        if (lpage < pn)
                                        {
                                            currentPageStartIndex = lnum;
                                        }
                                        else
                                        {

                                            if ((lnum - currentPageStartIndex) == e)
                                            {
                                                // let's do it!
                                                console.log("found the item: Page = " + lpage + ", Line = " + lnum);
                                                console.log(pline);
                                                let textIndex = 0;

                                                pline.getText(function (t)
                                                {
                                                    console.log(t);
                                                    textIndex = t.indexOf(text, 0);
                                                });

                                                pline.getStart(function (h)
                                                {
                                                    console.log("pline.getStart:");
                                                    console.log(h);
                                                    realLinkStart = pageStartIndex + h + textIndex + text.length;
                                                    console.log("realLinkStart:");
                                                    console.log(realLinkStart);
                                                    realLinkStart = h + textIndex + text.length;
                                                    console.log("realLinkStart 2:");
                                                    console.log(realLinkStart);

                                                    let bounds = { "start": realLinkStart, "length": 0 };
                                                    console.log("bounds:");
                                                    console.log(bounds);
                                                    TXTextControl.selection.setBounds(bounds);
                                                });

                                            }

                                        }


                                    });

                                });

                            });

                        }

                    });

                });

            });

        });
    }
    catch (e)
    {
        console.log("Error setting cursor position, leaving at default");
    }

    closeHyperLinkDiv();
    //remove contents of list after completion
    HyperLinkList.innerHTML = '';
}
*/

/*
// Take the selected input value and update the current HyperLink with this value
function updateHyPerLink_HoldTest()
{
    console.log(TXTextControl);

    let text = $("#currentSelection").val();
    let realLinkStart = 0;
    var waitForMe = true;

    //HyperLinkList.innerHTML = '';

    //Enable commands to update hyperlink using object
    TXTextControl.enableCommands(true);
    TXTextControl.sendCommand(55, 0, 0, 0, { linkedText: text, linkTarget: pKey });

    try
    {

        TXTextControl.inputPosition.getLine(function (e)
        {
            console.log("inputPosition.getLine:");
            console.log(e);

            TXTextControl.inputPosition.getPage(function (p)
            {
                console.log("inputPosition.getPage:");
                console.log(p);

                var pageStartIndex = 0;

                TXTextControl.pages.forEach(function (pp)
                {
                    console.log(pp);

                    pp.getNumber(function (pn)
                    {

                        if (pn < p)
                        {
                            pp.getLength(function (pl)
                            {
                                pageStartIndex += pl;
                                console.log("page length after page " + pn + ":");
                                console.log(pageStartIndex);
                            });
                        }
                        else if (pn == p)
                        {
                            console.log("now do stuff for your page");

                            TXTextControl.lines.forEach(function (pline)
                            {

                                pline.getPage(function (lpage)
                                {
                                    console.log("getting page number from pline:");
                                    console.log(lpage);

                                    if (lpage == p)
                                    {
                                        console.log("lpage equals the page we are looking for");
                                        console.log(p);

                                        pline.getNumber(function (lnum)
                                        {
                                            console.log("getting line number from pline:");
                                            console.log(lnum);

                                            if (lnum == e)
                                            {
                                                console.log("found the item: Page = " + lpage + ", Line = " + lnum);
                                                console.log(pline);
                                                let textIndex = 0;

                                                pline.getText(function (t)
                                                {
                                                    console.log(t);
                                                    textIndex = t.indexOf(text, 0);
                                                });

                                                pline.getStart(function (h)
                                                {
                                                    realLinkStart = pageStartIndex + h + textIndex + text.length;
                                                    console.log("realLinkStart:");
                                                    console.log(realLinkStart);

                                                    let bounds = { "start": realLinkStart, "length": 0 };
                                                    console.log("bounds:");
                                                    console.log(bounds);
                                                    TXTextControl.selection.setBounds(bounds);
                                                });

                                            }

                                        });

                                    }

                                });


                            });

                        }

                    });

                });

            });

        });
    }
    catch (e)
    {
        console.log("Error setting cursor position, leaving at default");
    }

    closeHyperLinkDiv();
    //remove contents of list after completion
    HyperLinkList.innerHTML = '';
}
*/

/*
// Take the selected input value and update the current HyperLink with this value
function updateHyPerLink_LastGood()
{
    console.log(TXTextControl);

    let text = $("#currentSelection").val();
    let realLinkStart = 0;

    //HyperLinkList.innerHTML = '';

    //Enable commands to update hyperlink using object
    TXTextControl.enableCommands(true);
    TXTextControl.sendCommand(55, 0, 0, 0, { linkedText: text, linkTarget: pKey });

    try
    {
        TXTextControl.inputPosition.getLine(function (e)
        {

            TXTextControl.lines.elementAt(e - 1, function (g)
            {
                let textIndex = 0;

                g.getText(function (t)
                {
                    console.log(t);
                    textIndex = t.indexOf(text, 0);
                });

                g.getStart(function (h)
                {
                    realLinkStart = h + textIndex + text.length;

                    let bounds = { "start": realLinkStart, "length": 0 };
                    TXTextControl.selection.setBounds(bounds);
                });
            })
        });
    }
    catch (e)
    {
        console.log("Error setting cursor position, leaving at default");
    }

    closeHyperLinkDiv();
    //remove contents of list after completion
    HyperLinkList.innerHTML = '';
}
*/

/*
// Take the selected input value and update the current HyperLink with this value
function updateHyPerLink_CodeAttempts()
{
    console.log(TXTextControl.hypertextLinks);
    let text = $("#currentSelection").val();
    //let text = document.getElementById("currentSelection").value;
    //let cursorPosition = text.length;
    //let cursorPosition = 0;
    let realLinkStart = 0;
    HyperLinkList.innerHTML = '';

    //console.log(text);
    //Enable commands to update hyperlink using object
    TXTextControl.enableCommands(true);

    TXTextControl.selection.getForeColor(function (t) {
        console.log("getForeColor:");
        console.log(t);
    });

    TXTextControl.selection.getTextBackColor(function (t) {
        console.log("getTextBackColor:");
        console.log(t);
    });

    TXTextControl.hypertextLinks.forEach(function (e) {
        console.log(e);

        e.getTarget(function (l) {
            console.log("getTarget:");
            console.log(l);
        });

        e.getText(function (t) {
            console.log("getText:");
            console.log(t);
        });

        e.getBounds(function (b) {
            console.log("getBounds:");
            console.log(b);
        });

        e.getHighlightColor(function (h) {
            console.log("getHighlightColor:");
            console.log(h);
        });

        e.getHighlightMode(function (h) {
            console.log("getHighlightMode:");
            console.log(h);
        });

        e.getStart(function (h) {
            console.log("getStart:");
            console.log(h);
        });

        e.getLength(function (h) {
            console.log("getLength:");
            console.log(h);
        });

        //e.setHighlightMode(0);
        e.setHighlightColor("red");

    });

    TXTextControl.hypertextLinks.forEach(function (e) {
        //console.log("setHighlightMode:");
        //e.setHighlightMode(0);
        console.log("setHighlightColor:");
        e.setHighlightColor("rgba(0,0,255,0)");
    });

    //console.log("pKey Should be after me");
    //console.log(pKey);

    TXTextControl.sendCommand(55, 0, 0, 0, { linkedText: text, linkTarget: pKey });

    TXTextControl.inputPosition.getLine(function (e) {
        //console.log("Input Position getLine: ");
        //console.log(e);

        TXTextControl.lines.elementAt(e - 1, function (g) {
            let textIndex = 0;

            g.getText(function (t) {
                //console.log("text of line: ");
                //console.log(t);
                textIndex = t.indexOf(text, 0);
                //console.log("current line index of " + text);
                //console.log(textIndex);
            });

            g.getStart(function (h) {
                //console.log("Line Text Start: ");
                //console.log(h);
                //cursorPosition = h;
                //realLinkStart = cursorPosition + text.length;
                realLinkStart = h + textIndex + text.length;

                let bounds = { "start": realLinkStart, "length": 0 };
                //console.log(bounds);
                TXTextControl.selection.setBounds(bounds);
            });
        })
    });

    TXTextControl.selection.getText(function (e)
    {
        //console.log(e);
        console.log("check last index of " + text);
        let realLinkStart = e.lastIndexOf(text, cursorPosition + 2);
        //console.log(e.lastIndexOf(text, cursorPosition));
        console.log(realLinkStart);

        realLinkStart = ((realLinkStart < 1) ? cursorPosition : realLinkStart);

        console.log("Actual start Index: ");
        console.log(realLinkStart);

        //console.log(e);
        console.log(text.length);
        let realLinkEnd = realLinkStart + text.length;
        console.log(realLinkEnd);
        //let bounds = { "start": e.lastIndexOf(" ", cursorPosition), "length": 0 };
        //let bounds = { "start": realLinkEnd, "length": 4 };
        let bounds = { "start": realLinkEnd, "length": 0 };
        console.log(bounds);
        console.log("Text at position " + realLinkStart + ":");
        console.log(e.substring(realLinkStart, realLinkEnd));
        //TXTextControl.selection.setBounds(bounds)

        //console.log(TXTextControl.selection.listFormat);
        TXTextControl.selection.setStart(realLinkEnd);

        console.log("Length of text in box: ");
        console.log(e.length);

        //TXTextControl.selection.setText("WTFF");
    });
    
    TXTextControl.getText(function (e)
    {
        //console.log("text from TXTextControl: ");
        //console.log(e);

        console.log("Length of text from TXTextControl: ");
        console.log(e.length);
    });

    TXTextControl.selection.getText(function (e)
    {
        //console.log("text from TXTextControl.selection: ");
        //console.log(e);

        console.log("Length of text from TXTextControl.selection: ");
        console.log(e.length);
    });
    
    TXTextControl.selection.getText(function (e) {
        console.log(e);
        console.log("check last index of " + text);
        let realLinkStart = e.lastIndexOf(text, cursorPosition + 2);
        //console.log(e.lastIndexOf(text, cursorPosition));
        console.log(realLinkStart);

        realLinkStart = ((realLinkStart < 1) ? cursorPosition : realLinkStart);

        console.log("Actual start Index: ");
        console.log(realLinkStart);

        console.log(e);
        console.log(text.length);
        let realLinkEnd = realLinkStart + text.length;
        console.log(realLinkEnd);
        //let bounds = { "start": e.lastIndexOf(" ", cursorPosition), "length": 0 };
        //let bounds = { "start": realLinkEnd, "length": 4 };
        let bounds = { "start": realLinkEnd, "length": 0 };
        console.log(bounds);
        console.log("Text at position " + realLinkStart + ":");
        console.log(e.substring(realLinkStart, realLinkEnd));
        TXTextControl.selection.setBounds(bounds)

        console.log("Length of text in box: ");
        console.log(e.length);
    });
    
    closeHyperLinkDiv();
    //remove contents of list after completion
    HyperLinkList.innerHTML = '';
}
*/

function ToggleEditMode(IsInEditMode)
{
    $(".toggleEdit").each(function ()
    {
        //console.log();
        $(this).prop('disabled', IsInEditMode);
    });

    if (IsInEditMode)
    {
        console.log("edit summary open, start session check timer");
        checkLoginTimeout = setTimeout(checkSessionValid, msecCheckTimeout);
    }
    else
    {
        console.log("edit summary closed, cancel session timer");
        clearTimeout(checkLoginTimeout);
    }
    
}

var checkLoginTimeout;
var msecCheckTimeout = 60000;
//var msecCheckTimeout = 6000;

function checkSessionValid()
{
    let guid = $("#hiddenGUID").val();
    //var strUrl = 'SummaryHandler.ashx?op=CheckValidSession&guid=' + guid;
    var strUrl = 'API/Sessions/CheckValidity?GUID=' + guid;

    //start ajax request
    $.ajax({
        url: strUrl,
        //force to handle it as text
        dataType: "text",
        success: function (data)
        {
            console.log("RCV FOR: " + strUrl);

            //if (data == 1)
            if (data)
            {
                // session is still valid restart the timer
                console.log("Session is still valid - restart the timer");
                checkLoginTimeout = setTimeout(checkSessionValid, msecCheckTimeout);
            }
            else
            {
                console.log("Session has expired, force autosave and show timeout");

                saveTxTextAndAutosave();
                //ShowTimeoutMessage();
                /*
                // close study and regen PDF
                console.log("Session is not valid");
                let selectedStudy = $("#hiddenSelectedStudy").val();
                let guid = $("#hiddenGUID").val();
                let username = $("#hiddenUsername").val();

                strUrl = 'SummaryHandler.ashx?op=CloseStudyAndRegen&guid=' + guid + '&studyID=' + selectedStudy + '&username=' + username + '&TrackingID=' + trackingId;

                $.ajax({
                    url: strUrl,
                    //force to handle it as text
                    dataType: "text",
                    success: function (data)
                    {
                        // perform logut
                        console.log("RCV FOR: " + strUrl);

                        ShowTimeoutMessage();
                    }
                });
                */

            }

        }
    });

}

/*
function setupAutosave()
{
    //console.log("fn: setupAutosave()");

    TXTextControl.addEventListener("textControlChanged", function ()
    {
        //console.log("textControlChanged");
        //console.log("[fn: doAutosaveIfDirty()] - bTXTextDirty set to true");
        bTXTextDirty = true;
    });

    teardownAutosave();
    timeoutAutosave = setTimeout(doAutosaveIfDirty, msecCheckTimeout);
    bAutosaveToreDown = false;
}
*/

TXTextControl.addEventListener("ribbonTabsLoaded", function (e)
{
    console.log("ribbon Tabs Loaded");
    TXTextControl.showRibbonBar(false);
    TXTextControl.showHorizontalRuler(false);
    TXTextControl.showVerticalRuler(false);
    reformatSummaryEdit();
    //restoreWindowRpt();
});