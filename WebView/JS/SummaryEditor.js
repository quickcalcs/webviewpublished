﻿// Hide TxText Editor
/*
TXTextControl.onload = function () {
    setTimeout('Decrement()', 60); 

    function Decrement() {
        document.getElementById('txBtnRibbonHide').click();
    }
    
};
*/

//document.getElementsByClassName('tabs').style.display = "none";
//document.getElementById('txBtnRibbonHide').click();

// Simulate Bold Btn click
function btnBold() {
    document.getElementById('ribbonTabHome_btnBold').click();
}
// Simulate Italic Btn click
function btnItalic() {
    document.getElementById('ribbonTabHome_btnItalic').click();
}
// Simulate Underline Btn click
function btnUnderline() {
    document.getElementById('ribbonTabHome_drpDnBtnUnderlineStyles').click();
    document.getElementById('ribbonTabHome_mnuItemUnderlineStyle_Single').click();

}
// Simulate align left Btn click
function btnAlignLeft()
{
    document.getElementById('ribbonTabHome_tglBtnAlignLeft').click();
}

// Simulate align middle Btn click
function btnAlignMiddle()
{
    document.getElementById('ribbonTabHome_tglBtnAlignCenter').click();
}
// Simulate align right Btn click
function btnAlignright() {
    document.getElementById('ribbonTabHome_tglBtnAlignRight').click();
}

// Simulate bulletpoint Btn click
function btnBulletPoint()
{
    document.getElementById('ribbonTabHome_drpDnBtnBulletList').click();
    document.getElementById('ribbonTabHome_mnuItemBulletList_Circle').click();
}

// Simulate cut Btn click
function btnCut()
{
    document.getElementById('ribbonTabHome_btnCut').click();
}

// Simulate copy Btn click
function btnCopy()
{
    document.getElementById('ribbonTabHome_btnCopy').click();
}
// Simulate paste Btn click
function btnPaste() {
    document.getElementById('ribbonTabHome_btnServerPaste').click();
    document.getElementById('ribbonTabHome_mnuItemServerPaste_FormattedText').click();
}

// Simulate undo Btn click
function btnUndo()
{
    document.getElementById('ribbonTabHome_btnUndo').click();
}

// Simulate clear Btn click
function btnClear()
{
    TXTextControl.resetContents();
}

// Simulate spell check Btn click
function btnSpellCheck()
{
    TXTextControl.enableCommands();
    TXTextControl.sendCommand(TXTextControl.Command.EnableSpellChecking, 1);
}

// Simulate Link Btn click
function btnLink()
{
    $("#ribbonTabInsert_mnuItemInsertHyperlink").click()
}

// Simulate spell check Btn click
function btnParagraph()
{
    console.log("fn: btnParagraph() not yet implemented");
}

$("#summaryFontFamily").change(function ()
{
    var end = this.value;

    //console.log(end);
    switch (end)
    {
        case 'Times New Roman':
            TXTextControl.selection.setFontName("Times New Roman");
            break;
        case 'Verdana':
            TXTextControl.selection.setFontName("Verdana");
            break;
        case 'Trebuchet MS':
            TXTextControl.selection.setFontName("Trebuchet MS");
            break;
        default:
            console.log('Sorry Font Not Available');
    }

});

$("#summaryFontSize").change(function () {
    var end = this.value;
    //console.log(end);
    switch (end) {
        case '10':
            TXTextControl.selection.setFontSize(10)
            break;
        case '11':
            TXTextControl.selection.setFontSize(11)
            break;
        case '12':
            TXTextControl.selection.setFontSize(12)
            break;
        case '13':
            TXTextControl.selection.setFontSize(13)
            break;
        case '14':
            TXTextControl.selection.setFontSize(14)
            break;
        case '15':
            TXTextControl.selection.setFontSize(15)
            break;
        case '16':
            TXTextControl.selection.setFontSize(16)
            break;
        case '17':
            TXTextControl.selection.setFontSize(17)
            break;
        case '18':
            TXTextControl.selection.setFontSize(18)
            break;
        case '19':
            TXTextControl.selection.setFontSize(19)
            break;
        case '20':
            TXTextControl.selection.setFontSize(20)
            break;
        case '21':
            TXTextControl.selection.setFontSize(21)
            break;
        case '22':
            TXTextControl.selection.setFontSize(22)
            break;
        case '23':
            TXTextControl.selection.setFontSize(23)
            break;
        case '24':
            TXTextControl.selection.setFontSize(24)
            break;
        case '25':
            TXTextControl.selection.setFontSize(25)
            break;
        case '26':
            TXTextControl.selection.setFontSize(26)
            break;
        case '27':
            TXTextControl.selection.setFontSize(27)
            break;
        case '28':
            TXTextControl.selection.setFontSize(28)
            break;
        case '36':
            TXTextControl.selection.setFontSize(36)
            break;
        case '48':
            TXTextControl.selection.setFontSize(48)
            break;
        case '72':
            TXTextControl.selection.setFontSize(72)
            break;
        default:
            console.log('Sorry Font Not Available');
    }
});