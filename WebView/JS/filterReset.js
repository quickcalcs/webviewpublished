﻿
//const filterOfff = () =>
const filterOfff = function()
{
    // Set all hidden filter term and col to blank string, counter back to 0, and turn the display of the filter tool tip off
    $("#hiddenFilterTerm").val('');
    $("#hiddenFilterCol").val('');
    $("#hiddenFilterTerm2").val('');
    $("#hiddenFilterCol2").val('');
    $("#hiddenFilterTerm3").val('');
    $("#hiddenFilterCol3").val('');
    $("#hiddenFilterTerm4").val('');
    $("#hiddenFilterCol4").val('');
    $("#hiddenFilterTerm5").val('');
    $("#hiddenFilterCol5").val('');
    $("#hiddenFilterTerm6").val('');
    $("#hiddenFilterCol6").val('');
    $("#hiddenFilterTerm7").val('');
    $("#hiddenFilterCol7").val('');
    $("#hiddenFilterTerm8").val('');
    $("#hiddenFilterCol8").val('');
    $("#hiddenFilterTerm9").val('');
    $("#hiddenFilterCol9").val('');
    $("#hiddenFilterTerm10").val('');
    $("#hiddenFilterCol10").val('');
    $('#hiddenRSStartPosition').val(0);
    $(".filter-alert").css('display', 'none');
    $("#filter-applied").css('visibility', 'hidden');
    $(".gvTable tr").addClass('gvSelectedRowHover1');
    $(".gvTable tr").removeClass('gvSelectedRowHover2');
    filterMode = false;
    counter = 0;
    fnFillGrid();
}

$(document).ready(function () 
{
    $("#report-summary").draggable();
});

function draggable() 
{
    $("#report-summary").draggable();
}