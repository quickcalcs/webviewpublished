﻿function MouseoverImage(img, path) {
    if (debug) { $("#Info").html(img + " " + path); }
    $("#" + img).attr("src", path);
}

function setTabImages() {
    $("#imgRecall").attr("src", "images/b_list_f3.gif");
    $("#imgRecall").attr("onmouseout", "MouseoverImage('imgRecall', 'images/b_list_f3.gif')");

    $("#imgReport").attr("src", "images/b_report_f3.gif");
    $("#imgReport").attr("onmouseout", "MouseoverImage('imgReport', 'images/b_report_f3.gif')");

    $("#imgImages").attr("src", "images/b_images_f3.gif");
    $("#imgImages").attr("onmouseout", "MouseoverImage('imgImages', 'images/b_images_f3.gif')");

    // 4.3 Add Summary
    $("#imgSummary").attr("src", "images/b_summary_f3.gif");
    $("#imgSummary").attr("onmouseout", "MouseoverImage('imgSummary', 'images/b_summary_f3.gif')");

    $("#imgProfile").attr("src", "images/b_profile_f3.gif");
    $("#imgProfile").attr("onmouseout", "MouseoverImage('imgProfile', 'images/b_profile_f3.gif')");

    $("#navAdmin").attr("src", "images/b_admin_f3.gif");
    $("#navAdmin").attr("onmouseover", "MouseoverImage('navAdmin', 'images/b_admin_f2.gif')");
    $("#navAdmin").attr("onmouseout", "MouseoverImage('navAdmin', 'images/b_admin_f3.gif')");
}

function setNavTabs(currentForm) {

    //var currentForm = document.getElementById("hiddenCurrentForm").value();
    //alert(currentForm);

	setTabImages();
    switch (currentForm) {

        case "Recall":
            //alert("Recall tab");
            $("#imgRecall").attr("src", "images/b_list.gif");
            $("#imgRecall").attr("onmouseout", "MouseoverImage('imgRecall', 'images/b_list.gif')");
            break;

        case "Report":
            //alert("Report tab");
            $("#imgReport").attr("src", "images/b_report.gif");
            $("#imgReport").attr("onmouseout", "MouseoverImage('imgReport', 'images/b_report.gif')");

            break;

        case "Images":
            //alert("Images tab");
            $("#imgImages").attr("src", "images/b_images.gif");
            $("#imgImages").attr("onmouseout", "MouseoverImage('imgImages', 'images/b_images.gif')");

            break;

        case "Profile":
            //alert("profile tab");
            $("#imgProfile").attr("src", "images/b_profile.gif");
            $("#imgProfile").attr("onmouseout", "MouseoverImage('imgProfile', 'images/b_profile.gif')");

            break;

        case "Admin":
            //alert("admin tab");
            $("#navAdmin").attr("src", "images/b_admin.gif");
            $("#navAdmin").attr("onmouseout", "MouseoverImage('navAdmin', 'images/b_admin.gif')");

            break;
    }
}

function setNavTabs2(currentFormPageTitle) {
	setTabImages();
    switch (currentFormPageTitle) {

        case "Patient List":		// "Recall":
            //alert("Recall tab");
            $("#imgRecall").attr("src", "images/b_list.gif");
            $("#imgRecall").attr("onmouseout", "MouseoverImage('imgRecall', 'images/b_list.gif')");
            break;

        case "Report":
            //alert("Report tab");
            $("#imgReport").attr("src", "images/b_report.gif");
            $("#imgReport").attr("onmouseout", "MouseoverImage('imgReport', 'images/b_report.gif')");

            break;

        case "Images":
            //alert("Images tab");
            $("#imgImages").attr("src", "images/b_images.gif");
            $("#imgImages").attr("onmouseout", "MouseoverImage('imgImages', 'images/b_images.gif')");

            break;

        case "Summary":
            // 4.3 Add Summary
            $("#imgSummary").attr("src", "images/b_summary_f3.gif");
            $("#imgSummary").attr("onmouseout", "MouseoverImage('imgSummary', 'images/b_summary_f3.gif')");

            break;

        case "Profile":
            //alert("profile tab");
            $("#imgProfile").attr("src", "images/b_profile.gif");
            $("#imgProfile").attr("onmouseout", "MouseoverImage('imgProfile', 'images/b_profile.gif')");

            break;

        case "Admin":
            //alert("admin tab");
            $("#navAdmin").attr("src", "images/b_admin.gif");
            $("#navAdmin").attr("onmouseout", "MouseoverImage('navAdmin', 'images/b_admin.gif')");

            break;
    }
}
