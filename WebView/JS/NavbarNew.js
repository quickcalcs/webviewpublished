﻿// MMC - WV 4.3 Refactoring



/*
Pulls the needed values from the hidden fields on site.master
Bitmask bit positions are:
1	- GUID
2 - StudyID
*/
function GenerateNavbarQueryString(bitmaskFields)
{
    //               1            2               4               8                16
    var arrFields = ["guid", "selectedstudy", "selectedRow", "scrollPosition", "gridpage"];
    var arrHiddenFieldIDs = ["hiddenGUID", "hiddenSelectedStudy", "hiddenSelectedRow", "hiddenScrollPosition", "hiddenCurrentPage"];

    //alert(bitmaskFields);

    if (bitmaskFields == 0)
    {
        return "";
    }
    
    var strQueryString = "";
    var strKey, strVal, bitmaskTest;

    for (i = 0; i < arrFields.length; i++)
    {
        bitmaskTest = Math.pow(2, i);

        if ((bitmaskFields & bitmaskTest) != 0) 
        {
            strKey = arrFields[i];
            // alert(strKey);

            try 
            {
                //alert(arrHiddenFieldIDs[i]);
                strVal = document.getElementById(arrHiddenFieldIDs[i]).value;
                //alert(strVal);
            }
            catch (err) 
            {
                console.log(err)
            }

            if (i > 0)
            {
                strQueryString = strQueryString + '&';
            }
            
            strQueryString = strQueryString + strKey + '=' + encodeURIComponent(strVal);
        }

    }

    return strQueryString;
}

function filterClause()
{
    var strVal = "";

    strVal = strVal + "&filterCol=" + document.getElementById("hiddenFilterCol").value;
    strVal = strVal + "&filterTerm=" + document.getElementById("hiddenFilterTerm").value;

    //for (i = 2; i < 10; i++)
    for (i = 2; i <= 10; i++)
    {
        try {
            
            strVal = strVal + "&filterCol" + i +"=" + document.getElementById("hiddenFilterCol" + i).value;
            strVal = strVal + "&filterTerm" + i + "=" + document.getElementById("hiddenFilterTerm" + i).value;
          
        }

        catch (err) {
            console.log(err)
        }
    }

    var SortColumn = "";

    if (document.getElementById("hiddenSortColumn").value == "")
    {
        SortColumn = "[Name]";
    }
    else
    {
        SortColumn = document.getElementById("hiddenSortColumn").value;
    }

    strVal = strVal + "&SortColumn=" + document.getElementById("hiddenSortColumn").value;
    strVal = strVal + "&SortDirection=" + document.getElementById("hiddenSortDirection").value;
    strVal = strVal + "&SearchTerm=" + document.getElementById("hiddenSearchTerm").value;

    return strVal;
}
function NavigateToPartialURL(strPartialURL) 
{
    $(location).attr('href', strPartialURL + '?' + GenerateNavbarQueryString(3))
}

function isCurrPagePatientList() 
{
    var re = /patientlist.aspx/gi;
    var appname = location.pathname.toString();

    if (appname.match(re))
    {
        return true;
    }
    
    return false;
}

function generateFullFormUrl(strPartialURL) 
{
    var re = /\/[^\/]*.aspx/gi;
    var appname = location.pathname.toString();
    appname = appname.replace(re, "\/" + strPartialURL)
    return location.protocol + "//" + location.hostname + ":" + location.port + appname;
}

function FormPostPartialURL(strPartialURL) 
{
    var nBMFieldsInQS = 15;

    if (isCurrPagePatientList()) 
    {
        //alert("Navigating away from patientlist.aspx");
        console.log("Navigating away from patientlist.aspx");
        nBMFieldsInQS += 16;
    }

    //$('#formInfoBarHidden').attr('action', generateFullFormUrl(strPartialURL)).submit();
    //var url = generateFullFormUrl(strPartialURL) + '?' + GenerateNavbarQueryString(nBMFieldsInQS);
    var url = strPartialURL + '?' + GenerateNavbarQueryString(nBMFieldsInQS);

    //4.3 DWC Add filter settings to URL

    url = url + filterClause();

    //alert(url);
    console.log(url);
    $('#formInfoBarHidden').attr('action', url);
    $('#formInfoBarHidden').submit();
}

function GoToDiv2(strTabName)
{

    switch (strTabName) 
    {
        case "Recall":
            NavigateToPartialURL('patientlist.aspx');
            break
        case "Report":
            NavigateToPartialURL('report.aspx');
            break
        case "Images":
            var isPortrait = false;
            // MMC - 4.3 - TODO:  Need to reconcile old behavior
            // we used to set overflow-y to "" just before loading new images into #mainpanel
            // then set overflow-y to hidden in the Images.aspx <script> block
            // $("#mainpanel").css("overflow-y", "");
            $("#divPatientList").html("<br/><br/><br/><br/><br/><center><p><h1>Opening Study...</h1></p></center>");
            NavigateToPartialURL('images.aspx');
            break
            // 4.3 Add Summary
        case "Summary":
            MouseoverImage("imgSummary", "images/b_summary.gif");
            NavigateToPartialURL('summary.aspx');
            // TODO - If there is a summary.aspx, need to add OnLoad() to it on startup
            // $("#mainpanel").load("summary.aspx?guid=" + guid + "&selectedstudy=" + selectedstudy, function () { OnLoad(); });
            break
        case "Profile":
            NavigateToPartialURL('profile.aspx');
            break
        case "Admin":
            NavigateToPartialURL('admin.aspx');
            break
        default:
            alert('GoToDiv2: Unrecognized tab: ' + strTabName);
            break;
    }

}

function GoToDiv3(strTabName) 
{
    console.log("GoToDiv3 " + strTabName);

    if ($("#tbl1").length)
    {
        var scrollableDiv = $("#tbl1").closest("div");
        $("#hiddenScrollPosition").val(scrollableDiv.scrollTop());
    }

    console.log($("#hiddenScrollPosition").val());

    switch (strTabName) 
    {
        case "Recall":
            FormPostPartialURL('patientlist.aspx');
            break
        case "Report":
            FormPostPartialURL('report.aspx');
            break
        case "Images":
            var isPortrait = false;
            // MMC - 4.3 - TODO:  Need to reconcile old behavior
            // we used to set overflow-y to "" just before loading new images into #mainpanel
            // then set overflow-y to hidden in the Images.aspx <script> block
            // $("#mainpanel").css("overflow-y", "");
            $("#divPatientList").html("<br/><br/><br/><br/><br/><center><p><h1>Opening Study...</h1></p></center>");
            FormPostPartialURL('images.aspx');
            break
        case "Profile":
            FormPostPartialURL('profile.aspx');
            break
        case "Admin":
            FormPostPartialURL('admin.aspx');
            break
        default:
            alert('GoToDiv3: Unrecognized tab: ' + strTabName);
            break;
    }

}

function showNavigation2(bShow) 
{

    if (bShow) 
    {
        $("#Nav").css('visibility', 'visible');
        $("#imgLogin").css('visibility', 'visible');
        $("#imgRecall").show();
		$("#imgReport").show();
		$("#imgImages").show();

        if ($("#hiddenUserLevel").val() == '1') 
        {
			$("#navAdmin").show();
        }

	}
    else 
    {
	    $("#Nav").css('visibility', 'hidden');
	    $("#imgLogin").css('visibility', 'hidden');
	    $("#imgRecall").hide();
		$("#imgReport").hide();
		$("#imgImages").hide();
		$("#navAdmin").hide();
    }

}

function updateInfoBarHidden(strName, strPatientID, strDate) 
{
    $("#hiddenIB_Name").val(strName);
    $("#hiddenIB_PatientID").val(strPatientID);
    $("#hiddenIB_StudyDate").val(strDate);
}

function updateInfoBar(strName, strPatientID, strDate) 
{
    //$("#Info").attr("style", "background-color: blue; color: white; position: absolute; top: 85px; left: 50px");
    $("#InfoBar").html("<b> Name:   " + strName + "&nbsp;&nbsp;&nbsp;&nbsp; ID:   " + strPatientID + "&nbsp;&nbsp;&nbsp;&nbsp;Date:   " + strDate + "</b>").trigger("contentchange");
    //$("#Info").bigtext();
}

function hideInfoBar() 
{
    //alert("hideInfoBar() ");
    $("#InfoBar").html("").trigger("contentchange");
}