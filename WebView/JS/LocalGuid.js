﻿
var mainLoopTimer;

function Logout()
{
    //alert("LocalGUID Logout");
    console.log("Logout()");

    //AJAX call to release open by
    //if (document.getElementById("hiddenSelectedStudy").value !== '')
    if ($("#hiddenSelectedStudy").val())
    {
        let selectedStudy = $("#hiddenSelectedStudy").val();
        //let selectedStudy = document.getElementById("hiddenSelectedStudy").value;
        let guid = $("#hiddenGUID").val();
        //let guid = document.getElementById("hiddenGUID").value;
        let username = $("#hiddenUsername").val();
        //alert(username);
        console.log(username);
        //let username = document.getElementById("hiddenUsername").value;

        var trackingId;

        //if (document.getElementById("hiddenTrackingID"))
        if ($("#hiddenTrackingID").length) 
        {
            trackingId = $("#hiddenTrackingID").val();
            //trackingId = document.getElementById("hiddenTrackingID").value;
        }
        else 
        {
            trackingId = 0;
        }

        //alert('SummaryHandler.ashx?op=CloseStudy&guid=' + guid + '&studyID=' + selectedStudy + '&username=' + username + '&TrackingID=' + trackingId);
        console.log('SummaryHandler.ashx?op=CloseStudy&guid=' + guid + '&studyID=' + selectedStudy + '&username=' + username + '&TrackingID=' + trackingId);
        
        $.ajax({
            url: 'SummaryHandler.ashx?op=CloseStudy&guid=' + guid + '&studyID=' + selectedStudy + '&username=' + username + '&TrackingID=' + trackingId
        })
    }
    else
    {
        // For dev purposes
        console.log("No guid or selected study so no AJAX call");
    }

    console.log("moving on");
    //alert("To pause execution");
    //End AJAX call to close study
    var re = /\/[^\/]*.aspx/gi;
    var appname = location.pathname.toString();
    appname = appname.replace(re, "\/Login.aspx")

    console.log("building url");
    var strURL = location.protocol + "//" + location.hostname + ":" + location.port + appname;    //  location.pathname;
    var strQueryString = '';

    if ($('#hiddenGUID') && $('#hiddenUsername'))
    {
        strQueryString = '?op=Logout&GUID=' + $('#hiddenGUID').val() + '&Username=' + $('#hiddenUsername').val();
    }
    
    // alert('Logout: ' + strURL + strQueryString);  // MMC
    console.log(strURL + strQueryString);
    window.location = strURL + strQueryString;
}

function StartMainLoop() 
{

    if (!mainLoopTimer)
    {
        console.log("creating GUID Check interval");
        mainLoopTimer = setInterval(function () { GetLocalGuid($('#hiddenGUID').val()) }, 1000);
        //var mainLoopTimer = setInterval(function () { GetLocalGuid($('#hiddenGUID').val()) }, 1000);
    }
    else
    {
        console.log(" GUID Check interval already exists");
    }

}

function CancelMainLoop()
{
    
    if (mainLoopTimer)
    {
        clearInterval(mainLoopTimer);
    }

}

function SetLocalGuid(guid) 
{
    console.log("SetLocalGuid(guid)");
    localStorage.setItem("currentGuid", guid);
    var localGuid = localStorage.getItem("currentGuid");

    //alert("SetLocalGuid(): " + localGuid);

    StartMainLoop();
}

function GetLocalGuid(guid) 
{
    //alert("Before checking sessionStorage");
    //alert(sessionStorage.currentGuid);
    //return sessionStorage.currentGuid;
    //alert("Guid Passed: " + guid);
    var localGuid = localStorage.getItem("currentGuid");

    if (localGuid) 
    {
        //alert("localGuid:" + localGuid);

        if (localGuid != guid) 
        {
            console.log("stop guid check timer");
            CancelMainLoop();
            //alert("Close this session");
            //window.open(",'_parent',");

            Logout();
            window.close();
            //$("input").focus();
            //var e = jQuery.Event('keypress');
            //e.which = 23;
            //$("input").val(String.fromCharCode(e.which));
            //$("input").trigger(e);
        }

        //alert("GetLocalGuid(): " + localGuid);
    }

}