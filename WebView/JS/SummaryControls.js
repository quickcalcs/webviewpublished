﻿function Bold() {
    document.execCommand('bold');
}

function Italic() {
    document.execCommand('italic');
}

function Underline() {
    document.execCommand('underline');
}

function Undo() {
    document.execCommand('undo');
}

function Redo() {
    document.execCommand('redo');
}

function JustifyFull() {
    document.execCommand('justifyFull');
}

function JustifyCenter() {
    document.execCommand('justifyCenter');
}

function JustifyLeft() {
    document.execCommand('justifyLeft');
}

function JustifyRight() {
    document.execCommand('justifyRight');
}

function Cut() {
    document.execCommand('cut');
}

function Copy() {
    document.execCommand('copy');
}

function Paste() {
    document.execCommand('paste');
}

function FontFamily(value) {
    document.execCommand('fontName', false, value);
}

function FontSize(value) {
    document.execCommand('fontSize', false, value);
}

function Bullet() {
    document.execCommand('insertUnorderedList');
}

function Clear() {
    document.execCommand('selectAll');
    document.execCommand('delete');
}

function Link(value) {
    document.execCommand('createLink', false, value);
}

function UnLink() {
    document.execCommand('unlink');
}