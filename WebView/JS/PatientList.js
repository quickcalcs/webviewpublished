﻿var g_strTablePatientList_Heading = '';
var g_strTablePatientList_Grid = '';
var g_bReceivedRecordCount = false;
var g_strTablePatientList_WrittenOnce = false;

// 4.1 DWC Global variable to prevent multipe headers
var headerCount = 0;

// 4.3 Make global for retaining state of worklist
var StudyIdList

// 4.3 - MMC 
// Test the use of a global JS data structure
var globalhidden = {
    guid: "",
    studyid: "",
    OpenStudyTo: "",
    convertSvcUrl: "/converthandler.ashx"
};

var LookingForSelectedStudy = false;

$("#mainpanel").css("overflow-y", "hidden");

/* Methods and variables for scrolling below */
var scrollLoadActive = false;
var allRecordsAlreadyLoaded = false;
var userInitiatedScroll = false;

function scrollToSelectedStudy()
{
    console.log("userInitiatedScroll = " + userInitiatedScroll);

    if (!userInitiatedScroll)
    {
        console.log("no user interaction here, scroll to study");
        //var viewTable = document.getElementById("tbl1");
        //var scrollableDiv = $(viewTable).closest("div");
        var scrollableDiv = $("#tbl1").closest("div");
        //console.log("[fn: scrollToSelectedStudy] hiddenScrollPosition value = " + document.getElementById("hiddenScrollPosition").value);
        //var newScrollPosition = document.getElementById("hiddenScrollPosition").value;
        var newScrollPosition = $("#hiddenScrollPosition").val();

        console.log("newScrollPosition");
        console.log(newScrollPosition);

        scrollableDiv.scrollTop(newScrollPosition);

        if (scrollableDiv.scrollTop() != newScrollPosition)
        {
            console.log("couldn't scroll to previous position, try another load");
            loadAdditionalRecords();
        }
        else
        {
            console.log("now add in scroll event");
            $(".table-wrapper").scroll(scrollHandler2);
        }

    }
    else
    {
        console.log("user initiated scroll, Do Not move to highlighted record");
    }

    userInitiatedScroll = false;
}

function loadAdditionalRecords()
{
    console.log("[fn: loadAdditionalRecords()] scrollLoadActive = " + scrollLoadActive);

    if (!scrollLoadActive)
    {
        //load more records
        scrollLoadActive = true;

        if (!allRecordsAlreadyLoaded)
        {
            //var viewTable = document.getElementById("tbl1");
            //var jqViewTable = $(viewTable);
            var jqViewTable = $("#tbl1");
            var existingRecordCount = jqViewTable.find("tr").last().index() + 1;
            var nMaxRecords = parseInt($('#hiddenMaxRecords').val(), 10);
            //console.log("$('#hiddenMaxRecords').val()=" + nMaxRecords);
            //var totalRecords = parseInt(document.getElementById("hiddenTotalRecords").value, 10);
            var totalRecords = parseInt($("#hiddenTotalRecords").val(), 10);

            //console.log("existingRecordCount = " + existingRecordCount);
            //console.log("nMaxRecords = " + nMaxRecords);
            //console.log("totalRecords = " + totalRecords);

            if (existingRecordCount >= totalRecords)
            {
                //console.log("All records already loaded");
                scrollLoadActive = false;
                allRecordsAlreadyLoaded = true;
            }
            else
            {

                if ((existingRecordCount + nMaxRecords) >= totalRecords)
                {
                    //console.log("Last batch about to be loaded");
                    allRecordsAlreadyLoaded = true;
                }

                //console.log("loading more records");

                var nHiddenRSStartPosition = parseInt($('#hiddenRSStartPosition').val(), 10);

                //console.log("nMaxRecords = " + nMaxRecords);
                console.log("nHiddenRSStartPosition = " + nHiddenRSStartPosition);

                nHiddenRSStartPosition += ((nHiddenRSStartPosition == 0) ? 1 + nMaxRecords : nMaxRecords);
                //nHiddenRSStartPosition += nMaxRecords;

                console.log("nHiddenRSStartPosition = " + nHiddenRSStartPosition);

                if (nHiddenRSStartPosition < totalRecords)
                {
                    $('#hiddenRSStartPosition').val(nHiddenRSStartPosition);
                }

                var strURL = hiddenToQueryString();
                var strURL_Records = strURL + '&count=none';
                //console.log(strURL_Records);
                GetResponse(strURL_Records, completeRecordLoad);
            }

        }
        else
        {
            //console.log("All Records Already Loaded");
            scrollLoadActive = false;
        }

    }

}

function completeRecordLoad(XML)
{
    console.log("[fn: completeRecordLoad(XML)]");
    //console.log("[fn: completeRecordLoad] - XML = " + XML);
    console.log(XML);

    var StudyList = XML.getElementsByTagName("Study");
    StudyIdList = XML.getElementsByTagName("StudyID");

    // MMC - Fix bug with NULL recordsets
    if (StudyList.length == 0) 
    {
        var TimeoutItems = XML.getElementsByTagName("Timeout");

        if (TimeoutItems.length > 0)
        {
            //timeout is reached - force login again
            ShowTimeoutMessage();
            //$("#dialog").dialog();
        }
        else
        {
            // continue normally
            // set flag for all records loaded
            allRecordsAlreadyLoaded = true;
        }

    }
    else 
    {
        //4.2 Add OB Support SF # 9341
        var NumOfColumns = StudyList[0].childNodes.length;
        var ElementList = [];
        var NodeName;

        for (var CurrentColumn = 1; CurrentColumn < NumOfColumns; CurrentColumn++) 
        {
            NodeName = StudyList[0].childNodes[CurrentColumn].nodeName;
            fnDebug(NodeName);
            ElementList[CurrentColumn] = XML.getElementsByTagName(NodeName);
        }

        //var PageShowHide = 'style=display:none';
        var PageShowHide = '';
        var index = 0;

        //4.2 OB Support and column config
        var CurrentElement = [];

        if (StudyIdList.length > 0) 
        {
            //var viewTable = document.getElementById("tbl1");
            //var jqViewTable = $(viewTable);
            var jqViewTable = $("#tbl1");
            //console.log("[fn: completeRecordLoad] - Table Row Count = " + jqViewTable.find("tr").last().index())

            for (var i = index; i < StudyIdList.length; i++) 
            {
                var strPatientListGrid = '';

                for (var CurrentColumn = 1; CurrentColumn < NumOfColumns; CurrentColumn++) 
                {
                    CurrentElement = ElementList[CurrentColumn];

                    if (GetInnerText(CurrentElement[i]) == null) 
                    {
                        CurrentElement[i].innerText = " ";
                    }

                }

                CurrentElement = ElementList[1];   //4.2 temporary this is actually PatientNameList

                //4.3 DWC Change row id to Study id for retaining selected row state when filtered or navigating away from patient list
                //console.log(' <tr class="gvSelectedRow gvSelectedRowHover1"' + ' ' + 'id=trSelect' + GetInnerText(StudyIdList[i]) + ' ' + PageShowHide + ' onclick=SelectStudy(' + GetInnerText(StudyIdList[i]) + ',' + GetInnerText(StudyIdList[i]) + ') ondblclick=OpenStudy(' + GetInnerText(StudyIdList[i]) + ',' + GetInnerText(StudyIdList[i]) + ') >');
                strPatientListGrid = strPatientListGrid + ' <tr class="gvSelectedRow gvSelectedRowHover1"' + ' ' + 'id=trSelect' + GetInnerText(StudyIdList[i]) + ' ' + PageShowHide + ' onclick=SelectStudy(' + GetInnerText(StudyIdList[i]) + ',' + GetInnerText(StudyIdList[i]) + ') ondblclick=OpenStudy(' + GetInnerText(StudyIdList[i]) + ',' + GetInnerText(StudyIdList[i]) + ') >';

                strPatientListGrid = strPatientListGrid + '   <td style=width:15% class=leftgvItem>';
                strPatientListGrid = strPatientListGrid + ' <input class="not-visible" type=hidden value=' + GetInnerText(StudyIdList[i]) + ' name="chkSelect"/>' //4.2 Configurable column list

                //4.2 Hidden column only on first column
                strPatientListGrid = strPatientListGrid + GetInnerText(CurrentElement[i]);   //4.2 temporary this is actually PatientNameList
                strPatientListGrid = strPatientListGrid + '   </td>';

                //4.2 Start enumerating column list here
                for (var CurrentColumn = 2; CurrentColumn < NumOfColumns; CurrentColumn++) 
                {
                    CurrentElement = ElementList[CurrentColumn];
                    strPatientListGrid = strPatientListGrid + '   <td style=width:8% class=gvItem >';
                    strPatientListGrid = strPatientListGrid + GetInnerText(CurrentElement[i]);
                    strPatientListGrid = strPatientListGrid + '   </td>';
                }

                //4.2 OB Support
                // strPatientListGrid = GetPatientListResponse_AddOBCols(strPatientListGrid);
                strPatientListGrid = strPatientListGrid + '   </tr>';

                jqViewTable.find("tr").last().after(strPatientListGrid);
            }

        }
        else 
        {
            // set flag for all records loaded
            allRecordsAlreadyLoaded = true;
        }

    }

    //console.log("scroll load active again");
    scrollLoadActive = false;

    var selectedRowIndex = $("#hiddenSelectedRow").val();

    if (selectedRowIndex != "")
    {
        console.log("selected row found");
        //var viewTable = document.getElementById("tbl1");
        //var jqViewTable = $(viewTable);
        var jqViewTable = $("#tbl1");

        //console.log(jqViewTable.find("input[value='" + selectedRowIndex + "']").length);
        //var loopCount = 0;

        if ((jqViewTable.find("input[value='" + selectedRowIndex + "']").length == 0) && (!allRecordsAlreadyLoaded))
        {
            loadAdditionalRecords();
        }
        else
        {
            //scrollLoadActive = false;
            //var scrollableDiv = $(viewTable).closest("div");
            var scrollableDiv = jqViewTable.closest("div");
            var jqScrollableDiv = $(scrollableDiv);

            console.log("jqViewTable.height:");
            console.log(jqViewTable.height());
            console.log("jqScrollableDiv.height:");
            console.log(jqScrollableDiv.height());

            if ((jqScrollableDiv.height() > jqViewTable.height()) && (!allRecordsAlreadyLoaded))
            {
                console.log("[fn: completeRecordLod] - let's load more records");
                loadAdditionalRecords();
            }
            else
            {
                var holdRowId = $("#hiddenSelectedRow").val();

                if ($("#trSelect" + holdRowId).attr("class") != "selected")
                {
                    HighlightSelectedRow(holdRowId);
                }

                console.log("let us scroll");
                scrollToSelectedStudy();
                /*
                console.log("Let's take a scroll");
                console.log($("#hiddenSelectedRow").val());

                HighlightSelectedRow($("#hiddenSelectedRow").val());
                scrollToSelectedStudy();
                */
            }

        }

    }
    else
    {
        //scrollLoadActive = false;
        //var scrollableDiv = $(viewTable).closest("div");
        //var jqScrollableDiv = $(scrollableDiv);
        var jqViewTable = $("#tbl1");
        var jqScrollableDiv = jqViewTable.closest("div");

        console.log("jqViewTable.height:");
        console.log(jqViewTable.height());
        console.log("jqScrollableDiv.height:");
        console.log(jqScrollableDiv.height());

        if ((jqScrollableDiv.height() > jqViewTable.height()) && (!allRecordsAlreadyLoaded))
        {
            //console.log("[fn: completeRecordLod] - let's load more records");
            loadAdditionalRecords();
        }
        else
        {
            $(".table-wrapper").scroll(scrollHandler2);
        }

    }

}

function scrollHandler(e)
{
    console.log("scroll handler fired!");

    console.log(e);

    var jQe = $(e);
    //console.log("scroll!");
    //console.log("scrollTop = " + jQe.scrollTop());
    //console.log("height = " + jQe.height());
    //console.log("scrollTop + height = " + (jQe.scrollTop() + jQe.height()));
    //console.log("scrollHeight = " + jQe[0].scrollHeight);

    if ((jQe.scrollTop() + jQe.height()) >= (.9 * jQe[0].scrollHeight))
    {
        //console.log("load more records");
        userInitiatedScroll = true;
        loadAdditionalRecords();
    }

    /*
    console.log("[fn: scrollHandler] old hiddenScrollPosition value = " + document.getElementById("hiddenScrollPosition").value);
    $(document.getElementById("hiddenScrollPosition")).val(jQe.scrollTop());
    console.log("[fn: scrollHandler] new hiddenScrollPosition value = " + document.getElementById("hiddenScrollPosition").value);
    */
}

function scrollHandler2(e)
{
    console.log("scroll handler 2 fired!");

    console.log(e);

    var jQe = $(this);

    if ((jQe.scrollTop() + jQe.height()) >= (.9 * jQe[0].scrollHeight))
    {
        userInitiatedScroll = true;
        loadAdditionalRecords();
    }

}

/* Methods and variables for scrolling above */

/*
$(document).on("scroll", ".table-wrapper", function ()
{
    //console.log("div scroll occurred");
    console.log("scroll!");
    console.log($(this).scrollTop());
    console.log($(this).scrollTop() + $(this).height());

    if (($(this).scrollTop() + $(this).height()) >= (.9 * $(this)[0].scrollHeight))
    {
        console.log("load more records");
    }

});
*/

/*
 * hiddenMaxRecords - Largest RS that can be retrieved at a given time
 * hiddenRSStartPosition - Index to the starting position of the current RS (a multiple of hiddenMaxRecords, e.g. 0, n, 2n)
 * hiddenPageSize - User-chosen page size
 */
function NextRecordSet()
{
    //get current page from stored value
    g_currentPage = parseInt($('#hiddenCurrentPage').val(), 10);

    var nMaxRecords = parseInt($('#hiddenMaxRecords').val(), 10);
    var nHiddenRSStartPosition = parseInt($('#hiddenRSStartPosition').val(), 10);
    var nPageSize = parseInt($('#hiddenPageSize').val(), 10);

    var totalpages = (nHiddenRSStartPosition + nMaxRecords) / nPageSize;

    // If we are already on the last page in the current page group, we'll need to go back to the server for a new RS
    var bNeedNewRecordset = (g_currentPage == totalpages);

    if (bNeedNewRecordset)
    {
        // Increment the RS Start position
        nHiddenRSStartPosition += nMaxRecords;
        $('#hiddenRSStartPosition').val(nHiddenRSStartPosition);
    }

    $('#hiddenCurrentPage').val(++g_currentPage);

    if (bNeedNewRecordset)
    {
        fnFillGrid();      //4.2 Needed for paginated sort
    }
    else
    {
        PageIndexChanging(g_currentPage);
    }

}

function PrevRecordSet()
{
    //get current page from stored value
    g_currentPage = parseInt($('#hiddenCurrentPage').val(), 10);

    var firstPage = document.getElementById("hiddenFirstPage").value;
    var nFirstPage = parseInt(firstPage, 10);

    // If we are already on the first page in the current page group, we'll need to go back to the server for a new RS
    var bNeedNewRecordset = (g_currentPage == nFirstPage);

    if (bNeedNewRecordset) 
    {
        var nMaxRecords = parseInt($('#hiddenMaxRecords').val(), 10);
        var nHiddenRSStartPosition = parseInt($('#hiddenRSStartPosition').val(), 10);
        nHiddenRSStartPosition -= nMaxRecords;
        $('#hiddenRSStartPosition').val(nHiddenRSStartPosition);
    }

    $('#hiddenCurrentPage').val(--g_currentPage);

    if (bNeedNewRecordset)
        //4.2 Needed for paginated sort
        fnFillGrid();   // MMC - Refactoring.  fnFillGrid(column, g_direction);
    else
        PageIndexChanging(g_currentPage);
}

function defaultNumColumns(ProjectType)
{
    return (ProjectType == "ERS" ? 10 : 8);
}

// Construct the heading and store it in JS var
// This depends on the hiddenTotalRecords to be correct and set already in RecordCount
function ConstructHeading(PageNo, ProjectType)
{
    console.log("[fn: ConstructHeading]");

    // MMC - Refactoring - We will construct the heading in this function.  This is all of the GUI which allows filtering/display customization plus the grid column headers.
    var strHeading = '';

    if (debug) { fnDebug("ConstructHeading(" + PageNo + ") Called"); }

    headerCount = headerCount + 1;

    if (debug) { fnDebug("ConstructHeading(" + PageNo + ") headerCount: " + headerCount); }

    // Search filter
    // MMC Refactoring - remove var searchValueText in PatientList.aspx
    strHeading = '<input type="search" name="txtsearchon" id="txtsearchon" class="search-field" value="" onchange="javascript:fnFillGrid();" placeholder="Search"> &nbsp; &nbsp; ';

    if (debug) { fnDebug(strHeading); }

    //4.3 Added Filter and reset Filter buttons CM
    strHeading = strHeading + '<button id="filter" type="button" onclick="filterOn()">filter</button>';
    strHeading = strHeading + '<button id="filterReset" type="button" onclick="filterOfff()">Reset <span id="hide-sm">filter</span></button> &nbsp; &nbsp; &nbsp;';

    // Dropdown for date filter
    //strHeading = strHeading + '<td id="date-filter" >'
    strHeading = strHeading + '<select id=ddlDateFilter onchange=fnDateFilter()> '
    strHeading = strHeading + '         <option value=0>Current Date</option>'
    strHeading = strHeading + '         <option value=1>Last 3 Days</option>'
    strHeading = strHeading + '         <option value=2>Last 7 Days</option>'
    strHeading = strHeading + '         <option value=3>Last 30 Days</option>'
    strHeading = strHeading + '         <option value=4>Last 90 Days</option>'
    strHeading = strHeading + '         <option value=5>Last 180 Days</option>'
    strHeading = strHeading + '         <option value=6>Last 365 Days</option>'
    strHeading = strHeading + '         <option value=7>Show All</option>'
    strHeading = strHeading + '</select></br>';
    //strHeading = strHeading + '</td>';

    // Decide which style based on sort direction
    //var bSortDirectionAscending = (document.getElementById("sortDirection").value == "ASC");
    var bSortDirectionAscending = (document.getElementById("hiddenSortDirection").value == "ASC");
    var strClassAscending = '<i id="sortByCarrot" class="fas fa-caret-up"></i>';
    var strClassDescending = '<i id="sortByCarrot" class="fas fa-caret-up"></i>';
    var strClassSortDirection = bSortDirectionAscending ? strClassAscending : strClassDescending;

    var arrIDs = [];
    var arrLabels = [];
    var arrWidthPercent = [];
    var arrSortByString = [];

    if (ProjectType == "ERS")
    {
        // Length: 10
        arrIDs = ['name', 'id', 'studydate', 'studytime', 'ReferringMD', 'DateofBirth', 'Site', 'InterpretingMD', 'studytype', 'studystatus'];
        arrLabels = [' Patient Name', ' Patient ID', 'Study Date', ' Study Time', ' Referring MD', ' DOB', ' Site', ' Interpreting MD', ' Study Type', ' Study Status'];
        arrWidthPercent = ['15', '8', '4', '5', '15', '4', '5', '15', '10', '8'];
        arrSortByString = ['name', 'id', '[study%20date]', '[study%20time]', '[Referring%20MD]', '[Date%20of%20Birth]', 'Site', '[Interpreting%20MD]', '[study%20type]', '[study%20status]'];
    }
    else if (ProjectType == "OBW")
    {
        // Length: 8
        arrIDs = ['name', 'id', 'studydate', 'Firststudy', 'InterpretingMD', 'EDD', 'studytype', 'studystatus'];
        arrLabels = [' Patient Name', ' Patient ID', 'Study Date', ' 1st', ' Interpreting MD', ' EDD', 'Study Type', ' Study Status'];
        arrWidthPercent = ['15', '8', '4', '2', '15', '8', '10', '8'];
        arrSortByString = ['name', 'id', '[study%20date]', '[Firststudy]', '[Interpreting%20MD]', '[EDD]', '[study%20type]', '[study%20status]'];
    }

    //strHeading = strHeading + '<div class="table-wrapper container" onscroll="scrollHandler(this);">';
    strHeading = strHeading + '<div class="table-wrapper container">';
    //strHeading = strHeading + '<div class="table-wrapper container" onscroll="scrollHandler2(this);">';
    //strHeading = strHeading + '<div class="table-wrapper container">';
    strHeading = strHeading + '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="gvTable" id="tbl1">';
    // Writing out the column header
    strHeading = strHeading + '<thead>';
    strHeading = strHeading + '   <tr class="gvHeader">';

    console.log(arrIDs);

    for (i = 0; i < arrIDs.length; i++)
    {
        console.log("loading header column");
        strHeading = strHeading + '<th id=' + arrIDs[i] + ' style=width:' + arrWidthPercent[i] + '% class=gvHeader align=center onclick=javascript:SortBy("' + arrSortByString[i] + '");>';
        strHeading = strHeading + arrLabels[i];
        strHeading = strHeading + strClassSortDirection;
        strHeading = strHeading + '</th>';
    }

    strHeading = strHeading + '   </tr>';
    strHeading = strHeading + '</thead>';

    //----------------------------------------------------------

    // MMC - Refactoring - Save the constructed heading.
    g_strTablePatientList_Heading = strHeading;
    if (debug) { fnDebug(g_strTablePatientList_Heading); }
}

function ConstructHeading_OLD(PageNo, ProjectType)
{
    console.log("fnFillGrid - 2 - ConstructHeading");

    // MMC - Refactoring - We will construct the heading in this function.  This is all of the GUI which allows filtering/display customization plus the grid column headers.
    var strHeading = '';

    if (debug) { fnDebug("ConstructHeading(" + PageNo + ") Called"); }

    headerCount = headerCount + 1;

    if (debug) { fnDebug("ConstructHeading(" + PageNo + ") headerCount: " + headerCount); }

    var htmlPageDir = constructHTML_PageDirectory(PageNo);

    // Search filter
    // MMC Refactoring - remove var searchValueText in PatientList.aspx
    // strHeading = '<input type=search name=txtsearchon id=txtsearchon class=search-field value="' + searchValueText + '" onchange=javascript:fnFillGrid(); placeholder="Search" > </input> &nbsp; &nbsp; ';
    strHeading = '<input type=search name=txtsearchon id=txtsearchon class=search-field value="" onchange=javascript:fnFillGrid(); placeholder="Search" > </input> &nbsp; &nbsp; ';
    if (debug) { fnDebug(strHeading); }

    //4.3 Added Filter and reset Filter buttons CM
    strHeading = strHeading + '<button id="filter" type="button" onclick="filterOn()">filter</button>';
    strHeading = strHeading + '<button id="filterReset" type="button" onclick="filterOfff()">Reset <span id="hide-sm">filter</span></button> &nbsp; &nbsp; &nbsp;';

    // 4.3 added id="tbl1"
    strHeading = strHeading + ' <table width=100% border=0 cellpadding=2 cellspacing=1 class=gvTable id="tbl1">';
    //        strHeading = strHeading + '   <tr>';
    //        strHeading = strHeading + '    <td colspan=8 align=left class=JavaScriptGV></td>'; 
    //        strHeading = strHeading + '   </tr>';

    // Dropdown for page size
    strHeading = strHeading + '   <tr class="SelectedPage" >';
    strHeading = strHeading + '    <td  align=left colspan=1 class=SelectedPage> Select Page Size';
    strHeading = strHeading + '                              <select id=ddlPageSize onchange=PageSizeChanging()> '
    strHeading = strHeading + '                                 <option value=5>5</option>'
    strHeading = strHeading + '                                 <option value=10>10</option>'
    strHeading = strHeading + '                                 <option value=20>20</option>'
    strHeading = strHeading + '                                 <option value=50>50</option>'
    strHeading = strHeading + '                                 <option value=100>100</option>'
    strHeading = strHeading + '                              </select>';
    strHeading = strHeading + '     </td>';

    // Dropdown for date filter
    strHeading = strHeading + '<td id="date-filter" ><select id=ddlDateFilter onchange=fnDateFilter()> '
    strHeading = strHeading + '         <option value=0>Current Date</option>'
    strHeading = strHeading + '         <option value=1>Last 3 Days</option>'
    strHeading = strHeading + '         <option value=2>Last 7 Days</option>'
    strHeading = strHeading + '         <option value=3>Last 30 Days</option>'
    strHeading = strHeading + '         <option value=4>Last 90 Days</option>'
    strHeading = strHeading + '         <option value=5>Last 180 Days</option>'
    strHeading = strHeading + '         <option value=6>Last 365 Days</option>'
    strHeading = strHeading + '         <option value=7>Show All</option>'
    strHeading = strHeading + '</select></br></td>';

    strHeading = strHeading + '    <td colspan=4 align=left id=td_PageLink class=SelectedPage> Select Page ' + htmlPageDir;    // MMC - Refactored.  Was + Pagelink

    //==============================
    // Setup Prev and Next Links
    var htmlPrevLink = '';
    var htmlNextLink = '';

    var PageSize = parseInt(document.getElementById('hiddenPageSize').value, 10);
    var totalRecords = parseInt(document.getElementById("hiddenTotalRecords").value, 10);
    var nMaxPagesTotal_OneBased = Math.ceil(totalRecords / PageSize);
    console.log('ConstructHeading: PageNo=' + PageNo + ' PageSize=' + PageSize + ' totalRecords=' + totalRecords + ' nMaxPagesTotal_OneBased=' + nMaxPagesTotal_OneBased);

    // If on page 1, there is no Prev.  Else if on any other page, link PreRecordSet function to Prev link
    if (PageNo == 1) 
    {
        htmlPrevLink = 'Prev|';
    }
    else {
        htmlPrevLink = '<a  href=javascript:void(0) onclick=javacsript:PrevRecordSet()>Prev</a>|';
    }

    // If on last page, there is no Next.  Else if on any other page, link NextRecordSet function to Next link
    if (PageNo == nMaxPagesTotal_OneBased || nMaxPagesTotal_OneBased == 0) {
        htmlNextLink = 'Next|</td>';
    }
    else {
        htmlNextLink = '<a  href=javascript:void(0) onclick=javacsript:NextRecordSet()>Next</a>|</td>';
    }

    strHeading = strHeading + htmlPrevLink + htmlNextLink;      // Add these to the page directory
    //----------------------------------------------------------
    // Setup Page X of Y - right justified
    var strPageXOfY = (nMaxPagesTotal_OneBased > 0 ? ' Page ' + PageNo + ' of ' + nMaxPagesTotal_OneBased : ' Page 0 of 0');
    strHeading = strHeading + '    <td colspan=4 align=right class=SelectedPage id=td_pageNo>' + strPageXOfY + '</td>';
    //----------------------------------------------------------
    strHeading = strHeading + '   </tr>';

    //----------------------------------------------------------
    // Column header

    // Decide which style based on sort direction
    //var bSortDirectionAscending = (document.getElementById("sortDirection").value == "ASC");
    var bSortDirectionAscending = (document.getElementById("hiddenSortDirection").value == "ASC");
    var strClassAscending = '<i id="sortByCarrot" class="fas fa-caret-up"></i>';
    var strClassDescending = '<i id="sortByCarrot" class="fas fa-caret-up"></i>';
    var strClassSortDirection = bSortDirectionAscending ? strClassAscending : strClassDescending;

    var arrIDs = [];
    var arrLabels = [];
    var arrWidthPercent = [];
    var arrSortByString = [];

    if (ProjectType == "ERS") {
        // Length: 10
        arrIDs = ['name', 'id', 'studydate', 'studytime', 'ReferringMD', 'DateofBirth', 'Site', 'InterpretingMD', 'studytype', 'studystatus'];
        arrLabels = [' Patient Name', ' Patient ID', 'Study Date', ' Study Time', ' Referring MD', ' DOB', ' Site', ' Interpreting MD', ' Study Type', ' Study Status'];
        arrWidthPercent = ['15', '8', '4', '5', '15', '4', '5', '15', '10', '8'];
        arrSortByString = ['name', 'id', '[study%20date]', '[study%20time]', '[Referring%20MD]', '[Date%20of%20Birth]', 'Site', '[Interpreting%20MD]', '[study%20type]', '[study%20status]'];
    }
    else if (ProjectType == "OBW") {
        // Length: 8
        arrIDs = ['name', 'id', 'studydate', 'Firststudy', 'InterpretingMD', 'EDD', 'studytype', 'studystatus'];
        arrLabels = [' Patient Name', ' Patient ID', 'Study Date', ' 1st', ' Interpreting MD', ' EDD', 'Study Type', ' Study Status'];
        arrWidthPercent = ['15', '8', '4', '2', '15', '8', '10', '8'];
        arrSortByString = ['name', 'id', '[study%20date]', '[Firststudy]', '[Interpreting%20MD]', '[EDD]', '[study%20type]', '[study%20status]'];
    }

    // Writing out the column header
    strHeading = strHeading + '   <tr class="gvHeader">';
    for (i = 0; i < arrIDs.length; i++) {
        strHeading = strHeading + '   <td id=' + arrIDs[i] + ' style=width:' + arrWidthPercent[i] + '% class=gvHeader align=center onclick=javascript:SortBy("' + arrSortByString[i] + '");>';
        strHeading = strHeading + arrLabels[i];
        strHeading = strHeading + strClassSortDirection;
        strHeading = strHeading + '</td>';
    }
    strHeading = strHeading + '   </tr>';
    //----------------------------------------------------------

    // MMC - Refactoring - Save the constructed heading.
    g_strTablePatientList_Heading = strHeading;
    if (debug) { fnDebug(g_strTablePatientList_Heading); }
}

function highlightSortColumn()
{
    // Change CSS style to highlight which column is the sort column
    var column = document.getElementById("hiddenSortColumn").value;
    if (column == "")
    {
        column = "name"
    }

    column = column.toString();
    column = column.replace("%20", "");
    column = column.replace("[", "");
    column = column.replace("]", "");
    column = column.replace("%20", "");
    column = column.replace(" ", "");
    //alert(column);
    $("#" + column).css("color", "black").css("font-style", "italic").css("background-color", "white");
}

function wildcardSearch(strLog) 
{
    // console.log("wildcardSearch - Place " + strLog);
    updateCurrentPage(1);       // MMC - Need to clear any previous page numbers
    fnFillGrid();
}

// 4.1 DWC separate header values into separate function so it is called after table is rendered
function ReflectSettingsInGUIHeader()
{

    //Set the filters and pagination controls to the hidden stored values 
    if ($('#hiddenPageIndex').val() != '' && $('#hiddenPageSize').val() != '')
    {
        //    var PageSize = document.getElementById('hiddenPageSize').value;
        var PageIndex = document.getElementById('hiddenPageIndex').value;
        if (debug) { fnDebug("ConstructHeading() About to check hiddenPageIndex Value"); }
        if (debug) { fnDebug("ConstructHeading() PageIndex.Value = " + PageIndex.value); }

        try
        {
            document.getElementById('ddlPageSize').selectedIndex = PageIndex;
        }
        catch (e)
        {
            if (debug) { fnDebug(e.Message()); }
        }

    }
    // 4.3 - Restore across page loads, only page size is saved, find the associated selectedIndex
    else if ($('#hiddenPageIndex').val() == '' && $('#hiddenPageSize').val() != '')
    {
        var lbPageSize = document.getElementById('ddlPageSize');
        var strPageSize = $('#hiddenPageSize').val();

        try
        {

            for (var j = 0; j < lbPageSize.options.length; ++j)
            {

                if (lbPageSize.options[j].value === strPageSize)
                {
                    lbPageSize.selectedIndex = j;
                    $('#hiddenPageIndex').val(j);
                    console.log("ReflectSettingsInGUIHeader: strPageSize=" + strPageSize + ", lbPageSize.options[j].text=" + lbPageSize.options[j].text + ", selectedIndex=" + j);
                }

            }

        }
        catch (e)
        {
            $('#hiddenPageIndex').val(1);
        }

    }

    //set the date filter from stored value
    var DateFilter = document.getElementById("hiddenDateFilter");
    if (debug) { fnDebug("ConstructHeading() About to check hiddenDateFilter Value"); }
    if (debug) { fnDebug("ConstructHeading() DateFilter.Value = " + DateFilter.value); }

    if (DateFilter)
    {
        document.getElementById('ddlDateFilter').selectedIndex = DateFilter.value;
    }

    // Change CSS style to highlight which column is the sort column
    highlightSortColumn();

    // ---------------------------------------------------------------------------------------------
    // Wildcard search
    var searchtextElem = document.getElementById("txtsearchon");

    // 4.3 Func below used to keep focus blur at the end of input after WC search CM
    $(document).ready(function ()
    {
        $("#txtsearchon").focus(function ()
        {

            if (this.setSelectionRange)
            {
                var len = $(this).val().length;
                this.setSelectionRange(len, len);
            }
            else
            {
                $(this).val($(this).val());
            }

        });
        $("#txtsearchon").focus();
    });

    //4.3 Searchbar on click of x CM
    $('input[type=search]').on('search', function ()
    {
        wildcardSearch("1");
        // this function will be executed on click of X (clear button)
    });

    // Retains user-entered search value, after the new grid from search results
    var hiddenSearchTerm = document.getElementById("hiddenSearchTerm");

    if (hiddenSearchTerm.value != 'undefined')
    {
        searchtextElem.value = hiddenSearchTerm.value
    }

    // 4.3 New searchtextElem Wild Card Search with 500ms delay CM
    searchtextElem.addEventListener("keyup", search);

    var timeout = null;

    function search()
    {

        if (timeout)
        {
            window.clearTimeout(timeout);
        }

        timeout = setTimeout(function () { wildcardSearch("2"); }, 500);
    }

    // 4.3 Previous searchtextElem function still needed CM 
    searchtextElem.onchange = function (e)
    {
        //searchtextElem.preventDefault();
        var event = e || window.event;
        var charCode = event.which || event.keycode;

        if (charCode == '13')
        {
            wildcardSearch("3");
            return false;
        }

    }
    // ---------------------------------------------------------------------------------------------

}

/*
    From the XML response, creates the study grid rows HTML, adds that table into the DOM.
*/
function GetPatientListResponse(XML)
{
    console.log("fnFillGrid - 3 - GetPatientListResponse");

    // MMC - Refactoring - We will construct the patient list table rows in this routine
    var strPatientListGrid = '';
    if (debug) { fnDebug("GetPatientListResponse Called"); }
    //if (debug) { fnDebug(XML.nodeName()); }

    if (debug) { fnDebug("Test0"); }
    var StudyList = XML.getElementsByTagName("Study");
    if (debug) { fnDebug("Test1"); }
    StudyIdList = XML.getElementsByTagName("StudyID");
    if (debug) { fnDebug("Test2"); }
    
    // MMC - Fix bug with NULL recordsets
    if (StudyList.length == 0) 
    {
        strPatientListGrid = appendEmptyWorklistHTML(strPatientListGrid, defaultNumColumns(g_ProjectType));
    }
    else 
    {
        //4.2 Add OB Support SF # 9341
        var NumOfColumns = StudyList[0].childNodes.length;
        var ElementList = [];
        var NodeName;

        for (var CurrentColumn = 1; CurrentColumn < NumOfColumns; CurrentColumn++) 
        {
            NodeName = StudyList[0].childNodes[CurrentColumn].nodeName;
            fnDebug(NodeName);
            ElementList[CurrentColumn] = XML.getElementsByTagName(NodeName);
        }

        if (debug) { fnDebug("Test3"); }

        //var PatientIDList = XML.getElementsByTagName("ID");
        //NodeName = StudyList[0].childNodes[2].nodeName;
        //var PatientIDList = XML.getElementsByTagName(NodeName);
        //var PatientIDList = ElementList[2];
        /*
        var StudyDateList = XML.getElementsByTagName("StudyDate");
        var StudyTimeList = XML.getElementsByTagName("StudyTime");
        var ReferringMDList = XML.getElementsByTagName("ReferringMD");
        var InterpretingMDList = XML.getElementsByTagName("InterpretingMD");

        var StudyTypeList = XML.getElementsByTagName("StudyType");
        var StudyStatusList = XML.getElementsByTagName("StudyStatus");
        */
        //var PageShowHide = 'style=display:none';
        var PageShowHide = '';
        var index = 0;

        //4.2 OB Support and column config
        var CurrentElement = [];

        if (StudyIdList.length > 0) 
        {

            for (var i = index; i < StudyIdList.length; i++) 
            {

                for (var CurrentColumn = 1; CurrentColumn < NumOfColumns; CurrentColumn++) 
                {
                    CurrentElement = ElementList[CurrentColumn];

                    if (GetInnerText(CurrentElement[i]) == null) 
                    {
                        CurrentElement[i].innerText = " ";
                    }

                }

                CurrentElement = ElementList[1];   //4.2 temporary this is actually PatientNameList
                //            if (GetInnerText(ReferringMDList[i]) == null) { ReferringMDList[i].innerText = " "; }
                //            if (GetInnerText(PatientNameList[i]) == null) { PatientNameList[i].innerText = " "; }
                //            if (GetInnerText(PatientIDList[i]) == null) { PatientIDList[i].innerText = " "; }
                //            if (GetInnerText(StudyDateList[i]) == null) { ReferringMDList[i].innerText = " "; }
                //            if (GetInnerText(StudyTimeList[i]) == null) { StudyTimeList[i].innerText = " "; }
                //            if (GetInnerText(InterpretingMDList[i]) == null) { InterpretingMDList[i].innerText = " "; }
                //            if (GetInnerText(StudyTypeList[i]) == null) { StudyTypeList[i].innerText = " "; }
                //            if (GetInnerText(StudyStatusList[i]) == null) { StudyStatusList[i].innerText = " "; }

                //4.3 DWC Change row id to Study id for retaining selected row state when filtered or navigating away from patient list
                //console.log(' <tr class="gvSelectedRow gvSelectedRowHover1"' + ' ' + 'id=trSelect' + GetInnerText(StudyIdList[i]) + ' ' + PageShowHide + ' onclick=SelectStudy(' + GetInnerText(StudyIdList[i]) + ',' + GetInnerText(StudyIdList[i]) + ') ondblclick=OpenStudy(' + GetInnerText(StudyIdList[i]) + ',' + GetInnerText(StudyIdList[i]) + ') >');
                strPatientListGrid = strPatientListGrid + ' <tr class="gvSelectedRow gvSelectedRowHover1"' + ' ' + 'id=trSelect' + GetInnerText(StudyIdList[i]) + ' ' + PageShowHide + ' onclick=SelectStudy(' + GetInnerText(StudyIdList[i]) + ',' + GetInnerText(StudyIdList[i]) + ') ondblclick=OpenStudy(' + GetInnerText(StudyIdList[i]) + ',' + GetInnerText(StudyIdList[i]) + ') >';

                strPatientListGrid = strPatientListGrid + '   <td style=width:15% class=leftgvItem>';
                strPatientListGrid = strPatientListGrid + ' <input class="not-visible" type=hidden value=' + GetInnerText(StudyIdList[i]) + ' name="chkSelect"/>' //4.2 Configurable column list

                //4.2 Hidden column only on first column
                strPatientListGrid = strPatientListGrid + GetInnerText(CurrentElement[i]);   //4.2 temporary this is actually PatientNameList
                strPatientListGrid = strPatientListGrid + '   </td>';

                //4.2 Start enumerating column list here
                for (var CurrentColumn = 2; CurrentColumn < NumOfColumns; CurrentColumn++) 
                {
                    CurrentElement = ElementList[CurrentColumn];
                    strPatientListGrid = strPatientListGrid + '   <td style=width:8% class=gvItem >';
                    strPatientListGrid = strPatientListGrid + GetInnerText(CurrentElement[i]);
                    strPatientListGrid = strPatientListGrid + '   </td>';
                }

                //4.2 OB Support
                // strPatientListGrid = GetPatientListResponse_AddOBCols(strPatientListGrid);
                strPatientListGrid = strPatientListGrid + '   </tr>';
            }

        }
        else 
        {
            //alert("No studies");
            strPatientListGrid = appendEmptyWorklistHTML(strPatientListGrid, NumOfColumns);
        }

    }

    // ??????????????????? 4.1 DWC reduce to one table so column and column headers align
    strPatientListGrid = strPatientListGrid + ' </table>'
    strPatientListGrid = strPatientListGrid + ' </div>'

    // MMC - Refactoring - Save the grid rows (one per study).
    g_strTablePatientList_Grid = strPatientListGrid;
    allRecordsAlreadyLoaded = false;

    if (debug) { fnDebug("GetPatientListResponse() Before divPatientList insertion, g_strTablePatientList_Grid = " + g_strTablePatientList_Grid); }

    updatePatientListDiv();

    if (debug) { fnDebug("GetPatientListResponse() After divPatientList insertion"); }
}

/*
    From the XML response, creates the study grid rows HTML, adds that table into the DOM.
*/
function GetPatientListResponse_OLD(XML)
{
    console.log("fnFillGrid - 3 - GetPatientListResponse");

    // MMC - Refactoring - We will construct the patient list table rows in this routine
    var strPatientListGrid = '';
    if (debug) { fnDebug("GetPatientListResponse Called"); }
    //if (debug) { fnDebug(XML.nodeName()); }

    if (debug) { fnDebug("Test0"); }
    var StudyList = XML.getElementsByTagName("Study");
    if (debug) { fnDebug("Test1"); }
    StudyIdList = XML.getElementsByTagName("StudyID");
    if (debug) { fnDebug("Test2"); }

    // MMC - Fix bug with NULL recordsets
    if (StudyList.length == 0) {
        strPatientListGrid = appendEmptyWorklistHTML(strPatientListGrid, defaultNumColumns(g_ProjectType));
    }
    else {
        //4.2 Add OB Support SF # 9341
        var NumOfColumns = StudyList[0].childNodes.length;
        var ElementList = [];
        var NodeName;

        for (var CurrentColumn = 1; CurrentColumn < NumOfColumns; CurrentColumn++) {
            NodeName = StudyList[0].childNodes[CurrentColumn].nodeName;
            fnDebug(NodeName);
            ElementList[CurrentColumn] = XML.getElementsByTagName(NodeName);
        }

        if (debug) { fnDebug("Test3"); }

        //var PatientIDList = XML.getElementsByTagName("ID");
        //NodeName = StudyList[0].childNodes[2].nodeName;
        //var PatientIDList = XML.getElementsByTagName(NodeName);
        //var PatientIDList = ElementList[2];
        var StudyDateList = XML.getElementsByTagName("StudyDate");
        var StudyTimeList = XML.getElementsByTagName("StudyTime");
        var ReferringMDList = XML.getElementsByTagName("ReferringMD");
        var InterpretingMDList = XML.getElementsByTagName("InterpretingMD");

        var StudyTypeList = XML.getElementsByTagName("StudyType");
        var StudyStatusList = XML.getElementsByTagName("StudyStatus");

        var PageShowHide = 'style=display:none'
        var index = 0;

        //4.2 OB Support and column config
        var CurrentElement = [];

        if (StudyIdList.length > 0) {

            for (var i = index; i < StudyIdList.length; i++) {

                for (var CurrentColumn = 1; CurrentColumn < NumOfColumns; CurrentColumn++) {
                    CurrentElement = ElementList[CurrentColumn];

                    if (GetInnerText(CurrentElement[i]) == null) {
                        CurrentElement[i].innerText = " ";
                    }

                }

                CurrentElement = ElementList[1];   //4.2 temporary this is actually PatientNameList
                //            if (GetInnerText(ReferringMDList[i]) == null) { ReferringMDList[i].innerText = " "; }
                //            if (GetInnerText(PatientNameList[i]) == null) { PatientNameList[i].innerText = " "; }
                //            if (GetInnerText(PatientIDList[i]) == null) { PatientIDList[i].innerText = " "; }
                //            if (GetInnerText(StudyDateList[i]) == null) { ReferringMDList[i].innerText = " "; }
                //            if (GetInnerText(StudyTimeList[i]) == null) { StudyTimeList[i].innerText = " "; }
                //            if (GetInnerText(InterpretingMDList[i]) == null) { InterpretingMDList[i].innerText = " "; }
                //            if (GetInnerText(StudyTypeList[i]) == null) { StudyTypeList[i].innerText = " "; }
                //            if (GetInnerText(StudyStatusList[i]) == null) { StudyStatusList[i].innerText = " "; }

                //4.3 DWC Change row id to Study id for retaining selected row state when filtered or navigating away from patient list
                strPatientListGrid = strPatientListGrid + ' <tr class="gvSelectedRow gvSelectedRowHover1"' + ' ' + 'id=trSelect' + GetInnerText(StudyIdList[i]) + ' ' + PageShowHide + ' onclick=SelectStudy(' + GetInnerText(StudyIdList[i]) + ',' + GetInnerText(StudyIdList[i]) + ') ondblclick=OpenStudy(' + GetInnerText(StudyIdList[i]) + ',' + GetInnerText(StudyIdList[i]) + ') >';

                strPatientListGrid = strPatientListGrid + '   <td style=width:15% class=leftgvItem>';
                strPatientListGrid = strPatientListGrid + ' <input class="not-visible" type=hidden value=' + GetInnerText(StudyIdList[i]) + ' name="chkSelect"/>' //4.2 Configurable column list

                //4.2 Hidden column only on first column
                strPatientListGrid = strPatientListGrid + GetInnerText(CurrentElement[i]);   //4.2 temporary this is actually PatientNameList
                strPatientListGrid = strPatientListGrid + '   </td>';

                //4.2 Start enumerating column list here
                for (var CurrentColumn = 2; CurrentColumn < NumOfColumns; CurrentColumn++) {
                    CurrentElement = ElementList[CurrentColumn];
                    strPatientListGrid = strPatientListGrid + '   <td style=width:8% class=gvItem >';
                    strPatientListGrid = strPatientListGrid + GetInnerText(CurrentElement[i]);
                    strPatientListGrid = strPatientListGrid + '   </td>';
                }

                //4.2 OB Support
                // strPatientListGrid = GetPatientListResponse_AddOBCols(strPatientListGrid);

                strPatientListGrid = strPatientListGrid + '   </tr>';
            }

        }
        else {
            //alert("No studies");
            strPatientListGrid = appendEmptyWorklistHTML(strPatientListGrid, NumOfColumns);
        }

    }

    // ??????????????????? 4.1 DWC reduce to one table so column and column headers align
    strPatientListGrid = strPatientListGrid + ' </table>'

    // MMC - Refactoring - Save the grid rows (one per study).
    g_strTablePatientList_Grid = strPatientListGrid;

    if (debug) { fnDebug("GetPatientListResponse() Before divPatientList insertion, g_strTablePatientList_Grid = " + g_strTablePatientList_Grid); }

    updatePatientListDiv();

    if (debug) { fnDebug("GetPatientListResponse() After divPatientList insertion"); }
}

function appendEmptyWorklistHTML(strPatientListGrid, nColumns)
{
    strPatientListGrid = strPatientListGrid + '<tr>'
    strPatientListGrid = strPatientListGrid + '<td colspan=' + nColumns + ' align=left  class=EmptyCart>'
    strPatientListGrid = strPatientListGrid + 'No patient in the worklist'
    strPatientListGrid = strPatientListGrid + '</td>'
    strPatientListGrid = strPatientListGrid + '</tr>'
    return strPatientListGrid;
}

function GetPatientListResponse_AddOBCols(strPatientListGrid)
{
    //4.2 OB Support
    //strPatientListGrid = strPatientListGrid + GetInnerText(PatientIDList[i]);
    //            CurrentElement = ElementList[2]; //4.2 temporary this is actually PatientIDList
    //            strPatientListGrid = strPatientListGrid + GetInnerText(CurrentElement[i]);

    //            strPatientListGrid = strPatientListGrid + '   </td>';

    //            strPatientListGrid = strPatientListGrid + '   <td style=width:8% class=gvItem align=right>';
    //            strPatientListGrid = strPatientListGrid + GetInnerText(StudyDateList[i]);
    //            strPatientListGrid = strPatientListGrid + '   </td>';

    //            strPatientListGrid = strPatientListGrid + '   <td style=width:5% class=gvItem align=right>';
    //            strPatientListGrid = strPatientListGrid + GetInnerText(StudyTimeList[i]);
    //            strPatientListGrid = strPatientListGrid + '   </td>';

    //            strPatientListGrid = strPatientListGrid + '   <td style=width:15% class=gvItem align=right>';
    //            strPatientListGrid = strPatientListGrid + GetInnerText(ReferringMDList[i]);
    //            strPatientListGrid = strPatientListGrid + '   </td>';

    //            strPatientListGrid = strPatientListGrid + '   <td style=width:15% class=gvItem align=right>';
    //            strPatientListGrid = strPatientListGrid + GetInnerText(InterpretingMDList[i]);
    //            strPatientListGrid = strPatientListGrid + '   </td>';

    //            strPatientListGrid = strPatientListGrid + '   <td style=width:11% class=gvItem align=right>';
    //            strPatientListGrid = strPatientListGrid + GetInnerText(StudyTypeList[i]);
    //            strPatientListGrid = strPatientListGrid + '   </td>';

    //            strPatientListGrid = strPatientListGrid + '   <td style=width:8% class=rightgvItem align=right>';
    //            strPatientListGrid = strPatientListGrid + GetInnerText(StudyStatusList[i]);
    //            strPatientListGrid = strPatientListGrid + '   </td>';

    return strPatientListGrid;
}

function fnDateFilter()
{
    // 4.1 DWC Reset header count so page can render the new header
    headerCount = 0;

    var DateFilterSelect = document.getElementById('ddlDateFilter');

    if (DateFilterSelect)
    {
        document.getElementById('hiddenDateFilter').value = DateFilterSelect.selectedIndex;
    }

    // GetResponse('Handler.ashx?guid=' + guid + '&count=get', RecordCount);

    //4.1 DWC when changing date filter, start on page 1
    document.getElementById("hiddenFirstPage").value = 1;
    document.getElementById("hiddenCurrentPage").value = 1;

    fnFillGrid();

    //David, need to update page numbers here????
}

function PageSizeChanging()
{
    // 4.1 DWC Reset header count so page can render the new header
    console.log("[fn: PageSizeChanging()]");
    headerCount = 0;

    if (document.getElementById('ddlPageSize'))
    {
        PageSize = document.getElementById('ddlPageSize').value;
        document.getElementById('hiddenPageIndex').value = document.getElementById('ddlPageSize').selectedIndex;
        document.getElementById('hiddenPageSize').value = PageSize;

        //4.1 DWC Allow scroll on Patient List
        //alert(PageSize);
        //if (PageSize > 20) { $("#mainpanel").css("overflow-y", "auto"); }

        //4.1 DWC when changing page size, start on page 1
        document.getElementById("hiddenFirstPage").value = 1;
        document.getElementById("hiddenCurrentPage").value = 1;

        fnFillGrid();
    }

}

function GetInnerText(node)
{
    return (node.textContent || node.innerText || node.text);
}

function setRowsVisibility(nPageNo_OneBased)
{
    var nPageNo_ZeroBased = nPageNo_OneBased - 1;

    var nPageSize = parseInt(document.getElementById('hiddenPageSize').value, 10);
    var nRSPosition = parseInt(document.getElementById('hiddenRSStartPosition').value, 10);

    //-------------------------------------------------------------------------------------------------
    // Go through the rows and set some visible, other not

    var maxrecords = document.getElementById('hiddenMaxRecords').value;
    if (debug) { fnDebug("setRowsVisibility: nPageNo_ZeroBased: " + nPageNo_ZeroBased + " PageSize: " + nPageSize); }
    if (debug) { fnDebug("setRowsVisibility: arrProduct.length=" + arrProduct.length + ", maxrecords=" + maxrecords); }

    var arrProduct = document.getElementsByName('chkSelect');       // This should be all the studies in the recordset
    var bVisible = false;
    var nActualPos = 0;
    var strLabel = '';

    for (var i = 0; i < arrProduct.length; i++)
    {
        nActualPos = nRSPosition + i;
        bVisible = (nActualPos >= nPageNo_ZeroBased * nPageSize && nActualPos < (nPageNo_ZeroBased + 1) * nPageSize);

        if (document.getElementById('trSelect' + GetInnerText(StudyIdList[i])))
            document.getElementById('trSelect' + GetInnerText(StudyIdList[i])).style.display = bVisible ? '' : 'none';

        if (debug) { fnDebug("setRowsVisibility(" + nPageNo_ZeroBased + "): Turning row " + i + " of " + maxrecords + ' bVisible=' + bVisible ); }
    }

}

// Does nothing more than update the JS variable and hidden field
function updateCurrentPage(nPageNo_OneBased)
{
    g_currentPage = nPageNo_OneBased;

    // Save to the hidden
    var hiddenCurrentPage = document.getElementById("hiddenCurrentPage");

    if (hiddenCurrentPage)
    {
        hiddenCurrentPage.value = nPageNo_OneBased;
        console.log("updateCurrentPage: $('#hiddenCurrentPage').val()=" + $('#hiddenCurrentPage').val());
    }

}

// Current page is changing.  Based on the current page, nPageNo_OneBased, writes out the page-directory (page links in the same page group) into the "td_PageLink" element
// Also updates which rows are visible
// Also updates the hiddenCurrentPage field
function PageIndexChanging(nPageNo_OneBased)
{
    updateCurrentPage(nPageNo_OneBased);

    // 4.1 DWC Reset header count so page can render the new header
    headerCount = 0;
    if (debug) { fnDebug("PageIndexChanging(" + nPageNo_OneBased + ")"); }

    setRowsVisibility(nPageNo_OneBased);                // Go through the rows and set some visible, other not
    updateDOM_PageDirectory(nPageNo_OneBased);
}

// returns the page directory HTML
function constructHTML_PageDirectory(nPageNo_OneBased)
{
    var htmlPageDirectory = '';

    var firstPage = parseInt(document.getElementById("hiddenFirstPage").value, 10);
    var lastPage = parseInt(document.getElementById("hiddenLastPage").value, 10);
    if (debug) { fnDebug("constructHTML_PageDirectory(" + nPageNo_OneBased + ") firstPage: " + firstPage + " lastPage: " + lastPage); }

    for (var pageCount = firstPage; pageCount <= lastPage; pageCount++)
    {
        if (debug) { fnDebug("pageCount: " + pageCount + " nPageNo_OneBased: " + nPageNo_OneBased); }

        var strStyle = '';
        var strPageCountPad = '';

        if (pageCount == nPageNo_OneBased)
        {
            strStyle = 'style = "color: red;"';
        }
        
        if (pageCount < 10)
        {
            strPageCountPad = '&nbsp;';
        }

        htmlPageDirectory = htmlPageDirectory + '<a ' + strStyle + ' href=javascript:void(0) onclick=javacsript:PageIndexChanging(' + (pageCount) + '); id=a_' + pageCount + '> ' + strPageCountPad + pageCount + strPageCountPad + '</a> | ';
    }

    return htmlPageDirectory;
}

function updateDOM_PageDirectory(nPageNo_OneBased)
{
    //-------------------------------------------------------------------------------------------------
    // Construct the directory of page-number links
    var htmlPageDir = constructHTML_PageDirectory(nPageNo_OneBased);      // HTML of page links
    //------------------------------------------------
    //Setup Prev and Next Links
    //if on Page 1, disable previous recordset link
    var htmlPrevNextLinks = '';

    var maxrecords = parseInt(document.getElementById('hiddenMaxRecords').value, 10);
    var PageSize = parseInt(document.getElementById('hiddenPageSize').value, 10);
    var totalRecords = parseInt(document.getElementById("hiddenTotalRecords").value, 10);

    var nMaxPagesTotal_OneBased = Math.ceil(totalRecords / PageSize);
    var nMaxPagesVisibleInDir = Math.ceil(maxrecords / PageSize);

    if (nPageNo_OneBased == 1) { htmlPrevNextLinks = htmlPrevNextLinks + 'Prev|'; }
    //else if on any other page, link PreRecordSet function to Prev link
    else { htmlPrevNextLinks = htmlPrevNextLinks + '<a  href=javascript:void(0) onclick=javacsript:PrevRecordSet()>Prev</a>|'; }

    if (nPageNo_OneBased == nMaxPagesTotal_OneBased || nMaxPagesTotal_OneBased == 0) { htmlPrevNextLinks = htmlPrevNextLinks + 'Next|</td>'; }
    //else if on any other page, link PreRecordSet function to Prev link
    else { htmlPrevNextLinks = htmlPrevNextLinks + '<a  href=javascript:void(0) onclick=javacsript:NextRecordSet()>Next</a>|</td>'; }
    //------------------------------------------------
    //
    // Update the DOM element
    try
    {
        document.getElementById('td_PageLink').innerHTML = 'Select Page ' + htmlPageDir + htmlPrevNextLinks;
    }
    catch (e)
    {
        if (debug) { fnDebug(e.Message()); }
    }

    if (debug) { fnDebug("updateDOM_PageDirectory() totalRecords: " + totalRecords + " PageSize" + PageSize); }

    //------------------------------------------------
    // Setup Page X of Y
    var Pagelink = '';

    if (nMaxPagesTotal_OneBased > 0)
    {
        Pagelink = ' Page ' + nPageNo_OneBased + ' of ' + nMaxPagesTotal_OneBased;
    }
    else
    {
        Pagelink = ' Page 0 of 0';
    }

    try
    {
        document.getElementById('td_pageNo').innerHTML = Pagelink;
    }
    catch (e)
    {
        if (debug) { fnDebug(e.Message()); }
    }

    //------------------------------------------------
}

//*****************************************************************
// The following functions are extracted from PatientList.aspx.vb
// Refactored to be independent client-side functions.
//*****************************************************************

// MMC - WV 4.3 - Refactoring
function doOpeningStudyMsg()
{
    $("#divPatientList").html("<br/><br/><br/><br/><br/><center><p><h1>Opening Study...</h1></p></center>");
}

function emptyFilterObj() {
    //4.3 Inintialize filter term and col 1-10 CM
    var filterObj = {
        filterTerm: '',
        filterTerm2: '',
        filterTerm3: '',
        filterTerm4: '',
        filterTerm5: '',
        filterTerm6: '',
        filterTerm7: '',
        filterTerm8: '',
        filterTerm9: '',
        filterTerm10: '',
        filterCol: '',
        filterCol2: '',
        filterCol3: '',
        filterCol4: '',
        filterCol5: '',
        filterCol6: '',
        filterCol7: '',
        filterCol8: '',
        filterCol9: '',
        filterCol10: ''
    };
    return filterObj;
}
// MMC - WV 4.3 - Refactoring
function getFilterSettings_HiddenToObj()
{
    //4.3 Inintialize filter term and col 1-10 CM
    var filterObj = emptyFilterObj();
    
	var arrVariables = ['filterCol','filterCol2','filterCol3','filterCol4','filterCol5','filterCol6','filterCol7','filterCol8','filterCol9','filterCol10'];
	var arrHiddenFields = ['hiddenFilterCol','hiddenFilterCol2','hiddenFilterCol3','hiddenFilterCol4','hiddenFilterCol5','hiddenFilterCol6','hiddenFilterCol7','hiddenFilterCol8','hiddenFilterCol9','hiddenFilterCol10'];

	var arrVariables2 = ['filterTerm','filterTerm2','filterTerm3','filterTerm4','filterTerm5','filterTerm6','filterTerm7','filterTerm8','filterTerm9','filterTerm10'];
	var arrHiddenFields2 = ['hiddenFilterTerm','hiddenFilterTerm2','hiddenFilterTerm3','hiddenFilterTerm4','hiddenFilterTerm5','hiddenFilterTerm6','hiddenFilterTerm7','hiddenFilterTerm8','hiddenFilterTerm9','hiddenFilterTerm10'];
	
    //4.3 Set all hiddenfilter term and col to their values from filterhandler.js
	// Filter Cols
	if (arrVariables.length == arrHiddenFields.length) {
        for (i = 0; i < arrVariables.length; i++) {
			if (document.getElementById(arrHiddenFields[i]))
				filterObj[arrVariables[i]] = document.getElementById(arrHiddenFields[i]).value;
		}
	}

	// Filter Terms
	if (arrVariables2.length == arrHiddenFields2.length) {
        for (i = 0; i < arrVariables2.length; i++) {
			if (document.getElementById(arrHiddenFields2[i]))
				filterObj[arrVariables2[i]] = document.getElementById(arrHiddenFields2[i]).value;
		}
	}
	
	return filterObj;
}

function getFilterSettings_ObjToHidden(filterObj)
{
    var arrVariables = ['filterCol', 'filterCol2', 'filterCol3', 'filterCol4', 'filterCol5', 'filterCol6', 'filterCol7', 'filterCol8', 'filterCol9', 'filterCol10'];
    var arrHiddenFields = ['hiddenFilterCol', 'hiddenFilterCol2', 'hiddenFilterCol3', 'hiddenFilterCol4', 'hiddenFilterCol5', 'hiddenFilterCol6', 'hiddenFilterCol7', 'hiddenFilterCol8', 'hiddenFilterCol9', 'hiddenFilterCol10'];

    var arrVariables2 = ['filterTerm', 'filterTerm2', 'filterTerm3', 'filterTerm4', 'filterTerm5', 'filterTerm6', 'filterTerm7', 'filterTerm8', 'filterTerm9', 'filterTerm10'];
    var arrHiddenFields2 = ['hiddenFilterTerm', 'hiddenFilterTerm2', 'hiddenFilterTerm3', 'hiddenFilterTerm4', 'hiddenFilterTerm5', 'hiddenFilterTerm6', 'hiddenFilterTerm7', 'hiddenFilterTerm8', 'hiddenFilterTerm9', 'hiddenFilterTerm10'];

    //4.3 Set all hiddenfilter term and col to their values from filterhandler.js
    // Filter Cols
    if (arrVariables.length == arrHiddenFields.length) 
    {

        for (i = 0; i < arrVariables.length; i++)
        {

            if (document.getElementById(arrHiddenFields[i]))
            {

                document.getElementById(arrHiddenFields[i]).value = filterObj[arrVariables[i]];
                //console.log("getFilterSettings_ObjToHidden: Assigned " + arrHiddenFields[i] + " = " + filterObj[arrVariables[i]]);
            }

        }

    }

    // Filter Terms
    if (arrVariables2.length == arrHiddenFields2.length)
    {

        for (i = 0; i < arrVariables2.length; i++)
        {

            if (document.getElementById(arrHiddenFields2[i]))
            {
                document.getElementById(arrHiddenFields2[i]).value = filterObj[arrVariables2[i]];
                //console.log("getFilterSettings_ObjToHidden: Assigned " + arrHiddenFields2[i] + " = " + filterObj[arrVariables2[i]]);
            }

        }

    }

}

// obj to QS
function getFilterQueryString(filterObj) {
    var arrQSKeys = ["filterCol", "filterCol2", "filterCol3", "filterCol4", "filterCol5", "filterCol6", "filterCol7", "filterCol8", "filterCol9", "filterCol10"];
    var arrQSKeys2 = ["filterTerm", "filterTerm2", "filterTerm3", "filterTerm4", "filterTerm5", "filterTerm6", "filterTerm7", "filterTerm8", "filterTerm9", "filterTerm10"];

    var strQS = ""

    for (i = 0; i < arrQSKeys.length; i++) {
        if (i > 0)
            strQS += '&';
        strQS += arrQSKeys[i] + "=" + filterObj[arrQSKeys[i]];
    }
    for (i = 0; i < arrQSKeys2.length; i++) {
            strQS += '&';
        strQS += arrQSKeys2[i] + "=" + filterObj[arrQSKeys2[i]];
    }
    return strQS;
}
// QS to obj
function getFilter_QSToObj(strQueryString)
{
    var filterObj = emptyFilterObj();

    var nStartFilterQS = strQueryString.indexOf("filterCol");

    if (nStartFilterQS < 0)
    {
        return filterObj;
    }
    
    strQueryString = strQueryString.substring(nStartFilterQS);

    var arrKeyValuePairs = strQueryString.split('&');

    for (i = 0; i < arrKeyValuePairs.length; i++)
    {
        //console.log("getFilter_QSToObj: token: " + arrKeyValuePairs[i]);
        var KeyValue = arrKeyValuePairs[i].split('=');
        var key = KeyValue[0];
        var value = KeyValue[1];

        if (filterObj[key] != undefined)
        {
            filterObj[key] = value;
            //console.log("getFilter_QSToObj: Assigned obj['" + key + "'] = " + value);
        }

    }

    return filterObj;
}

// Make fresh requests to Handler.ashx
function fnFillGrid()
{
    //console.log("[fn: fnFillGrid()]");
    // MMC - Comment - A request for new data is coming, so clear out the preexisting HTML
    g_strTablePatientList_Heading = '';
    g_strTablePatientList_Grid = '';
    g_bReceivedRecordCount = false;
    
    //4.2 OB Support
    g_ProjectType = document.getElementById("hiddenProjectType").value;

    // MMC - Refactoring
    // Prior to sending request to server, preserve the current search text by copying value from txtsearchon to hidden field.
    // Note:  The request query string will be generated from the hiddenSearchTerm.
    var searchtextElem = document.getElementById("txtsearchon");
    var hiddensearchElem = document.getElementById("hiddenSearchTerm");

    if (searchtextElem && hiddensearchElem)
    {
        hiddensearchElem.value = searchtextElem.value;
    }
    
    g_maxRecords = parseInt(document.getElementById("hiddenMaxRecords").value, 10);
    g_pageSize = parseInt(document.getElementById("hiddenPageSize").value, 10);

    //console.log("[fn: fnFillGrid()] - g_maxRecords = " + g_maxRecords);
    //console.log("[fn: fnFillGrid()] - g_pageSize = " + g_pageSize);

    //4.1 DWC Allow scroll on Patient List
    //if (g_pageSize > 20) { $("#mainpanel").css("overflow-y", "auto"); }

    //4.3 font size increase
    /*
    if (g_pageSize > 10)
    {
        $("#mainpanel").css("overflow-y", "auto");
    }
    */

    //get current page from stored value
    g_currentPage = parseInt($('#hiddenCurrentPage').val(), 10);

    //console.log("[fn: fnFillGrid()] - g_currentPage = " + g_currentPage);

    //---------------------------------------------------------------------------------
    // Update firstPage and lastPage
    //var nRSPosition = parseInt($('#hiddenRSStartPosition').val(), 10);
    var nRSPosition = 0;
    $('#hiddenRSStartPosition').val(nRSPosition);
    var nHiddenFirstPage = (nRSPosition == 0 ? 1 : (nRSPosition / g_pageSize) + 1);
    $('#hiddenFirstPage').val(nHiddenFirstPage);
    var nHiddenLastPage = (nRSPosition + g_maxRecords) / g_pageSize;
    $('#hiddenLastPage').val(nHiddenLastPage);

    if (debug)
    {
        fnDebug("fnFillGrid() - nHiddenFirstPage=" + nHiddenFirstPage + ", hiddenLastPage=" + hiddenLastPage + " , nRSPosition: " + nRSPosition + " maxRecords: " + g_maxRecords + " pageSize: " + g_pageSize);
    }

    //---------------------------------------------------------------------------------
    // Make the requests
    var strURL = hiddenToQueryString();
	console.log("Debug strURL=" + strURL);
	
    var strURL_RecCount = strURL + '&count=get';
    GetResponse(strURL_RecCount, GetRecordCountResponse);

    var strURL_Records = strURL + '&count=none';
    //console.log(strURL_Records);
    GetResponse(strURL_Records, GetPatientListResponse);

    // 4.3 func below is used to keep focus blur if not result is shown
    //searchtext.onblur = function (event) { var blurEl = this; setTimeout(function () { blurEl.focus() }, 10) };
}

function hiddenToQueryString()
{

    if ($('#hiddenSearchTerm'))
    {
        var searchon = $('#hiddenSearchTerm').val();
    }

    var searchcolumn = document.getElementById("hiddenSortColumn");

    if (searchcolumn) { var column = searchcolumn.value; }

    //var sortDirectionElem = document.getElementById("sortDirection");
    var sortDirectionElem = document.getElementById("hiddenSortDirection");

    if (sortDirectionElem)
    {
        var direction = sortDirectionElem.value;
    }

    var hiddenGUID = document.getElementById("hiddenGUID");

    if (hiddenGUID)
    {
        var guid = hiddenGUID.value;
    }

    // 4.3
    var studyid = document.getElementById("hiddenSelectedStudy").value;
    var filterObj = getFilterSettings_HiddenToObj();    // get them from hidden fields
    var selectedRow = document.getElementById("hiddenSelectedRow").value;
    //var scrollPosition = document.getElementById("hiddenScrollPosition").value;
    var scrollPosition = $("#hiddenScrollPosition").val();

    var datefilter = document.getElementById("hiddenDateFilter");
    if (datefilter) { var dfilter = datefilter.value; }

    var rspositionElem = document.getElementById("hiddenRSStartPosition");

    if (rspositionElem)
    {
        var rsposition = rspositionElem.value;
        if (debug) { fnDebug("Found hidden RS Position"); }
    }

    // 4.3 - Added to QS, restore on patient list return
    var nHiddenPageSize = parseInt($('#hiddenPageSize').val(), 10);
    
    var strPartialQS = 'SortBy=' + column + '&direction=' + direction + '&guid=' + guid + "&studyid=" + studyid + "&datefilter=" + dfilter + "&rsposition=" + rsposition + "&selectedRow=" + selectedRow + "&scrollPosition=" + scrollPosition;
    strPartialQS = strPartialQS + '&pagesize=' + nHiddenPageSize;    // 4.3
    var strFilterQS = getFilterQueryString(filterObj);
    var strURL = 'Handler.ashx?searchtext=' + searchon + '&' + strPartialQS + '&' + strFilterQS;
    return strURL;
}

function queryStringToHidden() 
{
   //return
    var strURL = $('#hiddenLastQueryString').val();
    var strQueryString = strURL.split('Handler.ashx?')[1];
    //console.log("queryStringToHidden: querystring=" + strQueryString);

    var arrKeyValuePairs = strQueryString.split('&');

    for (i = 0; i < arrKeyValuePairs.length; i++) 
    {
        //console.log("queryStringToHidden: token: " + arrKeyValuePairs[i]);
        var KeyValue = arrKeyValuePairs[i].split('=');
        queryStringToHidden_Field(KeyValue[0], KeyValue[1])
    }

    queryStringToHidden_FilterFields(strQueryString);
}

function queryStringToHidden_Field(qsKey, qsValue)
{

    if (qsKey == 'searchtext')
    {

        if (qsValue)
        {
            $('#hiddenSearchTerm').val(qsValue);
        }
    }
    else if (qsKey == 'SortBy')
    {

        if (qsValue)
        {
            $('#hiddenSortColumn').val(qsValue);
        }
        
    }
    else if (qsKey == 'direction')
    {

        if (qsValue)
        {
            //$('#sortDirection').val(qsValue);
            $('#hiddenSortDirection').val(qsValue);
        }
        
    }
    else if (qsKey == 'datefilter')
    {

        if (qsValue)
        {
            $('#hiddenDateFilter').val(qsValue);
        }
        
    }
    else if (qsKey == 'rsposition') 
    {
        if (qsValue)
        {
            $('#hiddenRSStartPosition').val(qsValue);
        }
            
    }
    else if (qsKey == 'pagesize') 
    {

        if (qsValue) 
        {
            $('#hiddenPageSize').val(qsValue);
            $('#hiddenPageIndex').val('');
        }

    }

}

// This only handles the hidden fields for any filter fields, ignores everything else
function queryStringToHidden_FilterFields(strQS)
{
    var objFilter = getFilter_QSToObj(strQS);
    getFilterSettings_ObjToHidden(objFilter);
}

function fillGridOnLoad()
{
    var bPreviousFilter = ($('#hiddenLastQueryString').val() != '');
    console.log('FillaGridOnLoad: bPreviousFilter=' + bPreviousFilter + ' - prevURL=' + $('#hiddenLastQueryString').val());

    if (bPreviousFilter)
    {
        queryStringToHidden();
    }
        
    fnFillGrid();
    doFilterAlertDisplay();
}

function updateLastPage(count)
{
    pageSize = parseInt(document.getElementById("hiddenPageSize").value, 10);

    if (document.getElementById("hiddenLastPage").value > (count / pageSize))
    {
        document.getElementById("hiddenLastPage").value = Math.ceil(count / pageSize);
    }

}

// Fills hiddenTotalRecords with the total count of records
// Potentially updates hiddenLastPage
function GetRecordCountResponse(xml)
{
    console.log("fnFillGrid - 1 - GetRecordCountResponse");

    try
    {
        var count = xml.getElementsByTagName("theCount");
        count = GetInnerText(count[0]);
        count = parseInt(count, 10);

        if (debug)
        {
            fnDebug("GetRecordCountResponse(xml) count = " + count);
        }

    }
    catch (err)
    {
        console.log(err);
    }

    $('#hiddenTotalRecords').val(count);        // update hiddenTotalRecords
    g_bReceivedRecordCount = true;
    console.log('GetRecordCountResponse: hiddenTotalRecords=' + count);

    var nPageSize = parseInt(document.getElementById("hiddenPageSize").value, 10);
    var nNewLastPage = Math.ceil(count / nPageSize);
    var nOldLastPage = ($('#hiddenLastPage').val(), 10);

    if (nOldLastPage > nNewLastPage)
    {
        $('#hiddenLastPage').val(nNewLastPage); // update last page
    }
    
    ConstructHeading(g_currentPage, g_ProjectType);
    updatePatientListDiv();

    //console.log($(".table-wrapper").scrollHeight);
    //console.log($(".table-wrapper")[0].scrollHeight);

    /*
    $(".table-wrapper").scroll(function () {
        //console.log("div scroll occurred");
        console.log($(".table-wrapper").scrollTop());
        console.log($(".table-wrapper").scrollTop() + $(".table-wrapper").height());

        if (($(".table-wrapper").scrollTop() + $(".table-wrapper").height()) >= (.9 * $(".table-wrapper")[0].scrollHeight))
        {
            console.log("load more records");
        }

    });
    */
}

// Done only when all responses received
function updatePatientListDiv()
{
    console.log("fn: updatePatientListDiv()");

    if (g_strTablePatientList_Heading != '' && g_strTablePatientList_Grid != '')
    {
        //console.log("g_strTablePatientList_Heading:");
        //console.log(g_strTablePatientList_Heading);
        g_strTablePatientList_WrittenOnce = true;
        document.getElementById('divPatientList').innerHTML = g_strTablePatientList_Heading + g_strTablePatientList_Grid;

        ReflectSettingsInGUIHeader();

        g_currentPage = parseInt($('#hiddenCurrentPage').val(), 10);
        //console.log("After all responses received: g_currentPage=" + g_currentPage);
        //setRowsVisibility(g_currentPage);

        var selectedRowIndex = $("#hiddenSelectedRow").val();

        if (selectedRowIndex != "")
        {
            //console.log("selectedRowIndex != ''");
            var viewTable = document.getElementById("tbl1");
            var jqViewTable = $(viewTable);

            //console.log(jqViewTable.find("input[value='" + selectedRowIndex + "']").length);
            //var loopCount = 0;

            if ((jqViewTable.find("input[value='" + selectedRowIndex + "']").length == 0) && (!allRecordsAlreadyLoaded))
            //while((jqViewTable.find("input[value='" + selectedRowIndex + "']").length == 0) && (!allRecordsAlreadyLoaded))
            {
                //console.log("input[value='" + selectedRowIndex + "']");
                //console.log("Found Rows = " + jqViewTable.find("input[value='" + selectedRowIndex + "']").length);
                //console.log("allRecordsAlreadyLoaded = " + allRecordsAlreadyLoaded);
                //console.log("LOADING NEXT GROUP OF RECORDS");

                loadAdditionalRecords();

                //console.log("COMPLETED LOADING NEXT GROUP OF RECORDS");
                /*
                loopCount++;

                if (loopCount > 4)
                {
                    break;
                }
                */
            }
            else
            {
                /*
                console.log("row to select found, but still check if visible area filled");
                HighlightSelectedRow($("#hiddenSelectedRow").val());
                scrollToSelectedStudy();
                */

                //scrollLoadActive = false;
                var scrollableDiv = $(viewTable).closest("div");
                var jqScrollableDiv = $(scrollableDiv);

                console.log("jqViewTable.height:");
                console.log(jqViewTable.height());
                console.log("jqScrollableDiv.height:");
                console.log(jqScrollableDiv.height());

                if ((jqScrollableDiv.height() > jqViewTable.height()) && (!allRecordsAlreadyLoaded))
                {
                    console.log("[fn: updatePatientListDiv] - let's load more records");
                    loadAdditionalRecords();
                }
                else
                {
                    var holdRowId = $("#hiddenSelectedRow").val();

                    if ($("#trSelect" + holdRowId).attr("class") != "selected")
                    {
                        console.log("[fn: updatePatientListDiv] - let us scroll");
                        HighlightSelectedRow(holdRowId);
                        scrollToSelectedStudy();
                    }

                }

            }

        }
        else
        {
            //console.log("selectedRowIndex = ''");
            // check size of displayed list compared to available size  - load more if more screen space is available.  
            var viewTable = document.getElementById("tbl1");
            var jqViewTable = $(viewTable);

            var scrollableDiv = $(viewTable).closest("div");
            var jqScrollableDiv = $(scrollableDiv);

            //console.log("jqViewTable.height:");
            //console.log(jqViewTable.height());
            //console.log("jqScrollableDiv.height:");
            //console.log(jqScrollableDiv.height());

            if ((jqScrollableDiv.height() > jqViewTable.height()) && (!allRecordsAlreadyLoaded))
            {
                //console.log("let's load more records");
                loadAdditionalRecords();
            }
            else
            {
                $(".table-wrapper").scroll(scrollHandler2);
            }

        }

        /*
        if ($("#hiddenSelectedRow").val() != "")
        {
            HighlightSelectedRow($("#hiddenSelectedRow").val());
        }
        */

        /*
        if ($("#hiddenScrollPosition").val() != "")
        {
            $("#mainpanel").scrollTop($("#hiddenScrollPosition").val());
        }
        */
        //scrollToSelectedStudy();
    }

}

function SortBy(column) 
{
    // if sorting on a new column
    var bNewSortColumn = (document.getElementById("hiddenSortColumn").value != column);
    //console.log("SortBy - column=" + column + ", New sort column: " + bNewSortColumn);

    //var bSortDirectionAscending = (document.getElementById("sortDirection").value == "ASC");
    var bSortDirectionAscending = (document.getElementById("hiddenSortDirection").value == "ASC");

    if (bNewSortColumn)
    {
        g_direction = "ASC";
    }
    else
    {
        g_direction = (bSortDirectionAscending ? "DESC" : "ASC");
    }
    
    //document.getElementById("sortDirection").value = g_direction;
    document.getElementById("hiddenSortDirection").value = g_direction;

    document.getElementById("hiddenSortColumn").value = column;
    document.getElementById("hiddenRSStartPosition").value = '0';
    
    // document.getElementById("hiddenSearchTerm").value = "";     // MMC - Refactoring - Try taking this out

    document.getElementById("hiddenFirstPage").value = 1;
    document.getElementById("hiddenCurrentPage").value = 1;
    document.getElementById("hiddenLastPage").value = parseInt(g_maxRecords, 10) / parseInt(g_pageSize, 10);

    if (debug) { fnDebug("SortBy(" + column + ") LastPage: " + document.getElementById("hiddenLastPage").value); }

    fnFillGrid();   // MMC - Refactoring.  fnFillGrid(column, g_direction);
}

function ConvertStudy(studyid, Response) 
{

    if (location.pathname.indexOf(".aspx", 5) == -1) 
    {
        url = location.protocol + "//" + location.hostname + ":" + location.port + location.pathname + globalhidden.convertSvcUrl + "?studyid=" + studyid + "&type="
        url = url.toLowerCase();
        url = url.replace("/process.aspx", "")
    }
    else 
    {
        // MMC - WV 4.3
        var re = /\/[a-zA-Z0-9_]+.aspx/gi;  // grab the leading slash, because so will convertSvcUrl
        var appname = location.pathname.toString();
        appname = appname.replace(re, globalhidden.convertSvcUrl)

        url = location.protocol + "//" + location.hostname + ":" + location.port + appname + "?studyid=" + studyid + "&type="
        //alert('ConvertStudy URL: ' + url);
        url = url.toLowerCase();
        url = url.replace("/process.aspx", "")
    }

    // alert('ConvertStudy - URL: ' + url);

    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onload = function (e)
    {

        if (xhr.readyState === 4)
        {

            if (xhr.status === 200)
            {
                console.log(xhr.responseText);
            }
            else
            {
                console.error(xhr.statusText);
            }

        }

    };
    xhr.onerror = function (e)
    {
        console.error(xhr.statusText);
    };
    xhr.send(null);
    //ajaxpage(url, "debug2");
}

function SearchGrid()
{
    //4.1 DWC when searching, start on page 1
    document.getElementById("hiddenFirstPage").value = 1;
    document.getElementById("hiddenCurrentPage").value = 1;
    fnFillGrid();
}

function HighlightSelectedRow(RowID)
{
    $("tr.selected").attr("style", "");
    //$("tr.selected").attr("class", "gvSelectedRow");
    $("tr.selected").attr("class", "gvSelectedRow gvSelectedRowHover1");

    $("#trSelect" + RowID).attr("class", "selected");
    //4.3 DWC Bold is not necessary to indicate selected row
    //$("#trSelect" + RowID).attr("style", "background-color: #C8C8C8; color: black; font-weight: bold;");
    $("#trSelect" + RowID).attr("style", "background-color: #C8C8C8; color: black;");
}

// Prevent double requesting converthandler.ashx, upon double-click
var selectedStudyID = 0;

function SelectStudy(StudyID, RowID)
{
    if (!filterClick)
    {
        HighlightSelectedRow(RowID);
        document.getElementById("hiddenSelectedStudy").value = StudyID;

        //4.3 DWC Store selected row
        document.getElementById("hiddenSelectedRow").value = RowID;

        //4.3 DWC Store scroll position
        //document.getElementById("hiddenScrollPosition").value = $("#mainpanel").scrollTop();
        //var viewTable = document.getElementById("tbl1");
        //var scrollableDiv = $(viewTable).closest("div");
        var scrollableDiv = $("#tbl1").closest("div");

        //console.log("[fn: SelectStudy] old hiddenScrollPosition value = " + document.getElementById("hiddenScrollPosition").value);
        //document.getElementById("hiddenScrollPosition").value = scrollableDiv.scrollTop();
        $("#hiddenScrollPosition").val(scrollableDiv.scrollTop());
        //console.log("[fn: SelectStudy] new hiddenScrollPosition value = " + document.getElementById("hiddenScrollPosition").value);

        var strNamePlus = $("tr.selected").find("td:eq(0)").html();
        var re = /<input.*chkSelect">/gi;
        var strName = strNamePlus.replace(re, "");
        var strPatientID = $("tr.selected").find("td:eq(1)").html();
        var strDate = $("tr.selected").find("td:eq(2)").html();

        updateInfoBar(strName, strPatientID, strDate);
        updateInfoBarHidden(strName, strPatientID, strDate);

        if (selectedStudyID == StudyID)
        {
            console.log("Skipped 2nd time convert for Study ID " + StudyID);
            return;
        }

        selectedStudyID = StudyID;

        setTimeout(function () { ConvertStudy(StudyID, Response); }, 500);

        function Response(idk)
        {
            //alert(idk)
        }

        // CWT (06/5/20): cache the pdf file for the selected study
        console.log('API/Reports/CacheStudyPdf?studyID=' + selectedStudyID);
        console.log('converthandler.ashx?op=CachePdf&studyID=' + selectedStudyID);

        $.ajax({
            //url: 'converthandler.ashx?op=CachePdf&studyID=' + selectedStudyID
            url: 'API/Reports/CacheStudyPdf?studyID=' + selectedStudyID
        });
    }

    filterClick = false;
}

function OpenStudy(StudyID, RowID)
{
    //4.3 DWC Store scroll position
    //document.getElementById("hiddenScrollPosition").value = $("#mainpanel").scrollTop();
    var viewTable = document.getElementById("tbl1");
    var scrollableDiv = $(viewTable).closest("div");

    //console.log("[fn: OpenStudy] old hiddenScrollPosition value = " + document.getElementById("hiddenScrollPosition").value);
    //document.getElementById("hiddenScrollPosition").value = scrollableDiv.scrollTop();
    $("#hiddenScrollPosition").val(scrollableDiv.scrollTop());
    //console.log("[fn: OpenStudy] new hiddenScrollPosition value = " + document.getElementById("hiddenScrollPosition").value);

    $("#divPatientList").html("<br/><br/><br/><br/><br/><center><p><h1>Opening Study...</h1></p></center>");

    HighlightSelectedRow(RowID);
    document.getElementById("hiddenSelectedStudy").value = StudyID;
        
	var strTabOpenStudyTo = document.getElementById("hiddenpatientlist_OpenStudyTo").value;
    GoToDiv3(strTabOpenStudyTo);
}