﻿
//disable Right Click on all pages
function DisableRightClick() 
{
    document.onmousedown = clickHandler;

    function clickHandler() 
    {
        e = window.event;
        var rightClick = false;
        if (e.which) rightClick = (e.which == 3); //Netscape and Netscape based browser
        else if (e.button) rightClick = (e.button == 2);

        if (rightClick)
        {
            alert("Right click disabled");
        }
        
    }

}

function EnableRightClick() 
{
    document.onmousedown = clickHandler;

    function clickHandler() 
    {
        e = window.event;
        var rightClick = True;
        if (e.which) rightClick = (e.which == 3); //Netscape and Netscape based browser
        else if (e.button) rightClick = (e.button == 2);
    }

}