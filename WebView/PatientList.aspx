﻿<%@ Page Title="Patient List" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" Inherits="DigiSonics.PatientList" ClientIDMode="Static" Codebehind="PatientList.aspx.vb" %>
<%@ MasterType virtualPath="~/Site.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" type="text/css" href="Css/StyleSheet.css" />
    <script language="javascript" type="text/javascript" src="js/GetResponse.js"></script>
    <script language="javascript" type="text/javascript" src="js/PatientList.js"></script>
    <script language="javascript" type="text/javascript" src="js/filterHandler.js"></script>
    <script language="javascript" type="text/javascript" >
        var g_direction = "ASC";
        var g_pageSize = 0;
        var g_currentPage = 1;
        var g_maxRecords = 0;
        var g_ProjectType = '';       //4.2 OB Support
    </script>
    <style type="text/css">
		
		.container 
		{
			clear:both;           
			width:100%; 
			/*
            border-left: solid 1px #CCC;
			border-right: solid 1px #CCC;
			border-bottom: solid 1px #CCC;
			*/
			text-align:left;
		}

		.container h2 { margin-left: 15px;  margin-right: 15px;  margin-bottom: 10px; color: #5685bc; }

		.container p { margin-left: 15px; margin-right: 15px;  margin-top: 10px; margin-bottom: 10px; line-height: 1.3; font-size: small; }

		.container ul { margin-left: 25px; font-size: small; line-height: 1.4; list-style-type: disc; }

		.container li { padding-bottom: 5px; margin-left: 5px;}
		
		div.table-wrapper
		{
            /*
			border: 1px solid #ccc;
            */
            height: calc((.95 * 100vh) - 112px);
			width: 100%;
			overflow-y: auto;
		}
		
		/*
		.container table thead
		{
			text-align: left;
			position: sticky;
			top: 0px;
		}
		*/
		
		.container table thead tr th 
		{
			position: sticky;
			top: 0px;
		}
	</style>
</asp:Content>

<asp:content id="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<input type="text" id="sortDirection" value="ASC" style="color: black; display: none;" />--%>
    <input type="hidden" id="hiddenPageSize" value="20" />
    <input type="hidden" id="hiddenPageIndex" value="2" />
    <input type="hidden" id="hiddenDateFilter" value="<%=Me.objSettings.StudyFilter %>" />
    <input type="hidden" id="hiddenMaxRecords" value="<%=Me.objSettings.MaxRecords %>" />
<%--    <input type="hidden" id="hiddenSortColumn" value="name"/>
    <input type="hidden" id="hiddenSearchTerm" />--%>
    <input type="hidden" id="hiddenRSStartPosition" value="0" />
    <input type="hidden" id="hiddenPageLink" />
    <input type="hidden" id="hiddenClientStudyIndex" />
    <input type="hidden" id="hiddenCurrentPage" value="<%=m_strCurrPage %>"/>
    <input type="hidden" id="hiddenTotalRecords" />
    <input type="hidden" id="hiddenFirstPage" />
    <input type="hidden" id="hiddenLastPage" />
    <input type="hidden" id="hiddenCurrentForm" />
<%--    <input type="hidden" id="hiddenFilterTerm" />
    <input type="hidden" id="hiddenFilterTerm2" />
    <input type="hidden" id="hiddenFilterTerm3" />
    <input type="hidden" id="hiddenFilterTerm4" />
    <input type="hidden" id="hiddenFilterTerm5" />
    <input type="hidden" id="hiddenFilterTerm6" />
    <input type="hidden" id="hiddenFilterTerm7" />
    <input type="hidden" id="hiddenFilterTerm8" />
    <input type="hidden" id="hiddenFilterTerm9" />
    <input type="hidden" id="hiddenFilterTerm10" />
    <input type="hidden" id="hiddenFilterCol" />
    <input type="hidden" id="hiddenFilterCol2" />
    <input type="hidden" id="hiddenFilterCol3" />
    <input type="hidden" id="hiddenFilterCol4" />
    <input type="hidden" id="hiddenFilterCol5" />
    <input type="hidden" id="hiddenFilterCol6" />
    <input type="hidden" id="hiddenFilterCol7" />
    <input type="hidden" id="hiddenFilterCol8" />
    <input type="hidden" id="hiddenFilterCol9" />
    <input type="hidden" id="hiddenFilterCol10" />--%>
    <input type="hidden" id="hiddenLastQueryString" value="<%=Session("LastPatientListQuery") %>"/>
    <!--<input type="hidden" id="hiddenScrollPosition" value="<%=m_strScrollPosition %>" />-->
    <asp:Literal ID="litRecall" runat="server"></asp:Literal> 
    <input type="hidden" id="hiddenpatientlist_OpenStudyTo" value="<%=Me.OpenStudyTo.Value %>" />
    <div id="divPatientList" style="position: absolute; left: 1%; top: 5%; right: 1%; width: 98%; ">
    </div>    
</asp:content>

<asp:Content ID="Content3" ContentPlaceHolderID="endofbody" Runat="Server">
    <script src="js/filterReset.js" type="text/javascript"></script>
</asp:Content>
