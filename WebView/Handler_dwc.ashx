﻿<%@ WebHandler Language="VB" Class="Handler" %>

Imports DigiSonics
Imports wvSessions
Imports System
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.SessionState
Imports System.Xml

Public Class Handler : Implements IHttpHandler
    Public _context As HttpContext
    Public userlevel As String = 0
    Public username As String = ""
    Dim id As String = ""

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim appsettings As NameValueCollection
        
        Dim session_guid As String = System.Web.HttpContext.Current.Request.QueryString("guid").ToString()
        

        appsettings = modSettings.updateSettings()
        
        If IsValidSession(session_guid, userlevel, username) Then
            _context = context
        
            'This is a get count query, count the number of records and return in XML format
            If System.Web.HttpContext.Current.Request.QueryString("count").ToString() = "get" Then
            
                context.Response.ContentType = "Application/xml"
            
                Dim xmlCount As XmlTextWriter = New XmlTextWriter(context.Response.Output)
                
                Dim DS As New DataSet
                Dim whereclause As String = ""
        
                Dim SortBy As String = System.Web.HttpContext.Current.Request.QueryString("SortBy").ToString()
                Dim isdate As Boolean = False
                If SortBy = "[study date]" Or SortBy = "[Study Date]" Then isdate = True
                If SortBy = "undefined" Or SortBy = "" Then
                    SortBy = "[Name]"
                End If
        
                Dim Direction As String = System.Web.HttpContext.Current.Request.QueryString("direction").ToString()
        
                If Direction = "undefined" Or Direction = "" Then
                    Direction = "ASC"
                End If
        
                Dim SearchText As String = System.Web.HttpContext.Current.Request.QueryString("searchtext").ToString()
                Dim studyID As String = System.Web.HttpContext.Current.Request.QueryString("studyid").ToString()
                Dim dateFilter As String = System.Web.HttpContext.Current.Request.QueryString("datefilter").ToString()
        
                Dim sortPosition As String = System.Web.HttpContext.Current.Request.QueryString("sortposition").ToString()
                Dim maxRecords As String = objSettings.MaxRecords
                Dim lastRow As Integer = Convert.ToInt32(maxRecords) + Convert.ToInt32(sortPosition) - 1
        
                SearchText = SearchText.Replace("=", "")
                SearchText = SearchText.Replace("--", "")
                SearchText = SearchText.Replace(";", "")
        
                If SearchText = "undefined" Or SearchText = "" Then
                    SearchText = ""
                    whereclause = ""
            
                    If studyID <> "undefined" Then
                        Select Case objSettings.SingleStudy

                            Case "SelectedStudy"

                                whereclause = "WHERE [Study ID] = '" & studyID & "'"
                            Case "SelectedPatient"

                                whereclause = "WHERE [id] = '" & StudyIdToPID(studyID) & "'"
                            Case "ShowAll"
                                whereclause = ""
                            Case Else
                                whereclause = "WHERE [Study ID] = '" & studyID & "'"
                        End Select
            
                        If objSettings.VisibleStatuses = "FinalRevised" Then
                            whereclause = whereclause & " AND [Study Status] in ('Final','Revised')"
                        ElseIf objSettings.VisibleStatuses = "FinalRevisedPrelim" Then
                            whereclause = whereclause & " AND [Study Status] in ('Preliminary','Final','Revised')"
                        End If
                    Else
                        If objSettings.VisibleStatuses = "FinalRevised" Then
                            whereclause = whereclause & " WHERE [Study Status] in ('Final','Revised')"
                        ElseIf objSettings.VisibleStatuses = "FinalRevisedPrelim" Then
                            whereclause = whereclause & " WHERE [Study Status] in ('Preliminary','Final','Revised')"
                        End If
                    End If
                Else
                    If isdate Then
                        whereclause = "WHERE " & SortBy & " = '" & SearchText & "'"
                    Else
                        whereclause = "WHERE " & SortBy & " LIKE '" & SearchText & "%'"
                    End If
            
                    If studyID <> "undefined" Then
                        Select Case objSettings.SingleStudy
                            Case "SelectedStudy"
                                whereclause = whereclause & " WHERE [Study ID] = '" & studyID & "'"
                            Case "SelectedPatient"
                                whereclause = whereclause & " AND [id] = '" & StudyIdToPID(studyID) & "'"
                            Case "ShowAll"
                                whereclause = whereclause
                            Case Else
                                whereclause = whereclause & " AND [Study ID] = '" & studyID & "'"
                        End Select
                    End If
                
                    If objSettings.VisibleStatuses = "FinalRevised" Then
                        whereclause = whereclause & " AND [Study Status] in ('Final','Revised')"
                    ElseIf objSettings.VisibleStatuses = "FinalRevisedPrelim" Then
                        whereclause = whereclause & " AND [Study Status] in ('Preliminary','Final','Revised')"
                    End If
        
                End If
        
                If dateFilter <> "undefined" And dateFilter <> "7" Then
                    If whereclause = "" Then
                        whereclause = " WHERE [Study Date] "
                    Else
                        whereclause = whereclause & " AND [Study Date] "
                    End If
            
                    Select Case dateFilter
                        Case "0"
                            whereclause = whereclause & " = '" & DateTime.Now.ToShortDateString() & "'"
                        Case "1"
                            whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-3).ToShortDateString() & "'"
                        Case "2"
                            whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-7).ToShortDateString() & "'"
                        Case "3"
                            whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-30).ToShortDateString() & "'"
                        Case "4"
                            whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-90).ToShortDateString() & "'"
                        Case "5"
                            whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-180).ToShortDateString() & "'"
                        Case "6"
                            whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-365).ToShortDateString() & "'"

                    End Select
            
                End If

                Dim SqlString As String = ""

                SqlString = "SELECT COUNT(*) as theCount "
                ' SqlString = SqlString & " (SELECT Row_Number() OVER (ORDER BY " & SortBy & " " & Direction & ") AS RowNumber, * "
                SqlString = SqlString & " FROM Study " & whereclause & " @FilterClause "
        

        
                DS = Query(SqlString, "count")
                xmlCount.WriteStartElement("Root")
                
            
                'Dim sqlString As String = ""
                
                'sqlString = "SELECT COUNT(*) as theCount FROM Study"
                
                'Dim DS As New DataSet
                
                            
                Try
                    If (DS.Tables.Count > 0) Then
            
                        For Each dr As DataRow In DS.Tables(0).Rows
                
                            xmlCount.WriteStartElement("Records")
                
                            xmlCount.WriteElementString("theCount", dr("theCount").ToString())
                
                            xmlCount.WriteEndElement()
                
                        Next
                    
                        xmlCount.WriteEndElement()
                    
                    End If
                    
                    '4.x problem with ob support
                    DS = Nothing
                    
                Catch ex As Exception
            
                    ErrHandler.WriteError(ex.Message)
                    DS = Nothing
                    
                Finally
                    
                    DS = Nothing
                    
                End Try
            Else
                context.Response.ContentType = "Application/xml"
                '        context.Response.Cache.SetCacheability(Cacheability)
                Dim xml As XmlTextWriter = New XmlTextWriter(context.Response.Output)
        
                xml.WriteStartElement("Root")

                GetXML(xml)
        
                xml.WriteEndElement()
            End If            
        End If
    End Sub
    
    Private Sub GetXML(ByVal xml As XmlTextWriter)
        
        Dim DS As New DataSet
        Dim whereclause As String = ""
        
        Dim SortBy As String = System.Web.HttpContext.Current.Request.QueryString("SortBy").ToString()
        Dim isdate As Boolean = False
        If SortBy = "[study date]" Or SortBy = "[Study Date]" Then isdate = True
        If SortBy = "undefined" Or SortBy = "" Then
            SortBy = "[Name]"
        End If
        
        Dim Direction As String = System.Web.HttpContext.Current.Request.QueryString("direction").ToString()
        
        If Direction = "undefined" Or Direction = "" Then
            Direction = "ASC"
        End If
        
        Dim SearchText As String = System.Web.HttpContext.Current.Request.QueryString("searchtext").ToString()
        Dim studyID As String = System.Web.HttpContext.Current.Request.QueryString("studyid").ToString()
        Dim dateFilter As String = System.Web.HttpContext.Current.Request.QueryString("datefilter").ToString()
        
        Dim sortPosition As String = System.Web.HttpContext.Current.Request.QueryString("sortposition").ToString()
        Dim maxRecords As String = objSettings.MaxRecords
        Dim lastRow As Integer = Convert.ToInt32(maxRecords) + Convert.ToInt32(sortPosition) - 1
        
        SearchText = SearchText.Replace("=", "")
        SearchText = SearchText.Replace("--", "")
        SearchText = SearchText.Replace(";", "")
        
        If SearchText = "undefined" Or SearchText = "" Then
            SearchText = ""
            whereclause = ""
            
            If studyID <> "undefined" Then
                Select Case objSettings.SingleStudy

                    Case "SelectedStudy"

                        whereclause = "WHERE [Study ID] = '" & studyID & "'"
                    Case "SelectedPatient"

                        whereclause = "WHERE [id] = '" & StudyIdToPID(studyID) & "'"
                    Case "ShowAll"
                        whereclause = ""
                    Case Else
                        whereclause = "WHERE [Study ID] = '" & studyID & "'"
                End Select
            
                If objSettings.VisibleStatuses = "FinalRevised" Then
                    whereclause = whereclause & " AND [Study Status] in ('Final','Revised')"
                ElseIf objSettings.VisibleStatuses = "FinalRevisedPrelim" Then
                    whereclause = whereclause & " AND [Study Status] in ('Preliminary','Final','Revised')"
                End If
            Else
                If objSettings.VisibleStatuses = "FinalRevised" Then
                    whereclause = whereclause & " WHERE [Study Status] in ('Final','Revised')"
                ElseIf objSettings.VisibleStatuses = "FinalRevisedPrelim" Then
                    whereclause = whereclause & " WHERE [Study Status] in ('Preliminary','Final','Revised')"
                End If
            End If
        Else
            If isdate Then
                whereclause = "WHERE " & SortBy & " = '" & SearchText & "'"
            Else
                whereclause = "WHERE " & SortBy & " LIKE '" & SearchText & "%'"
            End If
            
            If studyID <> "undefined" Then
                Select Case objSettings.SingleStudy
                    Case "SelectedStudy"
                        whereclause = whereclause & " WHERE [Study ID] = '" & studyID & "'"
                    Case "SelectedPatient"
                        whereclause = whereclause & " AND [id] = '" & StudyIdToPID(studyID) & "'"
                    Case "ShowAll"
                        whereclause = whereclause
                    Case Else
                        whereclause = whereclause & " AND [Study ID] = '" & studyID & "'"
                End Select
            End If
                
            If objSettings.VisibleStatuses = "FinalRevised" Then
                whereclause = whereclause & " AND [Study Status] in ('Final','Revised')"
            ElseIf objSettings.VisibleStatuses = "FinalRevisedPrelim" Then
                whereclause = whereclause & " AND [Study Status] in ('Preliminary','Final','Revised')"
            End If
        
        End If
        
        If dateFilter <> "undefined" And dateFilter <> "7" Then
            If whereclause = "" Then
                whereclause = " WHERE [Study Date] "
            Else
                whereclause = whereclause & " AND [Study Date] "
            End If
            
            Select Case dateFilter
                Case "0"
                    whereclause = whereclause & " = '" & DateTime.Now.ToShortDateString() & "'"
                Case "1"
                    whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-3).ToShortDateString() & "'"
                Case "2"
                    whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-7).ToShortDateString() & "'"
                Case "3"
                    whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-30).ToShortDateString() & "'"
                Case "4"
                    whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-90).ToShortDateString() & "'"
                Case "5"
                    whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-180).ToShortDateString() & "'"
                Case "6"
                    whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-365).ToShortDateString() & "'"

            End Select
            
        End If

        Dim SqlString As String = ""
        
        If sortPosition = "0" Then
            SqlString = "SELECT TOP " & objSettings.MaxRecords & " [Study ID],[Name],[ID],CONVERT(varchar(10),[Study Date], 101) AS [StudyDate],CONVERT(varchar(5),[Study Time],108) AS [Study Time],[Referring MD],[Interpreting MD],[Study Type],[Study Status] FROM Study " & whereclause & " @FilterClause ORDER BY " & SortBy & " " & Direction
        Else
            SqlString = "SELECT [Study ID],[Name],[ID],CONVERT(varchar(10),[Study Date], 101) AS [StudyDate],CONVERT(varchar(5),[Study Time],108) AS [Study Time],[Referring MD],[Interpreting MD],[Study Type],[Study Status] FROM "
            SqlString = SqlString & " (SELECT Row_Number() OVER (ORDER BY " & SortBy & " " & Direction & ") AS RowNumber, * "
            SqlString = SqlString & " FROM Study " & whereclause & ") AS RowConstrainedResult WHERE RowNumber BETWEEN " & sortPosition & " AND " & lastRow & " @FilterClause "
        End If
        

        
        DS = Query(SqlString)
        ' WHERE " & SortBy & " LIKE '" & SearchText & "%'
        
        Try
            If (DS.Tables.Count > 0) Then
            
                For Each dr As DataRow In DS.Tables(0).Rows
                
                    xml.WriteStartElement("Study")
                
                    xml.WriteElementString("StudyID", dr("Study ID").ToString())
                    xml.WriteElementString("Name", dr("Name").ToString())
                    xml.WriteElementString("ID", dr("ID").ToString())
                    xml.WriteElementString("StudyDate", dr("StudyDate").ToString())
                    xml.WriteElementString("StudyTime", dr("Study Time").ToString())
                    xml.WriteElementString("ReferringMD", dr("Referring MD").ToString())
                    xml.WriteElementString("InterpretingMD", dr("Interpreting MD").ToString())
                    xml.WriteElementString("StudyType", dr("Study Type").ToString())
                    xml.WriteElementString("StudyStatus", dr("Study Status").ToString())
                
                    xml.WriteEndElement()
                
                Next
        
            End If

        Catch ex As Exception
            
            ErrHandler.WriteError(ex.Message)
        
        End Try
        

    End Sub
    

    Private Function Query(SQL As String, Optional DB As String = "ERSRDB") As DataSet

        Dim usercon As New SqlConnection(DecryptedUserAccountsConnectionstring)   'ConfigurationManager.ConnectionStrings("UserAccountsConnectionstring").ConnectionString)  'appsettings("UserAccountsConnectionstring"))
        'Dim usercon As New SqlConnection(ConfigurationManager.ConnectionStrings("UserAccountsConnectionstring").ConnectionString)  'appsettings("UserAccountsConnectionstring"))
        Dim usercmd As SqlCommand = usercon.CreateCommand()
        Dim FilterClause As String = ""
        Dim FilterParse As String()
        Dim CountClause As String = ""
        

        Dim strPath As String
        Select Case DB
            Case "ERSRDB"
                strPath = DecryptedSQLServerConnectionstring 'ConfigurationManager.ConnectionStrings("SQLServerConnectionstring").ConnectionString
            Case "count"
                strPath = DecryptedSQLServerConnectionstring 'ConfigurationManager.ConnectionStrings("SQLServerConnectionstring").ConnectionString
            Case Else
                strPath = DecryptedSQLServerConnectionstring_Images 'ConfigurationManager.ConnectionStrings("SQLServerConnectionstring_images").ConnectionString
        End Select
        
        'If DB = "ERSRDB" Then
        '    strPath = DecryptedSQLServerConnectionstring 'ConfigurationManager.ConnectionStrings("SQLServerConnectionstring").ConnectionString
        'Else
        '    strPath = DecryptedSQLServerConnectionstring_Images 'ConfigurationManager.ConnectionStrings("SQLServerConnectionstring_images").ConnectionString
        'End If

        Dim Conn As New System.Data.SqlClient.SqlConnection(strPath)
        Dim cmd As System.Data.SqlClient.SqlCommand = Conn.CreateCommand()
        cmd.CommandType = CommandType.Text


        If appsettings("database_type") = "OBW" Then
            SQL = SQL.Replace("[Study ID]", "RecordID As [Study ID]")
            SQL = SQL.Replace("[Study Date]", "StudyDate")
            SQL = SQL.Replace("[study date]", "StudyDate")
            SQL = SQL.Replace("[Study Time],108) AS [Study Time]", "[StudyTime],108) AS [Study Time]")
            SQL = SQL.Replace("ORDER BY [Referring MD]", "ORDER BY RefMD")
            SQL = SQL.Replace("ORDER BY [Interpreting MD]", "ORDER BY InterpretingMD")
            SQL = SQL.Replace("[Interpreting MD]", "InterpretingMD AS [Interpreting MD]")
            SQL = SQL.Replace("[Referring MD]", "RefMD AS [Referring MD]")
            SQL = SQL.Replace("[Study Type]", "StudyType AS [Study Type]")
            SQL = SQL.Replace("[Study Status]", "Status AS [Study Status]")
            SQL = SQL.Replace("FROM Study", "FROM Table1")
        End If
        '        Dim reader As System.Data.SqlClient.SqlDataReader
        
        cmd.CommandText = SQL
        
        If userlevel = "0" Then
            usercmd.CommandType = CommandType.StoredProcedure
            usercmd.CommandText = "GetFilterInfo"
            usercmd.Parameters.Add("@UserName", SqlDbType.VarChar, 20)
            usercmd.Parameters("@UserName").Value = username

            Dim result As String
            Try
                usercon.Open()
                result = CStr(usercmd.ExecuteScalar())
            Catch ex As Exception
                ErrHandler.WriteError(ex.Message())
                System.Diagnostics.Debug.Write(ex.Message)
            Finally
                usercon.Close()
            End Try


            'Build query clause
            'Just in case filter isn't there, we won't crash
            If Len(result) <> 0 Then
                If InStr(SQL, "WHERE") > 0 Then
                    FilterClause = "AND ("
                Else
                    FilterClause = "WHERE ("
                End If
                FilterParse = result.Split("|")
                
                Dim sz As String
                For Each sz In FilterParse
                    If Not ((sz = "||") Or (sz = "")) Then
                        FilterClause &= "( [Referring MD]" & " = " + "'" + sz + "'" + " or " & "[Interpreting MD]" & " = " + "'" + sz + "'" + ") or "
                        CountClause &= sz + ", "
                    End If
                Next sz
                ' Chop off last " or "'
                FilterClause = FilterClause.Remove(FilterClause.Length - 3, 3)
                CountClause = CountClause.Remove(CountClause.Length - 1, 1)
                FilterClause &= ") "
                '                cmd.CommandText = appsettings("filtermore").Replace("@NameList", GetDistinctValues(search, orderby, start, searchon))
                cmd.CommandText = cmd.CommandText.Replace("@FilterClause", FilterClause)

            End If
        Else
            cmd.CommandText = cmd.CommandText.Replace("@FilterClause", "")
        End If
        
        Try
            Conn.Open()
        
            Dim Reader As SqlDataAdapter = New SqlDataAdapter(cmd)
            Dim DS As New DataSet
            Reader.Fill(DS)
            Return DS
        
        Catch exception As Exception
            
            ErrHandler.WriteError(exception.Message())
            Conn.Close()
        Finally
            Conn.Close()
        End Try

    End Function
    
    Private Function StudyIdToPID(ByVal StudyId As String) As String


        Dim strPath As String
        Dim Sql As String = "SELECT ID FROM Study WHERE [Study ID] = " & StudyId

        strPath = DecryptedSQLServerConnectionstring ' ConfigurationManager.ConnectionStrings("SQLServerConnectionstring").ConnectionString


        Dim Conn As New System.Data.SqlClient.SqlConnection(strPath)
        Dim cmd As System.Data.SqlClient.SqlCommand = Conn.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = Sql

        Try
            Conn.Open()

            Dim Reader As SqlDataReader = cmd.ExecuteReader()

            Reader.Read()

            id = Reader("ID").ToString()


            Return id

        Catch exception As Exception
            ErrHandler.WriteError(exception.Message())
        Finally
            Conn.Close()
        End Try

        '        End If

    End Function
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class