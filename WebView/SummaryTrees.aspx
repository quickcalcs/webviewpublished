﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="DigiSonics.SummaryTrees" Codebehind="SummaryTrees.aspx.vb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title></title>
    </head>
    <body>
        <form id="summaryTreeForm" runat="server">
            <div>
                <asp:TreeView ID="TreeComments" runat="server" Target="_self" Height="300px" style="background-color:white;" BorderColor="White" BorderStyle="Solid" BorderWidth="1px" NodeWrap="true"></asp:TreeView>
                <asp:TreeView ID="TreeMacros" runat="server" Target="_self" Height="300px" style="background-color:white;" BorderColor="White" BorderStyle="Solid" BorderWidth="1px"></asp:TreeView>
            </div>
        </form>
    </body>
</html>