﻿<%@ Page Title="Report" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" Inherits="DigiSonics.WVReport"  ClientIDMode="Static" Codebehind="Report.aspx.vb" %>
<%@ MasterType virtualPath="~/Site.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        #thediv
        {
            margin: auto;
            height: 93%;
            width: 100%;
            overflow-y: scroll;
            overflow-x: auto;
            bottom: 0;
            position: absolute;
        }
        
        img
        {
            position: relative;
            top: auto;
        }

        .loadingBackground
        {
            background: transparent url('./Images/test_spinner.gif') center no-repeat;
            height: 100%;
        }
        
        #pic
        {
            background-color: white;
        }

        .pic
        {
            background-color: white;
        }

        .zoomButton
        {
            width: 24px;
        }

        .actionButtonBar
        {
            margin-left: 15px;
        }

        .noTitlePopup .ui-dialog-titlebar 
        {
            display:none;
        }
    </style>
     <!-- 4.3 Jquery API -->
    <script type="text/javascript" src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" src="https://kit.fontawesome.com/4e9855bca9.js"></script>
    <asp:Literal ID="litFields" runat="server"></asp:Literal>
    <script type="text/javascript">
        function GetHyperlinks(Keys)
        {
            var strReturn = ''; array = {};
            var arKeys = Keys.split(',');

            for (i = 0; i < arKeys.length; i++)
            {

                if (strReturn == '')
                {
                    strReturn = (array[arKeys[i]]);
                }
                else
                {
                    strReturn = strReturn + ',' + (array[arKeys[i]]);
                }
                    
            }

            return strReturn;
        }

        function showPdfNotFoundMessage()
        {
            //alert('inside showPdfNotFoundMessage');
            //$('.actionButtonBar').hide();
            $('.loadingBackground').hide();
            $('#PDFNotFound').show();
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:literal id="litReport" runat="server"></asp:literal>
    <br />
    <asp:Literal ID="loadRtf" runat="server"></asp:Literal>
    <asp:Literal ID="saveRtf" runat="server"></asp:Literal>
    <input type="hidden" id="hiddenTrackingID" value="0"/>
    <input type="hidden" id="hiddenRetainLock" value="0"/>
    <div class="actionButtonBar">
        <asp:HiddenField runat="server" ID="ReportPageCount" />
        <input type="button" class="toggleEdit zoomButton" title="Zoom Out" value="-" onclick="zoom(0.9)" />
        <input type="button" class="toggleEdit zoomButton" title="Zoom In" value="+" onclick="zoom(1.1)" />
        <input type="button" class="toggleEdit" title="Previous Page" value="<" onclick="PreviousPage();" style="display: none;" />
        <input type="text" id="CurrentPage" value="1" style="width: 25px; text-align: center; display: none;" readonly />
        <input type="button" class="toggleEdit" title="Next Page" value=">" style="display: none;" onclick="nextPage();" />
        <input type="button" class="toggleEdit" value="Edit Summary" onclick="ToggleEditMode(true); InvokeHandler('EditSummaryBegin', 1 + 2 + 4 + 16);" />
        <input type="button" class="toggleEdit" value="E-Sign" onclick="ToggleEditMode(true); InvokeHandler('EditSummaryBegin', 1 + 2 + 4 + 16); InvokeHandler('CanESign', 1 + 2 + 4 + 16);" />
        <!--<input type="button" value="Close Summary" onclick="closeSummary()" />-->
    </div>
    <div id="PDFRefresh" class="Message" style="display: none; z-index: 3;" >
        <center>
            <h1>Hang on, almost there...</h1>
        </center>
    </div>
    <div id="PDFNotFound" class="Message" style="display: none; z-index: 3;" >
        <center>
            <h1>No Report File Found</h1>
        </center>
    </div>
    <div id="thediv">
        <center class="loadingBackground">
            <asp:Literal id="Literal1" Text="" runat="server" />
        </center>
    </div>

    <!-- Summary Start -->
    <div id="report-summary-wrapper">
        <div id="summaryHead" class="summaryHead">
            <div id="summaryHeadLeft" class="summaryHeadLeft">
                <input type="radio" name="cmtsmacros" id="comments" value="Comments" checked="checked" />
                <label for="comments">Comments</label>
                <input type="radio" name="cmtsmacros" id="macros" value="Macros" />
                <label for="macros">Macros</label>
            </div>
            <div class="summaryToolTip">Click on entry below to use report macro</div>
            <div id="summaryHeadRight" class="summaryHeadRight">
                <i class="fa fa-window-minimize" aria-hidden="true" onclick="minimizeWindowRpt()" title="Minimize"></i>
                <i class="fa fa-square-o" aria-hidden="true" onclick="maximizeWindowRpt()" title="Maximize"></i>
                <i class="fa fa-clone" aria-hidden="true" onclick="restoreWindowRpt()" title="Restore" style="display: none; " ></i>
                <i class="fa fa-times" aria-hidden="true" onclick="doEditCommitOnX(); closeSummary()" title="Close Window"></i>
            </div>
        </div>
        <div class="macroHead">
            <!--<div class="summaryToolTip">Click on entry below to use report macro</div>-->
        </div>
        <div id="summaryMacroSection" class="summaryMacroSection">
            <div id="SummaryTree" contenteditable="false" class="summary">
                <asp:TreeView ID="TreeComments" runat="server" Target="_self" Height="300px" style="background-color: white; " BorderColor="White" BorderStyle="Solid" BorderWidth="1px" NodeWrap = "true"></asp:TreeView>
                <asp:TreeView ID="TreeMacros" runat="server" Target="_self" Height="300px" style="background-color:white;" BorderColor="White" BorderStyle="Solid" BorderWidth="1px" ></asp:TreeView>
            </div>
        </div>
        <div id="summaryRibbon">
            <select id="summaryFontFamily">
                <option value="Times New Roman">Times New Roman</option>
                <option value="Times New Roman Baltic">Times New Roman Baltic</option>
                <option value="Times New Roman CE">Times New Roman CE</option>
                <option value="Times New Roman CYR">Times New Roman CYR</option>
                <option value="Times New Roman Greek">Times New Roman Greek</option>
                <option value="Times New Roman TUR">Times New Roman TUR</option>
                <option value="Trebuchet MS">Trebuchet MS</option>
                <option value="Verdana">Verdana</option>
            </select>
            <select id="summaryFontSize">
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="36">36</option>
                <option value="48">48</option>
                <option value="72">72</option>
            </select>
            <button type="button" id="btnBold" class="sm-btn" title="Bold"><i class="fas fa-bold" onclick="btnBold()"></i></button>
            <button type="button" id="btnItalic" class="sm-btn"><i class="fas fa-italic" onclick="btnItalic()"></i></button>
            <button type="button" id="btnUnderline" class="sm-btn"><i class="fas fa-underline" onclick="btnUnderline()"></i></button>

            <button type="button" id="btnLeftAlignText" class="sm-btn"><i class="fas fa-align-left" onclick="btnAlignLeft()"></i></button>
            <button type="button" id="btnMiddleAlignText" class="sm-btn"><i class="fas fa-align-justify" onclick="btnAlignMiddle()"></i></button>
            <button type="button" id="btnRightAlignText" class="sm-btn"><i class="fas fa-align-right" onclick="btnAlignright()"></i></button>
            <button type="button" id="btnBulletPoint" class="sm-btn"><i class="fas fa-list" onclick="btnBulletPoint()"></i></button>

            <button type="button" id="btnCut" class="sm-btn"><i class="fas fa-cut" onclick="btnCut()"></i></button>
            <button type="button" id="btnCopy" class="sm-btn"><i class="fas fa-copy" onclick="btnCopy()"></i></button>
            <button type="button" id="btnPaste" class="sm-btn"><i class="fas fa-clipboard" onclick="btnPaste()"></i></button>
            <button type="button" id="btnUndo" class="sm-btn"><i class="fas fa-undo" onclick="btnUndo()"></i></button>

            <button type="button" id="btnClear" onclick="TXTextControl.resetContents();">Clear</button>

            <button type="button" id="btnSpellCheck" class="sm-btn"><i class="fas fa-spell-check" onclick="btnSpellCheck()"></i></button>
            <button type="button" id="btnLink" class="sm-btn"><i class="fas fa-link" onclick="btnLink()"></i></button>
            <button type="button" id="btnParagraph" class="sm-btn"><i class="fas fa-paragraph" onclick="btnParagraph()"></i></button>
        </div>
        <div id="reportRichText" class="summaryRichText">
        </div>
        <div class="summaryBottom">
            <div class="summarybottomLeft">
                <input type="button" id="ButtonOKSummary" value="OK" title="Save and Close" onclick="saveTxTextAndSend();" />
                <input type="button" id="btnCancelSummary" value="Cancel" onclick="cancelEdit()" title="Close Window" />
            </div>
            <div class="summarybottomRight">
                <input ID="eSig" type="button" value="E-Signature" title="Electronic Signature" onclick="InvokeHandler('CanESign', 1 + 2 + 4 + 16);"/>
            </div>
        </div>
    </div>

    <!-- Start modal pop up for signature -->
    <div id="modalEsig" class="hover_bkgr_fricc">
        <span class="helper"></span>
        <div>
            <!--<div class="popupCloseButton">X</div>-->
            <p>You cannot electronically sign the current study until a Interpreting MD is selected. <br />Please select one below, or press cancel to exit.</p>
            <select id="modalSelectIMD" name="modalSelectIMD" class="modalSelectMD">
              <option value="" disabled selected>Interpreting MD</option>
              <option value="Doc1">Doc1</option>
              <option value="Doc2">Doc2</option>
              <option value="Doc3">Doc3</option>
              <option value="Doc4">Doc4</option>
            </select>
            <input type="text" name="drCodes" class="modalDrCodes" placeholder="Dr. Codes" />
            <input type="button" ID="DoEsign" value="OK" title="Confirm Electronic Signature" onclick="InvokeHandler('DoESign', 1 + 2 + 4 + 8);" />
            <input id="modalCancel" type="button" value="Cancel" onclick="" />
        </div>
    </div>

    <!-- Start modal pop up for canEsign Failure -->
    <div id="modalEsignFail" class="hover_bkgr_fricc">
        <span class="helper"></span>
        <div>
            <!--<div class="popupCloseButton">X</div>-->
            <p>Sorry, You must be the interpreting MD to edit the summary</p>
            <input id="modalEsignFailCancel" type="button" value="OK" onclick="" />
        </div>
    </div>

    <!-- Start modal pop up for You are not interpreting MD -->
    <div id="modalWrongMD" class="hover_bkgr_fricc">
        <span class="helper"></span>
        <div>
            <!--<div class="popupCloseButton">X</div>-->
            <p>Sorry, You must be the interpreting MD to edit the summary</p>
            <input id="modalCancelWrongMD" type="button" value="OK" onclick="" />
        </div>
    </div>

    <!-- Start modal pop up for this study is locked by -->
    <div id="modalLockedBy" class="hover_bkgr_fricc">
		<span class="helper"></span>
		<div>
			<div class="header">
				Study Locked
			</div>
			<hr />
			<div class="modal-content">
				<p>Sorry, this study has been locked by: <span id="lockedByInput"></span></p>
			</div>
			<div class="actions">
			    <input id="modalCancelLockedBy" type="button" value="OK" onclick="" />
			</div>
		</div>
    </div>

    <!-- Start Tree Pop Up Box for P key -->
    <div id="HyperLinkDiv" class="HyperLinkDiv">
        <div id="HyperLinkHead">
            <div></div>
            <div>Responses</div>
            <i class="fa fa-times" onclick="closeHyperLinkDiv()" aria-hidden="true" title="Close"></i>
        </div>
        <div id="HyperLinkCurrent">
            <input type="text" id="currentSelection"/>
        </div>
        <ul id="HyperlinkList">
        </ul>
        <div id="HyperLinkActions">
            <button type="button" id="HyperLinkOk" onclick="updateHyPerLink()" >OK</button>
            <%--
            <button type="button" id="HyperLinkAdd" onclick="addCustomListItem()" >Add</button>
            <button type="button">Delete</button>
            --%>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="endofbody" Runat="Server">
    <form id="SummaryEditForm" action="SummaryHandler.ashx">
    <input type="hidden" name="hiddenNewValComments" id="hiddenNewValComments"/>
    <input type="hidden" name="hiddenNewValSummaryString" id="hiddenNewValSummaryString"/>
    <input type="hidden" name="hiddenNewValTxText" id="hiddenNewValTxText"/>
    </form>

    <script type="text/javascript">
        var msecAutosave = 10000;       // 10 sec - autosave cycle
        var msecDelayPDFRegen = 100;    // Regenerate PDF - 100 ms after Summary commit/autosave
        TXTextControl.zoomFactor = 150;
        /*
        $(document).ready(function ()
        {
            TXTextControl.showRibbonBar(false);
        });
        */
    </script>
    <script src="js/FormPostHandler.js" type="text/javascript"></script>
    <script src="js/summaryHandler.js" type="text/javascript"></script>
    <script src="js/summaryGUI.js" type="text/javascript"></script>
    <script src="js/SummaryEditor.js" type="text/javascript"></script>
    <script src="js/TxText1.js" type="text/javascript"></script>
    <script src="js/SummaryResponses.js" type="text/javascript"></script>
    <script src="js/editSummary.js" type="text/javascript"></script>
    <script src="JS/Report.aspx.js" type="text/javascript"></script>
</asp:Content>