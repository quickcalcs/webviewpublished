﻿<%@ WebHandler Language="VB" Class="Handler" %>

Imports DigiSonics
Imports wvSessions
Imports System
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.SessionState
Imports System.Xml
Imports System.Diagnostics

Public Class Handler : Implements IHttpHandler, IReadOnlySessionState
    Public _context As HttpContext
    Public userlevel As String = 0
    Public username As String = ""
    Dim id As String = ""

    '4.3 Added filter term and col 1-10 CM
    Dim FilterCol As String
    Dim FilterCol2 As String
    Dim FilterCol3 As String
    Dim FilterCol4 As String
    Dim FilterCol5 As String
    Dim FilterCol6 As String
    Dim FilterCol7 As String
    Dim FilterCol8 As String
    Dim FilterCol9 As String
    Dim FilterCol10 As String
    Dim FilterTerm As String
    Dim FilterTerm2 As String
    Dim FilterTerm3 As String
    Dim FilterTerm4 As String
    Dim FilterTerm5 As String
    Dim FilterTerm6 As String
    Dim FilterTerm7 As String
    Dim FilterTerm8 As String
    Dim FilterTerm9 As String
    Dim FilterTerm10 As String

    Public Sub SetFilterCriteria()
        '4.3 Added filter term and col 1-10 CM
        Try
            FilterCol = System.Web.HttpContext.Current.Request.QueryString("filterCol")
            FilterCol2 = System.Web.HttpContext.Current.Request.QueryString("filterCol2")
            FilterCol3 = System.Web.HttpContext.Current.Request.QueryString("filterCol3")
            FilterCol4 = System.Web.HttpContext.Current.Request.QueryString("filterCol4")
            FilterCol5 = System.Web.HttpContext.Current.Request.QueryString("filterCol5")
            FilterCol6 = System.Web.HttpContext.Current.Request.QueryString("filterCol6")
            FilterCol7 = System.Web.HttpContext.Current.Request.QueryString("filterCol7")
            FilterCol8 = System.Web.HttpContext.Current.Request.QueryString("filterCol8")
            FilterCol9 = System.Web.HttpContext.Current.Request.QueryString("filterCol9")
            FilterCol10 = System.Web.HttpContext.Current.Request.QueryString("filterCol10")
            FilterTerm = System.Web.HttpContext.Current.Request.QueryString("filterTerm")
            FilterTerm2 = System.Web.HttpContext.Current.Request.QueryString("filterTerm2")
            FilterTerm3 = System.Web.HttpContext.Current.Request.QueryString("filterTerm3")
            FilterTerm4 = System.Web.HttpContext.Current.Request.QueryString("filterTerm4")
            FilterTerm5 = System.Web.HttpContext.Current.Request.QueryString("filterTerm5")
            FilterTerm6 = System.Web.HttpContext.Current.Request.QueryString("filterTerm6")
            FilterTerm7 = System.Web.HttpContext.Current.Request.QueryString("filterTerm7")
            FilterTerm8 = System.Web.HttpContext.Current.Request.QueryString("filterTerm8")
            FilterTerm9 = System.Web.HttpContext.Current.Request.QueryString("filterTerm9")
            FilterTerm10 = System.Web.HttpContext.Current.Request.QueryString("filterTerm10")
        Catch ex As Exception
            Debug.Print(ex.Message.ToString())
        End Try

    End Sub

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim appsettings As NameValueCollection
        Dim session_guid As String = System.Web.HttpContext.Current.Request.QueryString("guid").ToString()
        SetFilterCriteria()

        appsettings = modSettings.appsettings
        Debug.Print("Debug test: appsettings(""SharePath"")=" & appsettings("SharePath"))

        If wvSessions.IsValidSession(session_guid, userlevel, username) Then
            _context = context

            'This is a get count query, count the number of records and return in XML format
            If System.Web.HttpContext.Current.Request.QueryString("count").ToString() = "get" Then
                'context.Session("LastPatientListQuery") = System.Web.HttpContext.Current.Request.RawUrl

                Dim strLastPatientQuery = System.Web.HttpContext.Current.Request.RawUrl
                Debug.Print("LastPatientListQuery: " & System.Web.HttpContext.Current.Request.RawUrl)

                context.Response.ContentType = "Application/xml"

                Dim xmlCount As XmlTextWriter = New XmlTextWriter(context.Response.Output)

                Dim DS As New DataSet
                Dim whereclause As String = ""

                Dim SortBy As String = System.Web.HttpContext.Current.Request.QueryString("SortBy").ToString()

                Dim isdate As Boolean = False

                If SortBy = "[study date]" Or SortBy = "[Study Date]" Then isdate = True

                If SortBy = "undefined" Or SortBy = "" Then
                    SortBy = "[Name]"
                End If

                Dim Direction As String = System.Web.HttpContext.Current.Request.QueryString("direction").ToString()

                If Direction = "undefined" Or Direction = "" Then
                    Direction = "ASC"
                End If

                Dim SearchText As String = System.Web.HttpContext.Current.Request.QueryString("searchtext").ToString()
                Dim studyID As String = System.Web.HttpContext.Current.Request.QueryString("studyid").ToString()
                Dim dateFilter As String = System.Web.HttpContext.Current.Request.QueryString("datefilter").ToString()

                Dim rsPosition As String = System.Web.HttpContext.Current.Request.QueryString("rsposition").ToString()
                Dim maxRecords As String = objSettings.MaxRecords
                'Dim lastRow As Integer = Convert.ToInt32(maxRecords) + Convert.ToInt32(rsPosition) - 1

                SearchText = SearchText.Replace("=", "")
                SearchText = SearchText.Replace("--", "")
                SearchText = SearchText.Replace(";", "")

                If SearchText = "undefined" Or SearchText = "" Then
                    SearchText = ""
                    whereclause = ""

                    If studyID <> "undefined" Then

                        Select Case objSettings.SingleStudy

                            Case "SelectedStudy"

                                whereclause = "WHERE [Study ID] = '" & studyID & "'"
                            Case "SelectedPatient"

                                whereclause = "WHERE [id] = '" & StudyIdToPID(studyID) & "'"
                            Case "ShowAll"
                                whereclause = ""
                            Case Else
                                whereclause = "WHERE [Study ID] = '" & studyID & "'"
                        End Select

                        If objSettings.VisibleStatuses = "FinalRevised" Then
                            whereclause = whereclause & " AND [Study Status] in ('Final','Revised')"
                        ElseIf objSettings.VisibleStatuses = "FinalRevisedPrelim" Then
                            whereclause = whereclause & " AND [Study Status] in ('Preliminary','Final','Revised')"
                        End If

                    Else

                        If objSettings.VisibleStatuses = "FinalRevised" Then
                            whereclause = whereclause & " WHERE [Study Status] in ('Final','Revised')"
                        ElseIf objSettings.VisibleStatuses = "FinalRevisedPrelim" Then
                            whereclause = whereclause & " WHERE [Study Status] in ('Preliminary','Final','Revised')"
                        End If

                    End If

                Else

                    If isdate Then
                        whereclause = "WHERE " & SortBy & " = '" & SearchText & "'"
                    Else
                        whereclause = "WHERE " & SortBy & " LIKE '" & SearchText & "%'"
                    End If

                    If studyID <> "undefined" Then

                        Select Case objSettings.SingleStudy
                            Case "SelectedStudy"
                                whereclause = whereclause & " WHERE [Study ID] = '" & studyID & "'"
                            Case "SelectedPatient"
                                whereclause = whereclause & " AND [id] = '" & StudyIdToPID(studyID) & "'"
                            Case "ShowAll"
                                whereclause = whereclause
                            Case Else
                                whereclause = whereclause & " AND [Study ID] = '" & studyID & "'"
                        End Select

                    End If

                    If objSettings.VisibleStatuses = "FinalRevised" Then
                        whereclause = whereclause & " AND [Study Status] in ('Final','Revised')"
                    ElseIf objSettings.VisibleStatuses = "FinalRevisedPrelim" Then
                        whereclause = whereclause & " AND [Study Status] in ('Preliminary','Final','Revised')"
                    End If

                End If

                If dateFilter <> "undefined" And dateFilter <> "7" Then

                    If whereclause = "" Then
                        whereclause = " WHERE [Study Date] "
                    Else
                        whereclause = whereclause & " AND [Study Date] "
                    End If

                    Select Case dateFilter
                        Case "0"
                            whereclause = whereclause & " = '" & DateTime.Now.ToShortDateString() & "'"
                        Case "1"
                            whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-3).ToShortDateString() & "'"
                        Case "2"
                            whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-7).ToShortDateString() & "'"
                        Case "3"
                            whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-30).ToShortDateString() & "'"
                        Case "4"
                            whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-90).ToShortDateString() & "'"
                        Case "5"
                            whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-180).ToShortDateString() & "'"
                        Case "6"
                            whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-365).ToShortDateString() & "'"

                    End Select

                End If

                Dim SqlString As String = ""

                SqlString = "SELECT COUNT(*) as theCount "
                ' SqlString = SqlString & " (SELECT Row_Number() OVER (ORDER BY " & SortBy & " " & Direction & ") AS RowNumber, * "
                SqlString = SqlString & " FROM Study " & whereclause & " @FilterClause "

                DS = Query(SqlString, "count")
                xmlCount.WriteStartElement("Root")

                'Dim sqlString As String = ""
                'sqlString = "SELECT COUNT(*) as theCount FROM Study"
                'Dim DS As New DataSet

                Try

                    If (DS.Tables.Count > 0) Then

                        For Each dr As DataRow In DS.Tables(0).Rows
                            xmlCount.WriteStartElement("Records")
                            xmlCount.WriteElementString("theCount", dr("theCount").ToString())
                            xmlCount.WriteEndElement()
                        Next

                        xmlCount.WriteEndElement()
                    End If

                Catch ex As Exception
                    ErrHandler.WriteError(ex.Message)
                End Try

            Else
                context.Response.ContentType = "Application/xml"
                'context.Response.Cache.SetCacheability(Cacheability)
                Dim xml As XmlTextWriter = New XmlTextWriter(context.Response.Output)

                xml.WriteStartElement("Root")

                GetXML(xml)

                xml.WriteEndElement()
            End If

        Else
            ' CWT 5/15/20 - redirect here is useless since this is called through ajax every time.  Instead pass back session timeout message.  
            'context.Response.Redirect("Login.aspx")
            context.Response.ContentType = "Application/xml"
            Dim xml As XmlTextWriter = New XmlTextWriter(context.Response.Output)

            xml.WriteStartElement("Root")
            xml.WriteStartElement("Timeout")
            xml.WriteString("TIMEOUT REACHED")
            xml.WriteEndElement()
            xml.WriteEndElement()
        End If

    End Sub

    Private Sub GetXML(ByVal xml As XmlTextWriter)

        Dim DS As New DataSet
        Dim whereclause As String = ""
        Dim SortBy As String = System.Web.HttpContext.Current.Request.QueryString("SortBy").ToString()
        Dim isdate As Boolean = False

        '4.2 OB Support
        If UCase(SortBy) = "[STUDY DATE]" Or UCase(SortBy) = "[DOB]" Or UCase(SortBy) = "[EDD]" Then isdate = True

        If SortBy = "undefined" Or SortBy = "" Then
            SortBy = "[Name]"
        End If

        Dim Direction As String = System.Web.HttpContext.Current.Request.QueryString("direction").ToString()

        If Direction = "undefined" Or Direction = "" Then
            Direction = "ASC"
        End If

        Dim SearchText As String = System.Web.HttpContext.Current.Request.QueryString("searchtext").ToString()
        Dim studyID As String = System.Web.HttpContext.Current.Request.QueryString("studyid").ToString()
        Dim dateFilter As String = System.Web.HttpContext.Current.Request.QueryString("datefilter").ToString()

        Dim rsPosition As String = System.Web.HttpContext.Current.Request.QueryString("rsposition").ToString()
        Dim maxRecords As String = objSettings.MaxRecords
        Dim lastRow As Integer = Convert.ToInt32(maxRecords) + Convert.ToInt32(rsPosition) - 1

        SearchText = SearchText.Replace("=", "")
        SearchText = SearchText.Replace("--", "")
        SearchText = SearchText.Replace(";", "")

        If SearchText = "undefined" Or SearchText = "" Then
            SearchText = ""
            whereclause = ""

            If studyID <> "undefined" Then

                Select Case objSettings.SingleStudy

                    Case "SelectedStudy"

                        whereclause = "WHERE [Study ID] = '" & studyID & "'"
                    Case "SelectedPatient"

                        whereclause = "WHERE [id] = '" & StudyIdToPID(studyID) & "'"
                    Case "ShowAll"
                        whereclause = ""
                    Case Else
                        whereclause = "WHERE [Study ID] = '" & studyID & "'"
                End Select

                If objSettings.VisibleStatuses = "FinalRevised" Then
                    whereclause = whereclause & " AND [Study Status] in ('Final','Revised')"
                ElseIf objSettings.VisibleStatuses = "FinalRevisedPrelim" Then
                    whereclause = whereclause & " AND [Study Status] in ('Preliminary','Final','Revised')"
                End If

            Else

                If objSettings.VisibleStatuses = "FinalRevised" Then
                    whereclause = whereclause & " WHERE [Study Status] in ('Final','Revised')"
                ElseIf objSettings.VisibleStatuses = "FinalRevisedPrelim" Then
                    whereclause = whereclause & " WHERE [Study Status] in ('Preliminary','Final','Revised')"
                End If

            End If

        Else

            If isdate Then
                whereclause = "WHERE " & SortBy & " = '" & SearchText & "'"
            Else
                whereclause = "WHERE " & SortBy & " LIKE '" & SearchText & "%'"
            End If

            If studyID <> "undefined" Then

                Select Case objSettings.SingleStudy
                    Case "SelectedStudy"
                        whereclause = whereclause & " WHERE [Study ID] = '" & studyID & "'"
                    Case "SelectedPatient"
                        whereclause = whereclause & " AND [id] = '" & StudyIdToPID(studyID) & "'"
                    Case "ShowAll"
                        whereclause = whereclause
                    Case Else
                        whereclause = whereclause & " AND [Study ID] = '" & studyID & "'"
                End Select

            End If

            If objSettings.VisibleStatuses = "FinalRevised" Then
                whereclause = whereclause & " AND [Study Status] in ('Final','Revised')"
            ElseIf objSettings.VisibleStatuses = "FinalRevisedPrelim" Then
                whereclause = whereclause & " AND [Study Status] in ('Preliminary','Final','Revised')"
            End If

        End If

        If dateFilter <> "undefined" And dateFilter <> "7" Then

            If whereclause = "" Then
                whereclause = " WHERE [Study Date] "
            Else
                whereclause = whereclause & " AND [Study Date] "
            End If

            Select Case dateFilter
                Case "0"
                    whereclause = whereclause & " = '" & DateTime.Now.ToShortDateString() & "'"
                Case "1"
                    whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-3).ToShortDateString() & "'"
                Case "2"
                    whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-7).ToShortDateString() & "'"
                Case "3"
                    whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-30).ToShortDateString() & "'"
                Case "4"
                    whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-90).ToShortDateString() & "'"
                Case "5"
                    whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-180).ToShortDateString() & "'"
                Case "6"
                    whereclause = whereclause & " >= '" & DateTime.Now.AddDays(-365).ToShortDateString() & "'"
            End Select

        End If

        '4.3 Filtering implementation CM
        If Not (FilterTerm = "" Or FilterCol = "") Then

            If Not (FilterCol = "[Study Time]") And InStr(whereclause, "WHERE", vbTextCompare) = 0 Then
                whereclause = whereclause & " WHERE " & FilterCol & " = '" & FilterTerm & "'"
            ElseIf InStr(whereclause, "WHERE", vbTextCompare) = 0 And FilterCol = "[Study Time]" Then
                whereclause = whereclause & " WHERE " & FilterCol & " = ' " & FilterTerm & "'"
            ElseIf (FilterCol = "[Study Time]") Then
                whereclause = whereclause & " AND " & FilterCol & " = ' " & FilterTerm & "'"
            Else
                whereclause = whereclause & " AND " & FilterCol & " = '" & FilterTerm & "'"
            End If

        End If

        If Not (FilterTerm = "" Or FilterCol = "") And Not (FilterTerm2 = "" Or FilterCol2 = "") Then

            If Not (FilterCol = "[Study Time]") And InStr(whereclause, "WHERE", vbTextCompare) = 0 Then
                whereclause = whereclause & " WHERE " & FilterCol2 & " = '" & FilterTerm2 & "'"
            ElseIf InStr(whereclause, "WHERE", vbTextCompare) = 0 And FilterCol = "[Study Time]" Then
                whereclause = whereclause & " WHERE " & FilterCol2 & " = ' " & FilterTerm2 & "'"
            ElseIf (FilterCol = "[Study Time]") Then
                whereclause = whereclause & " AND " & FilterCol2 & " = ' " & FilterTerm2 & "'"
            Else
                whereclause = whereclause & " AND " & FilterCol2 & " = '" & FilterTerm2 & "'"
            End If

        End If

        If Not (FilterTerm = "" Or FilterCol = "") And Not (FilterTerm2 = "" Or FilterCol2 = "") And Not (FilterTerm3 = "" Or FilterCol3 = "") Then

            If Not (FilterCol = "[Study Time]") And InStr(whereclause, "WHERE", vbTextCompare) = 0 Then
                whereclause = whereclause & " WHERE " & FilterCol3 & " = '" & FilterTerm3 & "'"
            ElseIf InStr(whereclause, "WHERE", vbTextCompare) = 0 And FilterCol = "[Study Time]" Then
                whereclause = whereclause & " WHERE " & FilterCol3 & " = ' " & FilterTerm3 & "'"
            ElseIf (FilterCol = "[Study Time]") Then
                whereclause = whereclause & " AND " & FilterCol3 & " = ' " & FilterTerm3 & "'"
            Else
                whereclause = whereclause & " AND " & FilterCol3 & " = '" & FilterTerm3 & "'"
            End If

        End If

        If Not (FilterTerm = "" Or FilterCol = "") And Not (FilterTerm2 = "" Or FilterCol2 = "") And Not (FilterTerm3 = "" Or FilterCol3 = "") And Not (FilterTerm4 = "" Or FilterCol4 = "") Then

            If Not (FilterCol = "[Study Time]") And InStr(whereclause, "WHERE", vbTextCompare) = 0 Then
                whereclause = whereclause & " WHERE " & FilterCol4 & " = '" & FilterTerm4 & "'"
            ElseIf InStr(whereclause, "WHERE", vbTextCompare) = 0 And FilterCol = "[Study Time]" Then
                whereclause = whereclause & " WHERE " & FilterCol4 & " = ' " & FilterTerm4 & "'"
            ElseIf (FilterCol = "[Study Time]") Then
                whereclause = whereclause & " AND " & FilterCol4 & " = ' " & FilterTerm4 & "'"
            Else
                whereclause = whereclause & " AND " & FilterCol4 & " = '" & FilterTerm4 & "'"
            End If

        End If

        If Not (FilterTerm = "" Or FilterCol = "") And Not (FilterTerm2 = "" Or FilterCol2 = "") And Not (FilterTerm3 = "" Or FilterCol3 = "") And Not (FilterTerm4 = "" Or FilterCol4 = "") And Not (FilterTerm5 = "" Or FilterCol5 = "") Then

            If Not (FilterCol = "[Study Time]") And InStr(whereclause, "WHERE", vbTextCompare) = 0 Then
                whereclause = whereclause & " WHERE " & FilterCol5 & " = '" & FilterTerm5 & "'"
            ElseIf InStr(whereclause, "WHERE", vbTextCompare) = 0 And FilterCol = "[Study Time]" Then
                whereclause = whereclause & " WHERE " & FilterCol5 & " = ' " & FilterTerm5 & "'"
            ElseIf (FilterCol = "[Study Time]") Then
                whereclause = whereclause & " AND " & FilterCol5 & " = ' " & FilterTerm5 & "'"
            Else
                whereclause = whereclause & " AND " & FilterCol5 & " = '" & FilterTerm5 & "'"
            End If

        End If

        If Not (FilterTerm = "" Or FilterCol = "") And Not (FilterTerm2 = "" Or FilterCol2 = "") And Not (FilterTerm3 = "" Or FilterCol3 = "") And Not (FilterTerm4 = "" Or FilterCol4 = "") And Not (FilterTerm5 = "" Or FilterCol5 = "") And Not (FilterTerm6 = "" Or FilterCol6 = "") Then

            If Not (FilterCol = "[Study Time]") And InStr(whereclause, "WHERE", vbTextCompare) = 0 Then
                whereclause = whereclause & " WHERE " & FilterCol6 & " = '" & FilterTerm6 & "'"
            ElseIf InStr(whereclause, "WHERE", vbTextCompare) = 0 And FilterCol = "[Study Time]" Then
                whereclause = whereclause & " WHERE " & FilterCol6 & " = ' " & FilterTerm6 & "'"
            ElseIf (FilterCol = "[Study Time]") Then
                whereclause = whereclause & " AND " & FilterCol6 & " = ' " & FilterTerm6 & "'"
            Else
                whereclause = whereclause & " AND " & FilterCol6 & " = '" & FilterTerm6 & "'"
            End If

        End If

        If Not (FilterTerm = "" Or FilterCol = "") And Not (FilterTerm2 = "" Or FilterCol2 = "") And Not (FilterTerm3 = "" Or FilterCol3 = "") And Not (FilterTerm4 = "" Or FilterCol4 = "") And Not (FilterTerm5 = "" Or FilterCol5 = "") And Not (FilterTerm6 = "" Or FilterCol6 = "") And Not (FilterTerm7 = "" Or FilterCol7 = "") Then

            If Not (FilterCol = "[Study Time]") And InStr(whereclause, "WHERE", vbTextCompare) = 0 Then
                whereclause = whereclause & " WHERE " & FilterCol7 & " = '" & FilterTerm7 & "'"
            ElseIf InStr(whereclause, "WHERE", vbTextCompare) = 0 And FilterCol = "[Study Time]" Then
                whereclause = whereclause & " WHERE " & FilterCol7 & " = ' " & FilterTerm7 & "'"
            ElseIf (FilterCol = "[Study Time]") Then
                whereclause = whereclause & " AND " & FilterCol7 & " = ' " & FilterTerm7 & "'"
            Else
                whereclause = whereclause & " AND " & FilterCol7 & " = '" & FilterTerm7 & "'"
            End If

        End If

        If Not (FilterTerm = "" Or FilterCol = "") And Not (FilterTerm2 = "" Or FilterCol2 = "") And Not (FilterTerm3 = "" Or FilterCol3 = "") And Not (FilterTerm4 = "" Or FilterCol4 = "") And Not (FilterTerm5 = "" Or FilterCol5 = "") And Not (FilterTerm6 = "" Or FilterCol6 = "") And Not (FilterTerm7 = "" Or FilterCol7 = "") And Not (FilterTerm8 = "" Or FilterCol8 = "") Then

            If Not (FilterCol = "[Study Time]") And InStr(whereclause, "WHERE", vbTextCompare) = 0 Then
                whereclause = whereclause & " WHERE " & FilterCol8 & " = '" & FilterTerm8 & "'"
            ElseIf InStr(whereclause, "WHERE", vbTextCompare) = 0 And FilterCol = "[Study Time]" Then
                whereclause = whereclause & " WHERE " & FilterCol8 & " = ' " & FilterTerm8 & "'"
            ElseIf (FilterCol = "[Study Time]") Then
                whereclause = whereclause & " AND " & FilterCol8 & " = ' " & FilterTerm8 & "'"
            Else
                whereclause = whereclause & " AND " & FilterCol8 & " = '" & FilterTerm8 & "'"
            End If

        End If

        If Not (FilterTerm = "" Or FilterCol = "") And Not (FilterTerm2 = "" Or FilterCol2 = "") And Not (FilterTerm3 = "" Or FilterCol3 = "") And Not (FilterTerm4 = "" Or FilterCol4 = "") And Not (FilterTerm5 = "" Or FilterCol5 = "") And Not (FilterTerm6 = "" Or FilterCol6 = "") And Not (FilterTerm7 = "" Or FilterCol7 = "") And Not (FilterTerm8 = "" Or FilterCol8 = "") And Not (FilterTerm9 = "" Or FilterCol9 = "") Then

            If Not (FilterCol = "[Study Time]") And InStr(whereclause, "WHERE", vbTextCompare) = 0 Then
                whereclause = whereclause & " WHERE " & FilterCol9 & " = '" & FilterTerm9 & "'"
            ElseIf InStr(whereclause, "WHERE", vbTextCompare) = 0 And FilterCol = "[Study Time]" Then
                whereclause = whereclause & " WHERE " & FilterCol9 & " = ' " & FilterTerm9 & "'"
            ElseIf (FilterCol = "[Study Time]") Then
                whereclause = whereclause & " AND " & FilterCol9 & " = ' " & FilterTerm9 & "'"
            Else
                whereclause = whereclause & " AND " & FilterCol9 & " = '" & FilterTerm9 & "'"
            End If

        End If

        If Not (FilterTerm = "" Or FilterCol = "") And Not (FilterTerm2 = "" Or FilterCol2 = "") And Not (FilterTerm3 = "" Or FilterCol3 = "") And Not (FilterTerm4 = "" Or FilterCol4 = "") And Not (FilterTerm5 = "" Or FilterCol5 = "") And Not (FilterTerm6 = "" Or FilterCol6 = "") And Not (FilterTerm7 = "" Or FilterCol7 = "") And Not (FilterTerm8 = "" Or FilterCol8 = "") And Not (FilterTerm9 = "" Or FilterCol9 = "") And Not (FilterTerm10 = "" Or FilterCol10 = "") Then

            If Not (FilterCol = "[Study Time]") And InStr(whereclause, "WHERE", vbTextCompare) = 0 Then
                whereclause = whereclause & " WHERE " & FilterCol10 & " = '" & FilterTerm10 & "'"
            ElseIf InStr(whereclause, "WHERE", vbTextCompare) = 0 And FilterCol = "[Study Time]" Then
                whereclause = whereclause & " WHERE " & FilterCol10 & " = ' " & FilterTerm10 & "'"
            ElseIf (FilterCol = "[Study Time]") Then
                whereclause = whereclause & " AND " & FilterCol10 & " = ' " & FilterTerm10 & "'"
            Else
                whereclause = whereclause & " AND " & FilterCol10 & " = '" & FilterTerm10 & "'"
            End If

        End If

        Dim SqlString As String = ""

        If rsPosition = "0" Then
            '4.3 adding DOB and Site CM
            SqlString = "SELECT TOP " & objSettings.MaxRecords & " [Study ID],[Name],[ID],CONVERT(varchar(10),[Study Date], 101) AS [StudyDate],CONVERT(varchar(5),[Study Time],108) AS [Study Time],[Referring MD],[Interpreting MD],[Study Type],[Study Status],[Site], CONVERT(varchar(10),[Date of Birth], 101) AS [Date of Birth] FROM Study " & whereclause & " @FilterClause ORDER BY " & SortBy & " " & Direction
            'SqlString = "SELECT TOP " & objSettings.MaxRecords & " [Study ID],[Name],[ID],CONVERT(varchar(10),[Study Date], 101) AS [StudyDate],CONVERT(varchar(5),[Study Time],108) AS [Study Time],[Referring MD],[Interpreting MD],[Study Type],[Study Status] FROM Study " & whereclause & " @FilterClause ORDER BY " & SortBy & " " & Direction
        Else
            '4.3 adding DOB and Site CM'
            SqlString = "SELECT [Study ID],[Name],[ID],CONVERT(varchar(10),[Study Date], 101) AS [StudyDate],CONVERT(varchar(5),[Study Time],108) AS [Study Time],[Referring MD],[Interpreting MD],[Study Type],[Study Status],[Site], CONVERT(varchar(10),[Date of Birth], 101) AS [Date of Birth] FROM "
            'SqlString = "SELECT [Study ID],[Name],[ID],CONVERT(varchar(10),[Study Date], 101) AS [StudyDate],CONVERT(varchar(5),[Study Time],108) AS [Study Time],[Referring MD],[Interpreting MD],[Study Type],[Study Status] FROM "'
            SqlString = SqlString & " (SELECT Row_Number() OVER (ORDER BY " & SortBy & " " & Direction & ") AS RowNumber, * "
            SqlString = SqlString & " FROM Study " & whereclause & ") AS RowConstrainedResult WHERE RowNumber BETWEEN " & rsPosition & " AND " & lastRow & " @FilterClause "
        End If

        '4.2 OB Support - First Pregnance
        Dim FirstStudy As String = ""

        DS = Query(SqlString)
        ' WHERE " & SortBy & " LIKE '" & SearchText & "%'

        Try

            If (DS.Tables.Count > 0) Then

                '4.2 OB Support
                If appsettings("database_type") = "OBW" Then

                    For Each dr As DataRow In DS.Tables(0).Rows

                        xml.WriteStartElement("Study")

                        xml.WriteElementString("StudyID", dr("Study ID").ToString())
                        xml.WriteElementString("Name", dr("Name").ToString())
                        xml.WriteElementString("ID", dr("ID").ToString())
                        xml.WriteElementString("StudyDate", dr("Study Date").ToString())
                        xml.WriteElementString("DOB", dr("DOB").ToString())
                        xml.WriteElementString("Site", dr("Site").ToString())

                        If IsDBNull(dr("FIRSTSTUDY")) Then
                            dr("FIRSTSTUDY") = False
                        End If

                        If dr("FIRSTSTUDY") Then
                            FirstStudy = ChrW(&H2713)
                        Else
                            FirstStudy = " "
                        End If

                        xml.WriteElementString("First", FirstStudy)
                        'xml.WriteElementString("InterpretingMD2", dr("Interpreting MD").ToString())
                        xml.WriteElementString("InterpretingMD", dr("Interpreting MD").ToString())
                        xml.WriteElementString("EDD", dr("Expected Delivery Date").ToString())
                        xml.WriteElementString("StudyType", dr("Study Type").ToString())
                        xml.WriteElementString("StudyStatus", dr("Study Status").ToString())

                        xml.WriteEndElement()
                    Next

                Else

                    For Each dr As DataRow In DS.Tables(0).Rows
                        xml.WriteStartElement("Study")

                        xml.WriteElementString("StudyID", dr("Study ID").ToString())
                        xml.WriteElementString("Name", dr("Name").ToString())
                        xml.WriteElementString("ID", dr("ID").ToString())
                        xml.WriteElementString("StudyDate", dr("StudyDate").ToString())
                        xml.WriteElementString("StudyTime", dr("Study Time").ToString())
                        xml.WriteElementString("ReferringMD", dr("Referring MD").ToString())
                        '4.3 Add DOB CM
                        xml.WriteElementString("DOB", dr("Date of Birth").ToString())
                        '4.3 Add Site CM
                        xml.WriteElementString("Site", dr("Site").ToString())
                        xml.WriteElementString("InterpretingMD", dr("Interpreting MD").ToString())
                        xml.WriteElementString("StudyType", dr("Study Type").ToString())
                        xml.WriteElementString("StudyStatus", dr("Study Status").ToString())

                        xml.WriteEndElement()
                    Next

                End If

            End If

        Catch ex As Exception
            ErrHandler.WriteError(ex.Message)
        End Try

    End Sub

    Private Function Query(SQL As String, Optional DB As String = "ERSRDB") As DataSet

        Dim usercon As New SqlConnection(DecryptedUserAccountsConnectionstring)   'ConfigurationManager.ConnectionStrings("UserAccountsConnectionstring").ConnectionString)  'appsettings("UserAccountsConnectionstring"))
        'Dim usercon As New SqlConnection(ConfigurationManager.ConnectionStrings("UserAccountsConnectionstring").ConnectionString)  'appsettings("UserAccountsConnectionstring"))
        Dim usercmd As SqlCommand = usercon.CreateCommand()
        Dim FilterClause As String = ""
        Dim FilterParse As String()
        Dim CountClause As String = ""

        Dim strPath As String

        Select Case DB
            Case "ERSRDB"
                strPath = DecryptedSQLServerConnectionstring 'ConfigurationManager.ConnectionStrings("SQLServerConnectionstring").ConnectionString
            Case "count"
                strPath = DecryptedSQLServerConnectionstring 'ConfigurationManager.ConnectionStrings("SQLServerConnectionstring").ConnectionString
            Case Else
                strPath = DecryptedSQLServerConnectionstring_Images 'ConfigurationManager.ConnectionStrings("SQLServerConnectionstring_images").ConnectionString
        End Select

        'If DB = "ERSRDB" Then
        '    strPath = DecryptedSQLServerConnectionstring 'ConfigurationManager.ConnectionStrings("SQLServerConnectionstring").ConnectionString
        'Else
        '    strPath = DecryptedSQLServerConnectionstring_Images 'ConfigurationManager.ConnectionStrings("SQLServerConnectionstring_images").ConnectionString
        'End If

        Dim Conn As New System.Data.SqlClient.SqlConnection(strPath)
        Dim cmd As System.Data.SqlClient.SqlCommand = Conn.CreateCommand()
        cmd.CommandType = CommandType.Text

        '4.2 OB Support debug
        Debug.Print("Handler Setting: " & appsettings("database_type"))

        cmd.CommandText = SQL

        If appsettings("database_type") = "ERS,ERS" Then appsettings("database_type") = "ERS"
        If appsettings("database_type") = "OBW,OBW" Then appsettings("database_type") = "OBW"

        If userlevel = "0" Then
            usercmd.CommandType = CommandType.StoredProcedure
            usercmd.CommandText = "GetFilterInfo"
            usercmd.Parameters.Add("@UserName", SqlDbType.VarChar, 20)
            usercmd.Parameters("@UserName").Value = username

            Dim result As String

            Try
                usercon.Open()
                result = CStr(usercmd.ExecuteScalar())
            Catch ex As Exception
                ErrHandler.WriteError(ex.Message())
                System.Diagnostics.Debug.Write(ex.Message)
            Finally
                usercon.Close()
            End Try

            If appsettings("database_type") = "OBW" Then
                SQL = SQL.Replace("[Study ID]", "RecordID As [Study ID]")
                SQL = SQL.Replace("[Study Date]", "StudyDate")
                SQL = SQL.Replace("[study date]", "StudyDate")
                SQL = SQL.Replace("StudyDate, 101) AS [StudyDate]", "StudyDate, 101) AS [Study Date]")
                'SQL = SQL.Replace("[Study Time],108) AS [Study Time]", "[StudyTime],108) AS [Study Time]")
                SQL = SQL.Replace("CONVERT(varchar(5),[Study Time],108) AS [Study Time]", "CONVERT(varchar(10),[DOB],101) AS [Date Of Birth]")
                SQL = SQL.Replace("ORDER BY [Referring MD]", "ORDER BY RefMD")
                SQL = SQL.Replace("ORDER BY [Interpreting MD]", "ORDER BY InterpretingMD")
                SQL = SQL.Replace("[Interpreting MD]", "InterpretingMD AS [Interpreting MD]")
                'SQL = SQL.Replace("[Referring MD]", "RefMD AS [Referring MD]")
                SQL = SQL.Replace("[Referring MD]", "FIRSTSTUDY, CONVERT(varchar(10),[EDD],101) AS [Expected Delviery Date]")
                SQL = SQL.Replace("[Study Type]", "StudyType AS [Study Type]")
                SQL = SQL.Replace("[Study Status]", "Status AS [Study Status]")
                SQL = SQL.Replace("[Date of Birth]", "[DOB]")
                SQL = SQL.Replace("FROM Study", "FROM Table1")
            End If

            cmd.CommandText = SQL

            'Build query clause
            'Just in case filter isn't there, we won't crash
            If Len(result) <> 0 Then

                If InStr(SQL, "WHERE") > 0 Then
                    FilterClause = "AND ("
                Else
                    FilterClause = "WHERE ("
                End If

                FilterParse = result.Split("|")

                Dim sz As String

                For Each sz In FilterParse

                    If Not ((sz = "||") Or (sz = "")) Then
                        FilterClause &= "( [Referring MD]" & " = " + "'" + sz + "'" + " or " & "[Interpreting MD]" & " = " + "'" + sz + "'" + ") or "
                        CountClause &= sz + ", "
                    End If

                Next sz

                ' Chop off last " or "'
                FilterClause = FilterClause.Remove(FilterClause.Length - 3, 3)
                CountClause = CountClause.Remove(CountClause.Length - 1, 1)
                FilterClause &= ") "
                'cmd.CommandText = appsettings("filtermore").Replace("@NameList", GetDistinctValues(search, orderby, start, searchon))

                If appsettings("database_type") = "OBW" Then
                    FilterClause = FilterClause.Replace("[Study ID]", "RecordID")
                    FilterClause = FilterClause.Replace("[Study Date]", "StudyDate")
                    FilterClause = FilterClause.Replace("[study date]", "StudyDate")
                    FilterClause = FilterClause.Replace("[Study Time],108)", "[StudyTime],108)")
                    FilterClause = FilterClause.Replace("[Interpreting MD]", "InterpretingMD")
                    FilterClause = FilterClause.Replace("[Referring MD]", "RefMD")
                    FilterClause = FilterClause.Replace("[Study Type]", "StudyType")
                    FilterClause = FilterClause.Replace("[Study Status]", "Status")
                End If

                cmd.CommandText = cmd.CommandText.Replace("@FilterClause", FilterClause)
            End If

        Else
            cmd.CommandText = cmd.CommandText.Replace("@FilterClause", "")

            If appsettings("database_type") = "OBW" Then
                cmd.CommandText = cmd.CommandText.Replace("[Study ID]", "RecordID As [Study ID]")
                cmd.CommandText = cmd.CommandText.Replace("[Study Date]", "StudyDate")
                cmd.CommandText = cmd.CommandText.Replace("[study date]", "StudyDate")
                cmd.CommandText = cmd.CommandText.Replace("StudyDate, 101) AS [StudyDate]", "StudyDate, 101) AS [Study Date]")
                'cmd.CommandText = cmd.CommandText.Replace("[Study Time],108) AS [Study Time]", "[StudyTime],108) AS [Study Time]")
                cmd.CommandText = cmd.CommandText.Replace("CONVERT(varchar(5),[Study Time],108) AS [Study Time]", "CONVERT(varchar(10),[DOB],101) AS [Date of Birth]")
                cmd.CommandText = cmd.CommandText.Replace("ORDER BY [Referring MD]", "ORDER BY RefMD")
                cmd.CommandText = cmd.CommandText.Replace("ORDER BY [Interpreting MD]", "ORDER BY InterpretingMD")
                cmd.CommandText = cmd.CommandText.Replace("[Interpreting MD]", "InterpretingMD AS [Interpreting MD]")
                'cmd.CommandText = cmd.CommandText.Replace("[Referring MD]", "RefMD AS [Referring MD]")
                cmd.CommandText = cmd.CommandText.Replace("[Referring MD]", "FIRSTSTUDY, CONVERT(varchar(10),[EDD],101) AS [Expected Delivery Date]")
                cmd.CommandText = cmd.CommandText.Replace("[Study Type]", "StudyType AS [Study Type]")
                cmd.CommandText = cmd.CommandText.Replace("[Study Status]", "Status AS [Study Status]")
                cmd.CommandText = cmd.CommandText.Replace("[Date of Birth]", "[DOB]")
                cmd.CommandText = cmd.CommandText.Replace("FROM Study", "FROM Table1")
            End If

        End If

        Try
            Conn.Open()

            Dim Reader As SqlDataAdapter = New SqlDataAdapter(cmd)
            Dim DS As New DataSet
            Reader.Fill(DS)
            Return DS
        Catch exception As Exception
            ErrHandler.WriteError(exception.Message())
        Finally
            Conn.Close()
        End Try

    End Function

    Private Function StudyIdToPID(ByVal StudyId As String) As String
        Dim strPath As String
        Dim Sql As String = "SELECT ID FROM Study WHERE [Study ID] = " & StudyId

        strPath = DecryptedSQLServerConnectionstring ' ConfigurationManager.ConnectionStrings("SQLServerConnectionstring").ConnectionString

        Dim Conn As New System.Data.SqlClient.SqlConnection(strPath)
        Dim cmd As System.Data.SqlClient.SqlCommand = Conn.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = Sql

        Try
            Conn.Open()

            Dim Reader As SqlDataReader = cmd.ExecuteReader()

            Reader.Read()

            id = Reader("ID").ToString()

            Return id
        Catch exception As Exception
            ErrHandler.WriteError(exception.Message())
        Finally
            Conn.Close()
        End Try

        'End If

    End Function

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable

        Get
            Return False
        End Get

    End Property

End Class