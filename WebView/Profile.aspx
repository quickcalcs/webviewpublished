﻿<%@ Page Title="Profile" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" Inherits="DigiSonics.Profile" Codebehind="Profile.aspx.vb" %>
<%@ MasterType virtualPath="~/Site.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <!-- MMC commented out -->
		<!--<LINK href="library\digiview.css" rel="stylesheet">-->
	    <style type="text/css">
            .formfield
            {
                margin-bottom: 0px;
            }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
			<table cellSpacing="0" cellPadding="0" width="980" border="0">
				<TBODY>
					<tr>
						<td align="center" >
						    &nbsp;</td>
					</tr>
					<TR>
						<td align="center" height="20">
							<p><font face="Arial, Helvetica, sans-serif"  size="2"><em style="color: #FFFFFF">Profile<asp:literal id="ltrlscript" runat="server"></asp:literal><br>
							</em></font>
							</p>
							<table cellSpacing="5" cellPadding="0" border="0">
								<TBODY>
									<tr>
										<td class="formtext" noWrap align="right"><strong>Username</strong></td>
										<td width="11">&nbsp;</td>
										<td>&nbsp;
											<asp:textbox id="tbusername" runat="server" ReadOnly="True" CssClass="formfield"></asp:textbox>
										</td>
									</tr>
									<tr>
										<td class="formtext" noWrap align="right"><strong>First Name</strong></td>
										<td width="11">&nbsp;</td>
										<td>&nbsp;
											<asp:textbox id="tbfirstname" runat="server" CssClass="formfield" MaxLength="20" ReadOnly="True"></asp:textbox></td>
									</tr>
									<tr>
										<td class="formtext" noWrap align="right"><strong>Last Name</strong></td>
										<td width="11">&nbsp;</td>
										<td>&nbsp;
											<asp:textbox id="tblastname" runat="server" CssClass="formfield" MaxLength="20" ReadOnly="True"></asp:textbox></td>
									</tr>
                                 </TBODY>
                              </table>

                              <p />
                              <asp:Label ID="lblPasswordMessage" runat="server"></asp:Label>
                              <p />

                              <table cellSpacing="5" cellPadding="0" border="0">
									<tr>
										<td class="formtext" noWrap align="right"><strong><asp:Label runat="server" ID="lblCurrentPassword"></asp:Label></strong></td>
										<td width="11">&nbsp;</td>
										<td>&nbsp;
											<asp:textbox id="tbCurrentPassword" runat="server" CssClass="formfield" MaxLength="20" TextMode="Password"></asp:textbox></td>
									</tr>
									<tr>
										<td class="formtext" noWrap align="right"><strong>Password</strong></td>
										<td width="11">&nbsp;</td>
										<td>&nbsp;
											<asp:textbox id="tbpassword" runat="server" CssClass="formfield" MaxLength="20" TextMode="Password"></asp:textbox></td>
									</tr>
									<tr>
										<td class="formtext" noWrap align="right"><strong>Retype Password</strong></td>
										<td width="11">&nbsp;</td>
										<td>&nbsp;
											<asp:textbox id="tbpassword2" runat="server" CssClass="formfield" MaxLength="20" TextMode="Password"></asp:textbox></td>
									</tr>
									<tr>
										<td class="formtext" noWrap align="right"><strong>Password Expires In </strong>
										</td>
										<td width="11">&nbsp;</td>
										<td>&nbsp;
											<asp:textbox id="DatePassExpires1" readonly="True" runat="server" CssClass="formfield"></asp:textbox>
											<!--<asp:label id="DatePassExpires" runat="server" CssClass="formfield"></asp:label>--></td>
									</tr>
									<tr align="center">
										<td class="formtext" noWrap colSpan="3" height="18">&nbsp;
										</td>
									</tr>
									<tr align="center">
										<td class="formtext" noWrap colSpan="3" height="18"><asp:button id="Save" runat="server" Text="Save Changes" OnClientClick="save"></asp:button></td>
									</tr>
								</TBODY>
							</table>
						</td>
					</TR>
				</TBODY>
			</table>
</asp:Content>

