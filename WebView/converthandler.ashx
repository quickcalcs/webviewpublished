﻿<%@ WebHandler Language="VB" Class="converthandler" %>

Imports DigiSonics
Imports System
Imports System.Web
Imports System.Net
Imports System.IO

Public Class converthandler : Implements IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim studyid As String = context.Request.QueryString("studyid")
        Dim cachePdf As Boolean = False

        Dim url As String = objSettings.ConvertSvcURL
        Dim secondaryUrl As String = objSettings.SecondaryConvertSvcURL
        Dim path As String = context.Request.QueryString("path")
        Dim type As String = context.Request.QueryString("type")

        Dim requested As HttpWebRequest
        Dim responded As HttpWebResponse

        context.Response.ContentType = "text/plain"
        'context.Response.Write("Hello World")

        If studyid <> Nothing Then
            ErrHandler.WriteError("Study ID = " & studyid & " url = " & url)

            Try
                requested = HttpWebRequest.Create(url & "?studyid=" & studyid & "&type=")
                requested.Method = WebRequestMethods.Http.Get
                responded = requested.GetResponse()
            Catch ex As Exception
                ErrHandler.WriteError(ex.Message)
            End Try

        End If

        If path <> Nothing Then

            Try
                requested = HttpWebRequest.Create(secondaryUrl & "path/?path=" & path & "&type=")
                requested.Method = WebRequestMethods.Http.Get
                responded = requested.GetResponse()
            Catch ex As Exception
                ErrHandler.WriteError(ex.Message)
            End Try

        End If

        Try

            Dim reader As New StreamReader(responded.GetResponseStream())
            Dim tmp As String = reader.ReadToEnd()
            responded.Close()
            context.Response.Write(tmp)

        Catch ex As Exception
            ErrHandler.WriteError(ex.Message())
        End Try

        'If (Not (context.Request.QueryString("Op") Is Nothing)) Then
        '    cachePdf = context.Request.QueryString("Op").ToLower().Equals("cachepdf")
        'End If
        '
        'If (cachePdf) Then
        '    Utilities.CacheStudyPdf(studyid)
        'Else
        '    Dim url As String = objSettings.ConvertSvcURL
        '    Dim secondaryUrl As String = objSettings.SecondaryConvertSvcURL
        '    Dim path As String = context.Request.QueryString("path")
        '    Dim type As String = context.Request.QueryString("type")

        '    Dim requested As HttpWebRequest
        '    Dim responded As HttpWebResponse

        '    context.Response.ContentType = "text/plain"
        '    'context.Response.Write("Hello World")

        '    If studyid <> Nothing Then
        '        ErrHandler.WriteError("Study ID = " & studyid & " url = " & url)

        '        Try

        '            requested = HttpWebRequest.Create(url & "?studyid=" & studyid & "&type=")
        '            requested.Method = WebRequestMethods.Http.Get
        '            responded = requested.GetResponse()

        '        Catch ex As Exception

        '            ErrHandler.WriteError(ex.Message)

        '        End Try

        '    End If

        '    If Path <> Nothing Then

        '        Try
        '            requested = HttpWebRequest.Create(secondaryUrl & "path/?path=" & Path & "&type=")
        '            requested.Method = WebRequestMethods.Http.Get
        '            responded = requested.GetResponse()
        '        Catch ex As Exception
        '            ErrHandler.WriteError(ex.Message)
        '        End Try

        '    End If

        '    Try
        '        Dim reader As New StreamReader(responded.GetResponseStream())
        '        Dim tmp As String = reader.ReadToEnd()
        '        responded.Close()
        '        context.Response.Write(tmp)
        '    Catch ex As Exception
        '        ErrHandler.WriteError(ex.Message())
        '    End Try

        'End If

    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class